# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0007_auto_20160703_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registration',
            name='training_provider',
            field=models.ForeignKey(verbose_name='training provider', to='trainings.TrainingProvider', blank=True, null=True, related_name='registration_users'),
        ),
    ]
