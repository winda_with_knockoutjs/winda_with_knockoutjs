# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0005_auto_20160630_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registration',
            name='registration_type',
            field=models.CharField(verbose_name='registration type', max_length=30, choices=[('delegate', 'delegate'), ('organization', 'organization'), ('training provider admin', 'training provider')]),
        ),
    ]
