# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.test import TestCase
from mock import patch

from common.factories import EmailNotificationFactory
from common.models import EmailNotification


class EmailNotificationTestCase(TestCase):

    @patch('common.forms.EmailNotificationForm.verify_notification')
    def test_delivery_message(self, verification):
        verification.return_value = True
        self.notification = EmailNotificationFactory(
            ses_message_id='0100015602d77664-1832db12-4f0d-41be-8b23-c368bc777855-000000')
        body = b'{\n  "Type" : "Notification",\n  "MessageId" : "97718365-eb47-5e68-9b8e-d36de71de556",\n  "TopicArn" : "arn:aws:sns:us-east-1:681266967339:winda",\n  "Message" : "{\\"notificationType\\":\\"Delivery\\",\\"mail\\":{\\"timestamp\\":\\"2016-07-19T11:08:10.212Z\\",\\"source\\":\\"vracheva@steelkiwi.com\\",\\"sourceArn\\":\\"arn:aws:ses:us-east-1:681266967339:identity/vracheva@steelkiwi.com\\",\\"sendingAccountId\\":\\"681266967339\\",\\"messageId\\":\\"0100015602d77664-1832db12-4f0d-41be-8b23-c368bc777855-000000\\",\\"destination\\":[\\"success@simulator.amazonses.com\\"]},\\"delivery\\":{\\"timestamp\\":\\"2016-07-19T11:08:10.877Z\\",\\"processingTimeMillis\\":665,\\"recipients\\":[\\"success@simulator.amazonses.com\\"],\\"smtpResponse\\":\\"250 2.6.0 Message received\\",\\"reportingMTA\\":\\"a8-208.smtp-out.amazonses.com\\"}}",\n  "Timestamp" : "2016-07-19T11:08:10.953Z",\n  "SignatureVersion" : "1",\n  "Signature" : "QxuenIJrJkVytz8mo1BfzNTSQXs0Peq8jw2JS3R+qE1qaWZSezFkk0AnG3Ur2tBhpzxS1K9hLiJJGyeZ+6XljsuQZ+zrw/Wu7MrCMzDVsakMC2FJXsOCyj40DNE01MebN30rMzzGpBJhKuEmi2ir+L3QbZ1DjRaNdJCFp4K5F4H/maZhzJKCbAjTkcK9FqaHbBIEVpCGvjwI81mFdmamZoobP/ZJOObcElUhpY/qNdN5ffoH5B6Ms451JFAvLWpyszLWuK6svMZRvg3PK2rVat2FK+FOZrNoJMBIyI4gOq3CkKfIGG87n6qlUhmMXaQ3OyW/v19DUYuin56YVRgxRQ==",\n  "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem",\n  "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:681266967339:winda:589784ce-365b-4c01-ace9-390ec441e28c"\n}'
        url = reverse('sns_callback')
        response = self.client.post(url, body, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.notification.refresh_from_db()
        self.assertEqual(self.notification.notification_type, EmailNotification.DELIVERY)
        self.assertEqual(self.notification.bounce_type, '')
        self.assertEqual(self.notification.bounce_sub_type, '')
        self.assertEqual(self.notification.complaint_type, '')
        self.assertEqual(EmailNotification.objects.count(), 1)

    @patch('common.forms.EmailNotificationForm.verify_notification')
    def test_bounce_message(self, verification):
        verification.return_value = True
        self.notification = EmailNotificationFactory(
            ses_message_id='0100015602e91385-75d3e7fe-42fb-41f0-8554-dccdbccf2e5c-000000')
        body = b'{\n  "Type" : "Notification",\n  "MessageId" : "31bcb7d6-0e3b-516e-ac13-e7de1ee6e9e4",\n  "TopicArn" : "arn:aws:sns:us-east-1:681266967339:winda",\n  "Message" : "{\\"notificationType\\":\\"Bounce\\",\\"bounce\\":{\\"bounceType\\":\\"Permanent\\",\\"bounceSubType\\":\\"General\\",\\"bouncedRecipients\\":[{\\"emailAddress\\":\\"bounce@simulator.amazonses.com\\",\\"action\\":\\"failed\\",\\"status\\":\\"5.1.1\\",\\"diagnosticCode\\":\\"smtp; 550 5.1.1 user unknown\\"}],\\"timestamp\\":\\"2016-07-19T11:27:25.182Z\\",\\"feedbackId\\":\\"0100015602e915ba-e2d802f1-2268-4889-926a-4cb69302d98c-000000\\",\\"reportingMTA\\":\\"dsn; a8-208.smtp-out.amazonses.com\\"},\\"mail\\":{\\"timestamp\\":\\"2016-07-19T11:27:24.000Z\\",\\"source\\":\\"vracheva@steelkiwi.com\\",\\"sourceArn\\":\\"arn:aws:ses:us-east-1:681266967339:identity/vracheva@steelkiwi.com\\",\\"sendingAccountId\\":\\"681266967339\\",\\"messageId\\":\\"0100015602e91385-75d3e7fe-42fb-41f0-8554-dccdbccf2e5c-000000\\",\\"destination\\":[\\"bounce@simulator.amazonses.com\\"]}}",\n  "Timestamp" : "2016-07-19T11:27:25.235Z",\n  "SignatureVersion" : "1",\n  "Signature" : "UkRCqiiktJV3G0x7gIXkyRw46YERfaURowgdWJbXHw1Oh1MyWsch1SEYtqGu/9UOOI/SHMzrBhMCSSiHKALTCbXOtqAkqVrOfsa5jBsJ+to44RQBt591bjiR7qvEffuB7TN7KfUbYOAxJ+lPaOllC7LeIgKJ2JHfgCBrbE7l3W3idmq/nlZ1D3rHzUfXm2CIBgEh8pFuEeoQGOcupDtprMxBsXK7Xaq6li31hZNMHfkFzypiBtmAU9CFRWXZoJUPSJcVHrL/6Q3fwKeugFDoLhpAR/pR6PQl95voc8kM+62C/kdGP2tIqjJz+b1liLGVOxl2V7ErxIyFoLsPX7pgfQ==",\n  "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem",\n  "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:681266967339:winda:589784ce-365b-4c01-ace9-390ec441e28c"\n}'
        url = reverse('sns_callback')
        response = self.client.post(url, body, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.notification.refresh_from_db()
        self.assertEqual(self.notification.notification_type, EmailNotification.BOUNCE)
        self.assertEqual(self.notification.bounce_type, 'Permanent')
        self.assertEqual(self.notification.bounce_sub_type, 'General')
        self.assertEqual(self.notification.complaint_type, '')
        self.assertEqual(EmailNotification.objects.count(), 1)

    @patch('common.forms.EmailNotificationForm.verify_notification')
    def test_complaint_message(self, verification):
        verification.return_value = True
        self.notification = EmailNotificationFactory(
            ses_message_id='0100015602ecb859-2d5c59a4-e4f6-42c2-a136-55aec560e832-000000')
        body = b'{\n  "Type" : "Notification",\n  "MessageId" : "9fdb4a84-a3d7-5c2e-b1d4-1a8cb21b680f",\n  "TopicArn" : "arn:aws:sns:us-east-1:681266967339:winda",\n  "Message" : "{\\"notificationType\\":\\"Complaint\\",\\"complaint\\":{\\"complainedRecipients\\":[{\\"emailAddress\\":\\"complaint@simulator.amazonses.com\\"}],\\"timestamp\\":\\"2016-07-19T11:31:23.000Z\\",\\"feedbackId\\":\\"0100015602ecbad0-52bc82c9-4da4-11e6-bbc8-3f2f0076bedc-000000\\",\\"userAgent\\":\\"Amazon SES Mailbox Simulator\\",\\"complaintFeedbackType\\":\\"abuse\\"},\\"mail\\":{\\"timestamp\\":\\"2016-07-19T11:31:23.000Z\\",\\"source\\":\\"vracheva@steelkiwi.com\\",\\"sourceArn\\":\\"arn:aws:ses:us-east-1:681266967339:identity/vracheva@steelkiwi.com\\",\\"sendingAccountId\\":\\"681266967339\\",\\"messageId\\":\\"0100015602ecb859-2d5c59a4-e4f6-42c2-a136-55aec560e832-000000\\",\\"destination\\":[\\"complaint@simulator.amazonses.com\\"]}}",\n  "Timestamp" : "2016-07-19T11:31:24.177Z",\n  "SignatureVersion" : "1",\n  "Signature" : "SF36Wmn2DJnO1Kh/2Y28X5l0b0i+umDyLV4gPXnEejW4ZAJqY5NtAJ/js3PH0B84pm8oaWp3DFh0/1+arRmXlARt7IGaLAafsdjOsM172VBVbwIv5q378guCWFYkyRTtNX8itaLuC19RONvOJkiy4lniI8viwSBTvG+ZCQcvpgLhRQusJBZjHGAG5IWDasYcRsJ0U7CJx2h1ec3I9ik4GIW27eDq5A6YvKq5kZT3hlQL22pndcw8a0qGYN8x0svZzAut4ULW/jVbiqyZX5/S1m+Oz2viJOqRBTstPo9/NUzn/3tUcZyBU+FKsSvSvSnqvy0H8EMewvrwMSETCak6aQ==",\n  "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem",\n  "UnsubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:681266967339:winda:589784ce-365b-4c01-ace9-390ec441e28c"\n}'
        url = reverse('sns_callback')
        response = self.client.post(url, body, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.notification.refresh_from_db()
        self.assertEqual(self.notification.notification_type, EmailNotification.COMPLAINT)
        self.assertEqual(self.notification.complaint_type, 'abuse')
        self.assertEqual(self.notification.bounce_type, '')
        self.assertEqual(self.notification.bounce_sub_type, '')
        self.assertEqual(EmailNotification.objects.count(), 1)

    @patch('common.forms.EmailNotificationForm.verify_notification')
    def test_wrong_messageId(self, verification):
        verification.return_value = True
        body = b'{"Message": "{\\"notificationType\\":\\"Test\\"}", "Signature": "SF36Wmn2DJnO1Kh/2Y28X5l0b0i+umDyLV4gPXnEejW4ZAJqY5NtAJ/js3PH0B84pm8oaWp3DFh0/1+arRmXlARt7IGaLAafsdjOsM172VBVbwIv5q378guCWFYkyRTtNX8itaLuC19RONvOJkiy4lniI8viwSBTvG+ZCQcvpgLhRQusJBZjHGAG5IWDasYcRsJ0U7CJx2h1ec3I9ik4GIW27eDq5A6YvKq5kZT3hlQL22pndcw8a0qGYN8x0svZzAut4ULW/jVbiqyZX5/S1m+Oz2viJOqRBTstPo9/NUzn/3tUcZyBU+FKsSvSvSnqvy0H8EMewvrwMSETCak6aQ==", "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:681266967339:winda:589784ce-365b-4c01-ace9-390ec441e28c", "MessageId": "9fdb4a84-a3d7-5c2e-b1d4-1a8cb21b680f", "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem", "Timestamp": "2016-07-19T11:31:24.177Z", "Type": "Notification", "TopicArn": "arn:aws:sns:us-east-1:681266967339:winda", "SignatureVersion": "1"}'
        url = reverse('sns_callback')
        response = self.client.post(url, body, content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(EmailNotification.objects.count(), 0)

    @patch('common.forms.EmailNotificationForm.verify_notification')
    def test_wrong_message_data(self, verification):
        verification.return_value = True
        self.notification = EmailNotificationFactory(
            ses_message_id='0100015602ecb859-2d5c59a4-e4f6-42c2-a136-55aec560e832-000000')
        body = b'{"Message": "{\\"notificationType\\":\\"Complaint\\",\\"complaint\\":{\\"complainedRecipients\\":[{\\"emailAddress\\":\\"complaint@simulator.amazonses.com\\"}],\\"timestamp\\":\\"2016-07-19T11:31:23.000Z\\",\\"feedbackId\\":\\"0100015602ecbad0-52bc82c9-4da4-11e6-bbc8-3f2f0076bedc-000000\\",\\"userAgent\\":\\"Amazon SES Mailbox Simulator\\"},\\"mail\\":{\\"messageId\\":\\"0100015602ecb859-2d5c59a4-e4f6-42c2-a136-55aec560e832-000000\\"}}", "Signature": "SF36Wmn2DJnO1Kh/2Y28X5l0b0i+umDyLV4gPXnEejW4ZAJqY5NtAJ/js3PH0B84pm8oaWp3DFh0/1+arRmXlARt7IGaLAafsdjOsM172VBVbwIv5q378guCWFYkyRTtNX8itaLuC19RONvOJkiy4lniI8viwSBTvG+ZCQcvpgLhRQusJBZjHGAG5IWDasYcRsJ0U7CJx2h1ec3I9ik4GIW27eDq5A6YvKq5kZT3hlQL22pndcw8a0qGYN8x0svZzAut4ULW/jVbiqyZX5/S1m+Oz2viJOqRBTstPo9/NUzn/3tUcZyBU+FKsSvSvSnqvy0H8EMewvrwMSETCak6aQ==", "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:681266967339:winda:589784ce-365b-4c01-ace9-390ec441e28c", "MessageId": "9fdb4a84-a3d7-5c2e-b1d4-1a8cb21b680f", "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem", "Timestamp": "2016-07-19T11:31:24.177Z", "Type": "Notification", "TopicArn": "arn:aws:sns:us-east-1:681266967339:winda", "SignatureVersion": "1"}'
        url = reverse('sns_callback')
        response = self.client.post(url, body, content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(EmailNotification.objects.count(), 1)
