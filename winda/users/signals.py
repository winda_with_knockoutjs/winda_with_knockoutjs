# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.contrib.auth import user_logged_in
from django.dispatch import receiver
from django.utils import timezone


@receiver(user_logged_in)
def update_archived_from(sender, request, user, **kwargs):
    user.archived_from = timezone.now() + relativedelta(months=user.get_archived_from(user.role))
    user.save(update_fields=['archived_from'])
