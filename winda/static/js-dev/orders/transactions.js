$(document).ready(function(){
    initializeDataTable('table#transactions', {
        "order": [[1, 'desc']],
        "columns": [
            {"data": "", 'searchable': false, 'orderable': false, "className": "control"},
            { "data": "id"},
            { "data": "created" },
            { "data": "created_by" },
            { "data": "transaction_value"},
            { "data": "balance"},
            { "data": "order"},
            { "data": "upload_batch"},
            { "data": "description"}
        ]
    });
});
