# -*- coding: utf-8 -*-
from django.conf.urls import url

from common.views import HomeView, SendEmailNotificationRedirectView, SNSCallback, ArchiveRedirectView, \
    GenerateNotificationsRedirectView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^sns-callback/$', SNSCallback.as_view(), name='sns_callback'),
    url(r'^send-email-notification/$', SendEmailNotificationRedirectView.as_view(), name='send_email_notification'),
    url(r'^archive/$', ArchiveRedirectView.as_view(), name='archive'),
    url(r'^generate-notifications/$', GenerateNotificationsRedirectView.as_view(), name='generate_notifications'),
]
