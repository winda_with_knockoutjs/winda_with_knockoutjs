# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth.views import logout
from django.views.generic import RedirectView

from registration.views import DelegateSignupView, OrganizationSignupView, TrainingProviderSignupView, RegistrationView, \
    ConfirmEmailView, EmailVerificationSentView, TrainingProviderReview, PasswordResetFromKeyView


urlpatterns = [
    url(r'^register/', include([
        url(r'^$', RegistrationView.as_view(), name='signup'),
        url(r'^delegate/$', DelegateSignupView.as_view(), name='delegate_signup'),
        url(r'^organization/$', OrganizationSignupView.as_view(), name='organization_signup'),
        url(r'^training-provider/$', TrainingProviderSignupView.as_view(), name='training_provider_signup'),
        url(r'^(?P<role>delegate|organization|training-provider)/pending/$',
            EmailVerificationSentView.as_view(), name='account_email_verification_sent'),
        url(r'^training-provider/review/$', TrainingProviderReview.as_view(), name='review'),
        url(r'^(?P<activation_link>{}|{}|{})/(?P<key>{})/$'
            .format(settings.DELEGATE_ACTIVATION_URL, settings.ORGANIZATION_ACTIVATION_URL,
                    settings.TRAINING_PROVIDER_ACTIVATION_URL, settings.UUID_REGEX),
            ConfirmEmailView.as_view(), name='account_confirm_email'),
    ])),

    url(r'^password/reset/key/(?P<activation_code>{})/$'.format(settings.UUID_REGEX), PasswordResetFromKeyView.as_view(),
        name='account_reset_password_from_key'),
    url(r'^signup/$', RedirectView.as_view(pattern_name='signup'), name='account_signup'),
    url(r'^account/logout/$', logout, name='logout'),

    # exclude urls
    url('^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$', 'common.views.page_not_found_view'),
    url('^signup/$', 'common.views.page_not_found_view'),
    url('^confirm-email/(?P<key>\w+)/$', 'common.views.page_not_found_view'),

    # allauth urls
    url(r'^', include('allauth.urls')),
]
