# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import common.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='delegate_id',
            field=models.CharField(verbose_name='delegate id', max_length=8, default=None, unique=True, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='expires_at',
            field=models.DateTimeField(verbose_name='expires at', default=common.models.TimeStampedModel.user_expires_at_default),
        ),
    ]
