# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from database_admin.views import (
    CoursesListView, CourseUpdateView, TrainingProviderListView, TrainingProviderCreateView,
    TrainingProviderDetailView, TrainingProviderUpdateView, DatabaseAdminHomeView, CourseCreateView,
    CertificatesListView, CertificateCreateView, CertificateUpdateView, AccreditationListView,
    AccreditationCreateView, AccreditationUpdateView, DatabaseAdminAwardCreditView, DatabaseAdminReportsView,
    TrainingRecordReportView, SearchReportView, PaymentReportView, TrainingStatisticsReportView,
    DatabaseGrowthReportView, NotificationHistory, MailoutCreateView, TrainingProviderChangeHistoryView,
    CourseChangeHistoryView, CertificateChangeHistoryView, AccreditationChangeHistoryView, UsersChangeHistoryView,
    UserListView, UserDetailView)

urlpatterns = [
    url(r'^$', DatabaseAdminHomeView.as_view(), name='home'),
    url(r'^reports/', include([
            url(r'^$', DatabaseAdminReportsView.as_view(), name='reports'),
            url(r'^search_report/$', SearchReportView.as_view(), name='search_report'),
            url(r'^payment_report/$', PaymentReportView.as_view(), name='payment_report'),
            url(r'^training_record_report/$', TrainingRecordReportView.as_view(), name='training_record_report'),
            url(r'^training_statistics_report/$', TrainingStatisticsReportView.as_view(), name='training_statistics_report'),
            url(r'^database_growth_report/$', DatabaseGrowthReportView.as_view(), name='database_growth_report'),
        ])
    ),
    url(r'^courses/$', CoursesListView.as_view(), name='courses'),
    url(r'^courses/new$', CourseCreateView.as_view(), name='course_create'),
    url(r'^courses/(?P<pk>[\w]+)$', CourseUpdateView.as_view(), name='course_edit'),
    url(r'^courses/(?P<pk>[\w]+)/change-history/$', CourseChangeHistoryView.as_view(), name='course_change_history'),
    url(r'^training-providers/', include([
        url(r'^$', TrainingProviderListView.as_view(), name='training_providers'),
        url(r'^new/$', TrainingProviderCreateView.as_view(), name='training_provider_create'),
        url(r'^(?P<pk>\d+)/$', TrainingProviderDetailView.as_view(), name='training_provider_detail'),
        url(r'^(?P<pk>\d+)/change-history/$', TrainingProviderChangeHistoryView.as_view(), name='training_provider_history'),
        url(r'^(?P<pk>\d+)/edit/$', TrainingProviderUpdateView.as_view(), name='training_provider_update'),
        url(r'^(?P<pk>\d+)/award-credits/$', DatabaseAdminAwardCreditView.as_view(), name='training_provider_award_credits'),
        url(r'^(?P<parent_pk>\d+)/certifications/', include([
            url(r'^$', CertificatesListView.as_view(), name='training_provider_certifications'),
            url(r'^new/$', CertificateCreateView.as_view(), name='training_provider_certifications_create'),
            url(r'^(?P<pk>\d+)/$', CertificateUpdateView.as_view(), name='training_provider_certification_update'),
            url(r'^(?P<pk>\d+)/change-history/$', CertificateChangeHistoryView.as_view(), name='certification_change_history'),
        ])),
        url(r'^(?P<parent_pk>\d+)/accreditations/', include([
            url(r'^$', AccreditationListView.as_view(), name='training_provider_accreditations'),
            url(r'^new/$', AccreditationCreateView.as_view(), name='training_provider_accreditations_create'),
            url(r'^(?P<pk>\d+)/$', AccreditationUpdateView.as_view(), name='training_provider_accreditations_update'),
            url(r'^(?P<pk>\d+)/change-history/$', AccreditationChangeHistoryView.as_view(), name='accreditations_change_history'),
        ]))
    ])),
    url(r'^users/', include([
        url(r'^$', UserListView.as_view(), name='users'),
        url(r'^(?P<pk>\d+)/$', UserDetailView.as_view(), name='user_detail'),
        url(r'^(?P<pk>\d+)/notification-history/$', NotificationHistory.as_view(), name='notification_history'),
        url(r'^(?P<pk>\d+)/change-history/$', UsersChangeHistoryView.as_view(), name='user_change_history'),
    ])),
    url(r'^mailout/$', MailoutCreateView.as_view(), name='mailout'),
]
