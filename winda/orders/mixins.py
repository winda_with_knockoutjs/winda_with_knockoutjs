# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.db import models
from django.utils.translation import ugettext_lazy as _
from skd_tools.mixins import ActiveTabMixin

from trainings.models import TrainingProvider


class OrderMixin(ActiveTabMixin):

    def get_queryset(self):
        if self.request.user.is_training_provider_user or self.request.user.is_training_provider_admin:
            if self.request.user.training_provider:
                return self.filter_queryset()
            return self.model.objects.none()
        return self.model.objects.filter(training_provider_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['training_provider_id'] = self.kwargs['pk']
        context['training_provider'] = get_object_or_404(TrainingProvider, pk=self.kwargs['pk']) \
            if self.request.user.is_database_admin else self.request.user.training_provider
        return context

    def filter_queryset(self):
        return self.request.user.training_provider.orders.all()

    def get_active_tab(self):
        if self.request.user.is_database_admin:
            return 'training_provider'
        return 'credit'


class TransactionMixin(OrderMixin):

    def filter_queryset(self):
        return self.request.user.training_provider.transactions.all()

    def get_queryset(self):
        return super().get_queryset().select_related('order', 'upload_batch').annotate(
            description=models.Case(
                models.When(order_id__isnull=False, then=models.Value(_('purchase'))),
                models.When(upload_batch_id__isnull=False, then=models.Value(_('upload'))),
                default=models.F('reason'),
                output_field=models.CharField(),
            )).extra(select={'balance': 'SELECT SUM(transaction_value) FROM orders_transaction AS tr '
                                        'WHERE tr.id <= orders_transaction.id AND training_provider_id= %s'},
                     select_params=(self.kwargs['pk'],))
