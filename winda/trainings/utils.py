# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone

from dateutil.relativedelta import relativedelta

from courses.models import Course

User = get_user_model()


class ValidationUtil(object):

    def __init__(self, row, index, upload_batch,
                 accredited_courses={}, invalidate=False, saved_records={}, courses={},
                 delegates={}, training_records=[]):
        self.index = index
        self.upload_batch = upload_batch
        self.row = row
        self.invalidate = invalidate
        self.saved_records = saved_records
        self.accredited_courses = accredited_courses
        self.course = None
        self.parse_check = 0
        self.validation_check = 0
        self.duplicate_check = 0
        self.delegate_id = None
        self.course_code = None
        self.course_completion_date = None
        self.previous_course_valid_until = None
        self.fk_delegate_id = None
        self.courses = courses
        self.delegates = delegates
        self.training_records = training_records

    def reset_fields(self):
        self.delegate_id = None
        self.course_code = None
        self.course_completion_date = None
        self.previous_course_valid_until = None

    def parse_row(self):
        if len(self.row) < 2:
            return
        self.delegate_id = self.validate_delegate_id(self.row[0])
        self.course_code = self.validate_course_code(self.row[1])
        if len(self.row) >= 3:
            self.course_completion_date = self.validate_date(self.row[2], 'course_completion_date')
        if len(self.row) >= 4:
            self.previous_course_valid_until = self.validate_date(self.row[3], 'previous_course_valid_until')

    def validate(self, parse=True):
        if parse:
            self.parse_row()
        data = {
            'upload_batch': self.upload_batch,
            'source_row_number': self.index,
            'delegate_id': self.delegate_id,
            'course_code': self.course_code,
            'course_completion_date': self.course_completion_date,
            'previous_course_valid_until': self.previous_course_valid_until,
            'parse_check': self.calculate_parse_check(),
            'validation_check': 0,
            'duplicate_check': 0
        }
        data['validation_check'] = self.calculate_validation_check(data, self.accredited_courses)
        key = '-'.join([data['delegate_id'], data['course_code'], str(data['course_completion_date'])])
        data['duplicate_check'] = self.calculate_duplicate_check(data, key, self.saved_records, self.invalidate)
        data.update(self.calculate_current_dates())
        if not data['duplicate_check'] & 1: # if key is not duplicate for this upload
            self.saved_records[key] = data['duplicate_check']
        return data

    def get_saved_records(self):
        return self.saved_records

    def validate_delegate_id(self, value):
        """
        The value from the 1 st column converted to lower case with leading
        and trailing white space removed. Set to NULL if value is empty or
        value is wider than 8 characters.
        """
        if value:
            value = value.strip().lower()
            if len(value) > 8:
                self.parse_check += 4 # Delegate id is wider than 8 characters
                return ''
        return value if value is not None else ''

    def validate_course_code(self, value):
        """
        The value from the 2 nd column converted to upper case with leading
        and trailing white space removed. Set to NULL if value is empty or
        value is wider than 8 characters.
        """
        if value:
            value = value.strip().upper()
            if len(value) > 8:
                self.parse_check += 16 # Course code is wider than 8 chars
                return ''
        return value if value is not None else ''

    def validate_date(self, value, field_name):
        if value:
            value = value.strip()
            try:
                date = datetime.datetime.strptime(value, settings.PARSE_DATE_FORMAT).date()
            except ValueError:
                if field_name == 'course_completion_date':
                    self.parse_check += 64 # Invalid date format for completion date
                if field_name == 'previous_course_valid_until':
                    self.parse_check += 128 # Invalid date format for previous course valid until date
                return None
            else:
                return date
        return None

    def calculate_parse_check(self):
        result = self.parse_check
        if len(self.row) < 4:
            result += 1 # Missing columns
        if not self.delegate_id or not self.delegate_id.strip():
            result += 2 # Missing delegate id
        if not self.course_code or not self.course_code.strip():
            result += 8 # Missing course
        if not self.course_completion_date:
            result += 32 # Missing course completion date
        return result

    def calculate_validation_check(self, data, accredited_courses):
        result = self.validation_check
        if data['course_completion_date'] and data['course_completion_date'] > timezone.now().date():
            result += 1 # Course completion date is in future

        self.course = self.courses.get(data['course_code'])
        if not self.course:
            result += 2 # Unknown course code

        if self.course and not self.course.id in accredited_courses:
            result += 4 # Not accredited for course

        delegate = self.delegates.get(data['delegate_id'])
        if not delegate:
            result += 8 # Unknown delegate ID
        if delegate and delegate.archived_at:
            result += 16 # Delegate ID has been archived

        # Save delegate id for foreign key relation in order to use in duplicate_check
        if delegate:
            self.fk_delegate_id = delegate.id

        if self.course and self.course.course_type == Course.REFRESHER:
            if not data['previous_course_valid_until']:
                result += 32 # Previous course valid until date is required
            if self.course.require_existing_training_record and delegate and data['previous_course_valid_until']\
                and not self.course.refresher_for_course.training_records.filter(delegate_id=delegate.id,
                                                   valid_until=data['previous_course_valid_until']).exists():
                result += 128 # No matching previous course training record found

        if data['previous_course_valid_until'] and data['previous_course_valid_until'] < timezone.now().date():
            result += 64 # Previous course valid until date must be in future
        return result

    def calculate_duplicate_check(self, data, key, saved_records, invalidate=False):
        result = self.duplicate_check
        if data['parse_check'] or data['validation_check']:
            return 0
        if key in saved_records.keys():
            return saved_records.get(key) + 1 # Duplicate entry within upload (same record appears multiple times in upload)

        if self.course and self.fk_delegate_id:   # If course and delegate id exists - check if records exists
            records = (self.fk_delegate_id, self.course.id, data['course_completion_date']) in self.training_records
        else:  # Otherwise mark records as not existing
            records = False

        if records and not invalidate:
            result += 2
        elif not records and invalidate:
            result += 4
        return result

    def calculate_current_dates(self):
        return ValidationUtil.calculate_dates(
            self.invalidate,
            self.course_code,
            self.course_completion_date,
            self.course,
            self.previous_course_valid_until
        )

    @staticmethod
    def calculate_dates(invalidate, course_code, course_completion_date, course, previous_course_valid_until):
        result = {}
        if invalidate or not course_code or not course_completion_date or not course:
            return result
        if course.course_type == Course.FULL:
            result['calculated_valid_from'] = course_completion_date
        elif course.course_type == Course.REFRESHER:
            # If the Course is a refresher (Course.CourseType = ‘Refresher’) and previous course valid until
            # is missing (UploadBatchRecord.PreviousCourseValidUntil IS NULL) then set value to NULL.
            if not previous_course_valid_until:
                return result

            if previous_course_valid_until < timezone.now().date():
                return result
            #If the refresher was completed (UploadBatchRecord.CourseCompletionDate) earlier
            #than 2 months before expiry (UploadBatchRecord.PreviousCourseValidUntil)
            elif course_completion_date < previous_course_valid_until - relativedelta(months=2):
                result['calculated_valid_from'] = course_completion_date
            elif course_completion_date < previous_course_valid_until:
                # Else if the refresher was completed (UploadBatchRecord.CourseCompletionDate)
                # within 2 months before expiry (UploadBatchRecord.PreviousCourseValidUntil)
                result['calculated_valid_from'] = previous_course_valid_until + relativedelta(days=1)
        # Calculate valid until date
        if 'calculated_valid_from' in result:
            result['calculated_valid_until'] = result['calculated_valid_from'] + relativedelta(months=course.validity_months)
        return result
