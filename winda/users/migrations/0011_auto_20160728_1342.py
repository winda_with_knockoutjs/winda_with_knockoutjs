# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_user_archived_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='delegate_id',
            field=models.CharField(null=True, max_length=8, db_index=True, default=None, verbose_name='delegate id', unique=True),
        ),
    ]
