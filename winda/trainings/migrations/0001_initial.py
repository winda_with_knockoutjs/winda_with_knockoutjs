# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingProvider',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('address_1', models.CharField(blank=True, verbose_name='street address line 1', max_length=50)),
                ('address_2', models.CharField(blank=True, verbose_name='street address line 2', max_length=50)),
                ('city', models.CharField(blank=True, verbose_name='city', max_length=50)),
                ('postal_code', models.CharField(blank=True, verbose_name='postal code', max_length=20)),
                ('name', models.CharField(verbose_name='name', max_length=50)),
                ('is_active', models.BooleanField(default=True, verbose_name='Flag indicating if this training provider is active or not')),
                ('accepted_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Training Provider',
                'verbose_name_plural': 'Training Providers',
            },
        ),
    ]
