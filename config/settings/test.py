# -*- coding: utf-8 -*-
from .common import *  # noqa

SECRET_KEY = env("DJANGO_SECRET_KEY", default='CHANGEME!!!+bue)bn#l_y1ra73e3#d_ypr43$aq^o0=-%a1mbjj2r#gxa&yq')

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

TEST_RUNNER = 'django.test.runner.DiscoverRunner'
