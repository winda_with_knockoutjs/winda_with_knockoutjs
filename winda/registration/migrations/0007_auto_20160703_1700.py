# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0006_auto_20160702_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lostpassword',
            name='activation_code',
            field=models.UUIDField(editable=False, unique=True, default=uuid.uuid4),
        ),
        migrations.AlterField(
            model_name='lostpassword',
            name='user',
            field=models.ForeignKey(related_name='lost_passwords', to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
    ]
