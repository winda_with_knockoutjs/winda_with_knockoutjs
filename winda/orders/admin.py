# -*- coding: utf-8 -*-
from django.contrib import admin
from reversion.admin import VersionAdmin

from core.admin import ModelAdmin
from orders.models import Order, PaymentStatus, Pricing, Item, Transaction


@admin.register(Order)
class OrderAdmin(ModelAdmin):
    list_display = ('training_provider', 'currency', 'quantity', 'item_code', 'item_unit_price_including_vat',
                    'total_credits', 'payment_success')


@admin.register(PaymentStatus)
class PaymentStatusAdmin(ModelAdmin):
    list_display = ('callback_id', 'order', 'accepted', 'operation_type', 'amount', 'pending', 'balance', 'test_mode', )


class PricingInline(admin.TabularInline):
    model = Pricing


@admin.register(Item)
class ItemAdmin(VersionAdmin, ModelAdmin):
    list_display = ('item_code', 'item_name')
    inlines = [PricingInline]


@admin.register(Transaction)
class TransactionAdmin(ModelAdmin):
    list_display = ('training_provider', 'transaction_value', 'order', 'upload_batch', 'reason',
                    'last_updated_by', 'created_by')
