# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0003_auto_20160713_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='created_by',
            field=models.ForeignKey(related_name='transaction_created_by', default=1, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='paymentstatus',
            name='order',
            field=models.ForeignKey(related_name='payments', verbose_name='order', to='orders.Order'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='order',
            field=models.ForeignKey(null=True, to='orders.Order', blank=True, verbose_name='order'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='reason',
            field=models.CharField(max_length=200, blank=True, verbose_name='reason'),
        ),
    ]
