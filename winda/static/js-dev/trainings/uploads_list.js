$(document).ready(function() {
    initializeDataTable('table#upload_batch_list', {
        "order": [[1, 'asc']],
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "id"},
            { "data": "status" },
            { "data": "description"},
            { "data": "created" },
            { "data": "created_by", 'searchable': false, 'orderable': false},
            { "data": "purchased_at"},
            { "data": "purchased_by", 'searchable': false, 'orderable': false},
            { "data": "total_records"},
            { "data": "valid_records"},
            { "data": "duplicate_records"},
            { "data": "error_records"}
        ],
        "language": {
          "emptyTable": "No records found.",
          "oPaginate": {
              "sNext": '&raquo;',
              "sPrevious": '&laquo;'
          }
        }
    });
});
