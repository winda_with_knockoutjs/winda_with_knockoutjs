# -*- coding: utf-8 -*-
import json
from urllib.parse import urlencode
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.encoding import force_text

from orders.factories import TransactionFactory
from trainings.factories import TrainingProviderFactory
from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, TrainingProviderUserFactory, \
    DatabaseAdminFactory, OrganizationFactory, DelegateFactory

User = get_user_model()


class TransactionTestCase(TestCase):

    def setUp(self):
        self.training_provider = TrainingProviderFactory()
        self.user = TrainingProviderAdminFactory(training_provider=self.training_provider)
        self.training_provider_user = TrainingProviderUserFactory(training_provider=self.training_provider)
        self.transactions = TransactionFactory.create_batch(10, training_provider=self.training_provider)
        TransactionFactory.create_batch(5)
        self.transaction = self.transactions[0]
        self.url = reverse('transactions', args=(self.training_provider.pk,))
        self.detail_url = self.transaction.get_absolute_url()

    def test_access_transaction(self):
        """
        Only Training Provider Admin, Training Provider and Database Admin get transactions page
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

        for user in [self.user, self.training_provider_user, DatabaseAdminFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertContains(response, 'Transactions')

        for user in [OrganizationFactory(), DelegateFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 404)

    def test_filter_result(self):
        """
        Training Provider Admin get transactions page
        """
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))

        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        """
        Training Provider Admin get transactions with wrong sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 20,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'some text',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_valid_sort(self):
        """
        Training Provider Admin get transactions with correct sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '4',
            'sort_asc': '-',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_wrong_data(self):
        """
        Training Provider Admin get transactions wrong parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_transaction_detail(self):
        """
        Training Provider Admin get paid transaction detail page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(force_text(self.detail_url))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Transaction #{}'.format(self.transaction.pk))
