# -*- coding: utf-8 -*-
import factory
from factory import fuzzy
from django.utils import timezone
from common.models import Country

from delegate.models import TrainingRecord, DelegateCertificate, Search
from registration.factories import CountryFactory
from courses.factories import CourseFactory
from users.factories import DelegateFactory
from trainings.factories import UploadBatchRecordFactory, TrainingProviderFactory


class TrainingRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TrainingRecord

    delegate = factory.SubFactory(DelegateFactory)
    course = factory.SubFactory(CourseFactory)
    country = factory.LazyAttribute(lambda a: Country.objects.first() or CountryFactory())
    training_provider = factory.SubFactory(TrainingProviderFactory)
    completed_on = fuzzy.FuzzyDate(timezone.now().date() + timezone.timedelta(days=1),
                                   end_date=timezone.now().date() + timezone.timedelta(days=100))
    valid_from = fuzzy.FuzzyDate(timezone.now().date() - timezone.timedelta(days=30),
                                 end_date=timezone.now().date() - timezone.timedelta(days=1))
    valid_until = fuzzy.FuzzyDate(timezone.now().date() + timezone.timedelta(days=1),
                                  end_date=timezone.now().date() + timezone.timedelta(days=100))
    created_by_record = factory.SubFactory(UploadBatchRecordFactory)


class DelegateCertificateFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = DelegateCertificate

    delegate = factory.SubFactory(DelegateFactory)
    created_by = factory.SubFactory(DelegateFactory)
    certificate_file = factory.django.FileField(filename='testfile.pdf', data=b'certificate')


class SearchFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Search

    created_by = factory.SubFactory(DelegateFactory)
    delegate = factory.SubFactory(DelegateFactory)
