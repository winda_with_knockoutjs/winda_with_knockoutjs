# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0010_remove_trainingrecord_old_training_provider'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='trainingrecord',
            index_together=set([('delegate', 'course', 'completed_on')]),
        ),
    ]
