# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_auto_20160705_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delegatesequence',
            name='random_suffix',
            field=models.PositiveIntegerField(verbose_name='random suffix'),
        ),
        migrations.AlterField(
            model_name='delegatesequence',
            name='sequential_prefix',
            field=models.PositiveIntegerField(verbose_name='sequential prefix'),
        ),
    ]
