# -*- coding: utf-8 -*-
import os

from allauth.account.adapter import get_adapter
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.fields.files import FieldFile
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from braces.views import UserPassesTestMixin, JSONResponseMixin, AjaxResponseMixin, LoginRequiredMixin
from skd_tools.mixins import ActiveTabMixin

User = get_user_model()


class BaseRoleAccessMixin(LoginRequiredMixin, UserPassesTestMixin):

    roles = []
    admin_roles = []
    only_owner = False

    def test_func(self, user):
        if self.only_owner:
            is_owner = self.test_owner(user)
        else:
            is_owner = True
        return is_owner and (user.role in self.roles or user.role in self.admin_roles)

    def handle_no_permission(self, request):
        raise Http404

    def test_owner(self, user):
        # Override for non detail views
        return self.get_object().pk == user.pk or user.role in self.admin_roles


class DelegateAccessMixin(BaseRoleAccessMixin):

    roles = [User.DELEGATE]


class OrganizationAccessMixin(BaseRoleAccessMixin):

    roles = [User.ORGANIZATION]


class TrainingProviderAccessMixin(BaseRoleAccessMixin):

    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN]


class DatabaseAdminAccessMixin(BaseRoleAccessMixin):

    roles = [User.DATABASE_ADMIN]


class DatabaseSuperAdminAccessMixin(BaseRoleAccessMixin):

    roles = [User.DATABASE_SUPER_ADMIN]


class TechnicalSupportAccessMixin(BaseRoleAccessMixin):

    roles = [User.TECHNICAL_SUPPORT]


class FilterMixin(JSONResponseMixin, AjaxResponseMixin):
    paginate_by = 100
    filter_data = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context

    def get_ajax(self, request, *args, **kwargs):
        self.draw = self.request.GET.get('draw', 1)
        return self.data_management_filter(self.request.GET)

    def get_queryset(self):
        if self.request.is_ajax():
            return super().get_queryset()
        return self.model.objects.none()

    def get_filtered_queryset(self, queryset):
        ordering_field = list(self.form_class.FIELDS.keys())[self.filter_data.get('sort_column') or 1]
        return queryset.order_by('{}{}'.format(self.filter_data.get('sort_asc', ''), ordering_field))

    def data_management_filter(self, data):
        form = self.form_class(data=data)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def paginate_queryset(self, queryset, page_size):
        start, length = self.filter_data.get('start', 0), self.filter_data.get('length', page_size)
        page = int(start / length) + 1 if int(start / length) >= 0 else 1
        self.kwargs[self.page_kwarg] = page
        return super().paginate_queryset(queryset, length)

    def form_valid(self, form):
        self.filter_data = form.cleaned_data
        total_records = self.get_queryset()
        self.object_list = self.get_filtered_queryset(total_records)
        context = self.get_context_data()
        return self.render_json_response({
            'draw': self.draw,
            'recordsTotal': total_records.count(),
            'recordsFiltered': self.object_list.count(),
            'data': self.get_filtered_data(context['object_list'])
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'draw': self.draw,
            'recordsTotal': 0,
            'recordsFiltered': 0,
            'data': [],
            'errors': form.errors
        })


class ParentObjectMixin(object):

    parent_object = None
    parent_model = None
    parent_slug = 'parent_pk'
    parent_context_name = 'provider'

    def get_parent_object(self):
        return get_object_or_404(self.parent_model, pk=self.kwargs.get(self.parent_slug))

    def dispatch(self, request, *args, **kwargs):
        self.parent_object = self.get_parent_object()
        return super(ParentObjectMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ParentObjectMixin, self).get_context_data(*args, **kwargs)
        context[self.parent_context_name] = self.parent_object
        return context


class TrainingProviderCertificateFormMixin(object):
    error_messages = {
        'file_too_large': _('File attachment exceeds maximum size limit.'),
        'invalid_type': _('File attachment type not allowed.'),
        'future_valid_from': _('Date must be in the past.'),
        'past_valid_until': _('Date must be in the future.'),
        'valid_until_not_correct': _('Valid Until date must be after Valid From date.'),
    }

    class Meta:
        required_fields = ('certificate_number', 'certificate_file', 'valid_until', 'valid_from')
        labels = {
            'certificate_file': _('certificate PDF'),
        }
        help_texts = {
            'certificate_file': _('Important: Only PDF files less than {} are accepted.'
                                  .format(filesizeformat(settings.CERTIFICATE_MAX_UPLOAD_SIZE)))
        }

    def clean_certificate_file(self):
        certificate_file = self.cleaned_data['certificate_file']
        filename, ext = os.path.splitext(certificate_file.name)
        if ext not in settings.CERTIFICATE_ALLOWED_FILE_FORMAT.keys():
            raise forms.ValidationError(self.error_messages['invalid_type'])

        if isinstance(certificate_file, FieldFile):
            file_size = certificate_file.size
        else:
            file_size = certificate_file._size
        if file_size > settings.CERTIFICATE_MAX_UPLOAD_SIZE:
            raise forms.ValidationError(self.error_messages['file_too_large'])
        return certificate_file

    def clean_valid_from(self):
        valid_from = self.cleaned_data['valid_from']
        if valid_from >= timezone.now().date():
            raise forms.ValidationError(self.error_messages['future_valid_from'])
        return valid_from

    def clean_valid_until(self):
        valid_until = self.cleaned_data['valid_until']
        if valid_until <= timezone.now().date():
            raise forms.ValidationError(self.error_messages['past_valid_until'])
        return valid_until

    def clean(self):
        cd = super().clean()
        if self.errors:
            return cd
        if cd['valid_until'] <= cd['valid_from']:
            return self.add_error(None, self.error_messages['valid_until_not_correct'])
        return cd


class UserMixin(ActiveTabMixin):
    active_tab = 'users'

    def get_queryset(self):
        return super().get_queryset().filter(role__in=[User.DELEGATE, User.ORGANIZATION,
                                                       User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN])\
            .select_related('training_provider')


class UserFormMixin(object):
    error_messages = {
        'duplicate_email': _('A user is already registered with this e-mail address.'),
        'email_in_blacklist': _('Personal email addresses are not allowed.'),
    }

    def clean_email(self):
        value = self.cleaned_data["email"]
        value = get_adapter().clean_email(value)
        users = get_user_model().objects.filter(email__iexact=value)
        if self.instance:
            users = users.exclude(pk=self.instance.pk)
        if value and users.exists():
            raise forms.ValidationError(self.error_messages['duplicate_email'])
        return value

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in getattr(self.Meta, 'required_fields', ()):
            self.fields[field].required = True


class DataBaseUserFormMixin(UserFormMixin):

    def clean(self):
        cd = super().clean()
        if self.instance.archived_at:
            raise forms.ValidationError(_('You can not update archived user.'))
        return cd
