# -*- coding: utf-8 -*-
from allauth.account.adapter import get_adapter
from allauth.account.views import SignupView as AllAuthSignupView, \
    ConfirmEmailView as AllAuthConfirmEmailView, \
    EmailVerificationSentView as AllAuthEmailVerificationSentView
from allauth.utils import get_form_class

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect
from django.utils import timezone
from django.views.generic import TemplateView, FormView
from django.views.generic.edit import FormMixin
from extra_views.formsets import BaseFormSetMixin

import reversion

from common.models import EmailNotification, DelegateSequence
from common.reversion import bulk_create_revision
from courses.models import CourseProvider
from registration.forms import DelegateSignupForm, TrainingProviderSignupForm, OrganizationSignupForm, SetPasswordForm, \
    CertificateFormSet, ResetPasswordKeyForm
from registration.mixins import SignupViewMixin
from registration.models import Registration, LostPassword
from trainings.models import TrainingProvider, TrainingProviderCertificate


class RegistrationView(TemplateView):
    template_name = 'account/registration.html'


class DelegateSignupView(SignupViewMixin, AllAuthSignupView):
    form_class = DelegateSignupForm
    role = 'delegate'


class OrganizationSignupView(SignupViewMixin, AllAuthSignupView):
    form_class = OrganizationSignupForm
    role = 'organization'


class TrainingProviderSignupView(SignupViewMixin, BaseFormSetMixin, AllAuthSignupView):
    form_class = TrainingProviderSignupForm
    formset_class = CertificateFormSet
    template_name = 'account/signup.html'
    role = 'training-provider'

    def get_formset(self):
        return self.formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = super().get_formset_kwargs()
        kwargs['queryset'] = TrainingProviderCertificate.objects.none()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = kwargs.get('formset') or self.get_formset()
        context['max_file_size'] = settings.CERTIFICATE_MAX_UPLOAD_SIZE
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = self.get_formset()
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

    @bulk_create_revision()
    def form_valid(self, form, formset):
        # Set reversion comment
        reversion.set_comment('Added')
        system_user = get_user_model().objects.get(pk=settings.SYSTEM_USER_ID)
        reversion.set_user(system_user)

        cd = form.cleaned_data
        training_provider = TrainingProvider.objects.create(
            name=cd['training_provider'], **{name: value for name, value in cd.items() if
                                             name in ['country', 'address_1', 'address_2', 'city', 'postal_code']})
        training_provider_certificates = formset.save(commit=False)
        for instance in training_provider_certificates:
            instance.training_provider = training_provider

        start_time = timezone.now()
        TrainingProviderCertificate.objects.bulk_create(training_provider_certificates)
        end_time = timezone.now()

        # Select all created certificates and add them to revision
        q_obj = Q(created__gte=start_time) & Q(created__lte=end_time)
        created_certificates = TrainingProviderCertificate.objects.filter(q_obj)
        for certificate in created_certificates.distinct():
            reversion.add_to_revision(certificate)

        courses = dict()
        for certificate_form in formset:
            valid_from, valid_until = certificate_form.cleaned_data['valid_from'], certificate_form.cleaned_data['valid_until']
            for course in certificate_form.cleaned_data['courses']:
                if course in courses:
                    courses[course][0] = min(courses[course][0], valid_from)
                    courses[course][1] = max(courses[course][1], valid_until)
                else:
                    courses[course] = [valid_from, valid_until]

        course_providers = []
        for course, valid_dates in courses.items():
            course_providers.append(CourseProvider(course=course, training_provider=training_provider,
                                                   valid_from=valid_dates[0], valid_until=valid_dates[1]))
        start_time = timezone.now()
        CourseProvider.objects.bulk_create(course_providers)
        end_time = timezone.now()

        # Select all created accreditations and add them to revision
        q_obj = Q(created__gte=start_time) & Q(created__lte=end_time)
        created_providers = CourseProvider.objects.filter(q_obj)
        for provider in created_providers.distinct():
            reversion.add_to_revision(provider)

        cd['training_provider'] = training_provider
        return super().form_valid(form)


class EmailVerificationSentView(AllAuthEmailVerificationSentView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['registration_expiry_hours'] = settings.REGISTRATION_EXPIRY_HOURS
        context['role'] = self.kwargs['role'].replace('-', ' ')
        return context


class TrainingProviderReview(TemplateView):
    template_name = 'account/training_provider_review.html'


class ConfirmEmailView(FormMixin, AllAuthConfirmEmailView):
    form_class = SetPasswordForm

    def get_queryset(self):
        return Registration.objects.all()

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset.get(activation_code=self.kwargs['key'].lower())
        except Registration.DoesNotExist:
            raise Http404()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['confirmation'] = self.object
        context['form'] = self.get_form()
        return context

    def form_valid(self, form):
        User = get_user_model()
        try:
            user = User(**self.object.get_registration_data)
            self.object.confirm(self.request)
            user.set_password(form.cleaned_data['password1'])
            user.save()
        except DelegateSequence.DoesNotExist:
            return self.render_to_response(self.get_context_data(delegate_id_fail=True))

        if user.role == user.DELEGATE:
            EmailNotification.delegate_id_notification(self.request, user)

        if user.role != user.TRAINING_PROVIDER_ADMIN:
            get_adapter(self.request).login(self.request, user)
        else:
            EmailNotification.new_training_provider(self.request, user)
        redirect_url = self.get_redirect_url()
        return redirect(redirect_url)

    def get_redirect_url(self):
        if self.object.registration_type == self.object.TRAINING_PROVIDER_ADMIN:
            return reverse_lazy('review')
        return super().get_redirect_url()

    def post(self, *args, **kwargs):
        form = self.get_form()
        self.object = confirmation = self.get_object()
        if confirmation.is_activated:
            return self.render_to_response(self.get_context_data())
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)


class PasswordResetFromKeyView(FormView):
    template_name = 'account/password_reset_from_key.html'
    form_class = ResetPasswordKeyForm
    success_url = reverse_lazy('home')

    def dispatch(self, request, *args, **kwargs):
        try:
            self.lost_password = LostPassword.objects.get(activation_code=kwargs.get('activation_code'))
            return super().dispatch(request, *args, **kwargs)
        except LostPassword.DoesNotExist:
            self.lost_password = None
            return self.render_to_response(self.get_context_data(activation_code_fail=True))

    def get_form_class(self):
        return get_form_class(settings.ACCOUNT_FORMS, 'reset_password_from_key', self.form_class)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['lost_password'] = self.lost_password
        return context

    def post(self, request, *args, **kwargs):
        if self.lost_password.is_invalid:
            raise Http404()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(PasswordResetFromKeyView, self).get_form_kwargs()
        kwargs["user"] = self.lost_password.user
        return kwargs

    def form_valid(self, form):
        form.save()
        self.lost_password.claimed_at = timezone.now()
        self.lost_password.save()
        adapter = get_adapter(self.request)
        if adapter.can_login(self.lost_password.user) is True:
            adapter.login(self.request, self.lost_password.user)
        return HttpResponseRedirect(self.get_success_url())
