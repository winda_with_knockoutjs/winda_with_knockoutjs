function updateVisibleFields(value) {
    var refresher_for_course = $('#id_refresher_for_course').parents('.form-group'),
        refresher_existing = $('#id_require_existing_training_record').parents('.form-group');
    var method =  value != 'refresher'?'hide':'show';
    refresher_for_course[method]();
    refresher_existing[method]();
}
$(document).on('change', '#id_course_type', function(e) {
    var value = $(this).val();
    updateVisibleFields(value);
})
$(document).ready(function(e){
    var value = $('#id_course_type').val();
    updateVisibleFields(value);
})
