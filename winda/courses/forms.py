# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _

from core.forms import ModelForm
from courses.models import Course


class AdminCourseCreateForm(ModelForm):

    error_messages = {
        'duplicate_code': _('Course with the same code already exists')
    }

    class Meta:
        model = Course
        fields = ('code', 'title', 'course_type', 'refresher_for_course', 'require_existing_training_record',
                  'validity_months')

    def clean_code(self):
        code = self.cleaned_data.get('code')
        if code:
            code = code.strip().upper()
            courses = Course.objects.filter(code=code)
            if self.instance:
                courses = courses.exclude(id=self.instance.id)
            if courses.exists():
                raise forms.ValidationError(self.error_messages['duplicate_code'])
            self.cleaned_data['code'] = code
        return code

    def clean(self):
        course_type = self.cleaned_data.get('course_type')
        refresher_for_course = self.cleaned_data.get('refresher_for_course')
        if course_type == Course.REFRESHER and not refresher_for_course:
            raise forms.ValidationError(
                _('Refresher for course field is required for Refresher course'),
                code='refresher_for_course_required')


class AdminCourseEditForm(AdminCourseCreateForm):

    class Meta:
        model = Course
        fields = ('title', 'course_type', 'refresher_for_course', 'require_existing_training_record',
                  'validity_months')

    def __init__(self, *args, **kwargs):
        super(AdminCourseEditForm, self).__init__(*args, **kwargs)
