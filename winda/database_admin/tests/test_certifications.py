# -*- coding: utf-8 -*-
import json
from datetime import timedelta
from django.test import TestCase
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import get_user_model
from django.utils import timezone

from users.factories import UserFactory, TEST_USER_PASSWORD
from trainings.factories import TrainingProviderFactory, TrainingProviderCertificateFactory
from trainings.models import TrainingProviderCertificate


class DatabaseAdminCertificationsViewsTestCase(TestCase):

    def setUp(self):
        self.user = UserFactory.create(role=get_user_model().DATABASE_ADMIN)
        self.training_provider = TrainingProviderFactory.create()
        self.list_url = reverse('database_admin:training_provider_certifications',
                                kwargs={'parent_pk': self.training_provider.pk})
        self.create_url = reverse('database_admin:training_provider_certifications_create',
                                  kwargs={'parent_pk': self.training_provider.pk})

    def test_list(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Certifications')

    def test_filter(self):
        TrainingProviderCertificateFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_sort(self):
        TrainingProviderCertificateFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        TrainingProviderCertificateFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'hello',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_create_certification(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        pdf = SimpleUploadedFile(
            'random-name.pdf', str.encode('some content'), 'application/pdf'
        )

        data = {
            'certificate_number': '123',
            'certificate_file': pdf,
            'valid_from': (timezone.now() + timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT),
            'valid_until': (timezone.now() + timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT)
        }

        response = self.client.post(self.create_url, data=data, format='multipart')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProviderCertificate.objects.count(), 0)

        data['valid_from'] = (timezone.now() + timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT)
        data['valid_until'] = (timezone.now() - timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT)

        response = self.client.post(self.create_url, data=data, format='multipart')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProviderCertificate.objects.count(), 0)

        data['valid_from'] = (timezone.now() - timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT)
        data['valid_until'] = (timezone.now() + timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT)
        pdf.seek(0)
        data['certificate_file'] = pdf
        response = self.client.post(self.create_url, data=data, format='multipart')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TrainingProviderCertificate.objects.count(), 1)
        self.assertEqual(TrainingProviderCertificate.objects.filter(certificate_number='123').count(), 1)
        self.assertEqual(TrainingProviderCertificate.objects.filter(created_by=self.user.pk).count(), 1)

    def test_update_certification(self):
        certification = TrainingProviderCertificateFactory.create(training_provider=self.training_provider)
        pdf = SimpleUploadedFile(
            'random-name.pdf', str.encode('some content'), 'application/pdf'
        )
        certification.certificate_file.save('hello.pdf', pdf)
        url = certification.get_dba_absolute_url()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'certificate_number': '123',
            'valid_from': (timezone.now() + timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT),
            'valid_until': (timezone.now() + timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT),
            'is_deleted': False
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProviderCertificate.objects.filter(certificate_number=certification.certificate_number).count(), 1)

        data['valid_from'] = (timezone.now() + timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT)
        data['valid_until'] = (timezone.now() - timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT)

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProviderCertificate.objects.filter(certificate_number=certification.certificate_number).count(), 1)

        data['valid_from'] = (timezone.now() - timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT)
        data['valid_until'] = (timezone.now() + timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT)
        data['is_deleted'] = True
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TrainingProviderCertificate.objects.count(), 1)
        self.assertEqual(TrainingProviderCertificate.objects.filter(certificate_number='123').count(), 1)
        self.assertEqual(TrainingProviderCertificate.objects.filter(is_deleted=True).count(), 1)
