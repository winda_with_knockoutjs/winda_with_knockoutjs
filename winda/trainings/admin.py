# -*- coding: utf-8 -*-
from django.contrib import admin
from reversion.admin import VersionAdmin

from core.admin import ModelAdmin

from trainings.models import TrainingProvider, TrainingProviderCertificate, UploadBatch, UploadBatchRecord


@admin.register(TrainingProvider)
class TrainingProviderAdmin(VersionAdmin, ModelAdmin):
    list_display = ('name', 'is_active', 'accepted_by', 'last_updated_by', 'created_by')


@admin.register(TrainingProviderCertificate)
class TrainingProviderCertificateAdmin(VersionAdmin, ModelAdmin):
    list_display = ('certificate_number', 'training_provider', 'valid_from', 'valid_until', 'last_updated_by',
                    'created_by', 'is_deleted')
    list_filter = ('is_deleted',)


class UploadBatchRecordInline(admin.StackedInline):

    model = UploadBatchRecord
    extra = 0


@admin.register(UploadBatch)
class UploadBatchAdmin(ModelAdmin):
    list_display = ('training_provider', 'description', 'status', 'last_updated_by', 'created_by')
    inlines = (UploadBatchRecordInline, )


@admin.register(UploadBatchRecord)
class UploadBatchRecordAdmin(ModelAdmin):
    list_display = ('upload_batch', 'source_row_number', 'delegate_id', 'course_code', 'course_completion_date',
                    'previous_course_valid_until', 'calculated_valid_from', 'calculated_valid_until', 'parse_check',
                    'validation_check', 'duplicate_check')
