# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from trainings.views import (
    TrainingProviderHomeView, UserListView, UserCreateView, UserUpdateView,
    CompanyProfileDetailView, UploadBatchListView, SingleUploadCreateView,
    BulkUploadCreateView, ViewUploadView, DownloadErrorsView
)

urlpatterns = [
    url(r'^(?P<pk>\d+)/', include([
        url(r'^$', TrainingProviderHomeView.as_view(), name='home'),
        url(r'^company-profile/$', CompanyProfileDetailView.as_view(), name='company_profile'),
        url(r'^uploads/', include([
            url(r'^$', UploadBatchListView.as_view(), name='uploads'),
            url(r'^new-single/$', SingleUploadCreateView.as_view(), name='single-upload'),
            url(r'^new-bulk/$', BulkUploadCreateView.as_view(), name='bulk-upload'),
            url(r'^(?P<batch_pk>\d+)/', include([
                url(r'^$', ViewUploadView.as_view(), name='view-upload'),
                url(r'^errors.csv$', DownloadErrorsView.as_view(), name='download-errors')
            ])),
        ])),
    ])),
    url(r'^users/(?P<user_id>\d+)/', include([
        url(r'^$', UserListView.as_view(), name='users'),
        url(r'^new/$', UserCreateView.as_view(), name='user_create'),
        url(r'^(?P<pk>\d+)/edit/$', UserUpdateView.as_view(), name='user_update'),
    ]))
]
