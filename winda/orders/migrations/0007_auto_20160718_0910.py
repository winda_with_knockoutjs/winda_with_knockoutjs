# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='transaction_value',
            field=models.IntegerField(verbose_name='transaction value'),
        ),
    ]
