# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from database_super_admin.views import DatabaseSuperAdminHomeView, UserListView, UserCreateView, UserUpdateView


urlpatterns = [
    url(r'^$', DatabaseSuperAdminHomeView.as_view(), name='home'),
    url(r'^users/', include([
        url(r'^$', UserListView.as_view(), name='users'),
        url(r'^new/$', UserCreateView.as_view(), name='user_create'),
        url(r'^(?P<pk>\d+)/edit/$', UserUpdateView.as_view(), name='user_update'),
    ]))
]
