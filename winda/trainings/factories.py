# -*- coding: utf-8 -*-
import factory
import string
from factory import fuzzy
from django.utils import timezone
from common.models import Country

from registration.factories import CountryFactory
from trainings.models import (
    TrainingProviderCertificate, TrainingProvider,
    UploadBatchRecord, UploadBatch
)


class TrainingProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TrainingProvider

    name = fuzzy.FuzzyText()
    address_1 = fuzzy.FuzzyText()
    city = fuzzy.FuzzyText()
    postal_code = fuzzy.FuzzyText()
    country = factory.LazyAttribute(lambda a: Country.objects.first() or CountryFactory())


class TrainingProviderCertificateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TrainingProviderCertificate

    certificate_file = factory.django.FileField(filename='testfile.pdf', data=b'uhuh')
    certificate_number = fuzzy.FuzzyText()
    valid_from = fuzzy.FuzzyDate(timezone.now().date() - timezone.timedelta(days=30),
                                 end_date=timezone.now().date() - timezone.timedelta(days=1))
    valid_until = fuzzy.FuzzyDate(timezone.now().date() + timezone.timedelta(days=1),
                                  end_date=timezone.now().date() + timezone.timedelta(days=100))
    training_provider = factory.SubFactory(TrainingProviderFactory)


class UploadBatchFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = UploadBatch

    training_provider = factory.LazyAttribute(lambda a: TrainingProvider.objects.first() or TrainingProviderFactory())
    description = fuzzy.FuzzyText()
    status = UploadBatch.DRAFT
    total_records = fuzzy.FuzzyInteger(low=10)
    valid_records = fuzzy.FuzzyInteger(low=10)
    duplicate_records = fuzzy.FuzzyInteger(low=10)
    error_records = fuzzy.FuzzyInteger(low=10)


class UploadBatchRecordFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = UploadBatchRecord

    upload_batch = factory.SubFactory(UploadBatchFactory)
    delegate_id = fuzzy.FuzzyText(length=8, chars=string.ascii_lowercase)
    course_code = fuzzy.FuzzyText(length=8, chars=string.ascii_uppercase)
    parse_check = fuzzy.FuzzyInteger(low=0, high=len(UploadBatchRecord.PARSE_CHECK_CHOICES))
    validation_check = fuzzy.FuzzyInteger(low=0, high=len(UploadBatchRecord.VALIDATION_CHECK_CHOICES))
    duplicate_check = fuzzy.FuzzyInteger(low=0, high=len(UploadBatchRecord.DUPLICATE_CHECK_CHOICES))
    source_row_number = fuzzy.FuzzyInteger(low=10)
