# -*- coding: utf-8 -*-
"""
Django settings for winda project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from __future__ import absolute_import, unicode_literals

import environ
import sys
import os

from django.core.urlresolvers import reverse_lazy

ROOT_DIR = environ.Path(__file__) - 3  # (/a/b/myfile.py - 3 = /)
APPS_DIR = ROOT_DIR.path('winda')

sys.path.append('winda')

environ.Env.read_env(env_file=os.path.join(str(ROOT_DIR), '.env'))
env = environ.Env()

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin
    'django.contrib.admin',
    'django.contrib.flatpages',
)
THIRD_PARTY_APPS = (
    'widget_tweaks',
    'storages',
    'reversion',
    'django_ses',
    'timezone_field',
    'ckeditor',
    'ckeditor_uploader',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    # Your stuff: custom apps go here
    'core',
    'common',
    'users',
    'registration',
    'trainings',
    'courses',
    'delegate',
    'organization',
    'database_admin',
    'database_super_admin',
    'orders',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = (
    # Make sure djangosecure.middleware.SecurityMiddleware is listed first
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'common.middlewares.TimezoneMiddleware',
)

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
}

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ("""Vracheva Anna""", 'vracheva@steelkiwi.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'winda',
        'USER': '',
        'PASSWORD': '',
        'ATOMIC_REQUESTS': True
    }
}

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = False

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

DATE_FORMAT = 'Y-m-d'

DATETIME_FORMAT = 'Y-m-d H:i \\U\\T\\C O'

DATE_INPUT_FORMATS = ['%Y-%m-%d', '%y-%m-%d', '%d-%m-%Y']

DATETIME_INPUT_FORMATS = [
    '%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M:%S.%f', '%Y-%m-%d %H:%M', '%Y-%m-%d',
    '%d-%m-%Y %H:%M:%S', '%d-%m-%Y %H:%M:%S.%f', '%d-%m-%Y %H:%M', '%d-%m-%Y',
]

TEMPLATE_DATETIME_FORMAT = '%Y-%m-%d %H:%M %z'
TEMPLATE_DATE_FORMAT = '%Y-%m-%d'

FORMS_DATE_FORMAT = TEMPLATE_DATE_FORMAT
REPORTS_DATE_FORMAT = TEMPLATE_DATE_FORMAT
PARSE_DATE_FORMAT = '%Y-%m-%d'

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'common.context_processors.template_settings',
                # Your stuff: custom template context processors go here
            ],
        },
    },
]

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'registration.backends.AuthenticationBackend'
)

ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_ADAPTER = 'common.adapter.AccountAdapter'
ACCOUNT_EMAIL_SUBJECT_PREFIX = 'WINDA '
ACCOUNT_FORMS = {
    'login': 'registration.forms.LoginForm',
    'reset_password': 'registration.forms.ForgotPassword',
    'change_password': 'registration.forms.ChangePasswordForm',
    'reset_password_from_key': 'registration.forms.ResetPasswordKeyForm'
}

# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = reverse_lazy('home')
LOGIN_URL = 'account_login'
ACCOUNT_PASSWORD_MIN_LENGTH = 8
ACCOUNT_PASSWORD_MAX_LENGTH = 40

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# Your common stuff: Below this line define 3rd party library settings
SYSTEM_USER_ID = env('SYSTEM_USER_ID', default=1)
REGISTRATION_EXPIRY_HOURS = env('REGISTRATION_EXPIRY_HOURS', default=48)
PASSWORD_RESET_EXPIRY_HOURS = env('PASSWORD_RESET_EXPIRY_HOURS', default=48)
DELEGATE_ACTIVATION_URL = env('DELEGATE_ACTIVATION_URL', default='delegate-confirm-email')
ORGANIZATION_ACTIVATION_URL = env('ORGANIZATION_ACTIVATION_URL', default='organization-confirm-email')
TRAINING_PROVIDER_ACTIVATION_URL = env('TRAINING_PROVIDER_ACTIVATION_URL', default='training-provider-confirm-email')
ORGANIZATION_EMAIL_DOMAIN_BLACKLIST = env('ORGANIZATION_EMAIL_DOMAIN_BLACKLIST', default=[])
CERTIFICATE_MAX_UPLOAD_SIZE = env('CERTIFICATE_MAX_UPLOAD_SIZE', default=5 * 1024 * 1024)  # 5Mb
CERTIFICATE_ALLOWED_FILE_FORMAT = {'.pdf': 'application/pdf'}
EXPIRING_TRAINING_RECORD_MONTHS = env('EXPIRING_TRAINING_RECORD_MONTHS', default=2)  # 2 months
TRAINING_RECORD_UPLOAD_FILE_FORMAT = {'.csv': 'text/csv'}
TRAINING_RECORD_MAX_QUANTITY = env('TRAINING_RECORD_MAX_QUANTITY', default=20000)
TRAINING_RECORD_MAX_UPLOAD_SIZE = env('TRAINING_RECORD_MAX_UPLOAD_SIZE', default=5 * 1024 * 1024)  # 5Mb
TRAINING_RECORD_BULK_SAVE_SIZE = env('TRAINING_RECORD_BULK_SAVE_SIZE', default=50)

# S3
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID', default='')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY', default='')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME', default='bucket')
AWS_STORAGE_PREFIX = env('AWS_STORAGE_PREFIX', default='uploads')
AWS_DEFAULT_ACL = 'private'
DEFAULT_S3_PATH = AWS_STORAGE_PREFIX

if env.bool('USE_S3', default=False):
    DEFAULT_FILE_STORAGE = env('DEFAULT_FILE_STORAGE', default='storages.backends.s3boto.S3BotoStorage')

AWS_SES_ACCESS_KEY_ID = env('AWS_SES_ACCESS_KEY_ID', default='')
AWS_SES_SECRET_ACCESS_KEY = env('AWS_SES_SECRET_ACCESS_KEY', default='')
AWS_SES_REGION_ENDPOINT = env('AWS_SES_REGION_ENDPOINT', default='us-west-2')
AWS_SES_REGION_NAME = env('AWS_SES_REGION_NAME', default='email.{}.amazonaws.com'.format(AWS_SES_REGION_ENDPOINT))
AWS_SNS_REGION_ENDPOINT = env('AWS_SES_REGION_NAME', default='sns.{}.amazonaws.com'.format(AWS_SES_REGION_ENDPOINT))

WEASYPRINT_BASE_URL = str(APPS_DIR.path('static'))


# QuickPay

QUICKPAY_TEST_MODE = env.bool('QUICKPAY_TEST_MODE', default=True)
QUICKPAY_DEADLINE = env.int('QUICKPAY_DEADLINE', default=10)

QUICKPAY_MERCHANT_ID = env.int('QUICKPAY_MERCHANT_ID', default='')
QUICKPAY_AGREEMENT_ID = env.int('QUICKPAY_AGREEMENT_ID', default='')

QUICKPAY_CONTINUE_URL = env('QUICKPAY_CONTINUE_URL', default='')
QUICKPAY_CANCEL_URL = env('QUICKPAY_CANCEL_URL', default='')
QUICKPAY_CALLBACK_URL = env('QUICKPAY_CALLBACK_URL', default='quickpay-callback')

QUICKPAY_PAYMENT_API_KEY = env('QUICKPAY_PAYMENT_API_KEY', default='')
QUICKPAY_ACCOUNT_KEY = env('QUICKPAY_ACCOUNT_KEY', default='')

# The regex for UUID.
# For checking URLs when id of objects is it django.db.models UUIDField.
# http://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex#answer-6640851
# UUID_REGEX = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
UUID_REGEX = "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"


# CKEditor

CKEDITOR_UPLOAD_PATH = 'uploads/flatpages/'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
                                            'Undo', 'Redo', 'Source']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll']},
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-',
                       'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
            {'name': 'styles', 'items': ['FontSize', 'TextColor', 'BGColor', 'Image']},
        ],
        'allowedContent': True
    },
}

# Google analytics settings
GOOGLE_ANALYTICS_TRACKING_ID = env('GOOGLE_ANALYTICS_TRACKING_ID', default='')
