# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20160726_1218'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='archived_by',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, related_name='user_archived_by'),
        ),
    ]
