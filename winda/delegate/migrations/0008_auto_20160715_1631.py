# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0007_auto_20160715_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingrecord',
            name='training_provider',
            field=models.ForeignKey(to='trainings.TrainingProvider', related_name='training_provider_training_records', verbose_name='training provider'),
        ),
    ]
