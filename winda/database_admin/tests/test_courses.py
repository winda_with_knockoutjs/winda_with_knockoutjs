# -*- coding: utf-8 -*-
import json
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from users.factories import UserFactory, TEST_USER_PASSWORD
from courses.factories import CourseFactory
from courses.models import Course



class DatabaseAdminCoursesViewsTestCase(TestCase):

    def setUp(self):
        self.list_url = reverse('database_admin:courses')
        self.create_url = reverse('database_admin:course_create')
        self.user = UserFactory.create(role=get_user_model().DATABASE_ADMIN)

    def test_list(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Courses')

    def test_filter(self):
        CourseFactory.create_batch(10)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_sort(self):
        CourseFactory.create_batch(10)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        CourseFactory.create_batch(10)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'hello',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_create_course(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'code': 'test',
            'title': 'test',
            'validity_months': -1
        }

        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Course.objects.count(), 0)

        data['validity_months'] = 1
        data['course_type'] = Course.REFRESHER

        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Course.objects.count(), 0)

        data['course_type'] = Course.FULL
        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Course.objects.count(), 1)
        self.assertEqual(Course.objects.filter(code='TEST').count(), 1)
        self.assertEqual(Course.objects.filter(created_by=self.user.pk).count(), 1)

    def test_update_course(self):
        course = CourseFactory.create()
        course.code = 'TEST'
        course.save()
        url = course.get_absolute_url()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'code': course.code,
            'title': 'test',
            'validity_months': -1
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Course.objects.filter(title='test').count(), 0)

        data['validity_months'] = 1
        data['course_type'] = Course.REFRESHER

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Course.objects.filter(title='test').count(), 0)


        data['course_type'] = Course.FULL
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Course.objects.count(), 1)
        self.assertEqual(Course.objects.filter(title='test').count(), 1)
