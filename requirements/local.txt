# Local development dependencies go here
-r base.txt
django-extensions==1.6.7
django-test-plus==1.0.13
factory_boy==2.7.0

# django-debug-toolbar that works with Django 1.5+
django-debug-toolbar==1.4
