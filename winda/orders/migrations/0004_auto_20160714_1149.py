# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_auto_20160713_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='item_description',
            field=models.TextField(default=' ', verbose_name='item description'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='token_link',
            field=models.CharField(max_length=200, verbose_name='Link token', blank=True),
        ),
        migrations.AlterField(
            model_name='paymentstatus',
            name='order',
            field=models.ForeignKey(verbose_name='order', to='orders.Order', related_name='payments'),
        ),
    ]
