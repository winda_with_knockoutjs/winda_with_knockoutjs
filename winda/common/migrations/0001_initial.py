# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import common.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('code3', models.CharField(null=True, unique=True, max_length=3, blank=True)),
            ],
            options={
                'verbose_name': 'country',
                'verbose_name_plural': 'countries',
            },
        ),
        migrations.CreateModel(
            name='EmailNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('subject', models.CharField(verbose_name='subject', max_length=200)),
                ('body', models.TextField(verbose_name='body')),
                ('posted_at', models.DateTimeField(blank=True, verbose_name='posted at', null=True)),
                ('ses_message_id', models.CharField(db_index=True, blank=True, verbose_name='ses message id', max_length=200)),
                ('notified_at', models.DateTimeField(blank=True, verbose_name='notified at', null=True)),
                ('notification_type', models.CharField(choices=[('Bounce', 'Bounce'), ('Complaint', 'Complaint'), ('Delivery', 'Delivery')], blank=True, verbose_name='Notification Type', max_length=10)),
                ('bounce_type', models.CharField(blank=True, verbose_name='Bounce Type', max_length=20)),
                ('bounce_sub_type', models.CharField(blank=True, verbose_name='Bounce Sub Type', max_length=20)),
                ('complaint_type', models.CharField(blank=True, verbose_name='Complaint Type', max_length=20)),
                ('archived_from', models.DateTimeField(default=common.models.TimeStampedModel.archived_from_default, verbose_name='archived from')),
                ('archived_at', models.DateTimeField(blank=True, verbose_name='archived at', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
