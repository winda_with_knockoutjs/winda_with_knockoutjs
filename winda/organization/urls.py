# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from .views import (
    OrganizationHomeView, BulkSearchView, BulkSearchResultView,
    OrganizationProfileView, GridSearchResultView)

urlpatterns = [
    url(r'^search-bulk/$', BulkSearchView.as_view(), name='search'),
    url(r'^search-bulk/results/$', BulkSearchResultView.as_view(), name='search_results'),
    url(r'^search-bulk-grid/results/$', GridSearchResultView.as_view(), name='grid_search_results'),
    url(r'^(?P<pk>\d+)/', include([
        url(r'^$', OrganizationHomeView.as_view(), name='home'),
        url(r'^profile/$', OrganizationProfileView.as_view(), name='profile'),
    ])),
]
