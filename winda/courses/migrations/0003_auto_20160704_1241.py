# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_auto_20160702_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='validity_months',
            field=models.PositiveIntegerField(verbose_name='validity months'),
        ),
    ]
