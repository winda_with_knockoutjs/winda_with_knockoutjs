# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from users.factories import TEST_USER_PASSWORD, OrganizationFactory


class OrganizationViewsTestCase(TestCase):

    def setUp(self):
        self.url = reverse('home')
        self.user_count = get_user_model().objects.count()
        self.user = OrganizationFactory()
        self.home_url = reverse('organization:home', kwargs={'pk': self.user.pk})
        self.profile_url = reverse('organization:profile', kwargs={'pk': self.user.pk})

    def test_home(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertRedirects(response, self.home_url)

    def test_profile(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.profile_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.email)

        # Other user tries to access profile
        other_user = OrganizationFactory.create()
        self.client.login(username=other_user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.profile_url)
        self.assertEqual(response.status_code, 404)

        fields = ('job_title', 'first_name', 'last_name', 'email', 'phone', 'organization_name',
                  'address_1', 'address_2', 'city', 'postal_code', 'country')

        data = {key: getattr(self.user, key) for key in fields}

        data['country'] = data['country'].id
        data['email'] = 'hello@example.com'
        data['address_1'] = 'Test Address'
        data['city'] = 'City'
        data['phone'] = '1111111111111'
        data['postal_code'] = '12345'
        data['timezone'] = 'Europe/London'

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.profile_url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.home_url)
