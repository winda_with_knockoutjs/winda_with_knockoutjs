# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='course_type',
            field=models.CharField(verbose_name='course type', max_length=10, default='full', choices=[('full', 'full'), ('refresher', 'refresher')]),
        ),
        migrations.AlterField(
            model_name='course',
            name='require_existing_training_record',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='courseprovider',
            name='is_deleted',
            field=models.BooleanField(verbose_name='is deleted', default=False),
        ),
    ]
