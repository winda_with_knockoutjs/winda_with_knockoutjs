# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
        ('trainings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='training_provider',
            field=models.ForeignKey(blank=True, verbose_name='training provider', null=True, to='trainings.TrainingProvider'),
        ),
    ]
