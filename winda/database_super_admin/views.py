# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.conf import settings
from django.template.defaultfilters import yesno
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, ListView

from skd_tools.mixins import ActiveTabMixin
from common.models import EmailNotification
from common.mixins import DatabaseSuperAdminAccessMixin, FilterMixin
from core.views import CreateView, UpdateView
from database_super_admin.forms import UserFilterForm, UserForm

User = get_user_model()


class DatabaseSuperAdminHomeView(DatabaseSuperAdminAccessMixin, TemplateView):
    template_name = 'database_super_admin/home.html'


class UserListView(DatabaseSuperAdminAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    model = User
    form_class = UserFilterForm
    template_name = 'database_super_admin/users.html'
    active_tab = 'users'

    def get_queryset(self):
        return super().get_queryset().filter(role__in=[User.DATABASE_ADMIN, User.DATABASE_SUPER_ADMIN])

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'username': '<a href="{}">{}</a>'.format(reverse('database_super_admin:user_update', args=(obj.pk,)), obj.username)
                if obj != self.request.user else obj.username,
            'first_name': obj.first_name.title(),
            'last_name': obj.last_name.title(),
            'email': obj.email,
            'role': obj.get_role_display(),
            'is_active': yesno(obj.is_active).title(),
            'expires_at': obj.expires_at.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.expires_at else '',
        } for obj in object_list]


class UserCreateView(DatabaseSuperAdminAccessMixin, ActiveTabMixin, CreateView):
    model = User
    form_class = UserForm
    active_tab = 'users'
    template_name = 'database_super_admin/users_form.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.username = self.object.email
        self.object.archived_from = timezone.now() + relativedelta(months=24)
        self.object.can_upload = True
        self.object.can_use_credit = True
        self.object.can_buy_credit = True
        password = User.objects.make_random_password(length=16)
        self.object.set_password(password)
        self.object.save()
        EmailNotification.user_account_created(self.request, self.object, password)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('User was successfully created.'))
        return reverse_lazy('database_super_admin:users')


class UserUpdateView(DatabaseSuperAdminAccessMixin, ActiveTabMixin, UpdateView):
    model = User
    form_class = UserForm
    active_tab = 'users'
    template_name = 'database_super_admin/users_form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('User profile was successfully saved.'))
        return reverse_lazy('database_super_admin:users')

    def get_queryset(self):
        return super().get_queryset().filter(role__in=[User.DATABASE_ADMIN, User.DATABASE_SUPER_ADMIN])\
                                     .exclude(pk=self.request.user.pk)
