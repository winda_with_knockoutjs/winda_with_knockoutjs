# -*- coding: utf-8 -*-
from datetime import timedelta
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone

from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, DelegateFactory
from delegate.factories import TrainingRecordFactory
from courses.factories import CourseFactory, CourseProviderFactory, CourseRefresherFactory


class SingleUploadViewsTestCase(TestCase):

    def setUp(self):
        self.user_count = get_user_model().objects.count()
        self.user = TrainingProviderAdminFactory.create()
        self.training_provider = self.user.training_provider
        self.delegate = DelegateFactory.create()
        self.url = reverse('training_provider:single-upload', kwargs={'pk': self.training_provider.pk})
        self.course = CourseFactory.create(require_existing_training_record=True)
        self.refresher = CourseRefresherFactory.create(refresher_for_course=self.course)
        self.course_provider = CourseProviderFactory.create(course=self.course,
                                                            training_provider=self.user.training_provider)
        self.course_provider_refresher = CourseProviderFactory.create(course=self.refresher,
                                                            training_provider=self.user.training_provider)
        date = (timezone.now() - timedelta(days=1)).date()
        self.training_record = TrainingRecordFactory.create(course=self.course,
                                                            delegate=self.delegate,
                                                            completed_on=date)

    def test_post_valid(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        date = (timezone.now() - timedelta(days=2)).date()
        data = {
            'course': self.course.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': date.strftime(settings.FORMS_DATE_FORMAT),
            'previous_course_valid_until': self.training_record.valid_until.strftime(settings.FORMS_DATE_FORMAT),
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


    def test_duplicate_record(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'course': self.course.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': self.training_record.completed_on.strftime(settings.FORMS_DATE_FORMAT),
            'previous_course_valid_until': self.training_record.valid_until.strftime(settings.FORMS_DATE_FORMAT),
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)


    def test_date_in_future(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        invalid_date = (timezone.now() + timedelta(days=3)).date()
        valid_date = (timezone.now() - timedelta(days=3)).date()
        data = {
            'course': self.course.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': invalid_date.strftime(settings.FORMS_DATE_FORMAT),
            'previous_course_valid_until': self.training_record.valid_until.strftime(settings.FORMS_DATE_FORMAT),
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

        data['course_completion_date'] = valid_date.strftime(settings.FORMS_DATE_FORMAT)
        data['course'] = self.refresher.id
        data['previous_course_valid_until'] = valid_date.strftime(settings.FORMS_DATE_FORMAT)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

        data['previous_course_valid_until'] = invalid_date.strftime(settings.FORMS_DATE_FORMAT)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


    def test_delegate_archived(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        self.delegate.archived_at = timezone.now()
        self.delegate.save()
        valid_date = (timezone.now() - timedelta(days=3)).date()
        data = {
            'course': self.course.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': valid_date.strftime(settings.FORMS_DATE_FORMAT),
            'previous_course_valid_until': self.training_record.valid_until.strftime(settings.FORMS_DATE_FORMAT),
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_missing_previous_course_valid_until(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        valid_date = (timezone.now() - timedelta(days=3)).date()
        data = {
            'course': self.refresher.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': valid_date.strftime(settings.FORMS_DATE_FORMAT)
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

        data['course'] = self.course.id
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_invalid_previous_course(self):
        self.refresher.require_existing_training_record = True
        self.refresher.save()

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        valid_date = (timezone.now() - timedelta(days=3)).date()
        future_date = (timezone.now() + timedelta(days=5)).date()
        data = {
            'course': self.refresher.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': valid_date.strftime(settings.FORMS_DATE_FORMAT),
            'previous_course_valid_until': future_date.strftime(settings.FORMS_DATE_FORMAT)
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

        TrainingRecordFactory.create(course=self.refresher.refresher_for_course,
                                     delegate=self.delegate,
                                     completed_on=(timezone.now() - timedelta(days=5)).date(),
                                     valid_until=future_date)
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_no_training_record_to_invalidate(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        valid_date = (timezone.now() - timedelta(days=3)).date()
        data = {
            'course': self.course.id,
            'delegate_id': self.delegate.delegate_id,
            'description': 'test description',
            'course_completion_date': valid_date.strftime(settings.FORMS_DATE_FORMAT),
            'invalidate_records': True
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)

        data['course_completion_date'] = self.training_record.completed_on.strftime(settings.FORMS_DATE_FORMAT)
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)
