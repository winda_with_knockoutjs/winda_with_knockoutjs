# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import url, include

from orders.views import CreditTemplateView, PurchaseCreditView, PurchaseConfirmView, OrderView, OrderDetailView, \
    EmailOrderDetailView, QuickPayCallback, OrderPaymentLinkView, TransactionView, TransactionDetailView

urlpatterns = [
    url(r'^training-provider/(?P<pk>\d+)/', include([
        url(r'^credits/$', CreditTemplateView.as_view(), name='credit'),
        url(r'^credits/purchase/$', PurchaseCreditView.as_view(), name='purchase_credit'),
        url(r'^credits/purchase-confirm/$', PurchaseConfirmView.as_view(), name='purchase_credit_confirm'),
        url(r'^orders/$', OrderView.as_view(), name='orders'),
        url(r'^orders/(?P<order_pk>\d+)/$', OrderDetailView.as_view(), name='order_detail'),
        url(r'^orders/(?P<order_pk>\d+)/email/$', EmailOrderDetailView.as_view(), name='order_email'),
        url(r'^orders/(?P<order_pk>\d+)/link/$', OrderPaymentLinkView.as_view(), name='order_link'),
        url(r'^transactions/$', TransactionView.as_view(), name='transactions'),
        url(r'^transactions/(?P<transaction_pk>\d+)/$', TransactionDetailView.as_view(), name='transaction_detail'),
    ])),

    url(r'^{}/$'.format(settings.QUICKPAY_CALLBACK_URL), QuickPayCallback.as_view(), name='quickpay_callback'),
]
