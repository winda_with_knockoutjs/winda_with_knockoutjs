Amazon credentials
==================
For set up application such keys from Amazon need to be configured

For set up S3 you'll need to create user in Amazon AMI with AmazonS3FullAccess policy attached

**AWS_ACCESS_KEY_ID** is Access Key ID of this user

**AWS_SECRET_ACCESS_KEY** is Secret Access Key

**AWS_STORAGE_BUCKET_NAME** is a bucket name on S3

**USE_S3** enable or disable Amazon S3 usage

For set up SES you'll need to create user in Amazon AMI with AmazonSESFullAccess and AmazonSNSFullAccess policy attached

**DJANGO_EMAIL_BACKEND** django_ses.SESBackend - backend for using SES

**AWS_SES_ACCESS_KEY_ID** is Access Key ID of this user

**AWS_SES_SECRET_ACCESS_KEY** is Secret Access Key

**AWS_SES_REGION_NAME** region in which SES activated, for example us-west-2

**DJANGO_DEFAULT_FROM_EMAIL** email address that will be displayed in From field in all outgoing messages

In order to use SES you need to add your domain in SES console.
Right after creation you will be able to send messages in SandBox mode - 200 messages per day and only to verified email addresses. You add them **Email Adresses** section
In order to increase limits and send messages to any address you need to request a sending limit increase. Go to **Sending Statistics**, you'll see a blue button at the top.
You will need to fill request form and wait for approval.

For set up SNS notifications you will need to create new topic in SNS console menu **Topics**.
Make sure that you use the same region as is used for SES.
Right after creation you'll need to subscribe to topic by click **Subscribe to Topic** from Actions sub menu in SNS console.
As endpoint you need to setup url like {protocol}://{domain}/sns-callback/. After that confirmation query will be send to server and confirmed.
For check subscription you can use **Subscriptions** menu in SNS console.

After subscription was confirmed you'll need to go to SES console to menu Domain.
In the list of verified senders, click the domain for which you want to configure bounce and complaint notifications.
In the details pane of the verified sender, expand Notifications.
Click Edit Configuration.
Select your SNS topic in SNS Topic Configuration section
Save configuration.


QuickPay credentials
====================
In order to use QuickPay you need to create merchant account and setup it in menu Settings -> Merchant by adding Shop name, E-mail, Shop URL, Shopsystem values.
Then setup Nets/Teller acquirer in menu Settings -> Acquirers by adding International business number, Visa mpi merchant id, Mastercard mpi merchant id.

For set up application such keys from QuickPay need to be configured (menu Settings -> Integration in QuickPay account)

**QUICKPAY_MERCHANT_ID** QuickPay merchant ID (Merchant information -> Merchant id)

**QUICKPAY_AGREEMENT_ID** QuickPay agreement ID (User information -> Your user Agreement id)

**QUICKPAY_PAYMENT_API_KEY** QuickPay account private key for callback check (Merchant information -> Private key)

**QUICKPAY_ACCOUNT_KEY** QuickPay payment API key for payments (User information -> Your user API key)

**QUICKPAY_TEST_MODE** Flag indicating if we are in test mode.

**QUICKPAY_DEADLINE** Number of seconds that the card holder is given to complete a payment.

**QUICKPAY_CONTINUE_URL** absolute URL for QuickPay continue redirect

**QUICKPAY_CANCEL_URL**  absolute URL for QuickPay cancel redirect

**QUICKPAY_CALLBACK_URL** relative url for confirm payments


Google Analytics credentials
====================

Sign in to your Analytics account, go to Admin tab. Create project and get Tracking ID.
(https://support.google.com/analytics/answer/1008080)

**GOOGLE_ANALYTICS_TRACKING_ID** Google analytics tracking ID, retrieved for project


Development environment
=======================

* clone project
* cd Winda
* vagrant up
* vagrant ssh
* cd /vagrant
* setup database

    mysql -u root
    CREATE DATABASE winda DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
    CREATE USER 'vagrant'@'localhost';
    GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' WITH GRANT OPTION;

​* setup virtualenv

    virtualenv --python=/usr/bin/python3.4 --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements/local.txt

* configure env - copy env.example to .env

    cp -i env.example .env

* update values in .env
* create admin user

    ./manage.py createsuperuser --username=admin --email=admin@example.com

* for load flatpages fixtures
    ./manage.py loaddata winda/common/fixtures/flatpages.json

* ./manage.py runserver 0.0.0.0:8000 --settings=config.settings.local

Test environment
================

* clone project
* cd Winda
* vagrant up
* vagrant ssh
* cd /vagrant
* configure env

    cp -i env.example .env

* update values in .env
* setup database

    mysql -u root
    CREATE USER 'vagrant'@'localhost';
    GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' WITH GRANT OPTION;

​* setup virtualenv

    virtualenv --python=/usr/bin/python3.4 --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements/test.txt

* ./manage.py test --settings=config.settings.test --keepdb


Vagrant (CHANGELOG)
===================
Date:
27.06.2016

OsType:
ubuntu/trusty64

Soft:
virtualenv - 1.11.4
nodejs - 4.4.6 (LTS)
bower - 1.7.9
Python - 3.4.3
apache2 - 2.4.7
libapache2-mod-wsgi-py3 - 3.4-4
mysql-server - 5.5.49
