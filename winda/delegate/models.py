# -*- coding: utf-8 -*-
import uuid
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils import timezone

from dateutil.relativedelta import relativedelta

from common.models import TimeStampedModel, Country
from courses.models import Course
from delegate.managers import TrainingRecordManager
from trainings.models import TrainingProvider, UploadBatchRecord

User = settings.AUTH_USER_MODEL


class TrainingRecord(TimeStampedModel):

    INVALIDATED = 'invalidated'
    EXPIRED = 'expired'
    CURRENT = 'current'
    STATUSES = ()

    delegate = models.ForeignKey(User, verbose_name=_('delegate'), related_name='delegate_training_records')
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='training_records')
    country = models.ForeignKey(Country, verbose_name=_('country'))
    training_provider = models.ForeignKey(TrainingProvider, verbose_name=_('training provider'),
                                          related_name='training_provider_training_records')
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')
    completed_on = models.DateField(_('Completed On'))
    valid_from = models.DateField(_('valid from'))
    valid_until = models.DateField(_('valid until'))
    created_by_record = models.ForeignKey(UploadBatchRecord, verbose_name=_('created by record'),
                                          related_name='created_by_record')
    invalidated_at = models.DateTimeField(_('invalidated at'), null=True, blank=True)
    invalidated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_invalidated_by')
    invalidated_by_record = models.ForeignKey(UploadBatchRecord, verbose_name=_('invalidated by record'),
                                              related_name='invalidated_by_record', blank=True, null=True)

    objects = TrainingRecordManager()

    class Meta:
        verbose_name = _('Training Record')
        verbose_name_plural = _('Training Records')
        index_together = (
            ('delegate', 'course', 'completed_on', 'invalidated_at'),
        )

    def __str__(self):
        return self.delegate.username

    @staticmethod
    def get_training_record_status(obj):
        icons = {
            'red': 'icon-ic_status_rectangle',
            'yellow': 'icon-ic_status_triangle',
            'green': 'icon-ic_status_circle'
        }
        if obj.invalidated_at:
            return TrainingRecord.INVALIDATED, icons.get('red')
        if obj.valid_until < timezone.now().date():
            return TrainingRecord.EXPIRED, icons.get('red')
        if obj.valid_until < (timezone.now() + relativedelta(months=settings.EXPIRING_TRAINING_RECORD_MONTHS)).date():
            return TrainingRecord.CURRENT, icons.get('yellow')
        return TrainingRecord.CURRENT, icons.get('green')

    def get_status(self):
        if self.id:
            return TrainingRecord.get_training_record_status(self)
        return ugettext('No records')

    def get_status_display(self):
        status, icon_class = self.get_status()
        return '<span class="{0}"></span>{1}'.format(icon_class, status.title())

    @staticmethod
    def get_course_status(current=None, expired=None, invalidated=None, valid_until=None):
        icons = {
            'red': 'icon-ic_status_rectangle',
            'yellow': 'icon-ic_status_triangle',
            'green': 'icon-ic_status_circle'
        }
        if current:
            return valid_until, icons.get('green')
        if expired:
            return valid_until, icons.get('yellow')
        if invalidated:
            return ugettext('Not valid'), icons.get('red')
        return ugettext('Not taken'), icons.get('red')


def certificate_file_path(instance, filename):
    return '/'.join(map(str, [settings.AWS_STORAGE_PREFIX, 'certificates/delegate',
                              instance.created.strftime('%Y-%m'), uuid.uuid4(), filename]))


class DelegateCertificate(TimeStampedModel):

    certificate_code = models.UUIDField(default=uuid.uuid4, editable=False)
    certificate_file = models.FileField(_('certificate file'), upload_to=certificate_file_path, max_length=255)
    delegate = models.ForeignKey(User, verbose_name=_('delegate'), related_name='delegate_certificates')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        verbose_name = _('Delegate Certificate')
        verbose_name_plural = _('Delegate Certificates')

    def __str__(self):
        return 'Certificate: {0}'.format(self.certificate_code)

    def get_certificate_url(self):
        return self.certificate_file.url if self.certificate_file else ''


class Search(TimeStampedModel):
    created_by = models.ForeignKey(User, related_name='%(class)s_created_by')
    delegate = models.ForeignKey(User, verbose_name=_('delegate'), related_name='search')

    class Meta:
        verbose_name = _('Search')
        verbose_name_plural = _('Searches')

    def __str__(self):
        return self.delegate.username
