# -*- coding: utf-8 -*-
from collections import OrderedDict

from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from common.forms import BaseFilterForm
from common.mixins import UserFormMixin
from core.forms import ModelForm


User = get_user_model()


class UserFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('username', _('Username')),
        ('first_name', _('First Name')),
        ('last_name', _('Last Name')),
        ('email', _('Email Address')),
        ('role', _('Role Type')),
        ('is_active', _('Is Active')),
        ('expires_at', _('Expires At')),
    ])


class UserForm(UserFormMixin, ModelForm):

    ROLE_CHOICE = (
        (User.DATABASE_ADMIN, _('Database Admin')),
        (User.DATABASE_SUPER_ADMIN, _('Database Super Admin')),
    )

    role = forms.ChoiceField(label=_('role type'), choices=ROLE_CHOICE)
    expires_at = forms.DateField(label=_('expires at'))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'expires_at', 'is_active', 'role', 'email_status')
        required_fields = ('first_name', 'last_name', 'email', 'expires_at', 'role', 'email_status')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields.pop('email')
        else:
            self.fields.pop('email_status')
