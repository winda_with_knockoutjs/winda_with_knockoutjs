# -*- coding: utf-8 -*-
from django.contrib.auth.models import UserManager
from django.db import models
from django.utils import timezone


class ItemManager(UserManager):
    def items(self):
        from orders.models import Pricing
        queryset = Pricing.objects.filter(valid_from__lte=timezone.now()).order_by('-valid_from')
        return self.get_queryset()\
            .prefetch_related(models.Prefetch('prices', queryset=queryset, to_attr='prices_list'))


class TransactionManager(UserManager):

    def current_credit_balance(self):
        return self.get_queryset().aggregate(balance=models.Sum('transaction_value'))['balance'] or 0
