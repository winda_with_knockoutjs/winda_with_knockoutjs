# -*- coding: utf-8 -*-
from allauth.account.adapter import DefaultAccountAdapter
from allauth.utils import build_absolute_uri
from django import forms
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from common.templatetags.common import get_confirmation_url
from registration.models import Registration


class AccountAdapter(DefaultAccountAdapter):
    def new_user(self, request):
        return Registration()

    def save_user(self, request, registration, form, commit=True):
        data = form.cleaned_data
        for field, value in data.items():
            setattr(registration, field, value)
        registration.registration_type = form.registration_type
        registration.save()
        return registration

    def get_email_confirmation_url(self, request, confirmation):
        url = get_confirmation_url(confirmation.registration_type, confirmation.activation_code)
        ret = build_absolute_uri(request, url)
        return ret

    def send_confirmation_mail(self, request, emailconfirmation, signup):
        activate_url = self.get_email_confirmation_url(request, emailconfirmation)
        ctx = dict(user=emailconfirmation, activate_url=activate_url)
        if signup:
            email_template = 'account/email/email_confirmation_signup'
        else:
            email_template = 'account/email/email_confirmation'
        self.send_mail(email_template, emailconfirmation.email, ctx)

    def confirm_email(self, request, registration):
        return

    def respond_email_verification_sent(self, request, user):
        request.session['user_email'] = user.email

    def clean_password(self, password):
        if len(password) > settings.ACCOUNT_PASSWORD_MAX_LENGTH:
            raise forms.ValidationError(_("{0} character maximum.").format(settings.ACCOUNT_PASSWORD_MAX_LENGTH))
        min_length = settings.ACCOUNT_PASSWORD_MIN_LENGTH
        if len(password) < min_length:
            raise forms.ValidationError(_("Minimum password length is {0} characters.").format(min_length))
        return password

    def can_login(self, user):
        if not user.is_active:
            return 'account_inactive'
        if user.expires_at <= timezone.now():
            return 'account_expires'
        if user.archived_at is not None:
            return 'account_archived'
        if user.training_provider_id and not user.training_provider.is_active:
            return 'training_provider_inactive'
        return True
