# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import users.managers


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20160702_0846'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='user',
            managers=[
                ('objects', users.managers.WindaUserManager()),
            ],
        ),
    ]
