# -*- coding: utf-8 -*-
from collections import namedtuple
from django.contrib.auth.models import UserManager
from django.core.cache import cache
from django.db import connection
from django.utils import timezone


class TrainingRecordManager(UserManager):

    def search_query(self, delegate_id_list):
        return '''
            SELECT tr.id, dt.delegate_id as delegate_id, tr.first_name, tr.last_name, course.title as course_title,
                   course.code as course_code, tp.name as training_provider, country.name as country, tr.completed_on,
                   tr.valid_from, tr.valid_until, tr.invalidated_at
            FROM (
              SELECT tr.*, u.first_name, u.last_name, u.id as user_id, u.delegate_id as user_delegate_id
              FROM delegate_trainingrecord AS tr
              INNER JOIN users_user as u on tr.delegate_id = u.id
              WHERE (tr.valid_until >= CURDATE() OR tr.valid_until IS NULL) AND tr.invalidated_at IS NULL
                    AND u.delegate_id in ({0})
            ) AS tr
            INNER JOIN courses_course as course on course.id = tr.course_id
            INNER JOIN trainings_trainingprovider as tp on tp.id = tr.training_provider_id
            INNER JOIN common_country as country on tr.country_id = country.id
            RIGHT JOIN (SELECT %s COLLATE utf8_unicode_ci AS delegate_id {1}) as dt on dt.delegate_id=tr.user_delegate_id
        '''.format(', '.join(['%s'] * len(delegate_id_list)),
                   'UNION SELECT %s COLLATE utf8_unicode_ci ' * (len(delegate_id_list) - 1))

    def search(self, delegate_id_list, ordering_field, **kwargs):
        cursor = connection.cursor()
        sql = self.search_query(delegate_id_list) + '''
            ORDER BY {} {}, tr.completed_on ASC, tr.id,tr.user_id LIMIT %s OFFSET %s
        '''.format(ordering_field, 'asc' if kwargs['sort_asc'] else 'desc')
        delegate_id_list = self.get_search_params(delegate_id_list)
        delegate_id_list.extend([kwargs['length'], kwargs['start']])
        cursor.execute(sql, delegate_id_list)
        _result = namedtuple('TrainingRecord', ['id', 'delegate_id', 'first_name', 'last_name', 'course_title',
                                                'course_code', 'training_provider', 'country', 'completed_on',
                                                'valid_from', 'valid_until', 'invalidated_at'])
        return [_result(*item) for item in cursor.fetchall()]

    def get_search_params(self, delegate_id_list):
        delegate_id_list = delegate_id_list.copy()
        delegate_id_list.extend(delegate_id_list)
        return delegate_id_list

    def search_total_count(self, delegate_id_list, **kwargs):
        key = ('_{}' * len(delegate_id_list)).format(*delegate_id_list).strip('_')
        count = cache.get(key)
        if kwargs.get('draw') == 1:  # if user make first query on the page
            cache.delete(key)
            count = None
        if count:
            return count
        cursor = connection.cursor()
        sql = 'select count(*) from ({}) as q'.format(self.search_query(delegate_id_list))
        delegate_id_list = self.get_search_params(delegate_id_list)
        cursor.execute(sql, delegate_id_list)
        rows = cursor.fetchall()
        count = int(rows[0][0])
        cache.set(key, count, 60 * 60 * 24)
        return count

    def current_training(self):
        return self.model.objects.filter(valid_until__gte=timezone.now(), invalidated_at__isnull=True)
