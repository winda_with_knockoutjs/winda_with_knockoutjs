# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_auto_20160707_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delegatesequence',
            name='sequential_prefix',
            field=models.PositiveIntegerField(verbose_name='sequential prefix', unique=True),
        ),
    ]
