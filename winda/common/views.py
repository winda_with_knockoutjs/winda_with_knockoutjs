# -*- coding: utf-8 -*-
import logging
from braces.views import JSONResponseMixin

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.defaults import page_not_found
from django.views.generic import TemplateView, RedirectView

from common.forms import EmailNotificationForm
from common.models import EmailNotification
from core.views import CreateView


logger = logging.getLogger('sentry.errors')


class HomeView(TemplateView):
    template_name = 'common/home.html'

    home_mapping = {
        get_user_model().DELEGATE: ('delegate:home', 'delegate_id', 'delegate_id'),
        get_user_model().ORGANIZATION: ('organization:home', 'pk', 'pk'),
        get_user_model().TRAINING_PROVIDER: ('training_provider:home', 'pk', 'training_provider_id'),
        get_user_model().TRAINING_PROVIDER_ADMIN: ('training_provider:home', 'pk', 'training_provider_id'),
        get_user_model().DATABASE_ADMIN: ('database_admin:home', '', ''),
        get_user_model().DATABASE_SUPER_ADMIN: ('database_super_admin:home', '', ''),
    }

    def dispatch(self, request, *args, **kwargs):
        if request.user and request.user.is_authenticated():
            values = self.home_mapping.get(request.user.role)
            if values:
                url, slug_name, field = values
                kwargs = {slug_name: getattr(request.user, field)} if slug_name else {}
                return redirect(reverse(url, kwargs=kwargs))

        return super(HomeView, self).dispatch(request, *args, **kwargs)


class SendEmailNotificationRedirectView(RedirectView):
    permanent = False
    pattern_name = 'admin:common_emailnotification_changelist'

    def get(self, request, *args, **kwargs):
        call_command('deliveremails')
        return super().get(request, *args, **kwargs)


class ArchiveRedirectView(RedirectView):
    permanent = False
    pattern_name = 'admin:users_user_changelist'

    def get(self, request, *args, **kwargs):
        call_command('archive')
        return super().get(request, *args, **kwargs)


class GenerateNotificationsRedirectView(RedirectView):
    permanent = False
    pattern_name = 'admin:common_emailnotification_changelist'

    def get(self, request, *args, **kwargs):
        call_command('generatenotifications', date=request.GET.get('date', ''))
        return super().get(request, *args, **kwargs)


class SNSCallback(JSONResponseMixin, CreateView):
    http_method_names = ['post']
    model = EmailNotification
    form_class = EmailNotificationForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()
        return HttpResponse(status=200)

    def form_invalid(self, form):
        logger.error('{} - {}'.format(form.errors.as_text(), self.request.body))
        return self.render_json_response(form.errors, status=400)


def page_not_found_view(request, template_name='404.html', **kwargs):
    return page_not_found(request, template_name)
