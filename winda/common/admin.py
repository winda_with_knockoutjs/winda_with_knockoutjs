# -*- coding: utf-8 -*-
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.contrib.flatpages.models import FlatPage
from django.db.models import TextField
from reversion.admin import VersionAdmin

from common.models import DelegateSequence, Country, EmailNotification
from core.admin import ModelAdmin


@admin.register(EmailNotification)
class EmailNotificationAdmin(ModelAdmin):
    list_display = ('recipient', 'subject', 'notification_type', 'posted_at')
    search_fields = ('recipient', 'subject')
    list_filter = ('notification_type',)


@admin.register(Country)
class CountryAdmin(VersionAdmin, ModelAdmin):
    pass


admin.site.register(DelegateSequence, ModelAdmin)
admin.site.unregister(FlatPage)


@admin.register(FlatPage)
class FlatPageAdmin(ModelAdmin):
    formfield_overrides = {
        TextField: {'widget': CKEditorUploadingWidget}
    }
