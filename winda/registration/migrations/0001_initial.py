# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import common.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LostPassword',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('expires_at', models.DateTimeField(default=common.models.TimeStampedModel.expires_at_default, verbose_name='expires at')),
                ('claimed_at', models.DateTimeField(blank=True, verbose_name='claimed at', null=True)),
                ('activation_code', models.UUIDField(editable=False, default=uuid.uuid4)),
            ],
            options={
                'verbose_name': 'Lost Password',
                'verbose_name_plural': 'Lost Password',
            },
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('address_1', models.CharField(blank=True, verbose_name='street address line 1', max_length=50)),
                ('address_2', models.CharField(blank=True, verbose_name='street address line 2', max_length=50)),
                ('city', models.CharField(blank=True, verbose_name='city', max_length=50)),
                ('postal_code', models.CharField(blank=True, verbose_name='postal code', max_length=20)),
                ('archived_from', models.DateTimeField(default=common.models.TimeStampedModel.archived_from_default, verbose_name='archived from')),
                ('archived_at', models.DateTimeField(blank=True, verbose_name='archived at', null=True)),
                ('job_title', models.CharField(blank=True, verbose_name='job title', max_length=50)),
                ('organization_name', models.CharField(blank=True, verbose_name='organization name', max_length=50)),
                ('first_name', models.CharField(verbose_name='first name', max_length=30)),
                ('last_name', models.CharField(verbose_name='last name', max_length=30)),
                ('email', models.EmailField(verbose_name='email address', max_length=254)),
                ('registration_type', models.CharField(choices=[('delegate', 'delegate'), ('organization', 'organization'), ('training provider user', 'training provider user')], verbose_name='registration type', max_length=30)),
                ('expires_at', models.DateTimeField(default=common.models.TimeStampedModel.expires_at_default, verbose_name='expires at')),
                ('claimed_at', models.DateTimeField(blank=True, verbose_name='claimed at', null=True)),
                ('activation_code', models.UUIDField(editable=False, default=uuid.uuid4)),
                ('country', models.ForeignKey(blank=True, verbose_name='country', null=True, to='common.Country')),
            ],
            options={
                'verbose_name': 'Registration',
                'verbose_name_plural': 'Registration',
            },
        ),
    ]
