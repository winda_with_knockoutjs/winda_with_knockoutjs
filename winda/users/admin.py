# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from reversion.admin import VersionAdmin

from core.admin import ModelAdmin


@admin.register(get_user_model())
class UserAdmin(VersionAdmin, ModelAdmin, BaseUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'role', 'delegate_id',
                    'last_updated_by', 'created_by')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'role')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'phone', 'role', 'delegate_id', 'timezone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('User Information'), {'fields': ('company_name', 'job_title', 'organization_name', 'training_provider', 'email_status')}),
        (_('User Address'), {'fields': ('address_1', 'address_2', 'city', 'postal_code', 'country')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 'expires_at', 'archived_from', 'archived_at')}),
        (_('User Permissions'), {'fields': ('can_upload', 'can_buy_credit', 'can_use_credit')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'role'),
        }),
    )
    readonly_fields = ('delegate_id',)

    def get_queryset(self, request):
        return super().get_queryset(request).exclude(pk=settings.SYSTEM_USER_ID)


admin.site.unregister(Group)
