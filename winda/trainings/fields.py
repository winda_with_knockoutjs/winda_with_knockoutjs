# -*- coding:utf-8 -*-
from django.forms import ModelChoiceField
from django.forms.models import ModelChoiceIterator
from django.forms.fields import ChoiceField


class CourseModelChoiceIterator(ModelChoiceIterator):

    def choice(self, obj):
        return (self.field.prepare_value(obj), self.field.label_from_instance(obj), obj.course_type)


class CourseModelChoiceField(ModelChoiceField):

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices

        return CourseModelChoiceIterator(self)

    choices = property(_get_choices, ChoiceField._set_choices)
