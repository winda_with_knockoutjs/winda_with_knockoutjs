# -*- coding: utf-8 -*-
from unittest import mock
from datetime import date
from django.core.files.storage import FileSystemStorage
import io
import random
import factory
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase, override_settings
from django.utils import timezone, lorem_ipsum
from storages.backends.s3boto import S3BotoStorage

from common.models import EmailNotification
from courses.factories import CourseFactory
from courses.models import CourseProvider
from registration.factories import RegistrationFactory, CountryFactory
from registration.models import Registration
from trainings.factories import TrainingProviderCertificateFactory, TrainingProviderFactory
from trainings.models import TrainingProvider, TrainingProviderCertificate
from users.factories import UserFactory


fss = FileSystemStorage()


class TrainingProviderRegistrationTestCase(TestCase):

    def setUp(self):
        self.url = reverse('training_provider_signup')
        self.user_count = get_user_model().objects.count()
        self.system_user = get_user_model().objects.get(pk=settings.SYSTEM_USER_ID)
        self.country = CountryFactory()
        self.courses = CourseFactory.create_batch(5)

    @override_settings(CERTIFICATE_MAX_UPLOAD_SIZE=3)
    def test_check_pdf_certificate_loaded(self):
        """
        Test rule for certificate pdf file
        """
        image = io.StringIO('GIF87a\x01\x00\x01\x00\x80\x01\x00\x00\x00\x00ccc,\x00'
                            '\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00;')
        image.name = 'image.gif'
        data = self.data(**{'form-0-certificate_file': image})
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'File attachment type not allowed.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

        data = self.data()
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'File attachment exceeds maximum size limit.', 2)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

    def test_check_valid_from_and_valid_until_field(self):
        """
        Test rule for valid_from and valid_until fields
        """
        target_date = (timezone.now().date() + timezone.timedelta(days=1)).strftime(settings.FORMS_DATE_FORMAT)
        data = self.data(**{'form-0-valid_from': target_date})
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Date must be in the past.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

        data = self.data(**{'form-0-valid_until': timezone.now().date().strftime(settings.FORMS_DATE_FORMAT)})
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Date must be in the future.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

    def data(self, provider_certificate_count=2, **kwargs):
        data = factory.build(dict, FACTORY_CLASS=RegistrationFactory, terms=True, policy=True, country=self.country.pk,
                             training_provider=lorem_ipsum.words(1, False), agreement=True,
                             **{'form-TOTAL_FORMS': provider_certificate_count, 'form-INITIAL_FORMS': 0,
                                'form-MIN_NUM_FORMS': 0, 'form-MAX_NUM_FORMS': 20})
        for i in range(provider_certificate_count):
            courses = [course.pk for course in random.sample(self.courses, 3)]
            data.update({'form-{}-{}'.format(i, name): value for name, value in
                         factory.build(dict, FACTORY_CLASS=TrainingProviderCertificateFactory, courses=courses).items()})
        data.update(**kwargs)
        return data

    @mock.patch.object(S3BotoStorage, '_save', fss._save)
    def test_registration(self):
        """
        Anonymous user successfully register
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, '<li>Delegate Registration</li>')
        self.assertNotContains(response, '<li>Organization Registration</li>')
        self.assertContains(response, '<li>Training Provider Registration</li>')

        data_kwargs = {
            'form-0-valid_from': timezone.now().date() - timezone.timedelta(days=5),
            'form-0-valid_until': timezone.now().date() + timezone.timedelta(days=5),
            'form-0-courses': [course.pk for course in self.courses[:3]],

            'form-1-valid_from': timezone.now().date() - timezone.timedelta(days=2),
            'form-1-valid_until': timezone.now().date() + timezone.timedelta(days=15),
            'form-1-courses': [course.pk for course in self.courses[:4]],

            'form-2-valid_from': timezone.now().date() - timezone.timedelta(days=100),
            'form-2-valid_until': timezone.now().date() + timezone.timedelta(days=2),
            'form-2-courses': [course.pk for course in self.courses[1:3]],

            'form-3-valid_from': timezone.now().date() - timezone.timedelta(days=2),
            'form-3-valid_until': timezone.now().date() + timezone.timedelta(days=10),
            'form-3-courses': [self.courses[0].pk, self.courses[4].pk],
        }

        data = self.data(provider_certificate_count=4, **data_kwargs)
        for key, value in data.items():
            if isinstance(value, date):
                data[key] = value.strftime(settings.FORMS_DATE_FORMAT)

        response = self.client.post(self.url, data)
        self.assertRedirects(response, reverse('account_email_verification_sent', args=('training-provider', )))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA Training Provider Account Activation')

        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

        reg = Registration.objects.first()
        training_provider = TrainingProvider.objects.last()

        # check registration
        for field in ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(reg, field), data[field])
        self.assertEqual(reg.registration_type, Registration.TRAINING_PROVIDER_ADMIN)
        self.assertEqual(reg.country, self.country)
        self.assertEqual(reg.training_provider, training_provider)
        self.assertTrue(reg.activation_code)
        self.assertEqual(reg.expires_at.strftime(settings.DATETIME_FORMAT),
                         (timezone.now() + timezone.timedelta(hours=settings.REGISTRATION_EXPIRY_HOURS))
                         .strftime(settings.DATETIME_FORMAT))
        self.assertEqual(reg.archived_from.date(), timezone.now().date() + relativedelta(months=12))
        self.assertIsNone(reg.claimed_at)

        # check training provider
        for field in ('address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(training_provider, field), data[field])
        self.assertFalse(training_provider.is_active)
        self.assertEqual(training_provider.created_by, self.system_user)
        self.assertEqual(training_provider.country, self.country)
        self.assertEqual(training_provider.name, data['training_provider'])

        # check training provider certificate
        training_provider_certificates = TrainingProviderCertificate.objects.order_by('pk')
        for i, training_provider_certificate in enumerate(training_provider_certificates):
            self.assertEqual(getattr(training_provider_certificate, 'certificate_number'),
                             data['form-{}-{}'.format(i, 'certificate_number')])
            for field in ('valid_from', 'valid_until'):
                self.assertEqual(getattr(training_provider_certificate, field).strftime(settings.FORMS_DATE_FORMAT),
                                 data['form-{}-{}'.format(i, field)])
            self.assertIsNotNone(training_provider_certificate.certificate_file)
            self.assertEqual(training_provider_certificate.training_provider, training_provider)
            self.assertFalse(training_provider_certificate.is_deleted)
            self.assertEqual(training_provider_certificate.created_by, self.system_user)

        # check course provider
        course_provider = CourseProvider.objects.get(course_id=self.courses[0].pk)
        self.assertEqual(course_provider.valid_until, timezone.now().date() + timezone.timedelta(days=15))
        self.assertEqual(course_provider.valid_from, timezone.now().date() - timezone.timedelta(days=5))

        course_provider = CourseProvider.objects.get(course_id=self.courses[1].pk)
        self.assertEqual(course_provider.valid_until, timezone.now().date() + timezone.timedelta(days=15))
        self.assertEqual(course_provider.valid_from, timezone.now().date() - timezone.timedelta(days=100))

        course_provider = CourseProvider.objects.get(course_id=self.courses[2].pk)
        self.assertEqual(course_provider.valid_until, timezone.now().date() + timezone.timedelta(days=15))
        self.assertEqual(course_provider.valid_from, timezone.now().date() - timezone.timedelta(days=100))

        course_provider = CourseProvider.objects.get(course_id=self.courses[3].pk)
        self.assertEqual(course_provider.valid_until, timezone.now().date() + timezone.timedelta(days=15))
        self.assertEqual(course_provider.valid_from, timezone.now().date() - timezone.timedelta(days=2))

        course_providers = CourseProvider.objects.order_by('pk')
        for course_provider in course_providers:
            self.assertEqual(course_provider.training_provider, training_provider)
            self.assertFalse(course_provider.is_deleted)
            self.assertEqual(course_provider.created_by, self.system_user)

    def test_registration_invalid_data(self):
        """
        Anonymous user submit blank form and get error messages
        """
        response = self.client.post(self.url, {'email': 'test', 'form-TOTAL_FORMS': 1, 'form-INITIAL_FORMS': 0,
                                               'form-MIN_NUM_FORMS': 0, 'form-MAX_NUM_FORMS': 20})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 15)
        self.assertContains(response, 'Enter a valid email address.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

    def test_registration_email_already_used(self):
        """
        Anonymous user submit form with existing email and get error messages
        """
        user = UserFactory()
        data = self.data(email=user.email)
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This email address is already in use.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count + 1)
        self.assertEqual(Registration.objects.count(), 0)

    def test_confirm_email_get(self):
        """
        Anonymous user successfully get to activation link.
        """
        reg = RegistrationFactory(registration_type=Registration.TRAINING_PROVIDER_ADMIN)
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Training Provider Account Activation')
        self.assertContains(response, 'Activate and Login', 1)

    def test_confirm_email_post(self):
        """
        Anonymous user successfully set password and login
        """
        database_admins = UserFactory.create_batch(5, role=get_user_model().DATABASE_ADMIN)
        training_provider = TrainingProviderFactory()
        reg = RegistrationFactory(registration_type=Registration.TRAINING_PROVIDER_ADMIN,
                                  training_provider=training_provider)
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse('account_confirm_email', args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertRedirects(response, reverse('review'))
        self.assertEqual(len(mail.outbox), 0)

        self.assertEqual(get_user_model().objects.count(), self.user_count + len(database_admins) + 1)
        self.assertEqual(Registration.objects.count(), 1)

        self.assertEqual(EmailNotification.objects.count(), len(database_admins))

        reg.refresh_from_db()
        self.assertIsNotNone(reg.claimed_at)

        user = get_user_model().objects.last()
        for field in ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code',
                      'country', 'training_provider'):
            self.assertEqual(getattr(reg, field), getattr(user, field))
        self.assertEqual(reg.first_name, user.first_name)
        self.assertEqual(reg.last_name, user.last_name)
        self.assertEqual(reg.email, user.email)
        self.assertEqual(reg.registration_type, user.role)
        self.assertEqual(user.role, get_user_model().TRAINING_PROVIDER_ADMIN)
        self.assertEqual(user.expires_at, timezone.datetime(2099, 12, 31, tzinfo=timezone.UTC()))
        self.assertEqual(user.last_updated_by.pk, settings.SYSTEM_USER_ID)
        self.assertEqual(user.created_by.pk, settings.SYSTEM_USER_ID)
        self.assertEqual(user.training_provider.created_by.pk, settings.SYSTEM_USER_ID)
        self.assertEqual(user.training_provider, training_provider)
        self.assertIsNone(user.delegate_id)
        self.assertTrue(user.can_upload)
        self.assertTrue(user.can_buy_credit)
        self.assertTrue(user.can_use_credit)
        self.assertTrue(user.is_active)
        self.assertEqual(user.archived_from.date(), timezone.now().date() + relativedelta(months=24))
        self.assertIsNotNone(reg.claimed_at)

    def test_confirmation_email_already_used(self):
        """
        Anonymous user try to confirm existing email and get error
        """
        user = UserFactory()
        self.user_count = get_user_model().objects.count()
        reg = RegistrationFactory(email=user.email, registration_type=Registration.TRAINING_PROVIDER_ADMIN)
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Account has already been activated. Please login to your account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_wrong_password(self):
        """
        Anonymous user try to confirm his email and set wrong password
        """
        reg = RegistrationFactory(registration_type=Registration.TRAINING_PROVIDER_ADMIN)
        data = {
            'password1': '12345678',
            'password2': '12345679',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Passwords do not match.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_account_is_claimed(self):
        """
        Anonymous user try to confirm claimed email
        """
        reg = RegistrationFactory(claimed_at=timezone.now(), registration_type=Registration.TRAINING_PROVIDER_ADMIN)
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Account has already been activated. Please login to your account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_link_is_expired(self):
        """
        Anonymous user try to confirm expired email
        """
        reg = RegistrationFactory(expires_at=timezone.now() - timezone.timedelta(minutes=10),
                                  registration_type=Registration.TRAINING_PROVIDER_ADMIN)
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Activation code has expired. Please register for a new account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)
