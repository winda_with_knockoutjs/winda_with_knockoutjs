# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trainings', '0006_auto_20160707_1027'),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_code', models.CharField(verbose_name='item code', unique=True, max_length=10)),
                ('item_name', models.CharField(verbose_name='item name', max_length=30)),
                ('item_description', models.TextField(verbose_name='item description')),
            ],
            options={
                'verbose_name': 'Item',
                'verbose_name_plural': 'Items',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('currency', models.CharField(default='EUR', verbose_name='currency', max_length=3)),
                ('quantity', models.PositiveSmallIntegerField(verbose_name='quantity')),
                ('item_code', models.CharField(verbose_name='item code', max_length=10, choices=[('Single', 'Single'), ('Package', 'Package')])),
                ('item_name', models.CharField(verbose_name='item name', max_length=30)),
                ('item_unit_price_including_vat', models.PositiveIntegerField(verbose_name='item unit price including vat')),
                ('item_vat_rate', models.DecimalField(decimal_places=2, verbose_name='item vat rate', max_digits=3)),
                ('total_credits', models.PositiveIntegerField(verbose_name='total credits')),
                ('total_price_including_vat', models.PositiveIntegerField(verbose_name='total price including vat')),
                ('payment_success', models.BooleanField(verbose_name='Payment Success')),
                ('created_by', models.ForeignKey(related_name='order_created_by', to=settings.AUTH_USER_MODEL, default=1)),
                ('last_updated_by', models.ForeignKey(related_name='order_last_updated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
                ('training_provider', models.ForeignKey(related_name='orders', to='trainings.TrainingProvider', verbose_name='Training Provider')),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='PaymentStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('callback_id', models.PositiveIntegerField(verbose_name='callback id')),
                ('accepted', models.BooleanField(verbose_name='accepted')),
                ('test_mode', models.BooleanField(verbose_name='test mode')),
                ('operation_type', models.CharField(verbose_name='operation type', max_length=10)),
                ('amount', models.PositiveIntegerField(verbose_name='amount')),
                ('pending', models.BooleanField(verbose_name='pending')),
                ('quick_pay_status_code', models.CharField(verbose_name='quick pay status code', max_length=10)),
                ('quick_pay_status_message', models.CharField(verbose_name='quick pay status message', max_length=30)),
                ('acquirer_status_code', models.CharField(verbose_name='acquirer status code', max_length=10)),
                ('acquirer_status_message', models.CharField(verbose_name='acquirer status message', max_length=30)),
                ('balance', models.BooleanField(verbose_name='balance')),
                ('order', models.ForeignKey(verbose_name='order', to='orders.Order')),
            ],
            options={
                'verbose_name': 'Payment Status',
                'verbose_name_plural': 'PaymentStatuses',
            },
        ),
        migrations.CreateModel(
            name='Pricing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('currency', models.CharField(default='EUR', verbose_name='currency', max_length=3)),
                ('valid_from', models.DateField(verbose_name='valid from', unique=True)),
                ('unit_price_including_vat', models.IntegerField(verbose_name='unit price including vat')),
                ('vat_rate', models.DecimalField(decimal_places=2, verbose_name='vat rate', max_digits=3)),
                ('credits_per_unit', models.IntegerField(verbose_name='credits per unit')),
                ('created_by', models.ForeignKey(related_name='pricing_created_by', to=settings.AUTH_USER_MODEL, default=1)),
                ('item', models.ForeignKey(verbose_name='item', to='orders.Item')),
                ('last_updated_by', models.ForeignKey(related_name='pricing_last_updated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Pricing',
                'verbose_name_plural': 'Pricing',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('transaction_value', models.IntegerField(verbose_name='transaction value')),
                ('reason', models.CharField(verbose_name='reason', max_length=200)),
                ('order', models.ForeignKey(verbose_name='order', to='orders.Order')),
                ('training_provider', models.ForeignKey(verbose_name='training provider', to='trainings.TrainingProvider')),
                ('upload_batch', models.ForeignKey(verbose_name='upload batch', to='trainings.UploadBatch')),
            ],
            options={
                'verbose_name': 'Transaction',
                'verbose_name_plural': 'Transactions',
            },
        ),
    ]
