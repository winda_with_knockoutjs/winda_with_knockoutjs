# -*- coding: utf-8 -*-
from collections import OrderedDict

from django import forms
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

from common.forms import BaseFilterForm
from common.mixins import UserFormMixin
from core.forms import ModelForm

User = get_user_model()


class DelegateProfileForm(UserFormMixin, ModelForm):

    class Meta:
        model = User
        fields = ('delegate_id', 'first_name', 'last_name', 'email', 'timezone')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ('delegate_id', 'first_name', 'last_name'):
            self.fields[field].widget.attrs['readonly'] = True


class TrainingRecordFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('course__title', _('Course Title')),
        ('course__code', _('Course Code')),
        ('training_provider__name', _('Training Provider')),
        ('country__name', _('Country')),
        ('completed_on', _('Completion Date')),
        ('valid_from', _('Valid From')),
        ('valid_until', _('Valid Until')),
        ('status', _('Status'))
    ])


class SearchForm(forms.Form):
    delegate_id = forms.CharField(validators=[RegexValidator(r'^[0-9a-zA-Z ]*$', 'Only alphanumeric characters are allowed.')],
                                  label=_('Winda ID'))

    def clean_delegate_id(self):
        delegate_id = self.cleaned_data['delegate_id']
        return delegate_id.lower().strip()


class SearchFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('delegate__delegate_id', _('Winda ID')),
        ('delegate__first_name', _('First Name')),
        ('delegate__last_name', _('Last Name')),
        ('course__title', _('Course Title')),
        ('course__code', _('Course Code')),
        ('training_provider__training_provider__name', _('Training Provider')),
        ('country', _('Country')),
        ('completed_on', _('Completion Date')),
        ('valid_from', _('Valid From')),
        ('valid_until', _('Valid Until')),
        ('status', _('Status')),
    ])
