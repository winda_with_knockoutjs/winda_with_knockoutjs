# -*- coding: utf-8 -*-
import factory

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from common.models import EmailNotification
from registration.factories import CountryFactory
from trainings.factories import TrainingProviderFactory
from trainings.models import TrainingProvider
from users.factories import DatabaseAdminFactory, TEST_USER_PASSWORD, UserFactory


class TrainingProviderManageTestCase(TestCase):

    def setUp(self):
        self.training_provider = TrainingProviderFactory()
        UserFactory(training_provider=self.training_provider)
        self.url = reverse('database_admin:training_provider_detail', args=(self.training_provider.pk,))
        self.create_url = reverse('database_admin:training_provider_create')
        self.update_url = reverse('database_admin:training_provider_update', args=(self.training_provider.pk,))
        self.user = DatabaseAdminFactory()
        self.country = self.training_provider.country

    def test_detail(self):
        """
        Database Admin get training provider detail page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.training_provider.name.title())

    def test_create(self):
        """
        Database Admin create new training provider
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Create training provider', 1)

        data = factory.build(dict, FACTORY_CLASS=TrainingProviderFactory, is_active=True, country=self.country.pk)
        response = self.client.post(self.create_url, data)
        self.assertRedirects(response, reverse('database_admin:training_providers'))
        self.assertEqual(TrainingProvider.objects.count(), 2)

        training_provider = TrainingProvider.objects.last()
        for field in ('name', 'address_1', 'address_2', 'city', 'postal_code', 'is_active'):
            self.assertEqual(getattr(training_provider, field), data.get(field, ''))
        self.assertEqual(training_provider.is_active, True)
        self.assertIsNotNone(training_provider.accepted_at)
        self.assertEqual(training_provider.accepted_by, self.user)

    def test_create_fail(self):
        """
        Database Admin submit blank create new training provider form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProvider.objects.count(), 1)
        self.assertContains(response, 'This field is required.', 5)

    def test_update_accepted_training_provider(self):
        """
        Database Admin update accepted training provider form
        """
        self.training_provider.accepted_at = timezone.now()
        self.training_provider.save()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage Training Provider', 1)
        self.assertContains(response, self.training_provider.name.title(), 1)
        self.assertNotContains(response, 'Reject')
        self.assertNotContains(response, 'Accept')

        data = factory.build(dict, FACTORY_CLASS=TrainingProviderFactory, country=self.country.pk)
        response = self.client.post(self.update_url, data)
        self.assertRedirects(response, reverse('database_admin:training_providers'))
        self.assertEqual(TrainingProvider.objects.count(), 1)
        self.assertEqual(EmailNotification.objects.count(), 0)

        self.training_provider.refresh_from_db()
        self.assertIsNotNone(self.training_provider.accepted_at)
        self.assertIsNone(self.training_provider.accepted_by)
        self.assertFalse(self.training_provider.is_active)
        for field in ('name', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(self.training_provider, field), data.get(field, ''))

    def test_update_not_accepted_training_provider(self):
        """
        Database Admin accept training provider
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.update_url)
        self.assertContains(response, 'Reject', 1)
        self.assertContains(response, 'Accept', 1)

        data = factory.build(dict, FACTORY_CLASS=TrainingProviderFactory, country=self.country.pk, accept=True)
        response = self.client.post(self.update_url, data)
        self.assertRedirects(response, reverse('database_admin:training_providers'))
        self.assertEqual(TrainingProvider.objects.count(), 1)
        self.assertEqual(EmailNotification.objects.count(), 1)

        self.training_provider.refresh_from_db()
        self.assertIsNotNone(self.training_provider.accepted_at)
        self.assertEqual(self.training_provider.accepted_by, self.user)
        self.assertTrue(self.training_provider.is_active)
        for field in ('name', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(self.training_provider, field), data.get(field, ''))

    def test_reject(self):
        """
        Database Admin reject training provider
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {'reject': True}
        response = self.client.post(self.update_url, data)
        self.assertRedirects(response, reverse('database_admin:training_providers'))
        self.assertEqual(TrainingProvider.objects.count(), 0)
        self.assertEqual(EmailNotification.objects.count(), 0)

    def test_update_fail(self):
        """
        Database Admin submit blank create new training provider form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TrainingProvider.objects.count(), 1)
        self.assertContains(response, 'This field is required.', 5)
