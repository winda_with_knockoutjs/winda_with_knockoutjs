# -*- coding: utf-8 -*-
import json
from urllib.parse import urlencode

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils import timezone

from delegate.models import Search
from users.factories import TEST_USER_PASSWORD, DelegateFactory
from delegate.factories import TrainingRecordFactory


class SearchTestCase(TestCase):

    def setUp(self):
        self.delegate_user = DelegateFactory()
        self.search_delegate_id = self.delegate_user.delegate_id.lower()
        self.url = reverse('delegate:search')
        self.result_url = reverse('delegate:search_result', args=(self.search_delegate_id,))
        self.training_records = TrainingRecordFactory.create_batch(
            10, delegate=self.delegate_user, valid_until=timezone.now() + timezone.timedelta(days=2))

        self.expire_training_records = TrainingRecordFactory.create_batch(
            4, delegate=self.delegate_user, valid_until=timezone.now() - timezone.timedelta(days=2))

        self.invalidated_training_records = TrainingRecordFactory.create_batch(
            6, delegate=self.delegate_user, valid_until=timezone.now() + timezone.timedelta(days=2),
            invalidated_at=timezone.now())

        self.other_training_records = TrainingRecordFactory.create_batch(
            8, delegate=DelegateFactory(), valid_until=timezone.now() + timezone.timedelta(days=2))
        self.user = DelegateFactory()

        session = self.client.session
        session['search_by_delegate_id'] = True
        session['search_delegate_id'] = self.search_delegate_id
        session.save()

    def test_search(self):
        """
        Delegate user successfully search by delegate id
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertContains(response, 'Search')

        data = {'delegate_id': self.search_delegate_id}
        response = self.client.post(self.url, data)
        self.assertRedirects(response, self.result_url)

    def test_search_invalid_form(self):
        """
        Delegate user post wrong search data and get error
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 1)

    def test_search_result(self):
        """
        Delegate user get search result
        """
        self.delegate_user.refresh_from_db()
        archived_from = self.delegate_user.archived_from
        response = self.client.get(self.result_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }

        session = self.client.session
        del session['search_by_delegate_id']
        session.save()

        url = '{}?{}'.format(self.result_url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

        self.assertEqual(Search.objects.count(), 0)
        self.delegate_user.refresh_from_db()
        self.assertEqual(archived_from, self.delegate_user.archived_from)

    def test_search_by_other_delegate_id(self):
        """
        User go to search result and get redirect to search page
        """
        response = self.client.get(self.result_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        session = self.client.session
        del session['search_by_delegate_id']
        del session['search_delegate_id']
        session.save()

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.url)

    def test_search_result_with_session(self):
        """
        Delegate user get search result
        """
        archived_from = self.delegate_user.archived_from
        response = self.client.get(self.result_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

        self.assertEqual(Search.objects.count(), 1)
        self.delegate_user.refresh_from_db()
        self.assertNotEqual(archived_from, self.delegate_user.archived_from)

    def test_invalid_sort(self):
        """
        Delegate user get users with wrong sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 20,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'some text',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_valid_sort(self):
        """
        Delegate user get users with correct sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '4',
            'sort_asc': '-',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_wrong_data(self):
        """
        Delegate user get users wrong parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.result_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_wrong_search_delegate_id(self):
        """
        Delegate user get users wrong delegate_id
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        session = self.client.session
        session['search_by_delegate_id'] = True
        session['search_delegate_id'] = 'test1'
        session.save()
        self.result_url = reverse('delegate:search_result', args=('test1',))
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
