# -*- coding: utf-8 -*-
from collections import namedtuple
from django.conf import settings
from django.contrib.auth.models import UserManager

from django.db import connection, models
from django.utils import timezone
from courses.models import Course


class UserQuerySet(models.QuerySet):

    def delegates(self):
        return self.filter(role=self.model.DELEGATE)

    def organizations(self):
        return self.filter(role=self.model.ORGANIZATION)

    def training_providers(self):
        return self.filter(role__in=[self.model.TRAINING_PROVIDER, self.model.TRAINING_PROVIDER_ADMIN])

    def database_admins(self):
        return self.filter(role=self.model.DATABASE_ADMIN)

    def active(self):
        return self.filter(archived_at__isnull=True, expires_at__gte=timezone.now(), is_active=True)

    def search_query(self, delegate_id_list):
        return '''
            SELECT id, dt.delegate_id, first_name, last_name, courses FROM (
                SELECT id, delegate_id, first_name, last_name,
                      GROUP_CONCAT(CONCAT(course_id, ',', current_tr, ',', expired, ',', invalidated, ',', max_valid_until)
                                   ORDER BY course_id DESC SEPARATOR '|') as courses
                FROM (
                    SELECT u.id, u.delegate_id, u.first_name, u.last_name, tr.course_id,
                       SUM(CASE WHEN tr.invalidated_at IS NOT NULL OR tr.valid_until < CURDATE() THEN 1 ELSE 0 END) AS invalidated,
                       SUM(CASE WHEN tr.invalidated_at IS NULL AND tr.valid_until >= CURDATE() AND tr.valid_until < CURDATE() + INTERVAL %s MONTH THEN 1 ELSE 0 END) AS expired,
                       SUM(CASE WHEN tr.invalidated_at IS NULL AND tr.valid_until >= CURDATE() + INTERVAL %s MONTH THEN 1 ELSE 0 END) AS current_tr,
                       MAX(tr.valid_until) as max_valid_until

                    FROM users_user AS u
                    INNER JOIN delegate_trainingrecord AS tr ON tr.delegate_id = u.id
                    INNER JOIN courses_course AS course ON course.id = tr.course_id
                    WHERE u.delegate_id in ({0})
                    GROUP BY CASE WHEN course.course_type = %s THEN course.refresher_for_course_id
                       ELSE course.id END, u.id
                ) as u
                GROUP BY id
            ) as u
            RIGHT JOIN (SELECT %s COLLATE utf8_unicode_ci AS delegate_id {1}) AS dt ON dt.delegate_id=u.delegate_id
        '''.format(', '.join(['%s'] * len(delegate_id_list)),
                   'UNION SELECT %s COLLATE utf8_unicode_ci ' * (len(delegate_id_list) - 1))

    def search(self, delegate_id_list, ordering_field, **kwargs):
        cursor = connection.cursor()
        sql = self.search_query(delegate_id_list) + '''
            ORDER BY {} {} LIMIT %s OFFSET %s
        '''.format(ordering_field, 'asc' if kwargs['sort_asc'] else 'desc')
        delegate_id_list = self.get_search_params(delegate_id_list)
        delegate_id_list.extend([kwargs['length'], kwargs['start']])
        cursor.execute(sql, delegate_id_list)
        _result = namedtuple('User', ['id', 'delegate_id', 'first_name', 'last_name', 'courses'])
        return [_result(*item) for item in cursor.fetchall()]

    def get_search_params(self, delegate_id_list):
        delegate_id_list = delegate_id_list.copy()
        return [settings.EXPIRING_TRAINING_RECORD_MONTHS, settings.EXPIRING_TRAINING_RECORD_MONTHS] + \
            delegate_id_list + [Course.REFRESHER] + delegate_id_list

    def search_total_count(self, delegate_id_list, **kwargs):
        return len(delegate_id_list)


class WindaUserManager(UserManager.from_queryset(UserQuerySet)):
    pass
