$(document).ready(function() {

    var check_filter_by_course = function(self) {
        if (self.prop("checked")) {
            self.closest('.form-group').nextAll().removeClass('disabled');
        }
        else {
           self.closest('.form-group').nextAll().addClass('disabled').find('input, select').val('');
        }
    }

    check_filter_by_course($('#id_filter_by_course'));
    $('#id_filter_by_course').on('change', function(){
        check_filter_by_course($(this));
    });
});
