# -*- coding: utf-8 -*-
import re
from collections import OrderedDict

from django.conf import settings
from django.core.validators import RegexValidator
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from common.forms import BaseFilterForm
from common.mixins import UserFormMixin
from core.forms import ModelForm
from courses.models import Course


class BulkSearchForm(forms.Form):
    delegate_id = forms.CharField(widget=forms.Textarea(), max_length=1000,
                                  validators=[RegexValidator(r'^[0-9a-zA-Z\t ,\r\n]*$', 'Only alphanumeric characters are allowed.')],
                                  label=_('Enter up to 100 Winda IDs to search for'))

    def clean_delegate_id(self):
        delegate_id = self.cleaned_data['delegate_id']
        delegate_id = re.split(r'[\t ,\r\n]', delegate_id.strip('\t'))
        delegate_id = [item.lower() for item in delegate_id if item]
        if len(delegate_id) >= 100:
            raise forms.ValidationError(_('Maximum of 100 Winda  IDs allowed per search'))
        return delegate_id


class SearchFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('delegate_id', _('Winda ID')),
        ('first_name', _('First Name')),
        ('last_name', _('Last Name')),
        ('course_title', _('Course Title')),
        ('course_code', _('Course Code')),
        ('training_provider', _('Training Provider')),
        ('country', _('Country')),
        ('completed_on', _('Completion Date')),
        ('valid_from', _('Valid From')),
        ('valid_until', _('Valid Until')),
        ('status', _('Status')),
    ])


class GridSearchFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('delegate_id', _('Winda ID')),
        ('first_name', _('First Name')),
        ('last_name', _('Last Name')),
    ])

    SORTABLE_FIELDS = {
        'delegate_id', 'delegate_id',
        'first_name', 'first_name',
        'last_name', 'last_name'
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.courses = Course.objects.filter(course_type=Course.FULL).values_list('pk', 'title')
        self.FIELDS.update(self.courses)


class OrganizationUpdateForm(UserFormMixin, ModelForm):

    class Meta:
        model = get_user_model()
        fields = ('job_title', 'first_name', 'last_name', 'email', 'phone', 'organization_name',
                  'address_1', 'address_2', 'city', 'postal_code', 'country', 'timezone')
        required_fields = ('job_title', 'first_name', 'last_name', 'email', 'phone', 'organization_name',
                           'address_1', 'city', 'postal_code', 'country')
        labels = {
            'email': _('work email address')
        }
        help_texts = {
            'email': _('Please provide your work private email address. '
                       'Don\'t use a personal or shared email address.')
        }

    def clean_email(self):
        email = super().clean_email()
        if email.split('@')[1] in settings.ORGANIZATION_EMAIL_DOMAIN_BLACKLIST:
            raise forms.ValidationError(self.error_messages['email_in_blacklist'])
        return email
