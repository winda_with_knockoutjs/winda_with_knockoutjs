# -*- coding: utf-8 -*-
import json
from django.conf import settings
import hashlib
import hmac
from collections import OrderedDict

from django import forms
from django.utils.translation import ugettext_lazy as _

from common.forms import BaseFilterForm
from core.forms import ModelForm
from orders.models import Item, PaymentStatus, Transaction
from orders.widgets import ItemPriceSelectWidget


class PurchaseCreditForm(forms.Form):
    item = forms.ModelChoiceField(queryset=Item.objects.none(), required=True,
                                  label=_('item'), widget=ItemPriceSelectWidget())
    quantity = forms.IntegerField(min_value=1, max_value=10000, label=_('quantity'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['item'].queryset = Item.objects.items()


class OrderForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('Order Number')),
        ('created', _('Created At')),
        ('created_by', _('Created By')),
        ('payment_success', _('Status')),
        ('total_credits', _('Credits')),
        ('total_price_including_vat', _('Total')),
    ])


class PaymentStatusForm(ModelForm):

    class Meta:
        model = PaymentStatus
        fields = ('callback_id', 'accepted', 'test_mode', 'operation_type', 'amount', 'pending',
                  'quick_pay_status_code', 'quick_pay_status_message', 'order',
                  'acquirer_status_code', 'acquirer_status_message', 'balance')

    def __init__(self, *args, **kwargs):
        request = kwargs['request']
        kwargs['data'] = self.update_data(request)
        self.checksum = request.META['HTTP_QUICKPAY_CHECKSUM_SHA256']
        self.body = request.body
        super().__init__(*args, **kwargs)

    def clean(self):
        if self.checksum != self.sign():
            raise forms.ValidationError(_('Checksum does not valid.'))
        cd = super().clean()
        if not settings.QUICKPAY_TEST_MODE and cd['test_mode']:
            raise forms.ValidationError(_('Test payments are prohibited.'))
        return cd

    def sign(self):
        return hmac.new(bytearray(settings.QUICKPAY_PAYMENT_API_KEY, encoding='utf8'), self.body, hashlib.sha256).hexdigest()

    def update_data(self, request):
        data = json.loads(str(request.body, encoding='utf8'))

        for name, field_name in (('callback_id', 'id'), ('operation_type', 'type')):
            data[name] = data[field_name]
        fields = (('amount', 'amount'), ('pending', 'pending'), ('quick_pay_status_code', 'qp_status_code'),
                  ('quick_pay_status_message', 'qp_status_msg'), ('acquirer_status_code', 'aq_status_code'),
                  ('acquirer_status_message', 'aq_status_msg'))
        operation = data['operations'][-1]
        for name, field_name in fields:
            data[name] = operation[field_name]

        try:
            data['order'] = int(data['order_id'])
        except ValueError:
            pass
        return data


class TransactionForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('Transaction Number')),
        ('created', _('Created At')),
        ('created_by', _('Created By')),
        ('transaction_value', _('amount')),
        ('balance', _('Balance')),
        ('order', _('Order')),
        ('upload_batch', _('Upload')),
        ('description', _('Description')),
    ])


class AwardCreditForm(ModelForm):

    error_messages = {
        'invalid_amount': _('Amount must be a non-zero value between -100,000 and +100,000')
    }
    AMOUNT_MAX = 100000

    class Meta:
        model = Transaction
        fields = ('transaction_value', 'reason')
        labels = {
            'transaction_value': _('Amount'),
            'reason': _('Description')
        }
        required_fields = ('transaction_value', 'reason')

    def __init__(self, *args, **kwargs):
        self.training_provider = kwargs.pop('training_provider')
        super().__init__(*args, **kwargs)
        for field in self.Meta.required_fields:
            self.fields[field].required = True

    def clean_transaction_value(self):
        transaction_value = self.cleaned_data.get('transaction_value')
        if not transaction_value or abs(transaction_value) > self.AMOUNT_MAX:
            raise forms.ValidationError(self.error_messages.get('invalid_amount'))
        return transaction_value

    def save(self, commit=True):
        self.instance.training_provider = self.training_provider
        return super().save(commit)
