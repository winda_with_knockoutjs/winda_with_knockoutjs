# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid
from django.conf import settings
import delegate.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('delegate', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DelegateCertificate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('certificate_code', models.UUIDField(editable=False, default=uuid.uuid4)),
                ('certificate_file', models.FileField(verbose_name='certificate file', max_length=255, upload_to=delegate.models.certificate_file_path)),
                ('created_by', models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL, related_name='delegatecertificate_created_by')),
                ('delegate', models.ForeignKey(verbose_name='delegate', to=settings.AUTH_USER_MODEL, related_name='delegate_certificates')),
            ],
            options={
                'verbose_name': 'Delegate Certificate',
                'verbose_name_plural': 'Delegate Certificates',
            },
        ),
    ]
