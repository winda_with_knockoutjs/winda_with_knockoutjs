# -*- coding: utf-8 -*-
import uuid

from allauth.account import signals
from allauth.account.adapter import get_adapter
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from common.models import TimeStampedModel
from users.models import UserProfileAbstract


class Registration(UserProfileAbstract):
    REGISTRATION_TYPE_CHOICE = (
        (UserProfileAbstract.DELEGATE, _('delegate')),
        (UserProfileAbstract.ORGANIZATION, _('organization')),
        (UserProfileAbstract.TRAINING_PROVIDER_ADMIN, _('training provider')),
    )

    REGISTRATION_URLS = {
        UserProfileAbstract.DELEGATE: settings.DELEGATE_ACTIVATION_URL,
        UserProfileAbstract.ORGANIZATION: settings.ORGANIZATION_ACTIVATION_URL,
        UserProfileAbstract.TRAINING_PROVIDER_ADMIN: settings.TRAINING_PROVIDER_ACTIVATION_URL,
    }

    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    email = models.EmailField(_('email address'), max_length=255)
    registration_type = models.CharField(_('registration type'), max_length=30, choices=REGISTRATION_TYPE_CHOICE)
    expires_at = models.DateTimeField(_('expires at'), default=TimeStampedModel.expires_at_default)
    claimed_at = models.DateTimeField(_('claimed at'), null=True, blank=True)
    activation_code = models.UUIDField(default=uuid.uuid4, editable=False)

    class Meta:
        verbose_name = _('Registration')
        verbose_name_plural = _('Registration')

    def __str__(self):
        return self.email

    @property
    def key_expired(self):
        return self.expires_at <= timezone.now()

    @cached_property
    def verified(self):
        return get_user_model().objects.filter(email=self.email).exists()

    @property
    def is_activated(self):
        return self.key_expired or self.verified or self.claimed_at

    def confirm(self, request):
        if not self.is_activated:
            email_address = self.email
            self.claimed_at = timezone.now()
            self.save()
            get_adapter(request).confirm_email(request, self)
            signals.email_confirmed.send(sender=self.__class__,
                                         request=request,
                                         email_address=email_address)
            return email_address

    def send(self, request=None, signup=False):
        get_adapter(request).send_confirmation_mail(request, self, signup)
        signals.email_confirmation_sent.send(sender=self.__class__,
                                             confirmation=self)

    @property
    def get_registration_data(self):
        registration_data = {field: getattr(self, field) for field in ['first_name', 'last_name', 'email', 'address_1',
                                                                       'address_2', 'city', 'postal_code', 'country',
                                                                       'job_title', 'phone', 'timezone']}
        registration_data.update(
            username=self.email, last_login=timezone.now(),
            email_status=get_user_model().DELIVERY, role=self.registration_type,
            archived_from=timezone.now() + relativedelta(months=self.get_archived_from(self.registration_type)))
        if self.registration_type == self.DELEGATE:
            registration_data.update(dict(delegate_id=UserProfileAbstract.generate_delegate_id()))
        elif self.registration_type == self.ORGANIZATION:
            registration_data.update(dict(organization_name=self.organization_name))
        elif self.registration_type == self.TRAINING_PROVIDER_ADMIN:
            registration_data.update(dict(training_provider=self.training_provider,
                                          can_upload=True, can_buy_credit=True, can_use_credit=True))
        return registration_data


class LostPassword(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'), related_name='lost_passwords')
    expires_at = models.DateTimeField(_('expires at'), default=TimeStampedModel.expires_at_default)
    claimed_at = models.DateTimeField(_('claimed at'), null=True, blank=True)
    activation_code = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        verbose_name = _('Lost Password')
        verbose_name_plural = _('Lost Password')

    @property
    def key_expired(self):
        return self.expires_at <= timezone.now()

    @property
    def is_invalid(self):
        return self.key_expired or self.claimed_at
