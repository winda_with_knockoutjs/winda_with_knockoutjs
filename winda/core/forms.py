# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model
from django import forms
import reversion


class ModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        if self.request and self.request.user and self.request.user.is_authenticated():
            if self.instance.pk is None:
                self.instance.created_by = self.request.user
            else:
                self.instance.last_updated_by = self.request.user
        if reversion.is_active():
            if not reversion.get_user():
                system_user = get_user_model().objects.get(pk=settings.SYSTEM_USER_ID)
                reversion.set_user(system_user)
            if not self.instance.pk:
                reversion.set_comment('Added')
            elif self.changed_data:
                reversion.set_comment('Changed {}.'.format(', '.join(self.changed_data)))
        return super().save(commit)
