# -*- coding:utf-8 -*-
from itertools import chain

from django.forms.widgets import Select

from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text


class CustomSelect(Select):

    def render_option(self, selected_choices, option_value, option_label, course_type):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        return format_html('<option value="{}"{} data-type="{}">{}</option>', option_value, selected_html, course_type,force_text(option_label))

    def render_options(self, choices, selected_choices):
        # Normalize to strings.
        selected_choices = set(force_text(v) for v in selected_choices)
        output = []
        choices = chain(self.choices, choices)
        for item in choices:
            if len(item) == 2:
                option_value, option_label = item
                course_type = ''
            else:
                option_value, option_label, course_type = item
            output.append(self.render_option(selected_choices, option_value, option_label, course_type))
        return '\n'.join(output)


