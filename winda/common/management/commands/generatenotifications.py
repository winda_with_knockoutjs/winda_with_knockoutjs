# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Prefetch, Q
from django.utils import timezone

from common.models import EmailNotification
from delegate.models import TrainingRecord
from trainings.models import TrainingProviderCertificate, TrainingProvider

User = get_user_model()


class Command(BaseCommand):
    help = 'Generate user notifications'

    def add_arguments(self, parser):
        parser.add_argument('--date',
                            dest='input_date',
                            action='store_true',
                            default=timezone.now().date(),
                            help='Input date in ‘YYYY-MM-DD’ format for generate notifications as-on that date.')

    def handle(self, *args, **options):
        input_date = options['input_date'] or timezone.now().date()
        try:
            if isinstance(input_date, str):
                input_date = datetime.datetime.strptime(input_date, '%Y-%m-%d').date()
        except ValueError:
            raise CommandError('Invalid date')

        with transaction.atomic():
            self.training_records_expiring_soon(input_date)
            self.training_records_expired(input_date)
            self.delegate_account_expiring_soon(input_date)
            self.delegate_account_expired(input_date)
            self.training_provider_certificates_expiring_soon(input_date)
            self.organization_account_expiring_soon(input_date)
            self.organization_account_expired(input_date)

    @classmethod
    def training_records_expiring_soon(cls, input_date):
        valid_until = [input_date - relativedelta(months=3), input_date - timezone.timedelta(weeks=2),
                       input_date - timezone.timedelta(weeks=1)]
        queryset = TrainingRecord.objects.filter(valid_until__in=valid_until).select_related('course')
        users = User.objects\
            .filter(archived_at__isnull=True, delegate_training_records__valid_until__in=valid_until, role=User.DELEGATE)\
            .prefetch_related(Prefetch('delegate_training_records', queryset=queryset, to_attr='training_records'))\
            .distinct()

        EmailNotification.training_records_expiring_soon(users)

    @classmethod
    def training_records_expired(cls, input_date):
        valid_until = input_date - timezone.timedelta(days=1)

        queryset = TrainingRecord.objects.filter(valid_until=valid_until).select_related('course')
        users = User.objects \
            .filter(archived_at__isnull=True, delegate_training_records__valid_until=valid_until, role=User.DELEGATE) \
            .prefetch_related(Prefetch('delegate_training_records', queryset=queryset, to_attr='training_records')) \
            .distinct()

        EmailNotification.training_records_expired(users)

    @classmethod
    def delegate_account_expiring_soon(cls, input_date):
        query = Q()
        for archived_from in [input_date - relativedelta(months=3), input_date - timezone.timedelta(weeks=2),
                              input_date - timezone.timedelta(weeks=1)]:
            query |= Q(archived_from__range=(datetime.datetime.combine(archived_from, datetime.time.min),
                                             datetime.datetime.combine(archived_from, datetime.time.max)))

        users = User.objects.filter(Q(role=User.DELEGATE, archived_at__isnull=True, delegate_id__isnull=False), query)
        EmailNotification.delegate_account_expiring_soon(users)

    @classmethod
    def delegate_account_expired(cls, input_date):
        archived_from = input_date + timezone.timedelta(days=1)
        users = User.objects \
            .filter(role=User.DELEGATE, archived_at__isnull=True, delegate_id__isnull=False,
                    archived_from__range=(datetime.datetime.combine(archived_from, datetime.time.min),
                                          datetime.datetime.combine(archived_from, datetime.time.max)))
        EmailNotification.delegate_account_expired(users)

    @classmethod
    def training_provider_certificates_expiring_soon(cls, input_date):
        valid_until = [input_date - relativedelta(months=3), input_date - relativedelta(months=2),
                       input_date - relativedelta(months=1)]

        queryset = TrainingProviderCertificate.objects.filter(is_deleted=False, valid_until__in=valid_until)
        training_providers = TrainingProvider.objects\
            .filter(is_active=True)\
            .prefetch_related(Prefetch('training_provider_trainingprovidercertificate',
                                       queryset=queryset, to_attr='certificates')) \
            .select_related('country').distinct()

        users = User.objects.filter(role__in=[User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN])
        EmailNotification.training_provider_certificates_expiring_soon(users, training_providers)

    @classmethod
    def organization_account_expiring_soon(cls, input_date):
        archived_from = input_date - timezone.timedelta(weeks=1)
        users = User.objects \
            .filter(role=User.ORGANIZATION, archived_at__isnull=True,
                    archived_from__range=(datetime.datetime.combine(archived_from, datetime.time.min),
                                          datetime.datetime.combine(archived_from, datetime.time.max)))

        EmailNotification.organization_account_expiring_soon(users)

    @classmethod
    def organization_account_expired(cls, input_date):
        archived_from = input_date + timezone.timedelta(days=1)
        users = User.objects \
            .filter(role=User.ORGANIZATION, archived_at__isnull=True,
                    archived_from__range=(datetime.datetime.combine(archived_from, datetime.time.min),
                                          datetime.datetime.combine(archived_from, datetime.time.max)))
        EmailNotification.organization_account_expired(users)
