$(document).ready(function() {
    initializeDataTable('table#organization-search-result', {
        "order": [[ 1, 'desc' ], [ 8, 'asc' ]],
        "columns": [
            {"data": "", 'searchable': false, 'orderable': false, "className": "control"},
            {"data": "delegate_id"},
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "course_title"},
            {"data": "course_code"},
            {"data": "training_provider"},
            {"data": "country"},
            {"data": "completed_on"},
            {"data": "valid_from"},
            {"data": "valid_until"},
            {"data": "status"}
        ],
        'columnDefs': [{
            'targets': 11,
            'searchable': false,
            'orderable': false,
            'render': function (data, type, full, meta) {
                if (Object.prototype.toString.call(data) === '[object Array]') {
                    return '<span class="' + data[1] + '"></span>' + data[0];
                } else {
                    return data;
                }
            }
        }]
    });

    var $gridTable = $('table#organization-grid-search-result'),
            len = $gridTable.attr('data-fields'),
            range = function (start, stop) {
                if (stop == null) {
                    stop = start || 0;
                    start = 0;
                }

                var length = Math.max(Math.ceil(stop - start), 0);
                var range = Array(length);

                for (var idx = 0; idx < length; idx++, start += 1) {
                    range[idx] = start;
                }

                return range;
            };

    initializeDataTable($gridTable, {
        "order": [[1, 'desc']],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            "className": "control"
        }, {
            'targets': range(4, len),
            'searchable': false,
            'orderable': false,
            'render': function (data, type, full, meta) {
                if (Object.prototype.toString.call(data) === '[object Array]') {
                    return '<span class="' + data[1] + '"></span>' + data[0];
                } else {
                    return data;
                }
            }
        }]
    });
});
