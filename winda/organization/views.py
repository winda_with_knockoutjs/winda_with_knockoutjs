# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext as _
from django.views.generic import DetailView, FormView
from skd_tools.mixins import ActiveTabMixin

from core.views import UpdateView
from common.mixins import OrganizationAccessMixin, BaseRoleAccessMixin
from delegate.models import TrainingRecord
from organization.forms import BulkSearchForm, SearchFilterForm, OrganizationUpdateForm, GridSearchFilterForm
from organization.mixins import BulkSearchResultMixin


User = get_user_model()


class OrganizationHomeView(OrganizationAccessMixin, DetailView):

    template_name = 'organization/home.html'
    only_owner = True

    def get_queryset(self):
        return get_user_model().objects.organizations()


class BulkSearchView(BaseRoleAccessMixin, ActiveTabMixin, FormView):
    roles = [User.ORGANIZATION, User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN]
    template_name = 'delegate/search.html'
    active_tab = 'search'
    form_class = BulkSearchForm
    success_url = reverse_lazy('organization:search_results')
    only_owner = False

    def form_valid(self, form):
        self.request.session['search_delegate_id'] = form.cleaned_data['delegate_id']
        self.request.session['search_by_delegate_id'] = True
        return super().form_valid(form)


class BulkSearchResultView(BulkSearchResultMixin):
    model = TrainingRecord
    form_class = SearchFilterForm

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'delegate_id': '<a href="{}">{}</a>'.format(
                reverse('delegate:generate-certificate', args=(obj.delegate_id,)),
                obj.delegate_id) if obj.id else obj.delegate_id,
            'first_name': obj.first_name,
            'last_name': obj.last_name,
            'course_title': obj.course_title,
            'course_code': obj.course_code,
            'training_provider': obj.training_provider,
            'country': obj.country,
            'completed_on': obj.completed_on.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.completed_on else '',
            'valid_from': obj.valid_from.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_from else '',
            'valid_until': obj.valid_until.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_until else '',
            'status': self.model.get_status(obj)
        } for obj in object_list]


class GridSearchResultView(BulkSearchResultMixin):
    model = User
    form_class = GridSearchFilterForm

    def get_queryset(self):
        return super().get_queryset().delegates()

    def form_valid(self, form):
        self.courses = form.courses
        return super().form_valid(form)

    def get_ordering_field(self):
        return list(self.form_class.FIELDS.keys())[self.filter_data.get('sort_column') or 1]

    def get_filtered_data(self, object_list):
        result = []
        for obj in object_list:
            data = [
                '',
                '<a href="{}">{}</a>'.format(
                    reverse('delegate:generate-certificate', args=(obj.delegate_id,)),
                    obj.delegate_id) if obj.id else obj.delegate_id,
                obj.first_name,
                obj.last_name,
            ]
            user_courses = {}
            default = TrainingRecord.get_course_status() if obj.id else ''
            if obj.courses:
                for course in obj.courses.split('|'):
                    course_id, current, expired, invalidated, valid_until = course.split(',')
                    user_courses[course_id.strip()] = TrainingRecord.get_course_status(int(current), int(expired),
                                                                                       int(invalidated), valid_until)
            for course_id, name in self.courses:
                data.append(user_courses.get(str(course_id), default))
            result.append(data.copy())
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_grid_displayed'] = True
        return context


class OrganizationProfileView(OrganizationAccessMixin, ActiveTabMixin, UpdateView):

    active_tab = 'profile'
    template_name = 'organization/profile.html'
    only_owner = True
    form_class = OrganizationUpdateForm

    def get_queryset(self):
        return get_user_model().objects.organizations()

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('Your profile was successfully saved.'))
        return reverse_lazy('organization:home', kwargs={'pk': self.object.pk})
