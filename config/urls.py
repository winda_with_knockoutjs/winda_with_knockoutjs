# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.contrib.flatpages import views
from delegate.views import DelegateCertificateView


urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),
    # User management
    url(r'^', include('common.urls')),
    url(r'^', include('orders.urls')),
    url(r'^', include('registration.urls')),
    url(r'^admin/ses/', include('django_ses.urls')),
    url(r'^delegate/', include('delegate.urls', namespace='delegate')),
    url(r'^organization/', include('organization.urls', namespace='organization')),
    url(r'^training-provider/', include('trainings.urls', namespace='training_provider')),
    url(r'^database-admin/', include('database_admin.urls', namespace='database_admin')),
    url(r'^database-super-admin/', include('database_super_admin.urls', namespace='database_super_admin')),
    url(r'^certificate/(?P<certificate_code>[^/]+)$', DelegateCertificateView.as_view(), name='delegate-certificate'),
]

# flatpages
urlpatterns += [
    url(r'^about/', include([
        url(r'^$', views.flatpage, {'url': '/about/'}, name='about'),
        url(r'^terms-and-conditions/$', views.flatpage, {'url': '/terms-and-conditions/'}, name='terms'),
        url(r'^privacy-policy/$', views.flatpage, {'url': '/privacy-policy/'}, name='privacy'),
        url(r'^service-level-agreement/$', views.flatpage, {'url': '/service-level-agreement/'}, name='agreement'),
    ])),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request),
        url(r'^403/$', default_views.permission_denied),
        url(r'^404/$', default_views.page_not_found),
        url(r'^500/$', default_views.server_error),
    ]
