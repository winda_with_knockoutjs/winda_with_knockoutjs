# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views.generic import ListView
from skd_tools.mixins import ActiveTabMixin
from common.mixins import BaseRoleAccessMixin
from common.mixins import FilterMixin
from delegate.models import TrainingRecord
from delegate.models import Search


User = get_user_model()


class BulkSearchResultMixin(BaseRoleAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    roles = [User.ORGANIZATION, User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN]
    template_name = 'organization/search_result.html'
    active_tab = 'search'
    only_owner = False

    def dispatch(self, request, *args, **kwargs):
        self.delegate_id = self.request.session.get('search_delegate_id', [])
        if not self.delegate_id:
            return HttpResponseRedirect(reverse_lazy('organization:search'))
        return super().dispatch(request, *args, **kwargs)

    def get_ordering_field(self):
        return list(self.form_class.FIELDS.keys())[self.filter_data.get('sort_column') or 1]

    def get_filtered_queryset(self, queryset):
        ordering_field = self.get_ordering_field()
        today = timezone.now()
        trainings = self.model.objects.search(self.delegate_id, ordering_field, **self.filter_data)
        if self.request.session.get('search_by_delegate_id'):
            users = list(TrainingRecord.objects.current_training()
                         .filter(delegate__delegate_id__in=self.delegate_id)
                         .values_list('delegate__id', flat=True))
            searches = [Search(created_by=self.request.user, delegate_id=user) for user in users]
            Search.objects.bulk_create(searches)
            get_user_model().objects.filter(pk__in=users).update(archived_from=today + relativedelta(months=48))
            del self.request.session['search_by_delegate_id']
        return trainings

    def form_valid(self, form):
        self.filter_data = form.cleaned_data
        total_records = self.model.objects.search_total_count(self.delegate_id, **self.filter_data)
        self.object_list = self.get_filtered_queryset(None)
        return self.render_json_response({
            'draw': self.draw,
            'recordsTotal': total_records,
            'recordsFiltered': total_records,
            'data': self.get_filtered_data(self.object_list)
        })
