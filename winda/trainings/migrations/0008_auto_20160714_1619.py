# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0007_auto_20160713_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadbatchrecord',
            name='upload_batch',
            field=models.ForeignKey(verbose_name='upload batch', related_name='upload_batch_records', to='trainings.UploadBatch'),
        ),
    ]
