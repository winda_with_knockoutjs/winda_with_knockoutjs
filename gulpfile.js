'use strict';

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    sprite = require('gulp-sprite'),
    concat = require('gulp-concat');

    // paths:
    var path = {

        scripts: {
            _src: 'winda/static/js-dev/**/*.js',
            _dest: 'winda/static/js/'
        },

        styles: {
            _src: 'winda/static/less/main.less',
            _dest: 'winda/static/css/'
        }
    };

// ======= function

//=== js ===//

gulp.task('js:dev', function () {
  return gulp.src(path.scripts._src)
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest(path.scripts._dest));
});


//=== style ===//

gulp.task('style:dev', function () {
  return gulp.src(path.styles._src)
    .pipe(less().on('error', function(err){
        gutil.log(err);
        this.emit('end');
    }))
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(path.styles._dest));
});


//=== images ===//

gulp.task('sprites', function () {
    gulp.src('winda/static/images/sprites/*.*')
      .pipe(sprite('sprites.png', {
        imagePath: '../images',
        cssPath: 'winda/static/less/'
      }))
      .pipe(gulp.dest('winda/static/images'));
});

//=== watch ===//

gulp.task('watch', ['style:dev', 'js:dev'], function() {
    gulp.watch(path.scripts._src, ['js:dev']);
    gulp.watch('winda/static/less/**/*.less', ['style:dev']);
    gulp.watch('winda/static/images/sprites/*.*', ['sprites']);
});

gulp.task('default', ['watch', 'sprites']);
