# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trainings', '0002_auto_20160629_1003'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingProviderCertificate',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('valid_from', models.DateField(verbose_name='valid from')),
                ('valid_until', models.DateField(verbose_name='valid until')),
                ('is_deleted', models.BooleanField(verbose_name='is deleted')),
                ('certificate_file', models.FileField(upload_to='', verbose_name='certificate file')),
                ('certificate_number', models.CharField(verbose_name='certificate number', max_length=40)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1, related_name='trainingprovidercertificate_created_by')),
                ('last_updated_by', models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL, related_name='trainingprovidercertificate_last_updated_by')),
                ('training_provider', models.ForeignKey(to='trainings.TrainingProvider', related_name='training_provider_trainingprovidercertificate')),
            ],
            options={
                'verbose_name_plural': 'Training Provider Certificates',
                'verbose_name': 'Training Provider Certificate',
            },
        ),
    ]
