# -*- coding: utf-8 -*-
from allauth.utils import build_absolute_uri
from django.conf import settings
from django.core.urlresolvers import reverse
from quickpay_api_client import QPClient


class QuickPayClient(object):
    url = {
        'payments': '/payments',
        'link': '/payments/{}/link'
    }

    def __init__(self):
        self.client = QPClient(':{}'.format(settings.QUICKPAY_ACCOUNT_KEY))

    def payments(self, user, order):
        params = {
            'currency': order.currency,
            'order_id': order.order_id,
            'basket': [{
                'qty': order.quantity,
                'item_no': order.item_code,
                'item_name': order.item_name,
                'item_price': order.item_unit_price_including_vat,
                'vat_rate': float(order.item_vat_rate)
            }],
            'invoice_address': {
                'name': user.get_full_name(),
                'street': '{} {}'.format(user.address_1, user.address_2).strip(),
                'city': user.city,
                'zip_code': user.postal_code,
                'phone_number': user.phone,
                'email': user.email,
            }}

        if user.country:
            params['invoice_address']['country_code'] = user.country.code3

        return self.client.post(self.url['payments'], **params)

    def link(self, request, order):
        params = {
            'agreement_id': settings.QUICKPAY_AGREEMENT_ID,
            'continue_url': settings.QUICKPAY_CONTINUE_URL,
            'cancel_url': settings.QUICKPAY_CANCEL_URL,
            'amount': order.total_price_including_vat, 'deadline': settings.QUICKPAY_DEADLINE,
            'callback_url': build_absolute_uri(request, reverse('quickpay_callback'))
        }
        return self.client.put('/payments/{}/link'.format(order.token_link), **params)
