# -*- coding: utf-8 -*-
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from registration.models import LostPassword
from users.factories import UserFactory, TEST_USER_PASSWORD, TrainingProviderAdminFactory


class ResetPasswordTestCase(TestCase):

    def setUp(self):
        self.url = reverse('account_reset_password')
        self.user = UserFactory()

    def test_reset_password(self):
        """
        Anonymous user successfully send reset password email
        """
        data = {'email': self.user.email}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('account_reset_password_done'))
        self.assertEqual(LostPassword.objects.last().user, self.user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA Forgot Password Reset')

    def test_reset_password_with_wrong_email(self):
        """
        Anonymous user submit blank form and get error
        """
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 1)

    def test_user_inactive(self):
        """
        Anonymous inactive user reset password and get error
        """
        self.user = UserFactory(is_active=False)
        data = {'email': self.user.email}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('account_reset_password_done'))

    def test_change_password(self):
        """
        Anonymous user successfully change password
        """
        data = {'email': self.user.email}
        self.client.post(self.url, data)
        lost_password = LostPassword.objects.last()
        self.assertIsNone(lost_password.claimed_at)

        url = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        data = {'password1': TEST_USER_PASSWORD, 'password2': TEST_USER_PASSWORD}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))
        lost_password.refresh_from_db()
        self.assertIsNotNone(lost_password.claimed_at)

    def test_change_password_fail(self):
        """
        Anonymous user submit blank change password form and get error
        """
        data = {'email': self.user.email}
        self.client.post(self.url, data)
        lost_password = LostPassword.objects.last()
        self.assertIsNone(lost_password.claimed_at)
        url = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 2)

    def test_user_expires_at(self):
        """
        Lost password is expire
        """
        data = {'email': self.user.email}
        self.client.post(self.url, data)
        lost_password = LostPassword.objects.last()
        lost_password.expires_at = timezone.now()
        lost_password.save()

        url = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Password reset code has expired. Please reset your password again.', 1)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 404)

    def test_user_claimed_at(self):
        """
        Lost password is claim
        """
        data = {'email': self.user.email}
        self.client.post(self.url, data)
        lost_password = LostPassword.objects.last()
        lost_password.claimed_at = timezone.now()
        lost_password.save()

        url = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Password reset code has expired. Please reset your password again.', 1)

        response = self.client.post(url)
        self.assertEqual(response.status_code, 404)

    def test_inactive_training_provider_could_not_login_after_reset_password(self):
        """
        Inactive training provider do not login after reset password
        """
        user = TrainingProviderAdminFactory()
        user.training_provider.is_active = False
        user.training_provider.save()
        lost_password = LostPassword.objects.create(user=user)

        url = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        data = {'password1': TEST_USER_PASSWORD, 'password2': TEST_USER_PASSWORD}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')
