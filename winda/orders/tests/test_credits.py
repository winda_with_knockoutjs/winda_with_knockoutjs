# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from orders.factories import TransactionFactory, ItemFactory, PricingFactory
from orders.models import Order
from trainings.factories import TrainingProviderFactory
from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, TrainingProviderUserFactory, \
    DatabaseAdminFactory, OrganizationFactory, DelegateFactory

User = get_user_model()


class CreditTestCase(TestCase):

    def setUp(self):
        self.training_provider = TrainingProviderFactory()
        self.user = TrainingProviderAdminFactory(training_provider=self.training_provider)
        self.training_provider_user = TrainingProviderUserFactory(training_provider=self.training_provider)
        self.home_url = reverse('credit', args=(self.training_provider.pk,))
        self.purchase_credit_url = reverse('purchase_credit', args=(self.training_provider.pk,))
        self.purchase_credit_confirm_url = reverse('purchase_credit_confirm', args=(self.training_provider.pk,))
        self.item = ItemFactory()
        self.price = PricingFactory(item=self.item, valid_from=timezone.now().date(), credits_per_unit=5, vat_rate=0.25)
        PricingFactory(item=self.item, valid_from=timezone.now().date() - timezone.timedelta(days=1))
        PricingFactory(item=self.item, valid_from=timezone.now().date() + timezone.timedelta(days=1))

    def test_access_credit(self):
        """
        Only Training Provider Admin, Training Provider and Database Admin get credit page
        """
        response = self.client.get(self.home_url)
        self.assertEqual(response.status_code, 404)

        for user in [self.user, self.training_provider_user]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.home_url)
            self.assertContains(response, 'Credits')

        for user in [OrganizationFactory(), DelegateFactory(), DatabaseAdminFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.home_url)
            self.assertEqual(response.status_code, 404)

    def test_access_credit_purchase(self):
        """
        Only Training Provider Admin, Training Provider get purchase credit page
        """
        response = self.client.get(self.purchase_credit_url)
        self.assertEqual(response.status_code, 404)

        for user in [self.user, self.training_provider_user]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.purchase_credit_url)
            self.assertContains(response, 'Current Credit Balance 0')

        for user in [OrganizationFactory(), DelegateFactory(), DatabaseAdminFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.purchase_credit_url)
            self.assertEqual(response.status_code, 404)

        self.user.can_buy_credit = False
        self.user.save()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.purchase_credit_url)
        self.assertContains(response, 'You are not permitted to purchase credits. '
                                      'Ask your company administrator for assistance.', 1)
        self.assertNotContains(response, '<form')

    def test_purchase_credit_success(self):
        """
        Training Provider successfully purchase credit
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        TransactionFactory.create_batch(5, training_provider=self.training_provider, transaction_value=5)
        order_count = Order.objects.count()
        response = self.client.get(self.purchase_credit_url)
        self.assertContains(response, 'Current Credit Balance 25', 1)

        data = dict(item=self.item.pk, quantity=2)
        response = self.client.post(self.purchase_credit_url, data=data)
        self.assertRedirects(response, self.purchase_credit_confirm_url)
        self.assertEqual(Order.objects.count(), order_count)

    def test_purchase_credit_fail(self):
        """
        Training Provider submit blank form and get error
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.purchase_credit_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 2)
        self.assertEqual(Order.objects.count(), 0)

    def test_purchase_credit_after_back(self):
        """
        Training Provider back to purchase credit page from confirm page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        session = self.client.session
        session['purchase-credit-item'] = self.item.pk
        session['purchase-credit-quantity'] = 2
        session.save()
        response = self.client.get(self.purchase_credit_url)
        self.assertContains(response, self.item.item_description, 2)

    def test_confirm_credit(self):
        """
        Training Provider confirm purchase credit
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        session = self.client.session
        session['purchase-credit-item'] = self.item.pk
        session['purchase-credit-quantity'] = 2
        session.save()
        response = self.client.post(self.purchase_credit_confirm_url)
        self.assertRedirects(response, reverse('orders', args=(self.training_provider.pk,)))
        self.assertEqual(Order.objects.count(), 1)
        order = Order.objects.first()

        self.assertEqual(order.created_by, self.user)
        self.assertEqual(order.quantity, 2)
        self.assertEqual(order.item_code, self.item.item_code)
        self.assertEqual(order.item_name, self.item.item_name)
        self.assertEqual(order.item_unit_price_including_vat, self.price.unit_price_including_vat)
        self.assertEqual(order.item_vat_rate, self.price.vat_rate)
        self.assertEqual(order.training_provider, self.user.training_provider)
        self.assertEqual(order.total_credits, 2 * 5)
        self.assertEqual(order.total_price_including_vat, 2 * self.price.unit_price_including_vat)
