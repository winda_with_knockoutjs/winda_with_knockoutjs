# -*- coding: utf-8 -*-
from allauth.account.adapter import get_adapter
from allauth.account import signals

from django import forms
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _

import reversion
from reversion.views import RevisionMixin


class SignupFormMixin(forms.ModelForm):
    terms = forms.BooleanField()
    policy = forms.BooleanField()

    error_messages = {
        'duplicate_email': _('This email address is already in use.'),
        'email_in_blacklist': _('Personal email addresses are not allowed.')
    }

    def clean_email(self):
        value = self.cleaned_data["email"]
        value = get_adapter().clean_email(value)
        if value and get_user_model().objects.filter(email__iexact=value).exists():
            raise forms.ValidationError(self.error_messages['duplicate_email'])
        return value

    def clean_timezone(self):
        timezone = self.cleaned_data.get('timezone')
        self.errors.pop('timezone', None)
        return timezone or 'Europe/London'

    def save(self, request):
        if reversion.is_active():
            reversion.set_comment('Added')
        adapter = get_adapter(request)
        registration = adapter.new_user(request)
        adapter.save_user(request, registration, self)
        registration.send(request=request)
        return registration


class SignupViewMixin(RevisionMixin):

    def form_valid(self, form):
        self.user = form.save(self.request)
        adapter = get_adapter(self.request)
        signals.user_signed_up.send(sender=self.user.__class__,
                                    request=self.request,
                                    user=self.user)
        adapter.respond_email_verification_sent(self.request, self.user)
        return HttpResponseRedirect(reverse('account_email_verification_sent', args=(self.role,)))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_{}'.format(self.role.replace('-', '_'))] = True
        return context
