# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0005_auto_20160715_1517'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingrecord',
            name='old_training_provider',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='training provider', related_name='training_provider_training_records', null=True),
        ),
    ]
