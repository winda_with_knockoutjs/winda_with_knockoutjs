# -*- coding: utf-8 -*-
import reversion
from reversion.models import Version
from dateutil.relativedelta import relativedelta

from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db.models import F, Func, Value
from django.utils import timezone
from django.utils.translation import ugettext as _

from common.models import EmailNotification
from common.reversion import bulk_create_revision
from registration.models import Registration


User = get_user_model()


class Command(BaseCommand):
    help = 'Clear personal information from old records.'

    @bulk_create_revision()
    def handle(self, *args, **options):
        with transaction.atomic():
            Registration.objects.filter(archived_from__lte=timezone.now(), archived_at__isnull=True)\
                                .update(archived_from=timezone.now() + relativedelta(months=12), archived_at=timezone.now(),
                                        first_name=_('ARCHIVED'), last_name=_('ARCHIVED'), email=_('ARCHIVED'))
            self.stdout.write('Archive Registration records')
            start_time = timezone.now()
            func = Func(Value(' '), Value(_('ARCHIVED')), F('id'), function='CONCAT_WS')
            users = User.objects.filter(archived_from__lte=timezone.now(), archived_at__isnull=True).exclude(pk=settings.SYSTEM_USER_ID)

            users.filter(role=User.DELEGATE)\
                 .update(first_name=_('ARCHIVED'), last_name=_('ARCHIVED'), email=func, username=func,
                         archived_from=timezone.now() + relativedelta(months=48), archived_by_id=settings.SYSTEM_USER_ID,
                         archived_at=timezone.now(), is_active=False, modified=start_time)

            users.filter(role=User.ORGANIZATION)\
                 .update(first_name=_('ARCHIVED'), last_name=_('ARCHIVED'), email=func, username=func,
                         archived_from=timezone.now() + relativedelta(months=6), archived_by_id=settings.SYSTEM_USER_ID,
                         archived_at=timezone.now(), is_active=False, modified=start_time)

            users.filter(role__in=[User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN, User.DATABASE_SUPER_ADMIN])\
                 .update(first_name=_('ARCHIVED'), last_name=_('ARCHIVED'), email=func, is_active=False,
                         username=func, archived_from=timezone.now() + relativedelta(months=24), modified=start_time,
                         archived_at=timezone.now(), archived_by_id=settings.SYSTEM_USER_ID)

            users = User.objects.filter(modified__exact=start_time, archived_at__isnull=False)
            system_user = User.objects.get(pk=settings.SYSTEM_USER_ID)
            for instance in users:
                reversion.set_comment('Archived')
                reversion.set_user(system_user)
                reversion.add_to_revision(instance)
            self.stdout.write('Archive Users')

            EmailNotification.objects.filter(archived_from__lte=timezone.now(), archived_at__isnull=True)\
                                     .update(archived_from=timezone.now() + relativedelta(months=12),
                                             subject=_('ARCHIVED'), body=_('ARCHIVED'), archived_at=timezone.now())
            self.stdout.write('Archive Email Notification records')

            users = User.objects\
                .filter(archived_at__lte=timezone.now() - relativedelta(months=48))\
                .exclude(pk=settings.SYSTEM_USER_ID)\
                .values_list('pk', flat=True)
            content_type = ContentType.objects.get_for_model(User)
            Version.objects.filter(object_id__in=users, content_type=content_type).delete()
            self.stdout.write('Delete audit trail information for archive users')
