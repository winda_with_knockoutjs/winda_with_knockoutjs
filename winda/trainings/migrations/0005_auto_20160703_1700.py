# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import trainings.models


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0004_auto_20160702_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingprovidercertificate',
            name='certificate_file',
            field=models.FileField(upload_to=trainings.models.certificate_file_path, max_length=255, verbose_name='certificate file'),
        ),
    ]
