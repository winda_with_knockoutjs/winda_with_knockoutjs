# -*- coding: utf-8 -*-
from django.conf import settings
from django.core import mail
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from common.models import EmailNotification
from registration.factories import CountryFactory
from delegate.factories import TrainingRecordFactory
from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, OrganizationFactory, \
    DelegateFactory, DatabaseAdminFactory, TechnicalSupportFactory, TrainingProviderUserFactory


User = get_user_model()


class MailoutTestCase(TestCase):

    def setUp(self):
        self.training_provider_admin = TrainingProviderAdminFactory.create_batch(2)
        self.training_provider_user = TrainingProviderUserFactory.create_batch(2)
        self.technical_support = TechnicalSupportFactory.create_batch(2)
        self.database_admin = DatabaseAdminFactory()
        self.delegate = DelegateFactory.create_batch(2)
        self.organization = OrganizationFactory.create_batch(2)
        self.url = reverse('database_admin:mailout')

    def test_mailout_filter_by_role_user(self):
        """
        Mailout users filter by role
        """
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Mailout')

        for user in [self.training_provider_admin[0], self.training_provider_user[0], self.technical_support[0],
                     self.delegate[0], self.organization[0]]:
            EmailNotification.objects.all().delete()
            data = {
                'subject': 'subject',
                'body': 'some text',
                'role': user.role
            }
            response = self.client.post(self.url, data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(EmailNotification.objects.count(), 2)
            notification = EmailNotification.objects.last()
            self.assertEqual(notification.subject, 'WINDA ' + data['subject'])

    def test_filter_by_country(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_country': True,
            'country': self.organization[0].country.pk
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(EmailNotification.objects.count(), 1)
        notification = EmailNotification.objects.last()
        self.assertEqual(notification.subject, 'WINDA ' + data['subject'])
        self.assertEqual(notification.recipient, self.organization[0])

    def test_filter_by_training_record(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        training_record = TrainingRecordFactory(delegate=self.delegate[0])
        TrainingRecordFactory.create_batch(10)

        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_course': True,
            'training_provider': training_record.training_provider.pk,
            'course': training_record.course.pk,
            'completed_on_start': training_record.completed_on.strftime(settings.FORMS_DATE_FORMAT),
            'completed_on_end': (training_record.completed_on + timezone.timedelta(days=2)).strftime(settings.FORMS_DATE_FORMAT),
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(EmailNotification.objects.count(), 1)
        notification = EmailNotification.objects.last()
        self.assertEqual(notification.subject, 'WINDA ' + data['subject'])
        self.assertEqual(notification.recipient, self.delegate[0])

    def test_filter_by_country_wrong(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_country': True,
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(EmailNotification.objects.count(), 0)
        self.assertContains(response, 'This field is required.')

    def test_filter_by_training_record_wrong(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_course': True
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(EmailNotification.objects.count(), 0)
        self.assertContains(response, 'Training Provider or Course is required.')

    def test_form_not_valid(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(EmailNotification.objects.count(), 0)
        self.assertContains(response, 'This field is required.', 2)
        self.assertContains(response, 'At least one Recipient checkbox must be ticked.', 1)

    def test_send_mailout_notification(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_country': True,
            'country': self.organization[0].country.pk
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)

        notification = EmailNotification.objects.last()
        notification.send()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA subject')

    def test_blank_queryset(self):
        self.client.login(username=self.database_admin, password=TEST_USER_PASSWORD)
        data = {
            'subject': 'subject',
            'body': 'some text',
            'filter_by_country': True,
            'country': CountryFactory().pk
        }
        response = self.client.post(self.url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Recipients list is empty, please, add emails.')
