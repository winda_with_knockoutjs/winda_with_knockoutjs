# -*- coding: utf-8 -*-
import factory
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
from reversion.models import Revision

from registration.factories import CountryFactory
from users.factories import DatabaseAdminFactory, TEST_USER_PASSWORD, DelegateFactory, TrainingProviderAdminFactory, \
    TrainingProviderUserFactory, OrganizationFactory


User = get_user_model()


class UserUpdateTestCase(TestCase):

    def setUp(self):
        self.user = DatabaseAdminFactory(__sequence=10)
        self.delegate = DelegateFactory(__sequence=11)
        self.organization = OrganizationFactory(__sequence=12)
        self.training_provider_user = TrainingProviderUserFactory(__sequence=13)
        self.training_provider_admin = TrainingProviderAdminFactory(__sequence=14)

    def test_update_delegate_user(self):
        """
        Database Admin update delegate user
        """
        url = reverse('database_admin:user_detail', args=(self.delegate.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage Delegate User')

        data = factory.build(dict, FACTORY_CLASS=DelegateFactory, email_status=User.BOUNCE, is_active=True)
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was successfully updated.')
        self.delegate.refresh_from_db()
        for field in ('first_name', 'last_name', 'email', 'email_status'):
            self.assertEqual(getattr(self.delegate, field), data[field])
        self.assertTrue(self.delegate.is_active)

    def test_update_organization_user(self):
        """
        Database Admin update organization user
        """
        url = reverse('database_admin:user_detail', args=(self.organization.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage Organization User')

        data = factory.build(dict, FACTORY_CLASS=OrganizationFactory, email_status=User.BOUNCE, is_active=True,
                             country=CountryFactory().pk)
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was successfully updated.')
        self.organization.refresh_from_db()
        for field in ('first_name', 'last_name', 'email', 'email_status', 'job_title', 'phone',
                      'organization_name', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(self.organization, field), data.get(field, ''))
        self.assertTrue(self.organization.is_active)
        self.assertEqual(self.organization.country.pk, data['country'])

    def test_update_training_provider_user_user(self):
        """
        Database Admin update training provider user user
        """
        url = reverse('database_admin:user_detail', args=(self.training_provider_user.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage Training Provider User')

        data = factory.build(dict, FACTORY_CLASS=TrainingProviderAdminFactory, email_status=User.BOUNCE, is_active=True)
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was successfully updated.')
        self.training_provider_user.refresh_from_db()
        for field in ('role', 'first_name', 'last_name', 'email', 'email_status'):
            self.assertEqual(getattr(self.training_provider_user, field), data[field])
        self.assertTrue(self.training_provider_user.is_active)
        self.assertTrue(self.training_provider_user.can_upload)
        self.assertTrue(self.training_provider_user.can_use_credit)
        self.assertTrue(self.training_provider_user.can_buy_credit)

    def test_update_training_provider_admin_user(self):
        """
        Database Admin update training provider admin user
        """
        url = reverse('database_admin:user_detail', args=(self.training_provider_admin.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage Training Provider Admin User')

        data = factory.build(dict, FACTORY_CLASS=TrainingProviderUserFactory, email_status=User.BOUNCE, is_active=True,
                             can_upload=False, can_use_credit=False, can_buy_credit=False)
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was successfully updated.')
        self.training_provider_admin.refresh_from_db()
        for field in ('role', 'first_name', 'last_name', 'email', 'email_status'):
            self.assertEqual(getattr(self.training_provider_admin, field), data[field])
        self.assertTrue(self.training_provider_admin.is_active)
        self.assertFalse(self.training_provider_admin.can_upload)
        self.assertFalse(self.training_provider_admin.can_use_credit)
        self.assertFalse(self.training_provider_admin.can_buy_credit)

    def test_archive_delegate_user(self):
        """
        Database Admin archive delegate user
        """
        url = reverse('database_admin:user_detail', args=(self.delegate.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {'archive': ''}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was archived successfully.')
        self.delegate.refresh_from_db()
        self.assertEqual(self.delegate.first_name, 'ARCHIVED')
        self.assertEqual(self.delegate.last_name, 'ARCHIVED')
        self.assertEqual(self.delegate.username, 'ARCHIVED {}'.format(self.delegate.pk))
        self.assertEqual(self.delegate.email, 'ARCHIVED {}'.format(self.delegate.pk))
        self.assertIsNotNone(self.delegate.archived_at)
        self.assertEqual(self.delegate.archived_by, self.user)
        self.assertFalse(self.delegate.is_active)
        self.assertNotContains(response, self.delegate.email)

        reversion = Revision.objects.filter(version__object_id=self.delegate.pk).last()
        self.assertEqual(reversion.comment, 'Archived')
        self.assertEqual(reversion.user, self.user)

    def test_archive_organization_user(self):
        """
        Database Admin archive organization user
        """
        url = reverse('database_admin:user_detail', args=(self.organization.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {'archive': ''}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was archived successfully.')
        self.organization.refresh_from_db()
        self.assertEqual(self.organization.first_name, 'ARCHIVED')
        self.assertEqual(self.organization.last_name, 'ARCHIVED')
        self.assertEqual(self.organization.username, 'ARCHIVED {}'.format(self.organization.pk))
        self.assertEqual(self.organization.email, 'ARCHIVED {}'.format(self.organization.pk))
        self.assertIsNotNone(self.organization.archived_at)
        self.assertEqual(self.organization.archived_by, self.user)
        self.assertFalse(self.organization.is_active)
        self.assertNotContains(response, self.organization.email)

        reversion = Revision.objects.filter(version__object_id=self.organization.pk).last()
        self.assertEqual(reversion.comment, 'Archived')
        self.assertEqual(reversion.user, self.user)

    def test_archive_training_provider_user_user(self):
        """
        Database Admin archive training_provider_user user
        """
        url = reverse('database_admin:user_detail', args=(self.training_provider_user.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {'archive': ''}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was archived successfully.')
        self.training_provider_user.refresh_from_db()
        self.assertEqual(self.training_provider_user.first_name, 'ARCHIVED')
        self.assertEqual(self.training_provider_user.last_name, 'ARCHIVED')
        self.assertEqual(self.training_provider_user.username, 'ARCHIVED {}'.format(self.training_provider_user.pk))
        self.assertEqual(self.training_provider_user.email, 'ARCHIVED {}'.format(self.training_provider_user.pk))
        self.assertIsNotNone(self.training_provider_user.archived_at)
        self.assertEqual(self.training_provider_user.archived_by, self.user)
        self.assertFalse(self.training_provider_user.is_active)
        self.assertNotContains(response, self.training_provider_user.email)

        reversion = Revision.objects.filter(version__object_id=self.training_provider_user.pk).last()
        self.assertEqual(reversion.comment, 'Archived')
        self.assertEqual(reversion.user, self.user)

    def test_archive_training_provider_admin_user(self):
        """
        Database Admin archive training_provider_admin user
        """
        url = reverse('database_admin:user_detail', args=(self.training_provider_admin.pk, ))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {'archive': ''}
        response = self.client.post(url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'User was archived successfully.')
        self.training_provider_admin.refresh_from_db()
        self.assertEqual(self.training_provider_admin.first_name, 'ARCHIVED')
        self.assertEqual(self.training_provider_admin.last_name, 'ARCHIVED')
        self.assertEqual(self.training_provider_admin.username, 'ARCHIVED {}'.format(self.training_provider_admin.pk))
        self.assertEqual(self.training_provider_admin.email, 'ARCHIVED {}'.format(self.training_provider_admin.pk))
        self.assertIsNotNone(self.training_provider_admin.archived_at)
        self.assertEqual(self.training_provider_admin.archived_by, self.user)
        self.assertFalse(self.training_provider_admin.is_active)
        self.assertNotContains(response, self.training_provider_admin.email)

        reversion = Revision.objects.filter(version__object_id=self.training_provider_admin.pk).last()
        self.assertEqual(reversion.comment, 'Archived')
        self.assertEqual(reversion.user, self.user)

    def test_update_already_archived_user(self):
        """
        Database Admin try update already archived user and get error
        """
        self.training_provider_admin.archived_at = timezone.now()
        self.training_provider_admin.save()
        url = reverse('database_admin:user_detail', args=(self.training_provider_admin.pk,))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = factory.build(dict, FACTORY_CLASS=TrainingProviderAdminFactory, email_status=User.BOUNCE, is_active=True)
        response = self.client.post(url, data, follow=True)
        self.assertContains(response, 'You can not update archived user.')
