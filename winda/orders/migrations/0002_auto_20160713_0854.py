# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import orders.managers


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='item',
            managers=[
                ('objects', orders.managers.ItemManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='transaction',
            managers=[
                ('objects', orders.managers.TransactionManager()),
            ],
        ),
        migrations.AlterField(
            model_name='item',
            name='item_code',
            field=models.CharField(unique=True, verbose_name='item code', choices=[('SINGLE', 'Single'), ('PACKAGE', 'Package')], max_length=10),
        ),
        migrations.AlterField(
            model_name='item',
            name='item_name',
            field=models.CharField(verbose_name='item name', choices=[('SINGLE', 'Single'), ('PACKAGE', 'Package')], max_length=30),
        ),
        migrations.AlterField(
            model_name='order',
            name='currency',
            field=models.CharField(default='EUR', verbose_name='currency', choices=[('EUR', 'EUR')], max_length=3),
        ),
        migrations.AlterField(
            model_name='order',
            name='item_code',
            field=models.CharField(verbose_name='item code', choices=[('SINGLE', 'Single'), ('PACKAGE', 'Package')], max_length=10),
        ),
        migrations.AlterField(
            model_name='order',
            name='item_name',
            field=models.CharField(verbose_name='item name', choices=[('SINGLE', 'Single'), ('PACKAGE', 'Package')], max_length=30),
        ),
        migrations.AlterField(
            model_name='order',
            name='payment_success',
            field=models.BooleanField(verbose_name='Payment Success', default=False),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='credits_per_unit',
            field=models.PositiveIntegerField(verbose_name='credits per unit'),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='currency',
            field=models.CharField(default='EUR', verbose_name='currency', choices=[('EUR', 'EUR')], max_length=3),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='item',
            field=models.ForeignKey(related_name='prices', verbose_name='item', to='orders.Item'),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='unit_price_including_vat',
            field=models.PositiveIntegerField(verbose_name='unit price including vat'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='training_provider',
            field=models.ForeignKey(related_name='transactions', verbose_name='training provider', to='trainings.TrainingProvider'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_value',
            field=models.PositiveIntegerField(verbose_name='transaction value'),
        ),
    ]
