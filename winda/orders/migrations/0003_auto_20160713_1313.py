# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20160713_0854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='payment_success',
            field=models.NullBooleanField(verbose_name='Payment Success', choices=[(None, 'Unpaid'), (False, 'Unpaid'), (True, 'Paid')]),
        ),
    ]
