# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_auto_20160629_1857'),
    ]

    operations = [
        migrations.CreateModel(
            name='DelegateSequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('sort_order', models.PositiveSmallIntegerField(verbose_name='sort order')),
                ('identifier', models.CharField(max_length=8, verbose_name='delegate id', unique=True)),
                ('sequential_prefix', models.PositiveSmallIntegerField(verbose_name='sequential prefix')),
                ('random_suffix', models.PositiveSmallIntegerField(verbose_name='random suffix')),
            ],
            options={
                'verbose_name': 'country',
                'verbose_name_plural': 'countries',
            },
        ),
        migrations.AlterUniqueTogether(
            name='delegatesequence',
            unique_together=set([('sort_order', 'identifier')]),
        ),
    ]
