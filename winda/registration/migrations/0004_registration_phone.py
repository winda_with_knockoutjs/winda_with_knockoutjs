# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0003_lostpassword_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='phone',
            field=models.CharField(max_length=15, verbose_name='work phone number', blank=True),
        ),
    ]
