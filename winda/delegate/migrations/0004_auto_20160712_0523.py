# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import delegate.managers


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0003_search'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='trainingrecord',
            managers=[
                ('objects', delegate.managers.TrainingRecordManager()),
            ],
        ),
    ]
