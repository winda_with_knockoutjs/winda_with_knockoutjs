# -*- coding: utf-8 -*-
from django.conf import settings


def template_settings(request):
    return {
        'settings': settings
    }
