# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0003_auto_20160704_1241'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ('title',), 'verbose_name': 'Course', 'verbose_name_plural': 'Courses'},
        ),
    ]
