# -*- coding: utf-8 -*-
from collections import OrderedDict
import csv

from django import forms
from django.conf import settings
from django.db.models import Q, Count
from django.db import connection
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from common.forms import BaseFilterForm
from common.mixins import TrainingProviderCertificateFormMixin, DataBaseUserFormMixin
from common.models import EmailNotification, Country
from core.forms import ModelForm
from trainings.models import TrainingProvider, TrainingProviderCertificate
from courses.models import CourseProvider, Course
from database_admin.widgets import CustomFileInputWidget
from delegate.models import TrainingRecord
from orders.models import Order


User = get_user_model()


class TrainingProviderFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('pk', 'ID'),
        ('name', _('name')),
        ('country', _('country')),
        ('postal_code', _('postal code')),
        ('is_active', _('is active')),
        ('accepted_at', _('accepted at')),
    ])


class TrainingProviderForm(ModelForm):
    class Meta:
        model = TrainingProvider
        fields = ('name', 'address_1', 'address_2', 'city', 'postal_code', 'country', 'is_active')
        required_fields = ('name', 'address_1', 'city', 'postal_code', 'country')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.required_fields:
            self.fields[field].required = True


class CoursesFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('code', _('Code')),
        ('title', _('Title')),
        ('course_type', _('Course Type')),
        ('validity_months', _('Validity Period (Months)')),
    ])


class CertificatesFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('certificate_number', _('Certificate number')),
        ('valid_from', _('Valid from')),
        ('valid_until', _('Valid until')),
        ('certificate_file', _('Certificate file')),
        ('is_deleted', _('Is Archived'))
    ])


class TrainingProviderCertificateForm(TrainingProviderCertificateFormMixin, ModelForm):
    valid_from = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                 input_formats=settings.DATE_INPUT_FORMATS,
                                 label=_('Valid From'))
    valid_until = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                  input_formats=settings.DATE_INPUT_FORMATS,
                                  label=_('Valid Until'))

    class Meta(TrainingProviderCertificateFormMixin.Meta):
        model = TrainingProviderCertificate
        fields = ('certificate_number', 'certificate_file', 'valid_until', 'valid_from', 'is_deleted')
        labels = {
            'certificate_file': _('certificate PDF'),
            'is_deleted': _('Is Archived'),
        }
        widgets = {
            'certificate_file': CustomFileInputWidget
        }


class AccreditationFilterForm(BaseFilterForm):

    FIELDS = OrderedDict([
            ('', ''),
            ('course__code', _('Code')),
            ('course__title', _('Course Title')),
            ('valid_from', _('Valid From')),
            ('valid_until', _('Valid Until')),
            ('is_deleted', _('Is Archived'))
        ])


class AccreditationForm(ModelForm):
    error_messages = {
        'future_valid_from': _('Date must be in the past.'),
        'past_valid_until': _('Date must be in the future.'),
        'valid_until_not_correct': _('Valid Until date must be after Valid From date.'),
    }
    course = forms.ModelChoiceField(queryset=Course.objects.all().order_by('code'), label=_('Course'))
    valid_from = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                 input_formats=settings.DATE_INPUT_FORMATS,
                                 label=_('Valid From'))
    valid_until = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                  input_formats=settings.DATE_INPUT_FORMATS,
                                  label=_('Valid Until'))

    class Meta:
        model = CourseProvider
        fields = ('course', 'valid_from', 'valid_until', 'is_deleted')
        labels = {
            'is_deleted': _('Is Archived'),
        }

    def clean_valid_from(self):
        valid_from = self.cleaned_data['valid_from']
        if valid_from >= timezone.now().date():
            raise forms.ValidationError(self.error_messages['future_valid_from'])
        return valid_from

    def clean_valid_until(self):
        valid_until = self.cleaned_data['valid_until']
        if valid_until <= timezone.now().date():
            raise forms.ValidationError(self.error_messages['past_valid_until'])
        return valid_until

    def clean(self):
        cd = super().clean()
        if self.errors:
            return cd
        if cd['valid_until'] <= cd['valid_from']:
            return self.add_error(None, self.error_messages['valid_until_not_correct'])
        return cd


class BaseReportForm(forms.Form):
    error_messages = {
        'date_until_not_correct': _('Date Until must be after Date From.'),
    }
    date_from = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                input_formats=settings.DATE_INPUT_FORMATS,
                                label=_('Date From'))
    date_until = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                 input_formats=settings.DATE_INPUT_FORMATS,
                                 label=_('Date Until'))

    def clean(self):
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data
        if cleaned_data['date_until'] <= cleaned_data['date_from']:
            return self.add_error(None, self.error_messages['date_until_not_correct'])
        return cleaned_data


class CSVGenerationMixin(object):

    filename = 'search_report_{}.csv'
    header = [_('Organisation domain'), _('Count')]

    def save(self):
        response = HttpResponse(content_type='text/csv')
        date = timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT)
        filename = self.filename.format(date)
        response['Content-Disposition'] = 'attachment;filename={0}'.format(filename)
        writer = csv.writer(response)
        writer.writerow(self.header)
        self.write_rows(writer)
        return response

    def write_rows(self, writer):
        return writer


class TrainingRecordReportForm(CSVGenerationMixin, BaseReportForm):
    training_provider = forms.ModelChoiceField(queryset=TrainingProvider.objects.all().order_by('name'),
                                               label=_('Training Provider'), required=False)
    course = forms.ModelChoiceField(queryset=Course.objects.all().order_by('title'), label=_('Course'), required=False)

    filename = 'training_record_report.csv'
    header = [_('Training Record ID'), _('WINDA ID'), _('Delegate First Name'), _('Delegate Last Name'),
        _('Delegate Email'), _('Course Code'), _('Course Name'), _('Country'), _('Training Provider ID'),
        _('Training Provider Name'), _('Created At'), _('Completed On'), _('Valid From'), _('Valid Until')]

    def write_rows(self, writer):
        date_from = self.cleaned_data['date_from']
        date_until = self.cleaned_data['date_until']
        t_records = TrainingRecord.objects.filter(Q(created__lte=date_until) & Q(created__gte=date_from))
        t_records = t_records.select_related('delegate', 'training_provider', 'course', 'country')
        course = self.cleaned_data['course']
        training_provider = self.cleaned_data['training_provider']
        if training_provider:
            t_records = t_records.filter(training_provider_id=training_provider.id)
        if course:
            t_records = t_records.filter(course_id=course.id)

        for record in t_records.iterator():
            writer.writerow([
                record.id,
                record.delegate.delegate_id,
                record.delegate.first_name,
                record.delegate.last_name,
                record.delegate.email,
                record.course.code,
                record.course.title,
                record.country.name,
                record.training_provider_id,
                record.training_provider.name,
                record.created.strftime(settings.REPORTS_DATE_FORMAT),
                record.completed_on.strftime(settings.REPORTS_DATE_FORMAT),
                record.valid_from.strftime(settings.REPORTS_DATE_FORMAT),
                record.valid_until.strftime(settings.REPORTS_DATE_FORMAT)
            ])


class SearchReportForm(CSVGenerationMixin, BaseReportForm):

    filename = 'search_report_{}.csv'
    header = [_('Organisation domain'), _('Count')]

    def write_rows(self, writer):
        date_from = self.cleaned_data['date_from']
        date_until = self.cleaned_data['date_until']
        cursor = connection.cursor()
        # Count domains occurencies in user emails
        cursor.execute('SELECT substring_index(users_user.email, "@", -1), COUNT(*) '+
                       'FROM delegate_search INNER JOIN users_user ON (delegate_search.created_by_id=users_user.id)'+
                       ' where (delegate_search.created <= Date(%s) and delegate_search.created >=Date(%s)) '+
                       'GROUP BY substring_index(users_user.email, "@", -1) '+
                       'ORDER BY COUNT(*) DESC;', [date_until, date_from])
        searches = cursor.fetchall()
        for search in searches:
            writer.writerow([search[0], search[1]])
        return writer


class PaymentReportForm(CSVGenerationMixin, BaseReportForm):

    filename = 'payment_report_{}.csv'
    header = [_('Date'), _('Order ID'), _('Training Provider'), _('Training Provider Name'),
        _('Quantity'), _('Item Code'), _('Item Name'), _('Currency'), _('Unit Price Inc VAT'), _('VAT Rate'),
        _('Total Price Inc VAT'), _('Payment Success')]

    def write_rows(self, writer):
        date_from = self.cleaned_data['date_from']
        date_until = self.cleaned_data['date_until']
        orders = Order.objects.filter(Q(created__lte=date_until) & Q(created__gte=date_from))
        for order in orders.iterator():
            writer.writerow([
                order.created.strftime(settings.REPORTS_DATE_FORMAT),
                order.id,
                order.training_provider_id,
                order.training_provider.name,
                order.quantity,
                order.item_code,
                order.item_name,
                order.get_currency_display(),
                order.item_unit_price_including_vat,
                order.item_vat_rate,
                order.total_price_including_vat,
                order.payment_success if order.payment_success else False
            ])
        return writer


class TrainingStatisticsReportForm(CSVGenerationMixin, BaseReportForm):

    filename = 'training_statistics_report_{}.csv'
    header = []

    def write_rows(self, writer):
        date_from = self.cleaned_data['date_from']
        date_until = self.cleaned_data['date_until']
        t_records = TrainingRecord.objects.filter(Q(created__lte=date_until) & Q(created__gte=date_from))
        t_records = t_records.values('training_provider__name', 'course__code', 'training_provider__country__name').annotate(count=Count('id'))
        courses = list(t_records.order_by('course__code').distinct().values_list('course__code', flat=True))
        columns_number = len(courses)
        providers = list(t_records.order_by('training_provider__name').distinct().values_list('training_provider__name', 'training_provider__country__name'))
        data = {}
        for record in t_records:
            key = '-'.join([record['training_provider__name'], record['training_provider__country__name']])
            if key not in data:
                # Fill columns with data
                data[key] = [0] * columns_number
            data[key][courses.index(record['course__code'])] = record['count']

        courses.insert(0, 'Training Provider Name')
        courses.insert(1, 'Country')
        writer.writerow(courses)

        for provider, country in providers:
            key = '-'.join([provider, country])
            row = data[key]
            row.insert(0, provider)
            row.insert(1, country)
            writer.writerow(row)
        return writer


class DatabaseGrowthReportForm(CSVGenerationMixin, BaseReportForm):

    filename = 'database_growth_report_{}.csv'
    header = [_('Date'), _('Count')]

    def write_rows(self, writer):
        date_from = self.cleaned_data['date_from']
        date_until = self.cleaned_data['date_until']
        t_records = TrainingRecord.objects.filter(Q(created__lte=date_until) & Q(created__gte=date_from))
        t_records = t_records.extra({'created': 'date(created)'}).values('created').annotate(count=Count('id'))
        for row in t_records:
            writer.writerow([
                row['created'].strftime(settings.REPORTS_DATE_FORMAT),
                row['count']
            ])
        return writer


class EmailNotificationFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('Email ID')),
        ('subject', _('Subject')),
        ('created', _('Queued At')),
        ('posted_at', _('Sent At')),
        ('notified_at', _('Delivered At')),
        ('notification_type', _('Email Status')),
    ])


class MailoutForm(ModelForm):
    error_messages = {
        'is_required': _('This field is required.'),
        'recipients_required': _('At least one Recipient checkbox must be ticked.'),
        'filter_by_course_required': _('Training Provider or Course is required.')
    }

    ROLE_CHOICE = (
        (User.DELEGATE, _('All Delegate users')),
        (User.ORGANIZATION, _('All Organization users')),
        (User.TRAINING_PROVIDER, _('All Training Provider User users')),
        (User.TRAINING_PROVIDER_ADMIN, _('All Training Provider Admin users')),
        (User.DATABASE_ADMIN, _('All Database Admin users')),
        (User.DATABASE_SUPER_ADMIN, _('All Database Super Admin users')),
        (User.TECHNICAL_SUPPORT, _('All Technical Support users')),
    )

    role = forms.MultipleChoiceField(label=_('recipients'), choices=ROLE_CHOICE,
                                     required=False, widget=forms.CheckboxSelectMultiple())
    filter_by_country = forms.BooleanField(required=False, label=_('Limit mailout to Training Provider / '
                                                                   'Organization users to country'))
    country = forms.ModelChoiceField(queryset=Country.objects.none(), label='', required=False)
    filter_by_course = forms.BooleanField(required=False, label=_('All Delegates with Training Records that meet the '
                                                                  'following criteria'))
    training_provider = forms.ModelChoiceField(queryset=TrainingProvider.objects.none(),
                                               label='Training Provider was', required=False)
    course = forms.ModelChoiceField(queryset=Course.objects.none(), label='Course was', required=False)
    completed_on_start = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                         input_formats=settings.DATE_INPUT_FORMATS, label='', required=False)
    completed_on_end = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                       input_formats=settings.DATE_INPUT_FORMATS, label='', required=False)

    class Meta:
        model = EmailNotification
        fields = ('subject', 'body')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['training_provider'].queryset = TrainingProvider.objects.all().order_by('name')
        self.fields['course'].queryset = Course.objects.all().order_by('title')
        self.fields['country'].queryset = Country.objects.all()

    def clean(self):
        cd = super().clean()
        if not cd.get('role') and not cd.get('filter_by_country') and not cd.get('filter_by_course'):
            self.add_error('role', self.error_messages['recipients_required'])

        if cd.get('filter_by_country') and not cd.get('country'):
            raise forms.ValidationError({'country': self.error_messages['is_required']})

        if cd.get('filter_by_course'):
            if not cd.get('training_provider') and not cd.get('course'):
                self.add_error('filter_by_course', self.error_messages['filter_by_course_required'])
        return cd

    def clean_subject(self):
        subject = self.cleaned_data['subject']
        return subject.replace('\n', '').replace('\r', '').replace('\t', ' ').strip()

    def clean_body(self):
        body = self.cleaned_data['body']
        return body.strip()


class ChangeHistoryFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('Revision')),
        ('date_created', _('Date and Time')),
        ('user', _('User')),
        ('comment', _('Summary')),
        ('detail', _('Detail')),
    ])


class UserFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('User ID')),
        ('first_name', _('First Name')),
        ('last_name', _('Last Name')),
        ('email', _('Email Address')),
        ('email_status', _('Email Status')),
        ('role', _('Role Type')),
        ('is_active', _('Is Active')),
        ('last_login', _('Last Login')),
        ('delegate_id', _('Winda ID')),
        ('organization_name', _('Company Name')),
        ('training_provider', _('Training Provider')),
    ])


class DelegateUserForm(DataBaseUserFormMixin, ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'email_status', 'is_active')
        required_fields = ('first_name', 'last_name', 'email', 'email_status')


class OrganizationUserForm(DataBaseUserFormMixin, ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'email_status', 'is_active', 'job_title', 'phone',
                  'organization_name', 'address_1', 'address_2', 'city', 'postal_code', 'country')
        required_fields = ('first_name', 'last_name', 'email', 'email_status', 'job_title', 'phone',
                           'organization_name', 'address_1', 'city', 'postal_code', 'country')


class TrainingProviderUserForm(DataBaseUserFormMixin, ModelForm):
    ROLE_CHOICE = (
        (User.TRAINING_PROVIDER, _('Training Provider User')),
        (User.TRAINING_PROVIDER_ADMIN, _('Training Provider Admin')),
    )

    class Meta:
        model = User
        fields = ('role', 'first_name', 'last_name', 'email', 'email_status', 'is_active',
                  'can_upload', 'can_use_credit', 'can_buy_credit')
        required_fields = ('role', 'first_name', 'last_name', 'email', 'email_status')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['role'].choices = self.ROLE_CHOICE
