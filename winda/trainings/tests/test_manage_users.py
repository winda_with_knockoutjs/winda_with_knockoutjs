# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
import factory

from django.core.urlresolvers import reverse
from django.test import TestCase

from common.models import EmailNotification
from registration.factories import CountryFactory
from trainings.factories import TrainingProviderFactory
from users.factories import TEST_USER_PASSWORD, UserFactory, TrainingProviderAdminFactory

User = get_user_model()


class UserManageTestCase(TestCase):

    def setUp(self):
        self.training_provider = TrainingProviderFactory()
        self.user = TrainingProviderAdminFactory(training_provider=self.training_provider)
        self.training_provider_user = UserFactory(training_provider=self.training_provider, role=User.TRAINING_PROVIDER)
        self.create_url = reverse('training_provider:user_create', args=(self.user.pk,))
        self.update_url = reverse('training_provider:user_update', args=(self.user.pk, self.training_provider_user.pk))
        self.list_url = reverse('training_provider:users', args=(self.user.pk,))
        self.country = self.training_provider.country
        self.user_count = User.objects.count()

    def test_access_for_training_provider_admin(self):
        """
        Training Provider Admin get users page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        for url in [self.create_url, self.update_url, self.list_url]:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, self.training_provider.name)

    def test_access_for_training_provider(self):
        """
        Training Provider get users page and get permission error
        """
        self.client.login(username=self.training_provider_user.username, password=TEST_USER_PASSWORD)
        for url in [self.create_url, self.update_url, self.list_url,
                    reverse('training_provider:users', args=(self.training_provider_user.pk,)),
                    reverse('training_provider:user_create', args=(self.training_provider_user.pk,)),
                    reverse('training_provider:user_update', args=(self.training_provider_user.pk, self.training_provider_user.pk))]:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 404)

    def test_create(self):
        """
        Training Provider Admin create new training provider
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Create User', 1)

        data = factory.build(dict, FACTORY_CLASS=UserFactory, country=self.country.pk, role=User.TRAINING_PROVIDER)
        response = self.client.post(self.create_url, data)
        self.assertRedirects(response, self.list_url)
        self.assertEqual(User.objects.count(), self.user_count + 1)
        self.assertEqual(EmailNotification.objects.count(), 1)

        user = User.objects.last()
        for field in ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(user, field), data.get(field, ''))
        self.assertEqual(user.training_provider, self.training_provider)
        self.assertTrue(self.training_provider_user.is_active)

    def test_create_fail(self):
        """
        Training Provider Admin submit blank create new user form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertContains(response, 'This field is required.', 5)

    def test_update_user(self):
        """
        Training Provider Admin update user form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage User', 1)

        data = factory.build(dict, FACTORY_CLASS=UserFactory, country=self.country.pk, role=User.TRAINING_PROVIDER, is_active=True)
        response = self.client.post(self.update_url, data)
        self.assertRedirects(response, self.list_url)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertEqual(EmailNotification.objects.count(), 0)

        self.training_provider_user.refresh_from_db()
        for field in ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code'):
            self.assertEqual(getattr(self.training_provider_user, field), data.get(field, ''))
        self.assertTrue(self.training_provider_user.is_active)

    def test_update_fail(self):
        """
        Training Provider Admin submit blank update user form form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertContains(response, 'This field is required.', 5)
