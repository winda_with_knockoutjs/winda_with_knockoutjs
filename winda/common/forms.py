# -*- coding: utf-8 -*-
import dateutil.parser
from OpenSSL import crypto
import base64
import json
import pem
import boto.sns
from boto.exception import BotoServerError
from urllib.request import urlopen

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from common.models import EmailNotification
from core.forms import ModelForm


class BaseFilterForm(forms.Form):
    FIELDS = {}

    start = forms.IntegerField(initial=1)
    length = forms.IntegerField(initial=10)
    sort_asc = forms.ChoiceField(choices=(('', 'asc'), ('-', 'desc')), required=False)
    sort_column = forms.TypedChoiceField(coerce=int)
    search = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(BaseFilterForm, self).__init__(*args, **kwargs)
        self.fields['sort_column'].choices = zip(map(str, range(len(self.FIELDS))), range(len(self.FIELDS)))

    def clean(self):
        cd = super().clean()
        cd['search'] = self.data.get('search[value]', '')
        return cd


class EmailNotificationForm(ModelForm):
    NOTIFICATION_HASH_FORMAT = '''Message
{Message}
MessageId
{MessageId}
Timestamp
{Timestamp}
TopicArn
{TopicArn}
Type
{Type}
'''

    SUBSCRIPTION_HASH_FORMAT = '''Message
{Message}
MessageId
{MessageId}
SubscribeURL
{SubscribeURL}
Timestamp
{Timestamp}
Token
{Token}
TopicArn
{TopicArn}
Type
{Type}
'''

    class Meta:
        model = EmailNotification
        fields = ('notification_type', 'notified_at', 'bounce_type', 'bounce_sub_type', 'complaint_type')

    def __init__(self, *args, **kwargs):
        request = kwargs['request']
        kwargs['data'] = json.loads(request.body.decode())
        super().__init__(*args, **kwargs)

    def clean(self):
        super().clean()
        if not self.verify_notification(self.data):
            raise forms.ValidationError(_('Invalid Signature'))

        if self.data['Type'] == 'SubscriptionConfirmation':
            return self.approve_subscription(self.data)

        elif self.data['Type'] == 'UnsubscribeConfirmation':
            raise forms.ValidationError(_('Unsubscribe Confirmation Not Handled'))

        elif self.data['Type'] == 'Notification':
            try:
                self.data = json.loads(self.data['Message'])
                self.instance = EmailNotification.objects.get(ses_message_id=self.data.get('mail', {}).get('messageId'))
                return getattr(self, self.data.get('notificationType', '').lower(), {})
            except EmailNotification.DoesNotExist:
                raise forms.ValidationError(_('Email Notification does not exist'))

        raise forms.ValidationError(_('Unknown Notification Type'))

    def approve_subscription(self, data):
        """
        Confirm subscription
        """
        connection = boto.sns.connect_to_region(region_name=settings.AWS_SNS_REGION_ENDPOINT)
        client = boto.sns.SNSConnection(aws_access_key_id=settings.AWS_SES_ACCESS_KEY_ID, region=connection.region,
                                        aws_secret_access_key=settings.AWS_SES_SECRET_ACCESS_KEY)
        try:
            return client.confirm_subscription(topic=data['TopicArn'], token=data['Token'],
                                               authenticate_on_unsubscribe=True)
        except BotoServerError as e:
            raise forms.ValidationError(json.dumps(e.body))

    def verify_notification(self, data):
        """
        Verify notification came from a trusted source
        Returns True if verified, False if not verified
        """
        response = urlopen(data['SigningCertURL'])
        pem_file = response.read()
        certificates = pem.parse(pem_file)

        if len(certificates) != 1:
            raise forms.ValidationError(_('Invalid Certificate File'))

        cert = crypto.load_certificate(crypto.FILETYPE_PEM, pem_file)
        signature = base64.b64decode(bytearray(data['Signature'], encoding='utf-8'))

        if data['Type'] == "Notification":
            hash_format = self.NOTIFICATION_HASH_FORMAT
        else:
            hash_format = self.SUBSCRIPTION_HASH_FORMAT
        try:
            crypto.verify(cert, signature, hash_format.format(**data), 'sha1')
        except crypto.Error:
            return
        return True

    @property
    def delivery(self):
        if 'delivery' in self.data and 'timestamp' in self.data['delivery']:
            return {'notification_type': EmailNotification.DELIVERY,
                    'notified_at': dateutil.parser.parse(self.data['delivery']['timestamp'])}
        raise forms.ValidationError(_('Wrong delivery message'))

    @property
    def bounce(self):
        if 'bounce' in self.data and 'bounceType' in self.data['bounce'] and 'timestamp' in self.data['bounce']:
            return {'notification_type': EmailNotification.BOUNCE,
                    'bounce_type': self.data['bounce']['bounceType'],
                    'bounce_sub_type': self.data['bounce'].get('bounceSubType'),
                    'notified_at': dateutil.parser.parse(self.data['bounce']['timestamp'])}
        raise forms.ValidationError(_('Wrong bounce message'))

    @property
    def complaint(self):
        if 'complaint' in self.data and 'complaintFeedbackType' in self.data['complaint'] and 'timestamp' in self.data['complaint']:
            return {'notification_type': EmailNotification.COMPLAINT,
                    'complaint_type': self.data['complaint']['complaintFeedbackType'],
                    'notified_at': dateutil.parser.parse(self.data['complaint']['timestamp'])}
        raise forms.ValidationError(_('Wrong complaint message'))
