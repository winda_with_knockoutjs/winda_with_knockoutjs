# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from common.management.commands.generatenotifications import Command as gn_command
from common.models import EmailNotification
from delegate.factories import TrainingRecordFactory
from trainings.factories import TrainingProviderCertificateFactory
from trainings.models import TrainingProvider
from users.factories import DatabaseAdminFactory, DelegateFactory, TrainingProviderAdminFactory, OrganizationFactory


class GenerateNotificationsTestCase(TestCase):

    def setUp(self):
        self.delegate1, self.delegate2, delegate3 = DelegateFactory.create_batch(3)
        self.today = timezone.now()
        TrainingRecordFactory.create_batch(5, delegate=delegate3, valid_until=self.today - timezone.timedelta(days=3))

    def test_command(self):
        call_command('generatenotifications')
        call_command('generatenotifications', date='2016-02-15')
        call_command('generatenotifications', date='')

    def test_training_records_expiring_soon(self):
        TrainingRecordFactory.create_batch(2, delegate=self.delegate1, valid_until=self.today - relativedelta(months=3))
        TrainingRecordFactory.create_batch(2, delegate=self.delegate1, valid_until=self.today - timezone.timedelta(weeks=2))
        TrainingRecordFactory.create_batch(2, delegate=self.delegate1, valid_until=self.today - timezone.timedelta(weeks=1))
        TrainingRecordFactory.create_batch(4, delegate=self.delegate1, valid_until=self.today - timezone.timedelta(weeks=3))

        TrainingRecordFactory.create_batch(3, delegate=self.delegate2, valid_until=self.today - relativedelta(months=3))
        TrainingRecordFactory.create_batch(3, delegate=self.delegate2, valid_until=self.today - timezone.timedelta(weeks=2))
        TrainingRecordFactory.create_batch(3, delegate=self.delegate2, valid_until=self.today - timezone.timedelta(weeks=1))
        TrainingRecordFactory.create_batch(5, delegate=self.delegate2, valid_until=self.today - timezone.timedelta(days=3))

        gn_command.training_records_expiring_soon(self.today)
        self.assertEqual(EmailNotification.objects.count(), 2)
        self.assertNumQueries(3, lambda: gn_command.training_records_expiring_soon(self.today))

    def test_training_records_expired(self):
        TrainingRecordFactory.create_batch(2, delegate=self.delegate1, valid_until=self.today - timezone.timedelta(days=1))
        TrainingRecordFactory.create_batch(4, delegate=self.delegate1, valid_until=self.today - timezone.timedelta(days=2))

        TrainingRecordFactory.create_batch(3, delegate=self.delegate2, valid_until=self.today - timezone.timedelta(days=1))
        TrainingRecordFactory.create_batch(3, delegate=self.delegate2, valid_until=self.today - timezone.timedelta(days=2))

        gn_command.training_records_expired(self.today)
        self.assertEqual(EmailNotification.objects.count(), 2)
        self.assertNumQueries(3, lambda: gn_command.training_records_expired(self.today))

    def test_delegate_account_expiring_soon(self):
        for input_date in [self.today - relativedelta(months=3), self.today - timezone.timedelta(weeks=2),
                           self.today - timezone.timedelta(weeks=1)]:
            DelegateFactory(archived_from=input_date)

        gn_command.delegate_account_expiring_soon(self.today)
        self.assertEqual(EmailNotification.objects.count(), 3)
        self.assertNumQueries(2, lambda: gn_command.delegate_account_expiring_soon(self.today))

    def test_delegate_account_expired(self):
        DelegateFactory(archived_from=self.today + timezone.timedelta(days=1))

        gn_command.delegate_account_expired(self.today)
        self.assertEqual(EmailNotification.objects.count(), 1)
        self.assertNumQueries(2, lambda: gn_command.delegate_account_expired(self.today))

    def test_training_provider_certificates_expiring_soon(self):
        for input_date in [self.today - relativedelta(months=3), self.today - relativedelta(months=2),
                           self.today - relativedelta(months=1), self.today - timezone.timedelta(weeks=1)]:
            TrainingProviderCertificateFactory.create_batch(2, valid_until=input_date)
        count = TrainingProvider.objects.all().update(is_active=True)

        TrainingProviderAdminFactory()
        DatabaseAdminFactory()

        gn_command.training_provider_certificates_expiring_soon(self.today)
        self.assertEqual(EmailNotification.objects.count(), count * 2)
        self.assertNumQueries(4, lambda: gn_command.training_provider_certificates_expiring_soon(self.today))

    def test_organization_account_expiring_soon(self):
        OrganizationFactory(archived_from=self.today - timezone.timedelta(weeks=1))
        OrganizationFactory(archived_from=self.today - timezone.timedelta(weeks=2))

        gn_command.organization_account_expiring_soon(self.today)
        self.assertEqual(EmailNotification.objects.count(), 1)
        self.assertNumQueries(2, lambda: gn_command.organization_account_expiring_soon(self.today))

    def test_organization_account_expired(self):
        OrganizationFactory(archived_from=self.today + timezone.timedelta(days=1))
        OrganizationFactory(archived_from=self.today + timezone.timedelta(days=2))

        gn_command.organization_account_expired(self.today)
        self.assertEqual(EmailNotification.objects.count(), 1)
        self.assertNumQueries(2, lambda: gn_command.organization_account_expired(self.today))
