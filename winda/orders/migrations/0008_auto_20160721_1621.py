# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_auto_20160718_0910'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='paymentstatus',
            options={'verbose_name': 'Payment Status', 'verbose_name_plural': 'Payment Statuses'},
        ),
        migrations.AlterField(
            model_name='paymentstatus',
            name='acquirer_status_code',
            field=models.CharField(max_length=10, verbose_name='acquirer status code', blank=True),
        ),
        migrations.AlterField(
            model_name='paymentstatus',
            name='acquirer_status_message',
            field=models.CharField(max_length=30, verbose_name='acquirer status message', blank=True),
        ),
        migrations.AlterField(
            model_name='paymentstatus',
            name='amount',
            field=models.PositiveIntegerField(verbose_name='amount', null=True),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='vat_rate',
            field=models.DecimalField(validators=[django.core.validators.MaxValueValidator(0.991), django.core.validators.MinValueValidator(0)], verbose_name='vat rate', max_digits=3, decimal_places=2),
        ),
    ]
