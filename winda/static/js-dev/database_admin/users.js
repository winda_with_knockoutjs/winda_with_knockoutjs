$(document).ready(function() {
    var database_admin_users = $('table#database_admin_users').DataTable( {
        "iDisplayLength": 20,
        "scrollCollapse": true,
        "bLengthChange": false,
        "scrollY": true,
        "scrollX": false,
        "processing": true,
        "bInfo": false,
        "serverSide": true,
        "ajax": '',
        "order": [[ 4, 'asc' ]],
        "columns": [
            {"data": "", 'searchable': false, 'orderable': false, "className": "control"},
            {"data": "id"},
            {"data": "first_name"},
            {"data": "last_name"},
            {"data": "email"},
            {"data": "email_status"},
            {"data": "role"},
            {"data": "is_active"},
            {"data": "last_login"},
            {"data": "delegate_id"},
            {"data": "organization_name"},
            {"data": "training_provider"}
        ],
        "language": {
            "oPaginate": {
                "sNext": '&raquo;',
                "sPrevious": '&laquo;'
            }
        },
        fnServerParams: function ( aoData ) {
            if (aoData.order.length) {
                aoData.sort_column = aoData.order[0]['column'];
                aoData.sort_asc = aoData.order[0]['dir'] === 'asc' ? '' : '-';
            }
        },
        fnDrawCallback: function (oSettings) {
            var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate');
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                pgr.hide();
            } else {
                pgr.show()
            }
        },
        responsive: {
            details: {
                type: 'column'
            }
        }
    });


    $( "#search-database-admin-users" ).submit(function( event ) {
       event.preventDefault();
       var val = $(this).find('input').val();
        database_admin_users.search(val).draw();
    });
});
