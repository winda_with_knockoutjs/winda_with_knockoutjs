# -*- coding: utf-8 -*-
import factory
from factory import fuzzy

from common.models import Country, DelegateSequence
from registration.models import Registration


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    name = fuzzy.FuzzyText()
    code3 = factory.Sequence(lambda n: '{0}'.format(n))


class DelegateSequenceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DelegateSequence

    sort_order = fuzzy.FuzzyInteger(1)
    identifier = fuzzy.FuzzyText(length=8)
    sequential_prefix = fuzzy.FuzzyInteger(1)
    random_suffix = fuzzy.FuzzyInteger(1, high=999)


class RegistrationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Registration

    first_name = factory.Sequence(lambda n: 'testuserfirst{0}'.format(n))
    last_name = factory.Sequence(lambda n: 'testuserlast{0}'.format(n))
    email = factory.Sequence(lambda n: 'testuser_{0}@email.com'.format(n))
    registration_type = Registration.DELEGATE
    job_title = fuzzy.FuzzyText()
    phone = fuzzy.FuzzyText()
    organization_name = fuzzy.FuzzyText()
    address_1 = fuzzy.FuzzyText()
    address_2 = fuzzy.FuzzyText()
    city = fuzzy.FuzzyText()
    postal_code = fuzzy.FuzzyText()
    country = factory.SubFactory(CountryFactory)
    timezone = 'Europe/London'
