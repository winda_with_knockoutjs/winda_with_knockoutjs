# -*- coding: utf-8 -*-
import csv
import os
import io
from collections import OrderedDict

from django import forms
from django.db.models.fields.files import FieldFile
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.paginator import Paginator
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from dateutil.relativedelta import relativedelta

from common.forms import BaseFilterForm
from common.mixins import UserFormMixin
from common.models import EmailNotification
from core.forms import ModelForm
from courses.models import Course
from delegate.models import TrainingRecord
from trainings.models import UploadBatchRecord, UploadBatch
from trainings.widgets import CustomSelect
from trainings.fields import CourseModelChoiceField
from trainings.utils import ValidationUtil
from orders.models import Transaction

User = get_user_model()


class UserFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('first_name', _('first name')),
        ('last_name', _('last name')),
        ('email', _('Email Address')),
        ('role', _('Role Type')),
        ('is_active', _('is active')),
        ('can_upload', _('can upload')),
        ('can_use_credit', _('can use credit')),
        ('can_buy_credit', _('can buy credit')),
    ])


class UserForm(UserFormMixin, ModelForm):

    ROLE_CHOICE = (
        (User.TRAINING_PROVIDER, _('Training Provider User')),
        (User.TRAINING_PROVIDER_ADMIN, _('Training Provider Admin')),
    )

    role = forms.ChoiceField(label=_('role type'), choices=ROLE_CHOICE)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'role', 'timezone', 'is_active',
                  'can_upload', 'can_use_credit', 'can_buy_credit')
        labels = {
            'is_active': _('Can Login to WINDA')
        }


class UploadBatchFilterForm(BaseFilterForm):
    FIELDS = OrderedDict([
        ('', ''),
        ('id', _('id')),
        ('status', _('Status')),
        ('description', _('Description')),
        ('created', _('Uploaded At')),
        ('created_by', _('Uploaded By')),
        ('purchased_at', _('Purchased At')),
        ('purchased_by', _('Purchased By')),
        ('total_records', _('Total')),
        ('valid_records', _('Valid')),
        ('duplicate_records', _('Duplicate')),
        ('error_records', _('Errors'))
    ])


class UploadBatchForm(ModelForm):

    class Meta:
        model = UploadBatch
        exclude = ('id', 'created_by')


class SingleUploadForm(ModelForm):

    error_messages = {
        'previous_course_valid_until': _('Date must be in the future.'),
        'previous_course_valid_until_required': _('Previous course valid until date is required'),
        'course_completion_date': _('Date cannot be in the future.'),
        'invalid_delegate_id': _('The Winda ID entered does not match any of our records.'),
        'archived_delegate': _('Delegate has been archived'),
        'invalid_previous_course': _('Previous Course Valid Until Date does not match existing training records'),
        'no_training_record_to_invalidate': _('No matching training record found to invalidate'),
        'duplicate_record': _('Duplicate of existing training record'),
    }

    invalidate_records = forms.BooleanField(required=False, label=_('Revoke matching training records'))
    description = forms.CharField(max_length=100)
    course = CourseModelChoiceField(queryset=Course.objects.none(), widget=CustomSelect())
    course_completion_date = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                             input_formats=settings.DATE_INPUT_FORMATS,
                                             label=_('Course completion date'))
    previous_course_valid_until = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                                  input_formats=settings.DATE_INPUT_FORMATS,
                                                  label=_('Previous course valid until'),
                                                  required=False)

    class Meta:
        model = UploadBatchRecord
        fields = ('delegate_id', 'course_code', 'course_completion_date', 'previous_course_valid_until',
                  'description', 'invalidate_records', 'course')
        required_fields = ('delegate_id', 'course_completion_date', 'description', 'course')
        labels = {
            'delegate_id': _('Winda ID')
        }

    def __init__(self, *args, **kwargs):
        self.training_provider = kwargs.pop('training_provider', None)
        super().__init__(*args, **kwargs)
        courses = Course.objects.prefetch_related('course_providers').filter(
            course_providers__training_provider_id=self.training_provider.id,
            course_providers__valid_from__lte=timezone.now().date(),
            course_providers__valid_until__gte=timezone.now().date(),
            course_providers__is_deleted=False
        )
        self.fields['course'].queryset = courses.distinct()
        for field in self.Meta.required_fields:
            self.fields[field].required = True
        self.training_records = []

    def clean_delegate_id(self):
        delegate_id = self.cleaned_data.get('delegate_id')
        self.delegate = get_user_model().objects.delegates().filter(delegate_id=delegate_id).first()
        # Delegate does not exist
        if not self.delegate:
            raise forms.ValidationError(self.error_messages['invalid_delegate_id'])
        # Delegate is archived
        if self.delegate.archived_at:
            raise forms.ValidationError(self.error_messages['archived_delegate'])
        return delegate_id

    def clean_course_completion_date(self):
        course_completion_date = self.cleaned_data['course_completion_date']
        if course_completion_date > timezone.now().date():
            raise forms.ValidationError(self.error_messages['course_completion_date'])
        return course_completion_date

    def clean(self):
        course = self.cleaned_data.get('course')
        previous_course_valid_until = self.cleaned_data.get('previous_course_valid_until')
        course_completion_date = self.cleaned_data.get('course_completion_date')
        invalidate_records = self.cleaned_data.get('invalidate_records')

        if course and getattr(self, 'delegate', None):
            if course.course_type == Course.REFRESHER:
                if not previous_course_valid_until:
                    raise forms.ValidationError({'previous_course_valid_until': self.error_messages.get('previous_course_valid_until_required')})

                if previous_course_valid_until < timezone.now().date():
                    raise forms.ValidationError({'previous_course_valid_until':self.error_messages['previous_course_valid_until']})

            self.cleaned_data['course_code'] = course.code

            previous_course_valid_until = self.cleaned_data.get('previous_course_valid_until')
            # Course is refresher, previous course valid until is specified but no training records
            # exist for specified delegate/course/valid_until
            invalid_previous_course = previous_course_valid_until and course.course_type == Course.REFRESHER \
                and course.require_existing_training_record and \
                not course.refresher_for_course.training_records.filter(delegate_id=self.delegate.id,
                                                   valid_until=previous_course_valid_until).exists()
            if invalid_previous_course:
                raise forms.ValidationError({'previous_course_valid_until': self.error_messages.get('invalid_previous_course')})

            self.training_records = course.training_records.filter(delegate_id=self.delegate.id,
                                                                   completed_on=course_completion_date,
                                                                   invalidated_at=None).values_list('id', flat=True)
            if self.training_records and not invalidate_records:
                raise forms.ValidationError({'course_completion_date': self.error_messages.get('duplicate_record')})
            elif not self.training_records and invalidate_records:
                raise forms.ValidationError({'invalidate_records': self.error_messages.get('no_training_record_to_invalidate')})
        return self.cleaned_data

    def save(self, commit=True):
        # Create Upload batch
        upload_batch = None
        invalidate = self.cleaned_data.get('invalidate_records')
        batch_data = self.cleaned_data.copy()
        batch_data.update({
            'error_records': 0,
            'duplicate_records': 0,
            'training_provider': self.training_provider.id,
            'valid_records': 1,
            'total_records': 1,
            'status': UploadBatch.DRAFT if not invalidate else UploadBatch.INVALIDATION
        })
        upload_batch_form = UploadBatchForm(data=batch_data, request=self.request)
        if upload_batch_form.is_valid():
            upload_batch = upload_batch_form.save(commit=False)
            upload_batch.training_provider = self.training_provider
            upload_batch.save()

        course = self.cleaned_data.get('course')
        dates_data = ValidationUtil.calculate_dates(
            invalidate=invalidate,
            course_code=course.code,
            course_completion_date=self.cleaned_data.get('course_completion_date'),
            course=course,
            previous_course_valid_until=self.cleaned_data.get('previous_course_valid_until')
        )

        if 'calculated_valid_from' in dates_data and 'calculated_valid_until' in dates_data:
            self.instance.calculated_valid_from = dates_data['calculated_valid_from']
            self.instance.calculated_valid_until = dates_data['calculated_valid_until']

        # Fill missing fields for UploadBatchRecord
        self.instance.upload_batch = upload_batch
        self.instance.source_row_number = 1
        self.instance.parse_check = 0
        self.instance.validation_check = 0
        self.instance.duplicate_check = 0
        self.instance = super().save(commit)

        if invalidate and self.training_records and self.instance.is_valid():
            TrainingRecord.objects.filter(id__in=list(self.training_records)).update(
                invalidated_at=upload_batch.created,
                invalidated_by=self.request.user,
                invalidated_by_record=self.instance
            )
        return self.instance


class UploadBatchRecordForm(ModelForm):

    class Meta:
        model = UploadBatchRecord
        fields = ('upload_batch', 'source_row_number', 'delegate_id', 'course_code', 'course_completion_date',
            'previous_course_valid_until', 'calculated_valid_from', 'calculated_valid_until', 'parse_check',
            'validation_check', 'duplicate_check')


class BulkUploadForm(ModelForm):
    error_messages = {
        'file_too_large': _('File attachment exceeds maximum size limit.'),
        'invalid_type':  _('Upload file is not a valid CSV file. Please read the upload instructions or use the template provided.'), #Err138
        'invalid_headers': _('Upload file has incorrect headers. Please read the upload instructions or use the template provided.'), #Err139
        'invalid_number_of_records': _('Upload file contains too many records.'), #Err140
    }
    header_titles = ['WINDA ID', 'Course Code', 'Course Completion Date', 'Previous Course Valid Until Date']

    invalidate_records = forms.BooleanField(required=False, label=_('Revoke matching training records'))
    upload_file = forms.FileField(label=_('Upload file'))

    class Meta:
        model = UploadBatch
        fields = ('description', )

    def __init__(self, *args, **kwargs):
        self.file_data = []
        self.delegate_ids = []
        self.training_provider = kwargs.pop('training_provider', None)
        super().__init__(*args, **kwargs)

    def clean_upload_file(self):
        upload_file = self.cleaned_data['upload_file']
        filename, ext = os.path.splitext(upload_file.name)
        if ext not in settings.TRAINING_RECORD_UPLOAD_FILE_FORMAT.keys():
            raise forms.ValidationError(self.error_messages['invalid_type'])

        if isinstance(upload_file, FieldFile):
            file_size = upload_file.size
        else:
            file_size = upload_file._size
        if file_size > settings.TRAINING_RECORD_MAX_UPLOAD_SIZE:
            raise forms.ValidationError(self.error_messages['file_too_large'])

        if upload_file:
            try:
                self.process_upload_file(upload_file)
            except ValueError:
                # Malformed CSV file content
                raise forms.ValidationError(self.error_messages.get('invalid_type'))
            if len(self.file_data) > settings.TRAINING_RECORD_MAX_QUANTITY:
                raise forms.ValidationError(self.error_messages.get('invalid_number_of_records'))
        return upload_file

    def process_upload_file(self, upload_file):
        f = io.StringIO(upload_file.read().decode())
        reader = csv.reader(f)
        self.file_data = []
        for index, row in enumerate(reader):
            if not index:
                if len(row) < len(self.header_titles):
                    raise forms.ValidationError(self.error_messages.get('invalid_headers'))
                for index, title in enumerate(self.header_titles):
                    if not row[index] or row[index].strip().lower() != title.lower():
                        raise forms.ValidationError(self.error_messages.get('invalid_headers'))
            elif row:
                self.file_data.append(row)
                self.delegate_ids.append(row[0].lower())

    def save(self, commit=True):
        invalidate = self.cleaned_data.get('invalidate_records')
        self.instance.status = UploadBatch.DRAFT if not invalidate else UploadBatch.INVALIDATION
        self.instance.training_provider = self.training_provider
        self.instance.error_records = 0
        self.instance.duplicate_records = 0
        self.instance.valid_records = 0
        self.instance.total_records = 0
        self.instance = super().save(commit)

        accredited_courses = Course.objects.prefetch_related('course_providers').filter(
            course_providers__training_provider_id=self.training_provider.id,
            course_providers__valid_from__lte=timezone.now().date(),
            course_providers__valid_until__gte=timezone.now().date(),
            course_providers__is_deleted=False
        ).values_list('id', flat=True)

        saved_records = {} # Initial dict with keys for already created records
        current_save_batch = [] # List of records to be saved

        fields = ('upload_batch', 'source_row_number', 'delegate_id', 'course_code', 'course_completion_date',
            'previous_course_valid_until', 'calculated_valid_from', 'calculated_valid_until', 'parse_check',
            'validation_check', 'duplicate_check')

        # About 20 courses is expected, so we can load them all at once
        courses = {course.code.upper():course for course in Course.objects.all()}

        page_size = 100
        paginator = Paginator(self.file_data, page_size)

        for page in paginator.page_range:
            current_delegate_ids = []
            q_filters = []
            q_obj = None
            records_batch = []

            validation_init = ValidationUtil(None, None, None)
            for obj in paginator.page(page).object_list:
                validation_init.reset_fields()
                validation_init.row = obj
                validation_init.parse_row()

                # Get delegate ids to fetch in current batch
                current_delegate_ids.append(validation_init.delegate_id)
                # Get records to process in current batch
                records_batch.append(obj)
                # Get filter params to process in current batch for duplicate check
                q_filters.append((validation_init.delegate_id, validation_init.course_code, validation_init.course_completion_date))

            current_delegates = {user.delegate_id: user for user in User.objects.delegates().filter(delegate_id__in=current_delegate_ids)}

            # Replace delegate_id and course_code in filter params with delegate_id (as for fk) and course_id (as for fk)
            # Initialize q_obj for current batch request for duplicates check
            for delegate_id, course_code, completed_on in q_filters:
                delegate = current_delegates.get(delegate_id)
                course = courses.get(course_code)
                if delegate and course and completed_on:
                    current_q = (Q(course_id=course) & Q(delegate_id=delegate.id) &
                                 Q(completed_on=completed_on) & Q(invalidated_at=None))
                    q_obj = q_obj | current_q if q_obj else current_q

            # Fetch training records filter parameters
            if q_obj:
                training_records = [(r.delegate_id, r.course_id, r.completed_on) for r in TrainingRecord.objects.filter(q_obj)]
            else:
                training_records = []

            for current_index, record in enumerate(records_batch):
                index = (page - 1) * page_size + current_index

                validation = ValidationUtil(row=record,
                                            index=index + 2, # Start count from 1, first row is header.
                                            upload_batch=self.instance,
                                            accredited_courses=accredited_courses,
                                            invalidate=invalidate,
                                            saved_records=saved_records,
                                            courses=courses,
                                            delegates=current_delegates,
                                            training_records=training_records)
                data = validation.validate()

                batch_data = {field:data[field] for field in fields if field in data}

                record_instance = UploadBatchRecord(**batch_data)
                valid = not data['duplicate_check'] and not data['validation_check'] and not data['parse_check']
                # Calculate records for batch
                if data['duplicate_check']:
                    self.instance.duplicate_records += 1

                if invalidate:
                    record_instance.save()
                else:
                    current_save_batch.append(record_instance)

                if invalidate and valid:
                    TrainingRecord.objects.filter(id__in=list(validation.training_records)).update(
                        invalidated_at=self.instance.created,
                        invalidated_by=self.request.user,
                        invalidated_by_record=record_instance
                    )
                if valid:
                    self.instance.valid_records += 1
                if data['parse_check'] or data['validation_check']:
                    self.instance.error_records += 1

                self.instance.total_records += 1
                if len(current_save_batch) >= settings.TRAINING_RECORD_BULK_SAVE_SIZE:
                    UploadBatchRecord.objects.bulk_create(current_save_batch)
                    current_save_batch = []
                saved_records = validation.get_saved_records() # Get dictionary with keys for saved records with results of duplicate check
        # Save leftover records
        if len(current_save_batch):
            UploadBatchRecord.objects.bulk_create(current_save_batch)
        if commit:
            self.instance.save()
        return self.instance


class BulkPurchaseForm(ModelForm):

    error_messages = {
        'transaction_failed': _('Your transaction could not be completed at this time due to a conflict with another transaction. Please try again.'),
        'cannot_use_credits': _('You are not permitted to create uploads. Ask your company administrator for assistance.'),
        'insufficient_credits': _('You have insufficient credit to purchase this upload. Add additional credit to your account then try again.')
    }

    class Meta:
        model = UploadBatch
        fields = ('status',)

    def __init__(self, *args, **kwargs):
        self.training_provider = kwargs.pop('training_provider', None)
        self.parent_user = kwargs.pop('parent_user', None)
        super().__init__(*args, **kwargs)
        self.changed = True
        self.training_records = []
        self.purchased_at = timezone.now()
        self.valid_records = 0
        self.duplicate_records = 0
        self.delegate_ids = {}

    def clean(self):

        if not self.request.user.can_use_credit:
            raise forms.ValidationError(self.error_messages.get('cannot_use_credits'))
        if self.cleaned_data.get('status') == UploadBatch.PURCHASED:
            processed_records = {}  # Currently processed results
            self.delegate_ids = {}  # To get delegate id by delegate_id for foreign key in already processed delegates
            courses = {course.code.upper():course.id for course in Course.objects.all()}
            records = self.instance.upload_batch_records.order_by('-duplicate_check')

            paginator = Paginator(records, 100)

            for page in paginator.page_range:
                current_delegate_ids = []
                q_filters = []
                q_obj = None
                records_batch = []


                for obj in paginator.page(page).object_list:
                    # Get delegate ids to fetch in current batch
                    current_delegate_ids.append(obj.delegate_id)
                    # Get records to process in current batch
                    records_batch.append(obj)
                    # Get filter params to process in current batch for duplicate check
                    q_filters.append((obj.delegate_id, obj.course_code, obj.course_completion_date))

                current_delegates = {user.delegate_id: user.id for user in User.objects.delegates().filter(delegate_id__in=current_delegate_ids)}

                # Replace delegate_id and course_code in filter params with delegate_id (as for fk) and course_id (as for fk)
                # Initialize q_obj for current batch request for duplicates check
                for delegate_id, course_code, completed_on in q_filters:
                    delegate = current_delegates.get(delegate_id)
                    course = courses.get(course_code.upper())
                    if delegate and course:
                        current_q = (Q(course_id=course) & Q(delegate_id=delegate) &
                                     Q(completed_on=completed_on) & Q(invalidated_at=None))
                        q_obj = q_obj | current_q if q_obj else current_q

                # Fetch training records filter parameters
                if q_obj:
                    training_records = [(r.delegate_id, r.course_id, r.completed_on) for r in TrainingRecord.objects.filter(q_obj)]
                else:
                    training_records = []

                # Process current batch
                for record in records_batch:

                    key = '-'.join([record.delegate_id, record.course_code, str(record.course_completion_date)])

                    # Get already processed results for duplicates from current batch
                    if key in processed_records and record.duplicate_check & 1:
                        prev_duplicate_check = processed_records.get(key)
                        if prev_duplicate_check + 1 != record.duplicate_check:
                            record.duplicate_check += prev_duplicate_check
                            record.save()
                        if record.duplicate_check and not record.parse_check and not record.validation_check:
                            self.duplicate_records += 1
                        continue
                    increment = 0

                    # Get delegate id for foreign key
                    if record.delegate_id in self.delegate_ids:
                        delegate_id = self.delegate_ids.get(record.delegate_id)
                    else:
                        delegate_id = current_delegates.get(record.delegate_id)
                        self.delegate_ids[record.delegate_id] = delegate_id

                    # Get course id for foreign key
                    course_id = courses.get(record.course_code.upper())

                    # If record is invalid - do not check
                    if not record.parse_check and not record.validation_check:
                        exists = (delegate_id, course_id, record.course_completion_date) in training_records
                        if exists and not record.duplicate_check & 2:
                            increment = 2
                        elif not exists and record.duplicate_check & 2:
                            increment = -2
                        if increment:
                            record.duplicate_check += increment
                            record.save()

                        if record.duplicate_check:
                            self.duplicate_records += 1

                    processed_records[key] = increment
                    if record.is_valid():
                        self.valid_records += 1

                        self.training_records.append(TrainingRecord(
                            delegate_id=delegate_id,
                            course_id=course_id,
                            country_id=self.training_provider.country_id,
                            completed_on=record.course_completion_date,
                            valid_from=record.calculated_valid_from,
                            valid_until=record.calculated_valid_until,
                            created_by_record=record,
                            training_provider_id=self.training_provider.id,
                        ))

            changed_values = (self.instance.valid_records != self.valid_records or
                self.instance.duplicate_records != self.duplicate_records)
            if changed_values:
                # Update counters if changed
                UploadBatch.objects.filter(id=self.instance.id).update(
                    valid_records=self.valid_records,
                    duplicate_records=self.duplicate_records
                )
                raise forms.ValidationError(self.error_messages.get('transaction_failed'))
            if self.training_provider.current_credit_balance() - self.instance.valid_records < 0:
                raise forms.ValidationError(self.error_messages.get('insufficient_credits'))

    def save(self, commit=True):
        if self.instance.status == UploadBatch.DISCARDED:
            self.instance.discarded_by = self.request.user
            self.instance.discarded_at = timezone.now()
            self.instance.updated_by = self.request.user
        else:
            self.instance.purchased_by = self.request.user
            self.instance.purchased_at = self.purchased_at
            self.instance.last_updated_by = self.request.user
            Transaction.objects.create(
                created_by=self.request.user,
                training_provider=self.training_provider,
                transaction_value=-1*self.instance.valid_records,
                upload_batch=self.instance
            )
            TrainingRecord.objects.bulk_create(self.training_records)
            archived_from = self.purchased_at + relativedelta(months=48)
            users = User.objects.delegates().filter(id__in=list(self.delegate_ids.values()))
            users.update(archived_from=archived_from, modified=timezone.now())
            EmailNotification.new_training_records(users, self.training_records)
        return super().save(commit)
