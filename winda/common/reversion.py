from contextlib import contextmanager

from django.db import transaction, router

from reversion.revisions import _push_frame, _current_frame, _pop_frame, _ContextWrapper, _local
from reversion.models import Version


def _save_revision(versions, user=None, comment="", meta=(), date_created=None, using=None):
    from reversion.models import Revision
    # Bail early if there are no objects to save.
    if not versions:
        return
    # Save a new revision.
    revision = Revision(
        date_created=date_created,
        user=user,
        comment=comment,
    )
    # Save the revision.
    revision.save(using=using)
    # Save version models.
    for version in versions:
        version.revision = revision
    # Bulk create insted on save for each as in original one
    Version.objects.bulk_create(versions)
    # Save the meta information.
    for meta_model, meta_fields in meta:
        meta_model._default_manager.db_manager(using=using).create(
            revision=revision,
            **meta_fields
        )


@contextmanager
def _create_revision_context(manage_manually, using):
    _push_frame(manage_manually, using)
    try:
        with transaction.atomic(using=using):
            yield
            # Only save for a db if that's the last stack frame for that db.
            if not any(using in frame.db_versions for frame in _local.stack[:-1]):
                current_frame = _current_frame()
                _save_revision(
                    versions=current_frame.db_versions[using].values(),
                    user=current_frame.user,
                    comment=current_frame.comment,
                    meta=current_frame.meta,
                    date_created=current_frame.date_created,
                    using=using,
                )
    finally:
        _pop_frame()


def bulk_create_revision(manage_manually=False, using=None):
    from reversion.models import Revision
    using = using or router.db_for_write(Revision)
    return _ContextWrapper(_create_revision_context, (manage_manually, using))
