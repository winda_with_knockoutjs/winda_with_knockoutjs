# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from users.factories import (
    UserFactory, TEST_USER_PASSWORD, TrainingProviderAdminFactory, TrainingProviderUserFactory,
    DelegateFactory, OrganizationFactory
)
from trainings.factories import TrainingProviderFactory, TrainingProviderCertificateFactory
from courses.factories import CourseFactory, CourseProviderFactory


class DatabaseAdminChangeHistoryViewsTestCase(TestCase):

    def setUp(self):
        self.user = UserFactory.create(role=get_user_model().DATABASE_ADMIN)
        self.training_provider = TrainingProviderFactory.create()
        self.course = CourseFactory.create()
        self.certificate = TrainingProviderCertificateFactory.create(training_provider=self.training_provider)
        self.accreditation = CourseProviderFactory.create(course=self.course, training_provider=self.training_provider)

    def test_training_provider_history(self):
        url = reverse('database_admin:training_provider_history', kwargs={'pk': self.training_provider.pk})
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_course_history(self):
        url = reverse('database_admin:course_change_history', kwargs={'pk': self.course.pk})
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_certificate_history(self):
        url = reverse('database_admin:certification_change_history', kwargs={
            'parent_pk': self.training_provider.pk,
            'pk': self.certificate.pk
        })
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_accreditation_history(self):
        url = reverse('database_admin:accreditations_change_history', kwargs={
            'parent_pk': self.training_provider.pk,
            'pk': self.accreditation.pk
        })
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_user_history(self):
        url = reverse('database_admin:user_change_history', kwargs={
            'pk': self.user.pk
        })
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        users = [OrganizationFactory.create(), DelegateFactory.create(), TrainingProviderUserFactory.create(),
                TrainingProviderAdminFactory.create()]

        for user in users:
            url = reverse('database_admin:user_change_history', kwargs={
                'pk': user.pk
            })
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
