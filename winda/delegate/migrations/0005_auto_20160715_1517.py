# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0004_auto_20160712_0523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingrecord',
            name='invalidated_by_record',
            field=models.ForeignKey(related_name='invalidated_by_record', null=True, verbose_name='invalidated by record', to='trainings.UploadBatchRecord', blank=True),
        ),
    ]
