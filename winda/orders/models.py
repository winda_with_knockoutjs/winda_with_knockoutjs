# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.models import TimeStampedModel
from orders.managers import ItemManager, TransactionManager
from trainings.models import TrainingProvider, UploadBatch

User = settings.AUTH_USER_MODEL


class Order(TimeStampedModel):
    EUR = 'EUR'

    CURRENCY_CHOICES = (
        (EUR, _('EUR')),
    )

    PAYMENT_SUCCESS_CHOICES = (
        (None, _('Unpaid')),
        (False, _('Unpaid')),
        (True, _('Paid')),
    )

    training_provider = models.ForeignKey(TrainingProvider, verbose_name=_('Training Provider'), related_name='orders')
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')
    currency = models.CharField(_('currency'), max_length=3, default=EUR, choices=CURRENCY_CHOICES)
    quantity = models.PositiveSmallIntegerField(_('quantity'))
    item_code = models.CharField(_('item code'), max_length=10)
    item_name = models.CharField(_('item name'), max_length=30)
    item_description = models.TextField(_('item description'))
    item_unit_price_including_vat = models.PositiveIntegerField(_('item unit price including vat'))
    item_vat_rate = models.DecimalField(decimal_places=2, max_digits=3, verbose_name=_('item vat rate'))
    total_credits = models.PositiveIntegerField(_('total credits'))
    total_price_including_vat = models.PositiveIntegerField(_('total price including vat'))
    payment_success = models.NullBooleanField(_('Payment Success'), choices=PAYMENT_SUCCESS_CHOICES)
    token_link = models.CharField(_('Link token'), max_length=200, blank=True)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    def __str__(self):
        return self.training_provider.name

    @property
    def vat_included(self):
        return self.total_price_including_vat * self.item_vat_rate

    @property
    def get_vat_included_display(self):
        return self.vat_included / 100

    @property
    def paid_at(self):
        transaction = self.transaction_set.all().latest('created')
        return transaction and transaction.created

    @property
    def order_id(self):
        return str(self.pk).zfill(4)

    @property
    def get_item_vat_rate_display(self):
        return self.item_vat_rate * 100

    @property
    def get_item_unit_price_including_vat_display(self):
        return self.item_unit_price_including_vat / 100.0

    @property
    def get_total_price_including_vat_display(self):
        return self.total_price_including_vat / 100.0

    def get_absolute_url(self):
        return reverse_lazy('order_detail', args=(self.training_provider_id, self.pk))


class PaymentStatus(TimeStampedModel):
    APPROVED = '20000'

    callback_id = models.PositiveIntegerField(_('callback id'))
    order = models.ForeignKey(Order, verbose_name=_('order'), related_name='payments')
    accepted = models.BooleanField(_('accepted'))
    test_mode = models.BooleanField(_('test mode'))
    operation_type = models.CharField(_('operation type'), max_length=10)
    amount = models.PositiveIntegerField(_('amount'), null=True)
    pending = models.BooleanField(_('pending'))
    quick_pay_status_code = models.CharField(_('quick pay status code'), max_length=10)
    quick_pay_status_message = models.CharField(_('quick pay status message'), max_length=30)
    acquirer_status_code = models.CharField(_('acquirer status code'), max_length=10, blank=True)
    acquirer_status_message = models.CharField(_('acquirer status message'), max_length=30, blank=True)
    balance = models.BooleanField(_('balance'))

    class Meta:
        verbose_name = _('Payment Status')
        verbose_name_plural = _('Payment Statuses')

    def __str__(self):
        return '{}'.format(self.callback_id)

    @property
    def is_approved(self):
        return self.quick_pay_status_code == self.APPROVED


class Item(models.Model):
    item_code = models.CharField(_('item code'), max_length=10, unique=True)
    item_name = models.CharField(_('item name'), max_length=30)
    item_description = models.TextField(_('item description'))

    objects = ItemManager()

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')

    def __str__(self):
        return self.item_code


class Pricing(TimeStampedModel):
    currency = models.CharField(_('currency'), max_length=3, default=Order.EUR, choices=Order.CURRENCY_CHOICES)
    item = models.ForeignKey(Item, verbose_name=_('item'), related_name='prices')
    valid_from = models.DateField(_('valid from'))
    unit_price_including_vat = models.PositiveIntegerField(_('unit price including vat'))
    vat_rate = models.DecimalField(decimal_places=2, max_digits=3, verbose_name=_('vat rate'),
                                   validators=[MaxValueValidator(0.991), MinValueValidator(0)])
    credits_per_unit = models.PositiveIntegerField(_('credits per unit'))
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        verbose_name = _('Pricing')
        verbose_name_plural = _('Pricing')
        unique_together = (('item', 'valid_from'),)

    def __str__(self):
        return self.item.item_code

    def total_credits(self, quantity):
        return quantity * self.credits_per_unit

    def total_price_including_vat(self, quantity):
        return quantity * self.unit_price_including_vat

    @property
    def get_vat_rate_display(self):
        return self.vat_rate * 100

    @property
    def get_unit_price_including_vat_display(self):
        return self.unit_price_including_vat / 100.0

    def get_total_price_including_vat_display(self, quantity):
        return self.total_price_including_vat(quantity) / 100.0


class Transaction(TimeStampedModel):
    training_provider = models.ForeignKey(TrainingProvider, verbose_name=_('training provider'), related_name='transactions')
    transaction_value = models.IntegerField(_('transaction value'))
    order = models.ForeignKey(Order, verbose_name=_('order'), null=True, blank=True)
    upload_batch = models.ForeignKey(UploadBatch, verbose_name=_('upload batch'), null=True, blank=True)
    reason = models.CharField(_('reason'), max_length=200, blank=True)
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    objects = TransactionManager()

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')

    def __str__(self):
        return self.training_provider.name

    def get_absolute_url(self):
        return reverse_lazy('transaction_detail', args=(self.training_provider_id, self.pk))
