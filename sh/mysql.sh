#!/bin/bash

debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password mysql'
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password mysql'

apt-get update
apt-get install -y mysql-server mysql-server-5.5
apt-get install -y build-dep python-mysqldb
