# -*- coding: utf-8 -*-
import factory
from factory import fuzzy
from django.utils import timezone
from courses.models import Course, CourseProvider
from trainings.models import TrainingProvider
from trainings.factories import TrainingProviderFactory


class CourseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Course

    code = fuzzy.FuzzyText(length=8)
    title = fuzzy.FuzzyText()
    validity_months = fuzzy.FuzzyInteger(10)
    course_type = Course.FULL

class CourseRefresherFactory(CourseFactory):

    course_type = Course.REFRESHER


class CourseProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CourseProvider

    @factory.lazy_attribute
    def course(self):
        course = Course.objects.first()
        if not course:
            course = CourseFactory.create()
        return course

    @factory.lazy_attribute
    def valid_from(self):
        return timezone.now()

    @factory.lazy_attribute
    def valid_until(self):
        return timezone.now()

    @factory.lazy_attribute
    def training_provider(self):
        provider = TrainingProvider.objects.first()
        if not provider:
            provider = TrainingProviderFactory.create()
        return provider
