# -*- coding: utf-8 -*-
import uuid
from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.models import AddressAbstractModel, TimeStampedModel

User = settings.AUTH_USER_MODEL


class TrainingProvider(AddressAbstractModel):
    name = models.CharField(_('name'), max_length=50)
    is_active = models.BooleanField(_('is active'), default=False)
    accepted_by = models.ForeignKey(User, null=True, blank=True)
    accepted_at = models.DateTimeField(null=True, blank=True)
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        verbose_name = _('Training Provider')
        verbose_name_plural = _('Training Providers')

    def __str__(self):
        return self.name

    def current_credit_balance(self):
        return self.transactions.current_credit_balance()

    def draft_upload_count(self):
        return self.training_provider_uploadbatch.filter(status=UploadBatch.DRAFT).count()


class ProviderAbstract(TimeStampedModel):
    training_provider = models.ForeignKey(TrainingProvider, related_name='training_provider_%(class)s')
    valid_from = models.DateField(_('valid from'))
    valid_until = models.DateField(_('valid until'))
    is_deleted = models.BooleanField(_('is deleted'), default=False)
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        abstract = True


def certificate_file_path(instance, filename):
    return '/'.join(map(str, [settings.AWS_STORAGE_PREFIX, 'certificates/training-provider',
                              instance.created.strftime('%Y-%m'), uuid.uuid4(), filename]))


class TrainingProviderCertificate(ProviderAbstract):
    certificate_file = models.FileField(_('certificate file'), upload_to=certificate_file_path, max_length=255)
    certificate_number = models.CharField(_('certificate number'), max_length=40)

    class Meta:
        verbose_name = _('Training Provider Certificate')
        verbose_name_plural = _('Training Provider Certificates')

    def __str__(self):
        return self.certificate_number

    def get_dba_absolute_url(self):
        return reverse('database_admin:training_provider_certification_update',
                       kwargs={'parent_pk': self.training_provider_id, 'pk': self.pk})

    def get_certificate_url(self):
        return self.certificate_file.url if self.certificate_file else '#'


class UploadBatch(TimeStampedModel):

    DRAFT, DISCARDED, PURCHASED, INVALIDATION = 'draft', 'discarded', 'purchased', 'invalidation'

    STATUS_CHOICES = (
        (DRAFT, _('draft')),
        (DISCARDED, _('discarded')),
        (PURCHASED, _('purchased')),
        (INVALIDATION, _('invalidation')),
    )

    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')
    training_provider = models.ForeignKey(TrainingProvider, related_name='training_provider_%(class)s')
    description = models.CharField(_('description'), max_length=100)
    status = models.CharField(_('status'), max_length=20, choices=STATUS_CHOICES)
    purchased_by = models.ForeignKey(User, null=True, blank=True, related_name='purchased_by')
    purchased_at = models.DateTimeField(null=True, blank=True)
    discarded_by = models.ForeignKey(User, null=True, blank=True, related_name='discarded_by')
    discarded_at = models.DateTimeField(null=True, blank=True)
    total_records = models.PositiveSmallIntegerField(_('total records'))
    valid_records = models.PositiveSmallIntegerField(_('valid records'))
    duplicate_records = models.PositiveSmallIntegerField(_('duplicate records'))
    error_records = models.PositiveSmallIntegerField(_('error records'))

    class Meta:
        verbose_name = _('Upload Batch')
        verbose_name_plural = _('Upload Batches')

    def __str__(self):
        return self.description

    def is_draft(self):
        return self.status == UploadBatch.DRAFT

    def get_absolute_url(self):
        return reverse_lazy('training_provider:view-upload', args=(self.training_provider_id, self.pk))


class UploadBatchRecord(TimeStampedModel):
    PARSE_CHECK_CHOICES = (
        (0, _('Parsed okay')),
        (1, _('Missing columns (accept extra)')),
        (2, _('Missing Winda ID (empty)')),
        (4, _('Badly formatted Winda ID (wider than 8 characters)')),
        (8, _('Missing course code (empty)')),
        (16, _('Badly formatted course code (wider than 8 characters)')),
        (32, _('Missing course completion date (empty)')),
        (64, _('Badly formatted course completion date (can\'t convert or wider than 10 characters)')),
        (128, _('Badly formatted previous course valid until date (can\'t convert or wider than 10 characters)')),
    )

    VALIDATION_CHECK_CHOICES = (
        (0, _('Valid')),
        (1, _('Course completion date is in future')),
        (2, _('Unknown course code')),
        (4, _('Not accredited for course')),
        (8, _('Unknown Winda ID')),
        (16, _('Winda ID has been archived')),
        (32, _('Previous course valid until date is required')),
        (64, _('Previous course valid until date is in past')),
        (128, _('No matching previous course training record found'))
    )

    DUPLICATE_CHECK_CHOICES = (
        (0, _('Not a duplicate')),
        (1, _('Duplicate entry within upload (same record appears multiple times in upload)')),
        (2, _('Duplicate of existing training record')),
        (4, _('No matching training record found to invalidate')),
    )

    upload_batch = models.ForeignKey(UploadBatch, verbose_name=_('upload batch'), related_name='upload_batch_records')
    source_row_number = models.PositiveSmallIntegerField(_('source row number'))
    delegate_id = models.CharField(_('delegate id'), max_length=8, blank=True)
    course_code = models.CharField(_('course code'), max_length=8, blank=True)
    course_completion_date = models.DateField(_('course completion date'), null=True, blank=True)
    previous_course_valid_until = models.DateField(_('previous course valid until'), null=True, blank=True)
    calculated_valid_from = models.DateField(_('calculated valid from'), null=True, blank=True)
    calculated_valid_until = models.DateField(_('calculated valid until'), null=True, blank=True)
    parse_check = models.SmallIntegerField(_('parse check'))
    validation_check = models.SmallIntegerField(_('validation check'))
    duplicate_check = models.SmallIntegerField(_('duplicate check'))

    class Meta:
        verbose_name = _('Upload Batch Record')
        verbose_name_plural = _('Upload Batch Records')

    def __str__(self):
        return self.upload_batch.description

    def get_errors(self):
        errors = []
        errors += self.get_field_errors('parse_check', UploadBatchRecord.PARSE_CHECK_CHOICES)
        errors += self.get_field_errors('validation_check', UploadBatchRecord.VALIDATION_CHECK_CHOICES)
        errors += self.get_field_errors('duplicate_check', UploadBatchRecord.DUPLICATE_CHECK_CHOICES)
        return '; '.join(errors)

    def get_field_errors(self, field_name, choices):
        errors = []
        for code, title in choices:
            if code & getattr(self, field_name):
                errors.append(str(title))
        return errors

    def is_valid(self):
        return not self.parse_check and not self.validation_check and not self.duplicate_check
