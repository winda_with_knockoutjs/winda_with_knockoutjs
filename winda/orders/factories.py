# -*- coding: utf-8 -*-
import factory
import random
from factory import fuzzy

from django.utils import timezone
from orders.models import Pricing, Item, Order, Transaction
from trainings.factories import TrainingProviderFactory
from trainings.factories import UploadBatchFactory


class ItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Item

    item_code = fuzzy.FuzzyText(length=10)
    item_name = fuzzy.FuzzyText(length=10)
    item_description = fuzzy.FuzzyText()


class PricingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Pricing

    item = factory.LazyAttribute(lambda a: Item.objects.first() or ItemFactory())
    valid_from = fuzzy.FuzzyDate(start_date=timezone.now().date() - timezone.timedelta(days=30))
    unit_price_including_vat = fuzzy.FuzzyInteger(low=1, high=10)
    vat_rate = fuzzy.FuzzyDecimal(low=0.25, high=1)
    credits_per_unit = fuzzy.FuzzyInteger(low=1, high=10)


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    training_provider = factory.SubFactory(TrainingProviderFactory)
    quantity = fuzzy.FuzzyInteger(low=1, high=10)
    item_code = fuzzy.FuzzyText(length=10)
    item_name = fuzzy.FuzzyText(length=10)
    item_unit_price_including_vat = fuzzy.FuzzyInteger(low=1, high=10)
    item_vat_rate = fuzzy.FuzzyDecimal(low=0.1, high=1)
    total_credits = fuzzy.FuzzyInteger(low=1, high=10)
    total_price_including_vat = fuzzy.FuzzyInteger(low=1, high=10)


class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Transaction

    training_provider = factory.SubFactory(TrainingProviderFactory)
    transaction_value = fuzzy.FuzzyInteger(low=-10, high=100)
    order = factory.SubFactory(OrderFactory)
    upload_batch = factory.SubFactory(UploadBatchFactory)
    reason = fuzzy.FuzzyText()
