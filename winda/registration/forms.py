# -*- coding: utf-8 -*-
from collections import OrderedDict

from allauth.account.adapter import get_adapter
from allauth.account.forms import SetPasswordField, PasswordField, LoginForm as AllAuthLoginForm, \
    ResetPasswordForm as AllAuthResetPasswordForm, ChangePasswordForm as AllAuthChangePasswordForm, \
    ResetPasswordKeyForm as AllAuthResetPasswordKeyForm
from allauth.account.utils import get_login_redirect_url

from django.contrib.auth import get_user_model
from django import forms
from django.conf import settings
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from core.forms import ModelForm
from common.models import EmailNotification
from common.mixins import TrainingProviderCertificateFormMixin
from courses.models import Course
from registration.mixins import SignupFormMixin
from registration.models import Registration, LostPassword
from trainings.models import TrainingProviderCertificate


class DelegateSignupForm(SignupFormMixin):
    registration_type = Registration.DELEGATE

    class Meta:
        model = Registration
        fields = ('first_name', 'last_name', 'email', 'timezone')
        help_texts = {
            'email': _('Please provide your personal private email address. '
                       'Don\'t use a work or shared email address.')
        }
        widgets = {
            'timezone': forms.HiddenInput()
        }


class OrganizationSignupForm(SignupFormMixin):
    registration_type = Registration.ORGANIZATION

    class Meta:
        model = Registration
        fields = ('job_title', 'first_name', 'last_name', 'email', 'phone', 'organization_name',
                  'address_1', 'address_2', 'city', 'postal_code', 'country', 'timezone')
        required_fields = ('job_title', 'first_name', 'last_name', 'email', 'phone', 'organization_name',
                           'address_1', 'city', 'postal_code', 'country')
        labels = {
            'email': _('work email address')
        }
        help_texts = {
            'email': _('Please provide your work email address. '
                       'Don\'t use a personal or shared email address.')
        }
        widgets = {
            'timezone': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.required_fields:
            self.fields[field].required = True

    def clean_email(self):
        email = super().clean_email()
        if email.split('@')[1] in settings.ORGANIZATION_EMAIL_DOMAIN_BLACKLIST:
            raise forms.ValidationError(self.error_messages['email_in_blacklist'])
        return email


class TrainingProviderSignupForm(SignupFormMixin):
    registration_type = Registration.TRAINING_PROVIDER_ADMIN
    training_provider = forms.CharField(label=_('training provider name'), max_length=50)
    agreement = forms.BooleanField()

    class Meta:
        model = Registration
        fields = ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code', 'country', 'timezone')
        required_fields = ('first_name', 'last_name', 'email', 'address_1', 'city', 'postal_code', 'country')
        labels = {
            'email': _('work email address')
        }
        help_texts = {
            'email': _('Please provide your work email address. '
                       'Don\'t use a personal or shared email address.')
        }
        widgets = {
            'timezone': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.required_fields:
            self.fields[field].required = True
        key_order = ['first_name', 'last_name', 'email', 'training_provider', 'address_1', 'address_2',
                     'city', 'postal_code', 'country', 'agreement', 'terms', 'policy']
        self.fields = OrderedDict([(key, self.fields.pop(key)) for key in key_order])


class CertificateForm(TrainingProviderCertificateFormMixin, ModelForm):
    courses = forms.ModelMultipleChoiceField(queryset=Course.objects.all(), label=_('accredited courses'),
                                             widget=forms.CheckboxSelectMultiple)
    valid_from = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                 input_formats=settings.DATE_INPUT_FORMATS,
                                 label=_('Valid From'))
    valid_until = forms.DateField(widget=forms.DateInput(format=settings.FORMS_DATE_FORMAT),
                                  input_formats=settings.DATE_INPUT_FORMATS,
                                  label=_('Valid Until'))

    class Meta(TrainingProviderCertificateFormMixin.Meta):
        model = TrainingProviderCertificate
        fields = ('certificate_file', 'certificate_number', 'valid_from', 'valid_until')


CertificateFormSet = modelformset_factory(TrainingProviderCertificate, form=CertificateForm, extra=0, max_num=20,
                                          min_num=1, validate_min=True, validate_max=True)


class SetPasswordForm(forms.Form):
    password1 = SetPasswordField(label=_('Password'))
    password2 = PasswordField(label=_('Confirm Password'))

    def clean(self):
        super().clean()
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data and \
           self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_('Passwords do not match.'))
        return self.cleaned_data


class LoginForm(AllAuthLoginForm):
    error_messages = {
        'account_inactive': _("This account is inactive."),
        'account_expires': _('This account has expired.'),
        'account_archived': _('This account has been archived.'),
        'training_provider_inactive': _('The training provider associated with this account is inactive.'),

        'email_password_mismatch':
        _('Invalid email address, username and / or password combination; please try again.'),

        'username_password_mismatch':
        _('Invalid email address, username and / or password combination; please try again.'),

        'username_email_password_mismatch':
        _('Invalid email address, username and / or password combination; please try again.'),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('remember', None)
        self.fields['login'].label = _('Email Address')
        self.fields['login'].widget.attrs['size'] = 255

    def login(self, request, redirect_url=None):
        adapter = get_adapter(request)
        if not self.user.is_active:
            return adapter.respond_user_inactive(request, self.user)
        adapter.login(request, self.user)
        response = HttpResponseRedirect(
            get_login_redirect_url(request, redirect_url))
        request.session.set_expiry(settings.SESSION_COOKIE_AGE)
        return response

    def clean(self):
        cd = super().clean()
        if self.errors:
            return cd
        adapter = get_adapter(self.request)
        can_login = adapter.can_login(self.user)
        if can_login is True:
            return cd
        raise forms.ValidationError(self.error_messages[can_login])


class ForgotPassword(AllAuthResetPasswordForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['size'] = 255

    def clean_email(self):
        value = self.cleaned_data['email']
        value = get_adapter().clean_email(value)
        self.users = get_user_model().objects.filter(email__iexact=value)
        if not self.users.exists():
            raise forms.ValidationError(_('Email address not registered in system.'))
        return value

    def save(self, request, **kwargs):
        email = self.cleaned_data['email']
        user = self.users.first()
        lost_password = LostPassword.objects.create(user=user)
        EmailNotification.password_reset(request, user, lost_password)
        request.session['reset_password_email'] = email
        return email


class ChangePasswordForm(AllAuthChangePasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password2'].label = _('Confirm password')

    def clean_oldpassword(self):
        if not self.user.check_password(self.cleaned_data.get("oldpassword")):
            raise forms.ValidationError(_("Current password is incorrect."))
        return self.cleaned_data["oldpassword"]

    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_('Passwords do not match.'))
        return self.cleaned_data["password2"]


class ResetPasswordKeyForm(AllAuthResetPasswordKeyForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password2'].label = _('Confirm password')

    def clean_password2(self):
        try:
            super().clean_password2()
        except forms.ValidationError:
            raise forms.ValidationError(_('Passwords do not match.'))
