# -*- coding: utf-8 -*-
from boto.ses.exceptions import SESAddressNotVerifiedError
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone

from common.models import EmailNotification


class Command(BaseCommand):
    help = 'Send asynchronous email notifications'

    def handle(self, *args, **options):
        for notification in EmailNotification.objects\
                .filter(ses_message_id='', archived_at__isnull=True, posted_at__isnull=True,
                        recipient__archived_at__isnull=True, recipient__expires_at__gte=timezone.now(),
                        recipient__is_active=True,
                        recipient__email_status__in=[get_user_model().UNKNOWN, get_user_model().DELIVERY])\
                .select_related('recipient'):
            try:
                self.stdout.write('{} {}'.format(notification.pk, notification.subject))
                msg = notification.send()
                notification.posted_at = timezone.now()
                notification.ses_message_id = msg.extra_headers['message_id']
                notification.save()
                self.stdout.write('{} {}'.format(notification.pk, notification.ses_message_id))
            except SESAddressNotVerifiedError as e:
                self.stdout.write('{} {} {}'.format(notification.pk, notification.ses_message_id, e))
