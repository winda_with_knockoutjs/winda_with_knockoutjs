# -*- coding: utf-8 -*-
import json
from urllib.parse import urlencode
from django.contrib.auth import get_user_model

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils import timezone

from delegate.models import Search, TrainingRecord
from users.factories import TEST_USER_PASSWORD, OrganizationFactory, DelegateFactory, TrainingProviderAdminFactory, \
    TrainingProviderUserFactory, DatabaseAdminFactory
from delegate.factories import TrainingRecordFactory


class GridSearchTestCase(TestCase):

    def setUp(self):
        self.user = OrganizationFactory()
        self.url = reverse('organization:search')
        self.result_url = reverse('organization:grid_search_results')

        self.training_records = TrainingRecordFactory.create_batch(
            10, valid_until=timezone.now() + timezone.timedelta(days=2))

        self.expire_training_records = TrainingRecordFactory.create_batch(
            4, valid_until=timezone.now() - timezone.timedelta(days=2))

        self.invalidated_training_records = TrainingRecordFactory.create_batch(
            6, valid_until=timezone.now() + timezone.timedelta(days=2), invalidated_at=timezone.now())

        self.other_training_records = TrainingRecordFactory.create_batch(
            8, valid_until=timezone.now() + timezone.timedelta(days=2))

        self.search_delegate_id = list(TrainingRecord.objects.all().values_list('delegate__delegate_id', flat=True))
        self.search_delegate_id.extend(['hello', 'delegate'])
        self.search_delegate_id = [item.lower() for item in self.search_delegate_id]

        session = self.client.session
        session['search_by_delegate_id'] = True
        session['search_delegate_id'] = self.search_delegate_id
        session.save()
        self.result_count = len(self.search_delegate_id)

    def test_access(self):
        """
        Only organization, training provider and training provider admin have access to search by delegate id
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

        for user in [self.user, TrainingProviderAdminFactory(), TrainingProviderUserFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertContains(response, 'Search')

        for user in [DatabaseAdminFactory(), DelegateFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 404)

    def test_search_result_without_session(self):
        """
        Organization go to search result and get redirect to search page
        """
        response = self.client.get(self.result_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        session = self.client.session
        del session['search_by_delegate_id']
        del session['search_delegate_id']
        session.save()

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.url)

    def test_search_result_with_session(self):
        """
        Organization get search result
        """
        changed_ids = [tr.delegate_id for tr in self.training_records]
        changed_archived_from = [user.archived_from for user in get_user_model().objects.filter(pk__in=changed_ids).order_by('pk')]
        ids = [tr.delegate_id for tr in self.expire_training_records]
        ids.extend([tr.delegate_id for tr in self.invalidated_training_records])
        ids.extend([tr.delegate_id for tr in self.other_training_records])
        archived_from = [user.archived_from for user in get_user_model().objects.filter(pk__in=ids).order_by('pk')]

        response = self.client.get(self.result_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))

        self.assertEqual(result['recordsTotal'], self.result_count)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], self.result_count)

        self.assertEqual(Search.objects.count(), 18)
        new_changed_archived_from = [user.archived_from for user in get_user_model().objects.filter(pk__in=changed_ids).order_by('pk')]
        new_archived_from = [user.archived_from for user in get_user_model().objects.filter(pk__in=ids).order_by('pk')]
        self.assertNotEqual(changed_archived_from, new_changed_archived_from)
        self.assertNotEqual(archived_from, new_archived_from)

    def test_invalid_sort(self):
        """
        Organization get users with wrong sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '5',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'some text',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_valid_sort(self):
        """
        Organization get users with correct sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        url = '{}?{}'.format(self.result_url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], self.result_count)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], len(self.search_delegate_id))

    def test_wrong_data(self):
        """
        Organization get users wrong parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.result_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
