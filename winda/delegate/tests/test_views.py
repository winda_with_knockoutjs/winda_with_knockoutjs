# -*- coding: utf-8 -*-
import json
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from users.factories import UserFactory, TEST_USER_PASSWORD
from delegate.factories import TrainingRecordFactory


class DelegateViewsTestCase(TestCase):

    def setUp(self):
        self.url = reverse('home')
        self.user_count = get_user_model().objects.count()
        self.user = UserFactory.create(role=get_user_model().DELEGATE, delegate_id='hello')
        self.home_url = reverse('delegate:home', kwargs={'delegate_id': self.user.delegate_id})
        self.profile_url = reverse('delegate:profile', kwargs={'delegate_id': self.user.delegate_id})

    def test_home(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertRedirects(response, self.home_url)

    def test_profile(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.profile_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.email)

        # Other user tries to access profile
        other_user = UserFactory.create(role=get_user_model().DELEGATE, delegate_id='world')
        self.client.login(username=other_user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.profile_url)
        self.assertEqual(response.status_code, 404)

        data = {
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'delegate_id': self.user.delegate_id,
            'email': 'hello@example.com',
            'timezone': 'Europe/London'
        }

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.profile_url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.home_url)


class DelegateTrainingRecordTest(TestCase):

    def setUp(self):
        self.user_count = get_user_model().objects.count()
        self.user = UserFactory.create(role=get_user_model().DELEGATE, delegate_id='hello')
        self.list_url = reverse('delegate:training-record', kwargs={'delegate_id': self.user.delegate_id})

    def test_training_record_list(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Training Record')

    def test_filter(self):
        TrainingRecordFactory.create_batch(10, delegate=self.user)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_sort(self):
        TrainingRecordFactory.create_batch(10, delegate=self.user)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        TrainingRecordFactory.create_batch(10, delegate=self.user)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'hello',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
