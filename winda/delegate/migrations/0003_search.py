# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('delegate', '0002_delegatecertificate'),
    ]

    operations = [
        migrations.CreateModel(
            name='Search',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(related_name='search_created_by', to=settings.AUTH_USER_MODEL)),
                ('delegate', models.ForeignKey(verbose_name='delegate', to=settings.AUTH_USER_MODEL, related_name='search')),
            ],
            options={
                'verbose_name': 'Search',
                'verbose_name_plural': 'Searches',
            },
        ),
    ]
