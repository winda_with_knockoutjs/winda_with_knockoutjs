# -*- coding: utf-8 -*-
from django.conf import settings
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy, reverse
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.views.generic import DetailView, FormView, ListView
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import weasyprint
from skd_tools.mixins import ActiveTabMixin
from braces.views import LoginRequiredMixin, JSONResponseMixin

from core.views import UpdateView
from delegate.forms import DelegateProfileForm, TrainingRecordFilterForm
from delegate.models import TrainingRecord, DelegateCertificate, Search
from trainings.models import TrainingProvider
from delegate.forms import SearchForm, SearchFilterForm
from common.mixins import DelegateAccessMixin, FilterMixin


class DelegateMixin(DelegateAccessMixin):

    slug_field = 'delegate_id'
    slug_url_kwarg = 'delegate_id'
    only_owner = True

    def get_queryset(self):
        return get_user_model().objects.filter(role=get_user_model().DELEGATE)


class DelegateHomeView(DelegateMixin, DetailView):

    template_name = 'delegate/home.html'


class DelegateProfileView(DelegateMixin, ActiveTabMixin, UpdateView):
    active_tab = 'profile'
    template_name = 'delegate/profile.html'
    form_class = DelegateProfileForm

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('Your profile was successfully saved.'))
        return reverse_lazy('delegate:home', kwargs={'delegate_id': self.object.delegate_id})


class DelegateTrainingRecordsListView(DelegateMixin, FilterMixin, ActiveTabMixin, ListView):

    active_tab = 'training_record'
    template_name = 'delegate/training_records_list.html'
    model = TrainingRecord
    only_owner = False
    form_class = TrainingRecordFilterForm

    def get_queryset(self):
        return self.model.objects.filter(delegate_id=self.request.user.id)

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'course__title': obj.course.title,
            'course__code': obj.course.code,
            'training_provider__name': obj.training_provider.name,
            'country__name': obj.country.name,
            'completed_on': obj.completed_on.strftime(settings.TEMPLATE_DATE_FORMAT),
            'valid_from': obj.valid_from.strftime(settings.TEMPLATE_DATE_FORMAT),
            'valid_until': obj.valid_until.strftime(settings.TEMPLATE_DATE_FORMAT),
            'status': obj.get_status_display()
        } for obj in object_list]


class DelegateGenerateCertificateView(LoginRequiredMixin, JSONResponseMixin, DetailView):

    slug_field = 'delegate_id'
    slug_url_kwarg = 'delegate_id'
    template_name = 'delegate/generate_certificate.html'
    model = get_user_model()

    def render_to_response(self, context, **response_kwargs):
        delegate = self.object
        self.training_records = delegate.delegate_training_records.all()
        # Get Current Certificate
        try:
            certificate = delegate.delegate_certificates.latest('created')
        except:
            certificate = None

        if certificate:
            # Check if certificate has been created later than latest created training record
            created, certificate = self.check_latest(certificate, self.training_records, delegate, 'created')
            if not created:
                # If certificate has not been re-generated, check if certificate has been created
                # later than latest invalidated training record
                created, certificate = self.check_latest(certificate, self.training_records, delegate, 'invalidated_at')
            # Check if certificate was created before expiration of training record
            if not created and self.training_records.filter(Q(valid_until__lt=timezone.now().date()) &
                                                            Q(valid_until__gt=certificate.created.date())).exists():
                certificate = self.generate_certificate(delegate, self.training_records, self.request.user)
        else:
            # If certificate does not exist, generate it
            certificate = self.generate_certificate(delegate, self.training_records, self.request.user)

        if certificate:
            # If certificate was generated or it already exists - redirect to it's url
            if self.request.is_ajax():
                data = {'url': reverse('delegate-certificate', kwargs={'certificate_code': str(certificate.certificate_code)})}
                return self.render_json_response(data)
            else:
                return redirect('delegate-certificate', certificate_code=str(certificate.certificate_code))
        # If certificate was not generated - return page with error
        context['training_records'] = self.training_records
        return super().render_to_response(context, **response_kwargs)

    def check_latest(self, certificate, records, delegate, field_name):
        created = False
        latest_record = None
        try:
            latest_record = records.latest(field_name)
        except TrainingRecord.DoesNotExist:
            return created, certificate
        finally:
            value = getattr(latest_record, field_name, None)
            if value and value > certificate.created:
                certificate = self.generate_certificate(delegate, records, self.request.user)
                created = True
        return created, certificate

    def generate_certificate(self, delegate, records, created_by=None):
        # get current records, valid_until should be greater than current timestamp
        records = records.filter(valid_until__gte=timezone.now(), invalidated_at__isnull=True).order_by('completed_on')
        # If not records found - return
        if not records.count():
            self.training_records = records
            return None
        # Get related providers
        training_providers_ids = records.values_list('training_provider_id', flat=True)
        training_providers = TrainingProvider.objects.filter(id__in=training_providers_ids).distinct()
        # Create new certificate object
        certificate = DelegateCertificate.objects.create(delegate=delegate, created_by=created_by)
        # Generate PDF context data
        domain = self.request.build_absolute_uri('/')
        certificate_url = ''.join([domain.rstrip('/'), reverse('delegate-certificate',
                                                   kwargs={'certificate_code': certificate.certificate_code})])
        context = {
            'training_providers': training_providers,
            'training_records': records,
            'delegate': delegate,
            'certificate': certificate,
            'certificate_url': certificate_url,
            'domain': domain
        }
        # Render template with data and then create PDF file
        html = render_to_string('delegate/certificate_template.html', context=context)
        pdf = weasyprint.HTML(string=html, base_url=settings.WEASYPRINT_BASE_URL).write_pdf()
        filename = '{}.pdf'.format(certificate.certificate_code)
        # Save pdf file to created certificate
        certificate.certificate_file.save(filename, ContentFile(pdf))
        return certificate


class DelegateCertificateView(DetailView):

    slug_field = 'certificate_code'
    slug_url_kwarg = 'certificate_code'
    model = DelegateCertificate

    def render_to_response(self, context, **response_kwargs):
        if self.object and self.object.certificate_file:
            filename = self.object.certificate_file.name.split('/')[-1]
            response = HttpResponse(self.object.certificate_file, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
            return response
        raise Http404


class DelegateSearchView(DelegateAccessMixin, ActiveTabMixin, FormView):
    template_name = 'delegate/search.html'
    active_tab = 'search'
    form_class = SearchForm
    only_owner = False

    def form_valid(self, form):
        self.delegate_id = form.cleaned_data['delegate_id']
        self.request.session['search_by_delegate_id'] = True
        self.request.session['search_delegate_id'] = self.delegate_id
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('delegate:search_result', args=(self.delegate_id,))


class DelegateSearchResultView(DelegateAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    template_name = 'delegate/search_result.html'
    active_tab = 'search'
    model = TrainingRecord
    form_class = SearchFilterForm
    only_owner = False

    def dispatch(self, request, *args, **kwargs):
        if self.request.session.get('search_delegate_id') != self.kwargs['delegate_id']:
            return HttpResponseRedirect(reverse_lazy('delegate:search'))
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if not self.request.is_ajax():
            return self.model.objects.none()
        delegate_id = self.kwargs['delegate_id']
        today = timezone.now()
        try:
            user = get_user_model().objects.get(delegate_id=delegate_id)
        except get_user_model().DoesNotExist:
            return TrainingRecord.objects.none()

        trainings = TrainingRecord.objects.current_training().filter(delegate_id=user.pk)\
            .select_related('delegate', 'course', 'training_provider', 'country')

        if self.request.session.get('search_by_delegate_id'):
            Search.objects.create(created_by=self.request.user, delegate=user)
            user.archived_from = today + relativedelta(months=48)
            user.save()
            del self.request.session['search_by_delegate_id']
        return super().get_filtered_queryset(trainings)

    def get_filtered_queryset(self, queryset):
        return super().get_filtered_queryset(queryset).order_by('completed_on')

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'delegate__delegate_id': '<a href="{}">{}</a>'.format(
                reverse('delegate:generate-certificate', args=(obj.delegate.delegate_id,)),
                obj.delegate.delegate_id),
            'delegate__first_name': obj.delegate.first_name,
            'delegate__last_name': obj.delegate.last_name,
            'course__title': obj.course.title,
            'course__code': obj.course.code,
            'training_provider__name': obj.training_provider.name,
            'country': obj.country.name,
            'completed_on': obj.completed_on.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.completed_on else '',
            'valid_from': obj.valid_from.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_from else '',
            'valid_until': obj.valid_until.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_until else '',
            'status': obj.get_status_display()
        } for obj in object_list]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['delegate_id'] = self.kwargs['delegate_id']
        return context
