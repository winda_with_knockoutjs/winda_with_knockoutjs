# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0006_auto_20160707_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadbatchrecord',
            name='duplicate_check',
            field=models.SmallIntegerField(verbose_name='duplicate check'),
        ),
        migrations.AlterField(
            model_name='uploadbatchrecord',
            name='parse_check',
            field=models.SmallIntegerField(verbose_name='parse check'),
        ),
        migrations.AlterField(
            model_name='uploadbatchrecord',
            name='validation_check',
            field=models.SmallIntegerField(verbose_name='validation check'),
        ),
    ]
