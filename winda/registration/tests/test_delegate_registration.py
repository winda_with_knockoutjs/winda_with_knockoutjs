# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import lorem_ipsum, timezone
from common.models import DelegateSequence

from registration.factories import RegistrationFactory, DelegateSequenceFactory
from registration.models import Registration
from users.factories import UserFactory


class DelegateRegistrationTestCase(TestCase):

    def setUp(self):
        self.url = reverse('delegate_signup')
        self.user_count = get_user_model().objects.count()
        DelegateSequenceFactory()

    def test_registration_page(self):
        """
        Anonymous user successfully get registration page
        """
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)

    def test_home_page(self):
        """
        Anonymous user successfully get home page
        """
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_registration(self):
        """
        Anonymous user successfully register
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Delegate Registration')

        data = {
            'first_name': lorem_ipsum.words(1, False),
            'last_name': lorem_ipsum.words(1, False),
            'email': '{}@example.com'.format(lorem_ipsum.words(1, False)),
            'timezone': 'Africa/Addis_Ababa',
            'terms': True,
            'policy': True
        }
        response = self.client.post(self.url, data)
        self.assertRedirects(response, reverse('account_email_verification_sent', args=('delegate', )))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA Delegate Account Activation')

        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

        reg = Registration.objects.first()
        self.assertEqual(reg.first_name, data['first_name'])
        self.assertEqual(reg.last_name, data['last_name'])
        self.assertEqual(reg.email, data['email'])
        self.assertEqual(reg.registration_type, Registration.DELEGATE)
        self.assertTrue(reg.activation_code)
        self.assertEqual(reg.expires_at.strftime(settings.DATETIME_FORMAT),
                        (timezone.now() + timezone.timedelta(hours=settings.REGISTRATION_EXPIRY_HOURS))
                         .strftime(settings.DATETIME_FORMAT))
        self.assertEqual(reg.archived_from.date(), timezone.now().date() + relativedelta(months=12))
        self.assertIsNone(reg.claimed_at)

    def test_registration_invalid_data(self):
        """
        Anonymous user submit blank form and get error messages
        """
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This field is required.', 5)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 0)

    def test_registration_email_already_used(self):
        """
        Anonymous user submit form with existing email and get error messages
        """
        user = UserFactory()
        data = {
            'first_name': lorem_ipsum.words(1, False),
            'last_name': lorem_ipsum.words(1, False),
            'email': user.email,
            'timezone': 'Africa/Addis_Ababa',
            'terms': True,
            'policy': True
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This email address is already in use.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count + 1)
        self.assertEqual(Registration.objects.count(), 0)

    def test_confirm_email_get(self):
        """
        Anonymous user successfully get to activation link.
        """
        reg = RegistrationFactory()
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Delegate Account Activation')
        self.assertContains(response, 'Activate and Login', 1)

    def test_confirm_email_post(self):
        """
        Anonymous user successfully set password and login
        """
        reg = RegistrationFactory()
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertRedirects(response, reverse('home'), target_status_code=302)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA Winda ID')

        self.assertEqual(get_user_model().objects.count(), self.user_count + 1)
        self.assertEqual(Registration.objects.count(), 1)

        reg.refresh_from_db()
        self.assertIsNotNone(reg.claimed_at)

        user = get_user_model().objects.last()
        for field in ('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'postal_code', 'country'):
            self.assertEqual(getattr(reg, field), getattr(user, field))
        self.assertEqual(reg.registration_type, user.role)
        self.assertEqual(user.expires_at, timezone.datetime(2099, 12, 31, tzinfo=timezone.UTC()))
        self.assertEqual(user.last_updated_by.pk, settings.SYSTEM_USER_ID)
        self.assertEqual(user.created_by.pk, settings.SYSTEM_USER_ID)
        self.assertIsNotNone(user.delegate_id)
        self.assertEqual(user.archived_from.date(), timezone.now().date() + relativedelta(months=48))
        self.assertIsNotNone(reg.claimed_at)
        self.assertTrue(user.is_authenticated())

    def test_confirm_email_delegate_id_fail(self):
        """
        Anonymous user submit form and get error messages
        """
        DelegateSequence.objects.all().delete()
        reg = RegistrationFactory()
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Winda ID cannot be generated at this time, please try again later.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_email_already_used(self):
        """
        Anonymous user try to confirm existing email and get error
        """
        user = UserFactory()
        self.user_count = get_user_model().objects.count()
        reg = RegistrationFactory(email=user.email)
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Account has already been activated. Please login to your account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_wrong_password(self):
        """
        Anonymous user try to confirm his email and set wrong password
        """
        reg = RegistrationFactory()
        data = {
            'password1': '12345678',
            'password2': '12345679',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Passwords do not match.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_account_is_claimed(self):
        """
        Anonymous user try to confirm claimed email
        """
        reg = RegistrationFactory(claimed_at=timezone.now())
        data = {
            'password1': '12345678',
            'password2': '12345678',
        }
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Account has already been activated. Please login to your account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)

    def test_confirmation_link_is_expired(self):
        """
        Anonymous user try to confirm expired email
        """
        reg = RegistrationFactory(expires_at=timezone.now() - timezone.timedelta(minutes=10))
        url = reverse("account_confirm_email",
                      args=[reg.REGISTRATION_URLS[reg.registration_type], reg.activation_code])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Activation code has expired. Please register for a new account.', 1)
        self.assertEqual(get_user_model().objects.count(), self.user_count)
        self.assertEqual(Registration.objects.count(), 1)
