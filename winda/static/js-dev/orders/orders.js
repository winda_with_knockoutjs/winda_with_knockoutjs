$(document).ready(function(){
    var $items = $('.order-item'),
        $quantity = $('.order-quantity');

    $.fn.setOrderData = function() {
        var self = $(this).find("option:selected") || $(this).find("option")[0],
            quantity = parseInt($quantity.val()) || 0,
            currency = self.attr('data-currency');
        $('.order-description').text(self.attr('data-description'));
        $('.order-credit').text((parseFloat(self.attr('data-credits_per_unit')) * quantity).toFixed(0));
        $('.order-vat-rate').text(parseFloat(self.attr('data-vat_rate')) * 100);
        $('.order-unit-price-including-vat').text(currency + ' ' + parseFloat(self.attr('data-unit_price_including_vat')).toFixed(2));
        $('.order-total-price').text(currency + ' ' + (parseFloat(self.attr('data-unit_price_including_vat')) * quantity).toFixed(2));
        return $(this);
    };

    if ($items.length && $quantity.length) {
        $items.setOrderData().change(function () {
            $items.setOrderData();
        });

        $quantity.change(function () {
            $items.setOrderData();
        });
    }

    $('table#order_detail').DataTable({
        "iDisplayLength": 20,
        "scrollCollapse": true,
        "bLengthChange": false,
        "bInfo": false,
        "scrollY": true,
        "scrollX": false,
        "paging": false,
        "searching": false,
        "ordering": false,
        "order": [[ 1, "desc" ]],
        'columnDefs': [{
            'targets': 0,
            "width": "5%",
            'searchable':false,
            'orderable':false,
            'className': "control"
        }]
    });

    initializeDataTable('table#orders', {
        "order": [[1, 'desc']],
        "columns": [
            { "data": "", "className": "control" },
            { "data": "id"},
            { "data": "created" },
            { "data": "created_by" },
            { "data": "payment_success" },
            { "data": "total_credits"},
            { "data": "total_price_including_vat"}
        ]
    });
});
