# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20160705_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='can_buy_credit',
            field=models.BooleanField(default=False, verbose_name='can buy credits'),
        ),
        migrations.AlterField(
            model_name='user',
            name='can_upload',
            field=models.BooleanField(default=False, verbose_name='can upload files'),
        ),
        migrations.AlterField(
            model_name='user',
            name='can_use_credit',
            field=models.BooleanField(default=False, verbose_name='can use credits'),
        ),
    ]
