# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_auto_20160707_1027'),
        ('courses', '0003_auto_20160704_1241'),
        ('trainings', '0006_auto_20160707_1027'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('completed_on', models.DateField(verbose_name='Completed On')),
                ('valid_from', models.DateField(verbose_name='valid from')),
                ('valid_until', models.DateField(verbose_name='valid until')),
                ('invalidated_at', models.DateTimeField(blank=True, null=True, verbose_name='invalidated at')),
                ('country', models.ForeignKey(to='common.Country', verbose_name='country')),
                ('course', models.ForeignKey(related_name='training_records', to='courses.Course', verbose_name='course')),
                ('created_by', models.ForeignKey(related_name='trainingrecord_created_by', default=1, to=settings.AUTH_USER_MODEL)),
                ('created_by_record', models.ForeignKey(related_name='created_by_record', to='trainings.UploadBatchRecord', verbose_name='created by record')),
                ('delegate', models.ForeignKey(related_name='delegate_training_records', to=settings.AUTH_USER_MODEL, verbose_name='delegate')),
                ('invalidated_by', models.ForeignKey(related_name='trainingrecord_invalidated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
                ('invalidated_by_record', models.ForeignKey(related_name='invalidated_by_record', to='trainings.UploadBatchRecord', verbose_name='invalidated by record')),
                ('last_updated_by', models.ForeignKey(related_name='trainingrecord_last_updated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
                ('training_provider', models.ForeignKey(related_name='training_provider_training_records', to=settings.AUTH_USER_MODEL, verbose_name='training provider')),
            ],
            options={
                'verbose_name_plural': 'Training Records',
                'verbose_name': 'Training Record',
            },
        ),
    ]
