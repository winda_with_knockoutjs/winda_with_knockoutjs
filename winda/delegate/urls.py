# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from .views import DelegateHomeView, DelegateProfileView, DelegateSearchView, DelegateSearchResultView, \
    DelegateGenerateCertificateView, DelegateTrainingRecordsListView

urlpatterns = [
    url(r'^search/$', DelegateSearchView.as_view(), name='search'),
    url(r'^search/(?P<delegate_id>[\w ]+)/$', DelegateSearchResultView.as_view(), name='search_result'),

    url(r'^(?P<delegate_id>[\w]+)/', include([
        url(r'^$', DelegateHomeView.as_view(), name='home'),
        url(r'^profile/$', DelegateProfileView.as_view(), name='profile'),
        url(r'^training-record/$', DelegateTrainingRecordsListView.as_view(), name='training-record'),
        url(r'^certificate/$', DelegateGenerateCertificateView.as_view(), name='generate-certificate'),
    ])),
]
