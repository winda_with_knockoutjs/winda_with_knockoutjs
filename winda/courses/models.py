# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.models import TimeStampedModel
from trainings.models import ProviderAbstract

User = settings.AUTH_USER_MODEL


class Course(TimeStampedModel):
    FULL, REFRESHER = 'full', 'refresher'
    COURSE_TYPE_CHOICES = ((FULL, _('full')), (REFRESHER, _('refresher')))

    code = models.CharField(_('code'), max_length=8, unique=True)  # trigger to upper case was added in initial migration
    title = models.CharField(_('title'), max_length=50)
    course_type = models.CharField(_('course type'), max_length=10, choices=COURSE_TYPE_CHOICES, default=FULL)
    refresher_for_course = models.ForeignKey('self', verbose_name=_('refresher for course'), null=True, blank=True)
    require_existing_training_record = models.BooleanField(default=False)
    validity_months = models.PositiveIntegerField(_('validity months'))
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')
        ordering = ('title',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('database_admin:course_edit', kwargs={'pk': self.pk})


class CourseProvider(ProviderAbstract):
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='course_providers')

    class Meta:
        verbose_name = _('Course Provider')
        verbose_name_plural = _('Course Providers')

    def __str__(self):
        return self.course.title

    def get_dba_absolute_url(self):
        return reverse('database_admin:training_provider_accreditations_update',
                       kwargs={'parent_pk': self.training_provider_id, 'pk': self.pk})
