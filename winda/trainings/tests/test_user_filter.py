# -*- coding: utf-8 -*-
import json

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.http import urlencode

from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory


class UserFilterTestCase(TestCase):

    def setUp(self):
        self.user = TrainingProviderAdminFactory()
        self.url = reverse('training_provider:users', args=(self.user.pk,))
        self.users = TrainingProviderAdminFactory.create_batch(9, training_provider=self.user.training_provider)

    def test_get_template(self):
        """
        Training Provider Admin get users lists page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '{} - Users'.format(self.user.training_provider))

    def test_filter(self):
        """
        Training Provider Admin filter users
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        """
        Training Provider Admin get users with wrong sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 20,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'some text',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_valid_sort(self):
        """
        Training Provider Admin get users with correct sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '4',
            'sort_asc': '-',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_wrong_data(self):
        """
        Training Provider Admin get users wrong parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
