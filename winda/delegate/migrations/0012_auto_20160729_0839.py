# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0011_auto_20160728_1342'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='trainingrecord',
            index_together=set([('delegate', 'course', 'completed_on', 'invalidated_at')]),
        ),
    ]
