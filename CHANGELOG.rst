v0.3.9
======

- Fix: Fix search

*commit* `36516be1 <https://git.steelkiwi.com/web/Winda/commit/36516be1f7a38a97ab56d8d8f431acaad771c2e8>`_


v0.3.8
======

- Fix: Not activated delegate user can't use activation link again if previously there were no free WINDA ID (WINDA-223)

*commit* `afa2f370 <https://git.steelkiwi.com/web/Winda/commit/afa2f3707f9d2aaa0272239569c56a99a6a1e4a6>`_

v0.3.7
======

- Implement: Grid view on search screen for Organisation, TP admin and TP user (WINDA-217)
- Implement frontend: Safari layout issues observed on iPad (WINDA-218)
- Implement frontend: Grid view on search screen for Organisation, TP admin and TP user (WINDA-217)

*commit* `476100c9 <https://git.steelkiwi.com/web/Winda/commit/476100c9c10e116f4af3f85757dd036bbe8e44ef>`_


v0.3.6
======

- Implement: Edit template for Google Analytics
- Implement frontend: Safari layout issues observed on iPad (WINDA-218)
- Implement frontend: Layout issues observed on mobile device Android 5.1, Chrome (WINDA-220)

*commit* `82cb362e <https://git.steelkiwi.com/web/Winda/commit/82cb362ef6c8559f7b3dc47ea787cb94d0af4cfb>`_


v0.3.5
======

- Implement: - Notifications via email of important changes to users account (WINDA-176)
- Implement: - Google Analytics (WINDA-219)
- Implement: - Uploads optimizations (WINDA-178)

*commit* `ca5eae99 <https://git.steelkiwi.com/web/Winda/commit/ca5eae992190b009484eceb4e1398737ef403262>`_


v0.3.4
======

- Implement: Archiving (WINDA-199)
- Implement: reports placeholders change
- Implement frontend: Customize table with order details for print option (WINDA-212)

*commit* `4c7fc62a <https://git.steelkiwi.com/web/Winda/commit/4c7fc62ac48e8a5512dc4445801da5b3560ddb6f>`_


v0.3.3
======

- Implement: Add bulk_create revision tracking for accreditations/certifications (WINDA-174)
- Implement: Fix statistics report data overwriting (WINDA-105)
- Implement: Issues observed on Users screen as DB admin (WINDA-214)
- Implement: Add 2nd level menu according to wireframes (WINDA-209)
- Implement: Flat pages (WINDA-180)
- Implement: Users Section for Database Super Admin users only (WINDA-175)
- Implement frontend: Add bulk_create revision tracking for accreditations/certifications (WINDA-174)
- Implement frontend: Fix statistics report data overwriting (WINDA-105)

*commit* `35710485 <https://git.steelkiwi.com/web/Winda/commit/357104852e5212549689a22d0dc029a74fa8d6d7>`_


v0.3.2
======

- Implement: Users Section for Database Admin users only (WINDA-173)
- Implement: Archiving (WINDA-199)
- Implement: AWS settings tutorial (WINDA-208)
- Implement: Not accepted TP can login to service via forgot password functionality (WINDA-207)
- Implement: Feedback on Sprint 2 release (WINDA-210)
- Implement frontend: Reports UI for Database Admin (WINDA-186)
- Implement frontend: Mailout page UI for Database Admin (WINDA-197)
- Implement frontend: Accreditations (WINDA-193)
- Implement frontend: Users Section UI for Database Admin (WINDA-195)
- Implement frontend: DB_admin issues (WINDA-213)

*commit* `3558a775 <https://git.steelkiwi.com/web/Winda/commit/3558a77534a484a6b12126b1cc989d6e45ec7fd3>`_

v0.3.1
======

- Implement frontend: Mailout page UI for Database Admin -WINDA-197
- Implement frontend: Award Credits - WINDA-192
- Implement frontend: Certifications - WINDA-194
- Implement frontend: Add slider home page(mobile version)
- Implement: View change history for database admin WINDA-174
- Implement: Fix reupload csv file check

*commit* `c6852898 <https://git.steelkiwi.com/web/Winda/commit/c685289881861dce70342b5d48c0a7485ba2d71c>`_


v0.3.0
======

- Implement: Notification history for Database Admin users only (WINDA-103)
- Implement: Not accepted TP can login to service via forgot password functionality (WINDA-207)
- Implement: Mailout for Database Admin users only (WINDA-104)
- Implement: Changes according to clarifications (Q&A) (WINDA-206)
- Implement: Reports views (winda-105)
- Implement: Edit training records check for refresher (winda-162)

*commit* `f9ef10e7 <https://git.steelkiwi.com/web/Winda/commit/f9ef10e75b48e69ca7d3b57ccfb4bfe88ce81731>`_


v0.2.10
=======

- Fix: fix pdf rendering
- Fix: fix misprint  on Credits page

*commit* `3534351a <https://git.steelkiwi.com/web/Winda/commit/3534351afbbf5c47ae030c626348a6f12503ea14>`_

v0.2.9
======

- Fix: fix database admin urls, active tabs, fix complete order after card was rejected (WINDA-170)
- Fix: change price format (WINDA-172)
- Fix: Previous course valid until should be in future (WINDA-162)
- Fix: Some layout issues (WINDA-167)
- Fix: Issues observed while credit functionality testing (WINDA-163)
- Fix: General Menu (WINDA-116)
- Implement: print css

*commit* `5182f3cf <https://git.steelkiwi.com/web/Winda/commit/5182f3cf9fc8defb68868b02b358ad01b4a197a0>`_


v0.2.8
======

- Fix: Send email command (WINDA-101)

*commit* `d38151cc <https://git.steelkiwi.com/web/Winda/commit/d38151cc9ea0a51f9b2d8c4b796d63b41edb99e0>`_


v0.2.7
======

- Implement: Customize file widget for certificate view (WINDA-168)

*commit* `2c82315c <https://git.steelkiwi.com/web/Winda/commit/2c82315c306b9a0df9fa3cad32770c7fdaf4e4d2>`_


v0.2.6
======

- Implement: Paid orders are displayed with 'Unpaid' Status (WINDA-164)
- Implement: Fix training records save with duplicates (WINDA-165)
- Implement frontend: New Bulk upload (WINDA-161)
- Implement frontend: Text and icon home page updates

*commit* `92dc576a <https://git.steelkiwi.com/web/Winda/commit/92dc576a0cc25a2f3e53b73fd2b41bf855f5d0d1>`_


v0.2.5
======

- Implement: JS confirmation for upload invalidation

*commit* `1aa46146 <https://git.steelkiwi.com/web/Winda/commit/1aa461469b1e8e7fad7d42d1734bbd4a6fc0ae35>`_


v0.2.4
======

- Implement: Searching table for Delegate. (WINDA-153)
- Implement: Incorrect messages for Search Training record for delegate ID screen (WINDA-150)
- Implement: Incorrect displaying of Valid from and Until dates for Accredited (WINDA-158)
- Implement: Change Delegate ID to Winda ID in uploads
- Implement frontend: Pop-up is not displayed to db_admin user (WINDA-159)
- Implement frontend: Credits [html] (WINDA-100)

*commit* `cebe67dd <https://git.steelkiwi.com/web/Winda/commit/cebe67dd6d8e1ca2a510ec03ba509285582df70f>`_


v0.2.3
======

- Implement: Uploads for other TP are displayed to logged TP screen (fix pages permissions) (Winda-156)
- Implement: Downloaded error file is empty for Bulk upload by db_admin (fix invalidation records save) (Winda-157)
- Implement: Single upload invalidation errors output fix
- Implement: Send email command (WINDA-101)
- Implement: 'Reset password' page (WINDA-144)
- Implement: Error message when user tries to create new Organisation account (WINDA-151)
- Implement: Searching table for Delegate. (WINDA-153)
- Implement: Issues observed on home pages (WINDA-143)
- Implement frontend: Custom Spinner elements (WINDA-123)
- Implement frontend: Uploads [html] (WINDA-99)
- Implement frontend: Traiding Provider registration (WINDA-145)

*commit* `07b71f9f <https://git.steelkiwi.com/web/Winda/commit/07b71f9f69b9845482da4a5ee502e037de1ae734>`_

v0.2.2
======

- Implement: Award credits by Database administrator (WINDA-102)
- Implement: transactions (WINDA-98)
- Implement frontend: Custom Spinner elements (WINDA-123)
- Implement frontend: Uploads [html] (WINDA-99)
- Implement frontend: Traiding Provider registration (WINDA-145)

*commit* `617634a4 <https://git.steelkiwi.com/web/Winda/commit/617634a4707f75ae5b80004d5d3999858fd8f03f>`_

v0.2.1
======

- Implement: New Bulk Upload (WINDA-86)
- Implement: List of uploads (WINDA-87)
- Implement: Upload Parse Check (WINDA-88)
- Implement: New Single Upload (WINDA-90)
- Implement: Upload Duplicate Check (WINDA-91)
- Implement: Upload Validation Check (WINDA-92)
- Implement: View Upload (WINDA-93)
- Implement: Download file with error details (WINDA-94)
- Implement: Orders (WINDA-96)
- Implement: payment integration (WINDA-97)
- Implement frontend: Homepage for Anonymous (WINDA-124)
- Implement frontend: Delegate Training Certificate - generate and download (WINDA-62)
- Implement frontend: Uploads [html] (WINDA-99)

*commit* `25262018 <https://git.steelkiwi.com/web/Winda/commit/25262018037ad0c4d2c25aa86e9e9d72af589acd>`_

v0.2.0
======

- Implement: List of uploads for training provider backend
- Implement: Organization Profile backend
- Implement: Training records list backend
- Implement: Certificate generation backend
- Implement: Search for Delegate users
- Implement: Search for Organisation users
- Implement: Credits
- Implement: Orders
- Implement frontend: Forgot Password
- Implement frontend: Reset Password
- Implement frontend: Change Password
- Implement frontend: Delegate Registration/Activation Pending/Activation
- Implement frontend: Organization Registration/Activation Pending/Activation
- Implement frontend: Training Records for Delegate users
- Implement frontend: Training Provider Registration/Activation Pending/Activation/Account Review
- Implement frontend: My Profile for Delegate User
- Implement frontend: Search by Delegate ID
- Implement frontend: My Profile for Organisation User

*commit* `14103d2e <https://git.steelkiwi.com/web/Winda/commit/14103d2ed3a68315b94bfd54c483109a5a88e231>`_

v0.1.3
======

- login url in delegate id notification mail

*commit* `21bd3dfd <https://git.steelkiwi.com/web/Winda/commit/21bd3dfd3d7107f2821981ca0732343f5d8a0b5b>`_

v0.1.2
======

- fix course duplicate code

*commit* `39f86b3f <https://git.steelkiwi.com/web/Winda/commit/39f86b3f94d13ce5cc737c31490bb192e383ad39>`_

v0.1.1
======

- fix wrong message during registration if enter invalid password
- fix validation for password
- Fix Organisation and training provider activation email body typo

*commit* `354c4e6e <https://git.steelkiwi.com/web/Winda/commit/354c4e6e931704d33acafdc95b0f9cea8ad05260>`_

v0.1.0
======

- implement login
- implement registration for delegate user
- implement registration for organisation
- implement registration for training provider
- implement forgot password
- implement home pages for delegate, organisation,
   training provider, database administrator
- implement training provider company profile
- implement training provider users management
- implement database admin training provider management
- implement database admin accreditations management
- implement database admin courses management
- implement database admin certifications management

*commit* `a5ee3b50 <https://git.steelkiwi.com/web/Winda/commit/a5ee3b5098d004a7a4daff80ef556a1062028d41>`_
