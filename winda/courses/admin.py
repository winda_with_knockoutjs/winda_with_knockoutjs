# -*- coding: utf-8 -*-
from django.contrib import admin
from reversion.admin import VersionAdmin

from core.admin import ModelAdmin
from courses.models import Course, CourseProvider


@admin.register(Course)
class CourseAdmin(VersionAdmin, ModelAdmin):
    list_display = ('code', 'title', 'course_type', 'refresher_for_course', 'validity_months')
    search_fields = ('code', 'title')
    list_filter = ('course_type',)


@admin.register(CourseProvider)
class CourseProviderAdmin(VersionAdmin, ModelAdmin):
    list_display = ('course', 'training_provider', 'valid_from', 'valid_until', 'last_updated_by',
                    'created_by', 'is_deleted')
    list_filter = ('is_deleted',)
