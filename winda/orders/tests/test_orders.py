# -*- coding: utf-8 -*-
import json
import hashlib
import hmac
from urllib.parse import urlencode
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase, override_settings

from orders.factories import OrderFactory
from orders.models import Transaction
from orders.models import PaymentStatus
from trainings.factories import TrainingProviderFactory
from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, TrainingProviderUserFactory, \
    DatabaseAdminFactory, OrganizationFactory, DelegateFactory

User = get_user_model()


class OrderTestCase(TestCase):
    def setUp(self):
        self.training_provider = TrainingProviderFactory()
        self.user = TrainingProviderAdminFactory(training_provider=self.training_provider)
        self.training_provider_user = TrainingProviderUserFactory(training_provider=self.training_provider)
        self.orders = OrderFactory.create_batch(10, training_provider=self.training_provider)
        self.order = self.orders[0]
        OrderFactory.create_batch(5)
        self.url = reverse('orders', args=(self.training_provider.pk,))
        self.detail_url = reverse('order_detail', args=(self.training_provider.pk, self.order.pk))

    def test_access_orders(self):
        """
        Only Training Provider Admin, Training Provider and Database Admin get credit page
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

        for user in [self.user, self.training_provider_user, DatabaseAdminFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertContains(response, 'Orders')

        for user in [OrganizationFactory(), DelegateFactory()]:
            self.client.login(username=user.username, password=TEST_USER_PASSWORD)
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 404)

    def test_filter_result(self):
        """
        Training Provider Admin get orders page
        """
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))

        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        """
        Training Provider Admin get orders with wrong sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 20,
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'some text',
            'sort_asc': '',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_valid_sort(self):
        """
        Training Provider Admin get orders with correct sort parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '4',
            'sort_asc': '-',
        }
        url = '{}?{}'.format(self.url, urlencode(data))
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_wrong_data(self):
        """
        Training Provider Admin get orders wrong parameters
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_paid_order_detail(self):
        """
        Training Provider Admin get paid order detail page
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        self.order.payment_success = True
        self.order.save()
        response = self.client.get(self.detail_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Print')
        self.assertNotContains(response, 'Pay for this order now')

    def test_paid_order_send_by_email(self):
        """
        Training Provider Admin send order info by email
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('order_email', args=(self.training_provider.pk, self.order.pk))
        response = self.client.get(url)
        self.assertRedirects(response, reverse('order_detail', args=(self.training_provider.pk, self.order.pk)))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'WINDA Order #{}'.format(self.order.pk))

    @override_settings(QUICKPAY_PAYMENT_API_KEY='quickpay_account_key')
    def test_order_callback(self):
        """
        QuickPay callback
        """
        order = OrderFactory()
        body = '{"id":20340407,"merchant_id":19483,"order_id":"%s","accepted":true,"type":"Payment","text_on_statement":null,"branding_id":null,"variables":{},"currency":"EUR","state":"new","metadata":{"type":"card","origin":null,"brand":"visa","bin":"100000","last4":"0008","exp_month":12,"exp_year":2019,"country":"DNK","is_3d_secure":false,"hash":"7a739c6dc10bd21ecfaa3OU6TwJfVom2rsqYYzKzNp7WVF1Lpe6fq","number":null,"customer_ip":"78.26.165.20","customer_country":"UA","fraud_suspected":false,"fraud_remarks":[],"nin_number":null,"nin_country_code":null,"nin_gender":null},"link":{"url":"https://payment.quickpay.net/payments/925b6e9f2c549c7e9e76125ee15984743eb0d2cf3dfb0ffeff3a0ddb762711da","agreement_id":69453,"language":"en","amount":2,"continue_url":"http://82132420.ngrok.io/training-provider/6/orders/3/link/","cancel_url":"http://82132420.ngrok.io/training-provider/6/orders/3/link/","callback_url":"http://82132420.ngrok.io/quickpay-callback/","payment_methods":null,"auto_fee":null,"auto_capture":null,"branding_id":null,"google_analytics_client_id":null,"google_analytics_tracking_id":null,"version":"v10","acquirer":null,"deadline":100,"framed":false,"customer_email":null},"shipping_address":null,"invoice_address":{"name":"dfgdfgd fgdfgdf","att":null,"street":"","city":"","zip_code":"","region":null,"country_code":null,"vat_no":null,"house_number":null,"house_extension":null,"phone_number":"","mobile_number":null,"email":"vracheva@steelkiwi.com"},"basket":[{"qty":1,"item_no":"SINGLE","item_name":"SINGLE","item_price":2,"vat_rate":0.5}],"shipping":null,"operations":[{"id":1,"type":"authorize","amount":2,"pending":false,"qp_status_code":"20000","qp_status_msg":"Approved","aq_status_code":"20000","aq_status_msg":"Approved","data":{},"callback_url":"http://82132420.ngrok.io/quickpay-callback/","callback_success":null,"callback_response_code":null,"callback_duration":null,"callback_at":null,"created_at":"2016-07-16T12:08:50Z"}],"test_mode":true,"acquirer":"clearhaus","facilitator":null,"created_at":"2016-07-16T12:08:33Z","balance":0}' % order.order_id
        data = json.loads(body)
        checksum = hmac.new(b'quickpay_account_key', bytearray(body, encoding='utf-8'), hashlib.sha256).hexdigest()
        url = reverse('quickpay_callback')
        response = self.client.post(url, body, content_type="application/json", HTTP_QUICKPAY_CHECKSUM_SHA256=checksum)
        self.assertEqual(response.status_code, 200)
        order.refresh_from_db()
        self.assertTrue(order.payment_success)
        transaction = Transaction.objects.last()
        self.assertEqual(transaction.created_by, order.created_by)
        self.assertEqual(transaction.training_provider, order.training_provider)
        self.assertEqual(transaction.transaction_value, order.total_credits)
        self.assertEqual(transaction.order, order)
        payment = PaymentStatus.objects.last()
        self.assertEqual(payment.callback_id, data['id'])
        self.assertEqual(payment.accepted, data['accepted'])
        self.assertEqual(payment.test_mode, True)
        self.assertEqual(payment.operation_type, data['type'])
        self.assertEqual(payment.amount, data['operations'][0]['amount'])
        self.assertEqual(payment.pending, data['operations'][0]['pending'])
        self.assertEqual(payment.quick_pay_status_code, data['operations'][0]['qp_status_code'])
        self.assertEqual(payment.quick_pay_status_message, data['operations'][0]['qp_status_msg'])
        self.assertEqual(payment.acquirer_status_code, data['operations'][0]['aq_status_code'])
        self.assertEqual(payment.acquirer_status_message, data['operations'][0]['aq_status_msg'])
        self.assertEqual(payment.balance, data['balance'])
        self.assertEqual(payment.order, order)
