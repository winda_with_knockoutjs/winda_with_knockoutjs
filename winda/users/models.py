# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _
from timezone_field import TimeZoneField

from common.models import TimeStampedModel, Country, AddressAbstractModel, DelegateSequence
from trainings.models import TrainingProvider
from users.managers import WindaUserManager


class UserProfileAbstract(AddressAbstractModel):

    DELEGATE = 'delegate'
    ORGANIZATION = 'organization'
    TRAINING_PROVIDER = 'training provider user'
    TRAINING_PROVIDER_ADMIN = 'training provider admin'
    DATABASE_ADMIN = 'database admin'
    DATABASE_SUPER_ADMIN = 'database super admin'
    TECHNICAL_SUPPORT = 'technical support'

    ROLE_CHOICE = (
        (DELEGATE, _('delegate')),
        (ORGANIZATION, _('organization')),
        (TRAINING_PROVIDER, _('training provider user')),
        (TRAINING_PROVIDER_ADMIN, _('training provider admin')),
        (DATABASE_ADMIN, _('database admin')),
        (DATABASE_SUPER_ADMIN, _('database super admin')),
        (TECHNICAL_SUPPORT, _('technical support')),
    )

    archived_from = models.DateTimeField(_('archived from'), default=TimeStampedModel.archived_from_default)
    archived_at = models.DateTimeField(_('archived at'), null=True, blank=True)
    job_title = models.CharField(_('job title'), max_length=50, blank=True)
    organization_name = models.CharField(_('organization name'), max_length=50, blank=True)
    training_provider = models.ForeignKey(TrainingProvider, verbose_name=_('training provider'), null=True, blank=True,
                                          related_name='%(class)s_users')
    phone = models.CharField(_('work phone number'), max_length=15, blank=True)
    timezone = TimeZoneField(default='Europe/London')

    class Meta:
        abstract = True

    @classmethod
    def generate_delegate_id(cls):
        delegate_sequence = DelegateSequence.objects.order_by('sort_order', 'identifier').first()
        if not delegate_sequence:
            raise DelegateSequence.DoesNotExist
        delegate_sequence.delete()
        return delegate_sequence.identifier

    def get_archived_from(self, role):
        if role == self.DELEGATE:
            return 48
        elif role == self.ORGANIZATION:
            return 6
        return 24


class User(AbstractUser, UserProfileAbstract):

    UNKNOWN, BOUNCE, COMPLAINT, DELIVERY = 'Unknown', 'Bounce', 'Complaint', 'Delivery'

    EMAIL_STATUS_CHOICE = (
        (UNKNOWN, 'Unknown'),
        (BOUNCE, 'Bounce'),
        (COMPLAINT, 'Complaint'),
        (DELIVERY, 'Delivery')
    )

    delegate_id = models.CharField(_('delegate id'), unique=True, max_length=8, null=True, default=None, db_index=True)
    expires_at = models.DateTimeField(_('expires at'), default=TimeStampedModel.user_expires_at_default)
    email_status = models.CharField(_('email status'), max_length=10, choices=EMAIL_STATUS_CHOICE)
    role = models.CharField(_('role'), max_length=30, choices=UserProfileAbstract.ROLE_CHOICE, db_index=True)
    company_name = models.CharField(_('company name'), max_length=50, blank=True)
    last_updated_by = models.ForeignKey('self', default=settings.SYSTEM_USER_ID, null=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey('self', default=settings.SYSTEM_USER_ID, null=True, related_name='%(class)s_created_by')
    archived_by = models.ForeignKey('self', null=True, related_name='%(class)s_archived_by')
    can_upload = models.BooleanField(_('can upload files'), default=False)
    can_buy_credit = models.BooleanField(_('can buy credits'), default=False)
    can_use_credit = models.BooleanField(_('can use credits'), default=False)

    objects = WindaUserManager()

    AbstractUser._meta.get_field('first_name').blank = False
    AbstractUser._meta.get_field('last_name').blank = False
    AbstractUser._meta.get_field('email').blank = False
    AbstractUser._meta.get_field('username').max_length = 255
    AbstractUser._meta.get_field('email').max_length = 255

    def __str__(self):
        return self.username

    @property
    def is_training_provider_admin(self):
        return self.role == self.TRAINING_PROVIDER_ADMIN

    @property
    def is_training_provider_user(self):
        return self.role == self.TRAINING_PROVIDER

    @property
    def is_database_admin(self):
        return self.role == self.DATABASE_ADMIN

    @property
    def is_database_super_admin(self):
        return self.role == self.DATABASE_SUPER_ADMIN

    @property
    def is_delegate(self):
        return self.role == self.DELEGATE

    @property
    def is_organization(self):
        return self.role == self.ORGANIZATION

    @property
    def extra_title(self):
        if self.role == self.DELEGATE:
            return '(Winda ID: {})'.format(self.delegate_id)
        elif self.role == self.ORGANIZATION:
            return '(Organization: {})'.format(self.organization_name)
        elif self.role == self.TRAINING_PROVIDER and self.training_provider_id:
            return '(Training Provider: {})[user]'.format(self.training_provider.name)
        elif self.role == self.TRAINING_PROVIDER_ADMIN and self.training_provider_id:
            return '(Training Provider: {})[admin]'.format(self.training_provider.name)
        else:
            return '({0})'.format(self.get_role_display().title())
