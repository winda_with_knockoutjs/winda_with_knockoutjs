var initializeDataTable = function(table, options){
    var $table = $(table),
        baseOptions = {
            "iDisplayLength": 20,
            "scrollCollapse": true,
            "bLengthChange": false,
            "scrollY": true,
            "scrollX": false,
            "processing": true,
            "searching": false,
            "bInfo": false,
            "serverSide": true,
            "ajax": $table.attr('data-url') || '',
            "order": [[1, 'asc']],
            "language": {
                "oPaginate": {
                    "sNext": '&raquo;',
                    "sPrevious": '&laquo;'
                }
            },
            fnServerParams: function ( aoData ) {
                if (aoData.order.length) {
                    aoData.sort_column = aoData.order[0]['column'];
                    aoData.sort_asc = aoData.order[0]['dir'] === 'asc' ? '' : '-';
                }
            },
            fnDrawCallback: function (oSettings) {
                var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate');
                if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                    pgr.hide();
                } else {
                    pgr.show()
                }
            },
            responsive: {
                details: {
                    type: 'column'
                }
            }
        };
    options = $.extend(baseOptions, options || {});
    return $table.DataTable(options);
};
/* Project specific Javascript goes here. */
$(document).ready(function() {

    var $timezone = $('#id_timezone');

    $('.errors').each(function() {
        if ($(this).html()){
          $(this).parent().addClass('has-error');
        }
    });

    // btn-file-upload
    $('body')
      .on('change', '.btn-file input', function(){
          var filename = $(this).val().split('\\').pop();
          var fileSize = this.files[0];


          if (fileSize.size > 5242880 || fileSize.fileSize > 5242880) {
            $(this).closest('.form-group').find('.errors').text('Allowed file size exceeded.');
            $(this).parent().next('.file-name-wrapper').hide();
            this.value = null;
          } else {
            $(this).closest('.form-group').find('.errors').text('');
            $(this).parent().next('.file-name-wrapper').show().find('.file-name').text(filename);
          }
      })
      .on('click', '.file-name-wrapper .close',function(){
            $(this).parent().hide().prev('.btn-file').find('input').val('');
    });

    $('body')
    .on('click', '.icon-btn_calendar',function(){
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $(this).closest('.date').datepicker('show').off('focus');
    });


    function cloneFieldset() {
        var template = $('#clone-fieldset').html();
        dynamicCounter();

        $('.clone-wrapper').append(template);
    }

    function dynamicCounter(){
        setTimeout(function () {

            if ($('.clone-wrapper .clone-item').length == 1) {
                $('.remove-clone').hide();
            }else {
                $('.remove-clone').show();
            }

            $('.clone-wrapper .clone-item').each(function(index){
                $(this).find('.tittle .count').text(index + 1);

                $(this).find('[name]').each(function(){
                    var name = $(this).attr('name'),
                        id = $(this).attr('id');

                    $(this)
                        .attr('name', name.replace(/\d+/, index ))
                        .attr('id', id.replace(/\d+/, index));

                    $(this)
                        .attr('name', name.replace('__prefix__', index ))
                        .attr('id', id.replace('__prefix__', index));
                });

                $('#id_form-TOTAL_FORMS').val(index+1);

            });

        }, 0);
    }

    // cloneFieldset();

    $('body')
        .on('click', '.add-clone', function(e){
            e.preventDefault();
            e.stopPropagation();
            // cloneFieldset();
    })
        .on('click', '.remove-clone', function(e){
            e.preventDefault();
            e.stopPropagation();
            // dynamicCounter();
            // $(this).closest('.clone-item').remove();
    });

    // datatables-simple
    $('table.datatables-simple').DataTable({
        "iDisplayLength": 20,
        "scrollCollapse": true,
        "bLengthChange": false,
        "bInfo": false,
        "scrollY": true,
        "scrollX": false,
        "paging": false,
        "searching": false,
        "order": [[ 1, "asc" ]],
        'columnDefs': [{
            'targets': 0,
            "width": "5%",
            'searchable':false,
            'orderable':false,
            'className': "control"
        }]
    });


// spinner for all form submit
    if (!$('.report-wrapper, #database_admin_users').length) {
        $('form').submit(function() {
            $('.spinner-wrapper').show();
        });
    }

    // functionAllconfirm
    function confirmModal(message, self) {
        var form = self.closest('form');
        var confirm_modal = $('.confirm-modal');

        confirm_modal.modal('show');
        confirm_modal.find('.confirm-title').text(message);

        $('.confirm-btn-on').on('click', function(e) {
            e.preventDefault();
            form.submit();
            confirm_modal.modal('hide');
        });
    }

    // allConfirmBtn
    $('#discard').on('click', function(e) {
        e.preventDefault();
        var message = 'Are you sure you want to discard this upload?';
        $('form.purchase').find('input[name="status"]').val($(this).data('status'));
        confirmModal(message, $(this));
    });
    $('#purchase').on('click', function(e) {
        e.preventDefault();
        var message = 'Are you sure you want to purchase this upload?';
        $('form.purchase').find('input[name="status"]').val($(this).data('status'));
        confirmModal(message, $(this));
    });
    $('#bulk-upload').on('click', function(e){
        e.preventDefault();
        if ($('form.single-upload').find('#id_invalidate_records').is(':checked')) {
            var message = 'Are you sure you want to revoke matching training records?';
            confirmModal(message, $(this));
        } else {
            $(this).closest('form').submit();
        }
    });
    $('#single-upload').on('click', function(e){
        e.preventDefault();
        var confirmed = true;
        if ($('form.single-upload').find('#id_invalidate_records').is(':checked')) {
            var message = 'Are you sure you want to revoke matching training records?';
            confirmModal(message, $(this));
        } else {
            $(this).closest('form').submit();
        }
    });
    $('#send-mailout').on('click', function(e) {
        e.preventDefault();
        var message = 'Are you sure you want to send this mailout?';
        confirmModal(message, $(this));
    });
    var flag = true;
    $('.archive-btn').on('click', function(e) {
        var self = $(this);
        var message = 'Are you sure you want to archive this user?';
        var confirm_modal = $('.confirm-modal');
        if (flag) {
            e.preventDefault();

            confirm_modal.modal('show');
            confirm_modal.find('.confirm-title').text(message);
        }
        
        $('.confirm-btn-on').on('click', function(e) {
            e.preventDefault();
            flag = false;
            self.click();
            confirm_modal.modal('hide');
        });
    });
    // end allConfirmBtn

    $('body').on('click', '.navbar-toggle', function(e){
        $('.header-main').toggleClass('open').find('.main-logo, .logo-mobile').toggleClass('hidden');
    });

    if ($timezone.length && $timezone.is(':hidden')) {
        $timezone.val(moment.tz.guess());
    }

    function sliderHomePage() {
        if($(window).innerWidth() < 630) {
            if (!$('.list-menu-home-wrapper').length) {
                $('.list-menu-home').wrap('<div class="list-menu-home-wrapper"></div>');
                setTimeout(function () {
                    $('.list-menu-home-wrapper .list-menu-home').lightSlider({
                        item: 1,
                        adaptiveHeight:true,
                        loop: true,
                        slideMargin: 30
                    });
                }, 10);
            }
        }
    }

    sliderHomePage();

    $(window).resize(function() {
        sliderHomePage();
    });

});
