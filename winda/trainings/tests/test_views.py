# -*- coding: utf-8 -*-
import json

from datetime import timedelta
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from common.models import EmailNotification
from users.factories import TrainingProviderAdminFactory, TEST_USER_PASSWORD, DelegateFactory
from trainings.factories import UploadBatchFactory, UploadBatchRecordFactory
from trainings.models import UploadBatch
from courses.factories import CourseFactory
from orders.factories import TransactionFactory
from delegate.factories import TrainingRecordFactory


class TrainingProviderViewsTestCase(TestCase):

    def setUp(self):
        self.url = reverse('home')
        self.user_count = get_user_model().objects.count()
        self.user = TrainingProviderAdminFactory.create()

    def test_home(self):
        url = reverse('training_provider:home', kwargs={'pk': self.user.training_provider.pk})
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertRedirects(response, url)


class UploadsViewsTestCase(TestCase):

    def setUp(self):
        self.user_count = get_user_model().objects.count()
        self.user = TrainingProviderAdminFactory.create()
        self.training_provider = self.user.training_provider
        self.list_url = reverse('training_provider:uploads', kwargs={'pk': self.training_provider.pk})

    def test_training_record_list(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Uploads')

    def test_filter(self):
        UploadBatchFactory.create_batch(10, training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_sort(self):
        UploadBatchFactory.create_batch(10, training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        UploadBatchFactory.create_batch(10, training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'hello',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_view_upload_draft(self):
        upload_batch = UploadBatchFactory.create(training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('training_provider:view-upload', kwargs={'pk': self.training_provider.pk, 'batch_pk': upload_batch.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_discard_upload(self):
        upload_batch = UploadBatchFactory.create(training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('training_provider:view-upload', kwargs={'pk': self.training_provider.pk, 'batch_pk': upload_batch.pk})
        response = self.client.post(url, data={'status': 'discarded'})
        self.assertEqual(response.status_code, 302)
        upload_batch = UploadBatch.objects.get(id=upload_batch.id)
        self.assertEqual(upload_batch.status, UploadBatch.DISCARDED)
        self.assertEqual(upload_batch.discarded_by, self.user)

    def test_download_errors_view(self):
        upload_batch = UploadBatchFactory.create(training_provider=self.user.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('training_provider:download-errors', kwargs={'pk': self.training_provider.pk, 'batch_pk': upload_batch.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_purchase_upload_valid(self):
        delegate = DelegateFactory.create()
        course = CourseFactory.create()
        valid_date = (timezone.now() - timedelta(days=2)).date()
        upload_batch = UploadBatchFactory.create(training_provider=self.user.training_provider,
                                                 valid_records=1, duplicate_records=0)
        UploadBatchRecordFactory.create(
            upload_batch=upload_batch,
            delegate_id=delegate.delegate_id,
            course_code=course.code,
            course_completion_date=valid_date,
            previous_course_valid_until=valid_date,
            parse_check=0,
            duplicate_check=0,
            validation_check=0,
            calculated_valid_from=valid_date,
            calculated_valid_until=valid_date
        )
        TransactionFactory.create_batch(5, training_provider=self.user.training_provider, transaction_value=5)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('training_provider:view-upload', kwargs={'pk': self.training_provider.pk, 'batch_pk': upload_batch.pk})
        response = self.client.post(url, data={'status': 'purchased'})
        self.assertEqual(response.status_code, 302)
        upload_batch = UploadBatch.objects.get(id=upload_batch.id)
        self.assertEqual(upload_batch.status, UploadBatch.PURCHASED)
        self.assertEqual(upload_batch.purchased_by, self.user)
        self.assertTrue(delegate.delegate_training_records.filter(
            training_provider_id=self.user.training_provider_id,
            valid_from=valid_date,
            valid_until=valid_date,
            course=course
            ).exists())
        self.assertEqual(EmailNotification.objects.count(), 1)

    def test_purchase_upload_changed_duplicates(self):
        delegate = DelegateFactory.create()
        course = CourseFactory.create()
        valid_date = (timezone.now() - timedelta(days=2)).date()
        upload_batch = UploadBatchFactory.create(training_provider=self.user.training_provider,
                                                 valid_records=1, duplicate_records=0)
        UploadBatchRecordFactory.create(
            upload_batch=upload_batch,
            delegate_id=delegate.delegate_id,
            course_code=course.code,
            course_completion_date=valid_date,
            previous_course_valid_until=valid_date,
            parse_check=0,
            duplicate_check=0,
            validation_check=0,
            calculated_valid_from=valid_date,
            calculated_valid_until=valid_date
        )
        TrainingRecordFactory.create(course=course, completed_on=valid_date, delegate=delegate)
        TransactionFactory.create_batch(5, training_provider=self.user.training_provider, transaction_value=5)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        url = reverse('training_provider:view-upload', kwargs={'pk': self.training_provider.pk, 'batch_pk': upload_batch.pk})
        response = self.client.post(url, data={'status': 'purchased'})
        self.assertEqual(response.status_code, 200)
