# -*- coding: utf-8 -*-
from django.contrib import admin


class ModelAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if obj.pk is None:
            obj.created_by = request.user
        else:
            obj.last_updated_by = request.user
        super().save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        """
        Given an inline formset save it to the database.
        """
        instances = formset.save(commit=False)
        for obj in instances:
            if obj.pk is None:
                obj.created_by = request.user
            else:
                obj.last_updated_by = request.user
            obj.save()
