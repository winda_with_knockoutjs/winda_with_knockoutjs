# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
import reversion
from reversion.models import Revision, Version

from common.factories import EmailNotificationFactory
from common.models import EmailNotification
from common.reversion import bulk_create_revision
from registration.factories import RegistrationFactory
from registration.models import Registration
from users.factories import DatabaseAdminFactory, DelegateFactory, TrainingProviderAdminFactory, \
    TrainingProviderUserFactory, OrganizationFactory, DatabaseSuperAdminFactory


class ArchiveTestCase(TestCase):

    def setUp(self):
        DatabaseSuperAdminFactory(archived_from=timezone.now() - timezone.timedelta(days=1))
        DelegateFactory.create_batch(3, archived_from=timezone.now() - timezone.timedelta(days=1))
        OrganizationFactory.create_batch(3, archived_from=timezone.now() - timezone.timedelta(days=1))
        TrainingProviderUserFactory.create_batch(3, archived_from=timezone.now() - timezone.timedelta(days=1))
        TrainingProviderAdminFactory.create_batch(1, archived_from=timezone.now() - timezone.timedelta(days=1))
        DatabaseAdminFactory.create_batch(5, archived_from=timezone.now() - timezone.timedelta(days=1))
        DatabaseSuperAdminFactory.create_batch(4, archived_from=timezone.now() - timezone.timedelta(days=1))

        DatabaseSuperAdminFactory.create_batch(5)
        DelegateFactory.create_batch(5)
        OrganizationFactory.create_batch(5)
        TrainingProviderUserFactory.create_batch(5)
        TrainingProviderAdminFactory.create_batch(5)
        DatabaseAdminFactory.create_batch(10)
        DatabaseSuperAdminFactory.create_batch(5)

        RegistrationFactory.create_batch(10, archived_from=timezone.now() - timezone.timedelta(days=1))
        EmailNotificationFactory.create_batch(10, archived_from=timezone.now() - timezone.timedelta(days=1))

        RegistrationFactory.create_batch(2)
        EmailNotificationFactory.create_batch(2)

        DatabaseSuperAdminFactory(archived_at=timezone.now() - relativedelta(months=49))

        self.user_count = get_user_model().objects.count()
        self.notification_count = EmailNotification.objects.count()
        self.registration_count = Registration.objects.count()
        with bulk_create_revision():
            for instance in get_user_model().objects.all().exclude(pk=settings.SYSTEM_USER_ID):
                reversion.add_to_revision(instance)
        self.revision_count = Revision.objects.count()
        self.version_count = Version.objects.count()

    def test_archive_command(self):
        call_command('archive')
        self.assertEqual(self.user_count, get_user_model().objects.count())
        self.assertEqual(self.notification_count, EmailNotification.objects.count())
        self.assertEqual(self.registration_count, Registration.objects.count())

        self.assertEqual(Registration.objects.filter(
            archived_at__isnull=False,
            first_name='ARCHIVED', last_name='ARCHIVED', email='ARCHIVED').count(), 10)

        self.assertEqual(EmailNotification.objects.filter(
            archived_at__isnull=False,
            subject='ARCHIVED', body='ARCHIVED').count(), 10)

        self.assertEqual(get_user_model().objects.filter(
            archived_at__isnull=False, first_name='ARCHIVED', last_name='ARCHIVED').count(), 20)

        self.assertEqual(Revision.objects.order_by('pk').first().version_set.count(), self.version_count - 1)
        self.assertEqual(Revision.objects.filter(comment='Archived', user_id=settings.SYSTEM_USER_ID).count(), 1)
