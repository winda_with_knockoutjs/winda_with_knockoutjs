# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20160721_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='item_code',
            field=models.CharField(unique=True, max_length=10, verbose_name='item code'),
        ),
        migrations.AlterField(
            model_name='item',
            name='item_name',
            field=models.CharField(verbose_name='item name', max_length=30),
        ),
        migrations.AlterField(
            model_name='order',
            name='item_code',
            field=models.CharField(verbose_name='item code', max_length=10),
        ),
        migrations.AlterField(
            model_name='order',
            name='item_name',
            field=models.CharField(verbose_name='item name', max_length=30),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='valid_from',
            field=models.DateField(verbose_name='valid from'),
        ),
        migrations.AlterUniqueTogether(
            name='pricing',
            unique_together=set([('item', 'valid_from')]),
        ),
    ]
