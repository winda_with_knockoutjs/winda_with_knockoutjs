# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0004_registration_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registration',
            name='registration_type',
            field=models.CharField(verbose_name='registration type', max_length=30, choices=[('delegate', 'delegate'), ('organization', 'organization'), ('training provider user', 'training provider registration')]),
        ),
    ]
