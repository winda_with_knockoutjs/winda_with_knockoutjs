$(document).ready(function() {
    initializeDataTable('table#database_course', {
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "code"},
            { "data": "title" },
            { "data": "course_type" },
            { "data": "validity_months" }
        ]
    });
});

