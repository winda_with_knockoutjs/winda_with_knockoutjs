# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from trainings.factories import TrainingProviderFactory
from users.factories import UserFactory, TEST_USER_PASSWORD


class LoginTestCase(TestCase):

    def setUp(self):
        self.url = reverse('account_login')
        self.user = UserFactory()

    def test_user_login_by_username(self):
        """
        Anonymous user successfully login by username
        """
        archived_from = self. user.archived_from
        data = {'login': self.user.username, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))
        self.user.refresh_from_db()
        self.assertLess(archived_from, self. user.archived_from)

    def test_user_login_by_email(self):
        """
        Anonymous user successfully login by email
        """
        archived_from = self. user.archived_from
        data = {'login': self.user.email, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))
        self.user.refresh_from_db()
        self.assertLess(archived_from, self. user.archived_from)

    def test_user_with_wrong_email(self):
        data = {'login': 'user_wrong_email@example.com', 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Invalid email address, username and / or password combination; please try again', 1)

    def test_user_inactive(self):
        self.user = UserFactory(is_active=False)
        data = {'login': self.user.email, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This account is inactive.', 1)

    def test_user_expires_at(self):
        self.user = UserFactory(expires_at=timezone.now())
        data = {'login': self.user.email, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This account has expired.', 1)

    def test_user_archived_at(self):
        self.user = UserFactory(archived_at=timezone.now())
        data = {'login': self.user.email, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This account has been archived.', 1)

    def test_training_provider_inactive(self):
        training_provider = TrainingProviderFactory(is_active=False)
        self.user = UserFactory(role=get_user_model().TRAINING_PROVIDER_ADMIN, training_provider=training_provider)
        data = {'login': self.user.email, 'password': TEST_USER_PASSWORD}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'The training provider associated with this account is inactive.', 1)
