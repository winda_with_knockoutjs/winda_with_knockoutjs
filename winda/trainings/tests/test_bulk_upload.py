# -*- coding: utf-8 -*-
import json
import io
from datetime import timedelta
from django.test import TestCase
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone

from users.factories import TEST_USER_PASSWORD, TrainingProviderAdminFactory, DelegateFactory
from trainings.models import UploadBatch, UploadBatchRecord
from delegate.factories import TrainingRecordFactory
from courses.factories import CourseFactory, CourseProviderFactory, CourseRefresherFactory


class BulkUploadViewsTestCase(TestCase):

    def setUp(self):
        self.user_count = get_user_model().objects.count()
        self.user = TrainingProviderAdminFactory.create()
        self.training_provider = self.user.training_provider
        self.delegate = DelegateFactory.create()
        self.url = reverse('training_provider:bulk-upload', kwargs={'pk': self.training_provider.pk})
        self.course = CourseFactory.create(require_existing_training_record=True)
        self.refresher = CourseRefresherFactory.create(refresher_for_course=self.course)
        self.course_provider = CourseProviderFactory.create(course=self.course,
                                                            training_provider=self.user.training_provider)
        self.course_provider_refresher = CourseProviderFactory.create(course=self.refresher,
                                                            training_provider=self.user.training_provider)
        date = (timezone.now() - timedelta(days=1)).date()
        self.training_record = TrainingRecordFactory.create(course=self.course,
                                                            delegate=self.delegate,
                                                            completed_on=date)

    def test_upload_file(self):
        batch_count = UploadBatch.objects.count()
        batch_record_count = UploadBatchRecord.objects.count()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        input_string = 'winda ID,Course Code,Course Completion Date,Previous Course Valid Until Date\nSAMPLE01,EXFULL,2014-06-24,'
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(UploadBatch.objects.count(), batch_count + 1)
        self.assertEqual(UploadBatchRecord.objects.count(), batch_record_count + 1)

    def test_upload_file_invalid_header(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        input_string = 'Winda,Course Code,Course Completion Date,Previous Course Valid Until Date\nSAMPLE01,EXFULL,2014-06-24,'
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 200)

    def test_upload_file_invalid_type(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        input_string = 'Winda id,Course Code,Course Completion Date,Previous Course Valid Until Date\nSAMPLE01,EXFULL,2014-06-24,'
        data = {
            'upload_file': self.prepare_test_csv(input_string, 'test.txt'),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 200)

    def test_upload_file_invalid_number_of_records(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        header_rows = ['Winda Id,Course Code,Course Completion Date,Previous Course Valid Until Date',]
        input_rows = ['SAMPLE01,EXFULL,2014-06-24,' for i in range(0, settings.TRAINING_RECORD_MAX_QUANTITY+1)]
        input_string = '\n'.join(header_rows + input_rows)
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 200)

    def test_upload_file_with_different_parse_check_errors(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        header_rows = ['Winda Id,Course Code,Course Completion Date,Previous Course Valid Until Date',]
        input_rows = []
        valid_date = (timezone.now() - timedelta(days=3)).date().strftime(settings.PARSE_DATE_FORMAT)
        # Missing columns
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, valid_date]))
        # Missing delegate id
        input_rows.append(','.join(['    ', self.course.code, valid_date, valid_date]))
        # Invalid delegate id
        input_rows.append(','.join(['aaaaaaaaa', self.course.code, valid_date, valid_date]))
        # Missing course
        input_rows.append(','.join([self.delegate.delegate_id, '      ', valid_date, valid_date]))
        # Invalid course
        input_rows.append(','.join([self.delegate.delegate_id, 'aaaaaaaaa', valid_date, valid_date]))
        # Invalid course completion date
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, '', valid_date]))
        # Invalid format course completion date
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, '12/56/20', valid_date]))
        # Invalid format previous
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, valid_date, '12/56/20']))

        input_string = '\n'.join(header_rows + input_rows)
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 302)
        batch = UploadBatch.objects.get(description='test description')
        records = UploadBatchRecord.objects.filter(upload_batch_id=batch.id)
        self.assertTrue(records.get(source_row_number=2).parse_check & 1)
        self.assertTrue(records.get(source_row_number=3).parse_check & 2)
        self.assertTrue(records.get(source_row_number=4).parse_check & 4)
        self.assertTrue(records.get(source_row_number=5).parse_check & 8)
        self.assertTrue(records.get(source_row_number=6).parse_check & 16)
        self.assertTrue(records.get(source_row_number=7).parse_check & 32)
        self.assertTrue(records.get(source_row_number=8).parse_check & 64)
        self.assertTrue(records.get(source_row_number=9).parse_check & 128)

    def test_upload_file_with_different_validation_errors(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        header_rows = ['Winda Id,Course Code,Course Completion Date,Previous Course Valid Until Date',]
        input_rows = []
        valid_date = (timezone.now() - timedelta(days=3)).date().strftime(settings.PARSE_DATE_FORMAT)
        invalid_date = (timezone.now() + timedelta(days=3)).date().strftime(settings.PARSE_DATE_FORMAT)
        # Course completion date is in future
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, invalid_date]))
        # Unknown course code
        input_rows.append(','.join([self.delegate.delegate_id, 'wazzup', valid_date]))
        # Not accredited for course
        course = CourseFactory.create()
        input_rows.append(','.join([self.delegate.delegate_id, course.code, valid_date]))
        # Unknown delegate id
        input_rows.append(','.join(['wazzup', self.course.code, valid_date]))
        # Delegate is archived
        delegate = DelegateFactory.create(archived_at=timezone.now())
        input_rows.append(','.join([delegate.delegate_id, self.course.code, valid_date]))
        # Previous course valid until date is required
        input_rows.append(','.join([delegate.delegate_id, self.refresher.code, valid_date]))
        # Previous course valid until date must be in future
        input_rows.append(','.join([delegate.delegate_id, self.refresher.code, valid_date, valid_date]))
        # No matching previous course training record found
        refresher = CourseRefresherFactory.create(refresher_for_course=self.course, require_existing_training_record=True)
        input_rows.append(','.join([delegate.delegate_id, refresher.code, valid_date, invalid_date]))

        input_string = '\n'.join(header_rows + input_rows)
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 302)
        batch = UploadBatch.objects.get(description='test description')
        records = UploadBatchRecord.objects.filter(upload_batch_id=batch.id)
        self.assertTrue(records.get(source_row_number=2).validation_check & 1)
        self.assertTrue(records.get(source_row_number=3).validation_check & 2)
        self.assertTrue(records.get(source_row_number=4).validation_check & 4)
        self.assertTrue(records.get(source_row_number=5).validation_check & 8)
        self.assertTrue(records.get(source_row_number=6).validation_check & 16)
        self.assertTrue(records.get(source_row_number=7).validation_check & 32)
        self.assertTrue(records.get(source_row_number=8).validation_check & 64)
        self.assertTrue(records.get(source_row_number=9).validation_check & 128)

    def test_upload_file_with_duplicate_errors(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        header_rows = ['Winda Id,Course Code,Course Completion Date,Previous Course Valid Until Date',]
        input_rows = []
        valid_date = (timezone.now() - timedelta(days=3)).date().strftime(settings.PARSE_DATE_FORMAT)
        # Default
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, valid_date, '']))
        # Duplicate in this batch
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, valid_date, '']))
        # Duplicate in database
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code,
                                    self.training_record.completed_on.strftime(settings.PARSE_DATE_FORMAT), '']))
        # One mode duplicate
        input_rows.append(','.join([self.delegate.delegate_id, self.course.code, valid_date, '']))

        input_string = '\n'.join(header_rows + input_rows)
        data = {
            'upload_file': self.prepare_test_csv(input_string),
            'description': 'test description'
        }
        response = self.client.post(self.url, data=data, format='multipart')
        self.assertEqual(response.status_code, 302)
        batch = UploadBatch.objects.get(description='test description')
        records = UploadBatchRecord.objects.filter(upload_batch_id=batch.id)
        self.assertTrue(records.get(source_row_number=3).duplicate_check & 1)
        self.assertTrue(records.get(source_row_number=4).duplicate_check & 2)
        self.assertTrue(records.get(source_row_number=5).duplicate_check & 1)

    def prepare_test_csv(self, data='', filename='random-name.csv'):
        file_io = io.StringIO(data)
        file_io.seek(0)

        csv_file = InMemoryUploadedFile(
            file_io, None, filename, 'text/csv', file_io.tell(), None
        )
        return csv_file
