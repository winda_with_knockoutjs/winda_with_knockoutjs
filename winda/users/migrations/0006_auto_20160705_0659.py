# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20160702_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='training_provider',
            field=models.ForeignKey(verbose_name='training provider', to='trainings.TrainingProvider', blank=True, null=True, related_name='user_users'),
        ),
    ]
