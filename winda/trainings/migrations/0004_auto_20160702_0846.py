# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0003_trainingprovidercertificate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingprovider',
            name='is_active',
            field=models.BooleanField(verbose_name='Flag indicating if this training provider is active or not', default=False),
        ),
        migrations.AlterField(
            model_name='trainingprovidercertificate',
            name='is_deleted',
            field=models.BooleanField(verbose_name='is deleted', default=False),
        ),
    ]
