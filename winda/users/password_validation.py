# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class MaxLengthValidator(object):
    def __init__(self, max_length=8):
        self.max_length = max_length

    def validate(self, password, **kwargs):
        if len(password) < self.max_length:
            raise ValidationError(
                _('%(max_length)d character maximum.'),
                code='password_too_long',
                params={'max_length': self.max_length},
            )

    def get_help_text(self):
        return _(
            "Your password must contain less then %(max_length)d characters."
            % {'max_length': self.max_length}
        )
