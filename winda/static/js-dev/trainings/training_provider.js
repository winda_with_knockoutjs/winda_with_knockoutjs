$(document).ready(function() {
    initializeDataTable('table#training_provider', {
        "columns": [
            { "data": "", "className": "control" },
            { "data": "pk"},
            { "data": "name" },
            { "data": "country" },
            { "data": "postal_code" },
            { "data": "is_active"},
            { "data": "accepted_at"}
        ]
    });

    initializeDataTable('table#change_history', {
        "order": [[1, 'desc']],
        "columns": [
            { "data": "", "className": "control" },
            { "data": "id"},
            { "data": "date_created" },
            { "data": "user" },
            { "data": "comment" },
            { "data": "detail"}
        ]
    });
});
