# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0009_auto_20160715_1636'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trainingrecord',
            name='old_training_provider',
        ),
    ]
