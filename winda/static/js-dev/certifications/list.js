$(document).ready(function() {
    initializeDataTable('table#database_admin_certification', {
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "certificate_number"},
            { "data": "valid_from" },
            { "data": "valid_until" },
            { "data": "certificate_file" },
            { "data": "is_deleted"}
        ]
    });
});
