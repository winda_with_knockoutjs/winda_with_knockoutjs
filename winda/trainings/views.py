# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
import csv
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import yesno
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView
from skd_tools.mixins import ActiveTabMixin

from common.mixins import (
    TrainingProviderAccessMixin, FilterMixin, BaseRoleAccessMixin,
    ParentObjectMixin
)
from common.models import EmailNotification
from core.views import CreateView, UpdateView
from trainings.forms import (
    UserFilterForm, UserForm, UploadBatchFilterForm, SingleUploadForm, BulkUploadForm,
    BulkPurchaseForm
)
from trainings.models import UploadBatch, UploadBatchRecord, TrainingProvider

User = get_user_model()


class TrainingProviderHomeView(TrainingProviderAccessMixin, DetailView):

    template_name = 'trainings/home.html'
    only_owner = True

    def get_queryset(self):
        return TrainingProvider.objects.all()

    def test_owner(self, user):
        if not user.training_provider_id and user.role not in self.admin_roles:
            return False
        # Override for non detail views
        return self.get_object().pk == user.training_provider.pk or user.role in self.admin_roles


class UserListView(BaseRoleAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    roles = [User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    model = User
    form_class = UserFilterForm
    template_name = 'trainings/users.html'
    active_tab = 'user'

    def get_queryset(self):
        if self.request.user.training_provider:
            return self.request.user.training_provider.user_users.filter(archived_at__isnull=True)
        return User.objects.none()

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'first_name': obj.first_name if obj == self.request.user else '<a href="{}">{}</a>'
                .format(reverse('training_provider:user_update', args=(self.request.user.pk, obj.pk)), obj.first_name),
            'last_name': obj.last_name,
            'email': obj.email,
            'role': obj.get_role_display().title(),
            'is_active': yesno(obj.is_active),
            'can_upload': yesno(obj.can_upload),
            'can_use_credit': yesno(obj.can_use_credit),
            'can_buy_credit': yesno(obj.can_buy_credit),
        } for obj in object_list]


class UserCreateView(BaseRoleAccessMixin, ActiveTabMixin, CreateView):
    roles = [User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    model = User
    form_class = UserForm
    active_tab = 'user'
    template_name = 'trainings/users_form.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.username = self.object.email
        self.object.email_status = User.UNKNOWN
        self.object.archived_from = timezone.now() + relativedelta(months=24)
        self.object.is_active = True
        self.object.training_provider = self.request.user.training_provider
        password = User.objects.make_random_password(length=16)
        self.object.set_password(password)
        self.object.save()
        EmailNotification.user_account_created(self.request, self.object, password)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('training_provider:users', args=(self.request.user.pk,))


class UserUpdateView(BaseRoleAccessMixin, ActiveTabMixin, UpdateView):
    roles = [User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    model = User
    form_class = UserForm
    active_tab = 'user'
    template_name = 'trainings/users_form.html'
    success_url = reverse_lazy('training_provider:users')

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, _('User profile was successfully saved.'))
        return reverse_lazy('training_provider:users', args=(self.request.user.pk,))

    def get_queryset(self):
        return super().get_queryset().exclude(pk=self.request.user.pk)


class CompanyProfileDetailView(BaseRoleAccessMixin, ActiveTabMixin, DetailView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    model = TrainingProvider
    active_tab = 'company_profile'
    template_name = 'trainings/company_profile.html'
    context_object_name = 'training_provider'

    def get_active_tab(self):
        if self.request.user.is_database_admin:
            return 'training_provider'
        return super().get_active_tab()

    def get_object(self, queryset=None):
        if self.request.user.is_database_admin:
            return get_object_or_404(self.model, pk=self.kwargs['pk'])
        return self.request.user.training_provider

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['course_providers'] = self.object.training_provider_courseprovider.filter(
            valid_from__lte=timezone.now(), valid_until__gte=timezone.now(), is_deleted=False)
        context['certificates'] = self.object.training_provider_trainingprovidercertificate.filter(is_deleted=False)
        return context


class TrainingProviderUploadMixin(BaseRoleAccessMixin, ParentObjectMixin, ActiveTabMixin):

    roles = [User.ORGANIZATION, User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN]
    admin_roles = [User.DATABASE_ADMIN, User.DATABASE_SUPER_ADMIN]
    parent_model = TrainingProvider
    parent_slug = 'pk'
    active_tab = 'upload'
    only_owner = True

    def get_active_tab(self):
        if self.request.user.is_database_admin:
            return 'training_provider'
        return super().get_active_tab()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['training_provider'] = self.parent_object
        return kwargs

    def test_owner(self, user):
        return user.training_provider_id and self.get_parent_object() == user.training_provider or user.role in self.admin_roles


class UploadBatchListView(TrainingProviderUploadMixin, FilterMixin, ListView):

    model = UploadBatch
    template_name = 'trainings/upload_batch_list.html'
    form_class = UploadBatchFilterForm

    def get_queryset(self):
        return self.model.objects.filter(training_provider_id=self.parent_object.id)

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': obj.id,
            'status': obj.get_status_display().title(),
            'description': '<a href="{}">{}</a>'.format(
                reverse('training_provider:view-upload', args=(self.parent_object.pk, obj.pk)), obj.description),
            'created': obj.created.strftime(settings.TEMPLATE_DATE_FORMAT),
            'created_by': obj.created_by.get_full_name(),
            'purchased_at': obj.purchased_at.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.purchased_at else '',
            'purchased_by': obj.purchased_by.get_full_name() if obj.purchased_by else '',
            'total_records': obj.total_records,
            'valid_records': obj.valid_records,
            'duplicate_records': obj.duplicate_records,
            'error_records': obj.error_records
        } for obj in object_list]


class SingleUploadCreateView(TrainingProviderUploadMixin, CreateView):

    model = UploadBatchRecord
    form_class = SingleUploadForm
    template_name = 'trainings/single_upload.html'

    def get_success_url(self):
        return reverse_lazy('training_provider:view-upload', args=(self.parent_object.pk, self.object.upload_batch_id))


class BulkUploadCreateView(SingleUploadCreateView, CreateView):

    model = UploadBatch
    form_class = BulkUploadForm
    template_name = 'trainings/bulk_upload.html'

    def get_success_url(self):
        return reverse_lazy('training_provider:view-upload', args=(self.parent_object.pk, self.object.id))


class ViewUploadView(TrainingProviderUploadMixin, UpdateView):

    model = UploadBatch
    template_name = 'trainings/view_upload_template.html'
    form_class = BulkPurchaseForm
    pk_url_kwarg = 'batch_pk'

    def get_success_url(self):
        return reverse_lazy('training_provider:view-upload', args=(self.parent_object.pk, self.object.id))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['parent_user'] = self.parent_object
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # Get state from database
        context['object'] = self.get_object()
        return context


class DownloadErrorsView(TrainingProviderUploadMixin, DetailView):

    model = UploadBatch
    pk_url_kwarg = 'batch_pk'
    template_name = 'trainings/view_upload_template.html'

    def render_to_response(self, context, **response_kwargs):
        upload_batch = self.object
        date = timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT)
        filename = 'upload_#{0}_errors_{1}'.format(self.object.id, date)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename={}.csv'.format(filename)

        writer = csv.writer(response)
        # Write header
        writer.writerow(['WINDA ID', 'Course Code', 'Course Completion Date',
            'Previous Course Valid Until Date', 'Calculated Valid From Date',
            'Calculated Valid Until Date', 'Errors'])

        for record in upload_batch.upload_batch_records.all():
            row = [
                record.delegate_id, record.course_code,
                self.format_date(record.course_completion_date),
                self.format_date(record.previous_course_valid_until),
                self.format_date(record.calculated_valid_from),
                self.format_date(record.calculated_valid_until),
                record.get_errors()
            ]
            writer.writerow(row)
        return response

    def format_date(self, date_value):
        if date_value:
            return date_value.strftime(settings.TEMPLATE_DATE_FORMAT)
        return None
