# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import common.models
import django.contrib.auth.models
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
        ('trainings', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(blank=True, verbose_name='last login', null=True)),
                ('is_superuser', models.BooleanField(default=False, verbose_name='superuser status', help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('username', models.CharField(unique=True, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', verbose_name='username', max_length=30)),
                ('first_name', models.CharField(verbose_name='first name', max_length=30)),
                ('last_name', models.CharField(verbose_name='last name', max_length=30)),
                ('email', models.EmailField(verbose_name='email address', max_length=254)),
                ('is_staff', models.BooleanField(default=False, verbose_name='staff status', help_text='Designates whether the user can log into this admin site.')),
                ('is_active', models.BooleanField(default=True, verbose_name='active', help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('address_1', models.CharField(blank=True, verbose_name='street address line 1', max_length=50)),
                ('address_2', models.CharField(blank=True, verbose_name='street address line 2', max_length=50)),
                ('city', models.CharField(blank=True, verbose_name='city', max_length=50)),
                ('postal_code', models.CharField(blank=True, verbose_name='postal code', max_length=20)),
                ('archived_from', models.DateTimeField(default=common.models.TimeStampedModel.archived_from_default, verbose_name='archived from')),
                ('archived_at', models.DateTimeField(blank=True, verbose_name='archived at', null=True)),
                ('job_title', models.CharField(blank=True, verbose_name='job title', max_length=50)),
                ('organization_name', models.CharField(blank=True, verbose_name='organization name', max_length=50)),
                ('delegate_id', models.CharField(unique=True, verbose_name='delegate id', max_length=8)),
                ('expires_at', models.DateTimeField(verbose_name='expires at')),
                ('email_status', models.CharField(choices=[('Unknown', 'Unknown'), ('Bounce', 'Bounce'), ('Complaint', 'Complaint'), ('Delivery', 'Delivery')], verbose_name='email status', max_length=10)),
                ('role', models.CharField(db_index=True, choices=[('delegate', 'delegate'), ('organization', 'organization'), ('training provider user', 'training provider user'), ('training provider admin', 'training provider admin'), ('database admin', 'database admin'), ('database super admin', 'database super admin'), ('technical support', 'technical support')], verbose_name='role', max_length=30)),
                ('company_name', models.CharField(blank=True, verbose_name='company name', max_length=50)),
                ('can_upload', models.BooleanField(default=False, verbose_name='Flag indicating if this Training Provider User can upload files')),
                ('can_buy_credit', models.BooleanField(default=False, verbose_name='Flag indicating if this Training Provider User can buy credits')),
                ('can_use_credit', models.BooleanField(default=False, verbose_name='Flag indicating if this Training Provider User can use credits')),
                ('country', models.ForeignKey(blank=True, verbose_name='country', null=True, to='common.Country')),
                ('created_by', models.ForeignKey(default=1, null=True, to=settings.AUTH_USER_MODEL, related_name='user_created_by')),
                ('groups', models.ManyToManyField(blank=True, related_name='user_set', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups', to='auth.Group', related_query_name='user')),
                ('last_updated_by', models.ForeignKey(default=1, null=True, to=settings.AUTH_USER_MODEL, related_name='user_last_updated_by')),
                ('training_provider', models.ForeignKey(blank=True, verbose_name='training provider', null=True, to='trainings.TrainingProvider')),
                ('user_permissions', models.ManyToManyField(blank=True, related_name='user_set', help_text='Specific permissions for this user.', verbose_name='user permissions', to='auth.Permission', related_query_name='user')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
