# -*- coding: utf-8 -*-
from django.contrib import admin

from core.admin import ModelAdmin
from delegate.models import TrainingRecord, DelegateCertificate, Search


@admin.register(TrainingRecord)
class TrainingRecordAdmin(ModelAdmin):
    list_display = ('delegate', 'course', 'country', 'training_provider', 'completed_on', 'valid_from',
                    'valid_until', 'created_by_record', 'last_updated_by', 'created_by')


@admin.register(DelegateCertificate)
class DelegateCertificateAdmin(ModelAdmin):
    list_display = ('certificate_code', 'delegate')


@admin.register(Search)
class SearchAdmin(ModelAdmin):
    list_display = ('created', 'created_by', 'delegate')
