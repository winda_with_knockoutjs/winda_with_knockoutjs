# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trainings', '0005_auto_20160703_1700'),
    ]

    operations = [
        migrations.CreateModel(
            name='UploadBatch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('description', models.CharField(max_length=100, verbose_name='description')),
                ('status', models.CharField(max_length=20, choices=[('draft', 'draft'), ('discarded', 'discarded'), ('purchased', 'purchased'), ('invalidation', 'invalidation')], verbose_name='status')),
                ('purchased_at', models.DateTimeField(blank=True, null=True)),
                ('discarded_at', models.DateTimeField(blank=True, null=True)),
                ('total_records', models.PositiveSmallIntegerField(verbose_name='total records')),
                ('valid_records', models.PositiveSmallIntegerField(verbose_name='valid records')),
                ('duplicate_records', models.PositiveSmallIntegerField(verbose_name='duplicate records')),
                ('error_records', models.PositiveSmallIntegerField(verbose_name='error records')),
                ('created_by', models.ForeignKey(related_name='uploadbatch_created_by', default=1, to=settings.AUTH_USER_MODEL)),
                ('discarded_by', models.ForeignKey(related_name='discarded_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
                ('last_updated_by', models.ForeignKey(related_name='uploadbatch_last_updated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
                ('purchased_by', models.ForeignKey(related_name='purchased_by', to=settings.AUTH_USER_MODEL, blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Upload Batches',
                'verbose_name': 'Upload Batch',
            },
        ),
        migrations.CreateModel(
            name='UploadBatchRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('source_row_number', models.PositiveSmallIntegerField(verbose_name='source row number')),
                ('delegate_id', models.CharField(max_length=8, blank=True, verbose_name='delegate id')),
                ('course_code', models.CharField(max_length=8, blank=True, verbose_name='course code')),
                ('course_completion_date', models.DateField(blank=True, null=True, verbose_name='course completion date')),
                ('previous_course_valid_until', models.DateField(blank=True, null=True, verbose_name='previous course valid until')),
                ('calculated_valid_from', models.DateField(blank=True, null=True, verbose_name='calculated valid from')),
                ('calculated_valid_until', models.DateField(blank=True, null=True, verbose_name='calculated valid until')),
                ('parse_check', models.SmallIntegerField(choices=[(0, 'Parsed okay'), (1, 'Missing columns (accept extra)'), (2, 'Missing delegate ID (empty)'), (4, 'Badly formatted delegate ID (wider than 8 characters)'), (8, 'Missing course code (empty)'), (16, 'Badly formatted course code (wider than 8 characters)'), (32, 'Missing course completion date (empty)'), (64, "Badly formatted course completion date (can't convert or wider than 10 characters)"), (128, "Badly formatted previous course valid until date (can't convert or wider than 10 characters)")], verbose_name='parse check')),
                ('validation_check', models.SmallIntegerField(choices=[(0, 'Valid'), (1, 'Course completion date is in future'), (2, 'Unknown course code'), (4, 'Not accredited for course'), (8, 'Unknown delegate ID'), (16, 'Delegate ID has been archived'), (32, 'Previous course valid until date is required'), (64, 'Previous course valid until date is in future'), (128, 'No matching previous course training record found')], verbose_name='validation check')),
                ('duplicate_check', models.SmallIntegerField(choices=[(0, 'Not a duplicate'), (1, 'Duplicate entry within upload (same record appears multiple times in upload)'), (2, 'Duplicate of existing training record'), (4, 'No matching training record found to invalidate')], verbose_name='duplicate check')),
                ('upload_batch', models.ForeignKey(to='trainings.UploadBatch', verbose_name='upload batch')),
            ],
            options={
                'verbose_name_plural': 'Upload Batch Records',
                'verbose_name': 'Upload Batch Record',
            },
        ),
        migrations.AlterField(
            model_name='trainingprovider',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name='is active'),
        ),
        migrations.AddField(
            model_name='uploadbatch',
            name='training_provider',
            field=models.ForeignKey(related_name='training_provider_uploadbatch', to='trainings.TrainingProvider'),
        ),
    ]
