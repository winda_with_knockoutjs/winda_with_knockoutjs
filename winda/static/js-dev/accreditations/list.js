$(document).ready(function() {
    initializeDataTable('table#database_admin_accreditation', {
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "course__code"},
            { "data": "course__title" },
            { "data": "valid_from" },
            { "data": "valid_until" },
            { "data": "is_deleted"}
        ]
    });
});
