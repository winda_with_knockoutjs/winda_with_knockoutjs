# -*- coding: utf-8 -*-
from django.views import generic
from reversion.views import RevisionMixin

class CreateView(RevisionMixin, generic.CreateView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class UpdateView(RevisionMixin, generic.UpdateView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs
