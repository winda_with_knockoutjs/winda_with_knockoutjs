# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailnotification',
            name='created_by',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL, related_name='emailnotification_created_by'),
        ),
        migrations.AddField(
            model_name='emailnotification',
            name='recipient',
            field=models.ForeignKey(default=1, verbose_name='User to send this email to', to=settings.AUTH_USER_MODEL, related_name='recipient'),
        ),
        migrations.AddField(
            model_name='country',
            name='created_by',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL, related_name='country_created_by'),
        ),
        migrations.AddField(
            model_name='country',
            name='last_updated_by',
            field=models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL, related_name='country_last_updated_by'),
        ),
    ]
