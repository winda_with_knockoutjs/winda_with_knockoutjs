# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0004_auto_20160714_1149'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='last_updated_by',
            field=models.ForeignKey(related_name='transaction_last_updated_by', to=settings.AUTH_USER_MODEL, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='order',
            field=models.ForeignKey(to='orders.Order', blank=True, null=True, verbose_name='order'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='reason',
            field=models.CharField(blank=True, verbose_name='reason', max_length=200),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='upload_batch',
            field=models.ForeignKey(to='trainings.UploadBatch', blank=True, null=True, verbose_name='upload batch'),
        ),
    ]
