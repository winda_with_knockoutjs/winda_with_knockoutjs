# -*- coding: utf-8 -*-
from itertools import groupby

from allauth.utils import build_absolute_uri
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

from common.utils import get_protocol

User = settings.AUTH_USER_MODEL


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    @staticmethod
    def archived_from_default():
        return timezone.now() + relativedelta(months=12)

    @staticmethod
    def expires_at_default():
        return timezone.now() + timezone.timedelta(hours=settings.REGISTRATION_EXPIRY_HOURS)

    @staticmethod
    def lost_password_expires_at_default():
        return timezone.now() + timezone.timedelta(hours=settings.PASSWORD_RESET_EXPIRY_HOURS)

    @staticmethod
    def user_expires_at_default():
        return timezone.datetime(2099, 12, 31, tzinfo=timezone.UTC())


class AddressAbstractModel(TimeStampedModel):
    country = models.ForeignKey('Country', verbose_name=_('country'), null=True, blank=True)
    address_1 = models.CharField(_('street address line 1'), max_length=50, blank=True)
    address_2 = models.CharField(_('street address line 2'), max_length=50, blank=True)
    city = models.CharField(_('city'), max_length=50, blank=True)
    postal_code = models.CharField(_('postal code'), max_length=20, blank=True)

    class Meta:
        abstract = True


class Country(TimeStampedModel):
    name = models.CharField(max_length=50, unique=True)
    code3 = models.CharField(max_length=3, null=True, blank=True, unique=True)
    last_updated_by = models.ForeignKey(User, null=True, blank=True, related_name='%(class)s_last_updated_by')
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')

    def __str__(self):
        return self.name


class DelegateSequence(models.Model):
    sort_order = models.PositiveSmallIntegerField(_('sort order'))
    identifier = models.CharField(_('delegate id'), unique=True, max_length=8)
    sequential_prefix = models.PositiveIntegerField(_('sequential prefix'), unique=True)
    random_suffix = models.PositiveIntegerField(_('random suffix'))

    class Meta:
        unique_together = ('sort_order', 'identifier')
        verbose_name = _('Delegate Sequence')
        verbose_name_plural = _('Delegate Sequences')

    def __str__(self):
        return self.identifier


class EmailNotification(TimeStampedModel):
    BOUNCE, COMPLAINT, DELIVERY = 'Bounce', 'Complaint', 'Delivery'

    EMAIL_STATUS_CHOICE = (
        (BOUNCE, 'Bounce'),
        (COMPLAINT, 'Complaint'),
        (DELIVERY, 'Delivery')
    )

    recipient = models.ForeignKey(User, verbose_name=_('User to send this email to'),
                                  default=settings.SYSTEM_USER_ID, related_name='recipient')
    subject = models.CharField(_('subject'), max_length=200)
    body = models.TextField(_('body'))
    created_by = models.ForeignKey(User, default=settings.SYSTEM_USER_ID, related_name='%(class)s_created_by')
    posted_at = models.DateTimeField(_('posted at'), null=True, blank=True)
    ses_message_id = models.CharField(_('ses message id'), max_length=200, blank=True, db_index=True)
    notified_at = models.DateTimeField(_('notified at'), null=True, blank=True)
    notification_type = models.CharField(_('Notification Type'), max_length=10, choices=EMAIL_STATUS_CHOICE, blank=True)
    bounce_type = models.CharField(_('Bounce Type'), max_length=20, blank=True)
    bounce_sub_type = models.CharField(_('Bounce Sub Type'), max_length=20, blank=True)
    complaint_type = models.CharField(_('Complaint Type'), max_length=20, blank=True)
    archived_from = models.DateTimeField(_('archived from'), default=TimeStampedModel.archived_from_default)
    archived_at = models.DateTimeField(_('archived at'), null=True, blank=True)

    def __str__(self):
        return self.subject

    def send(self):
        msg = EmailMessage(self.subject, self.body, settings.DEFAULT_FROM_EMAIL, [self.recipient.email])
        msg.send()
        return msg

    @classmethod
    def create_notification(cls, recipient, template_name, context, commit=True):
        subject_template = 'emails/{}/subject.html'.format(template_name)
        message_template = 'emails/{}/body.html'.format(template_name)
        subject = render_to_string(subject_template, context).replace('\n', '').replace('\r', '').replace('\t', ' ').strip()
        prefix = settings.ACCOUNT_EMAIL_SUBJECT_PREFIX
        body = render_to_string(message_template, context)
        notification = cls(recipient=recipient, subject=prefix + subject, body=body)
        if commit:
            notification.save()
        return notification

    @classmethod
    def delegate_id_notification(cls, request, user):
        login_url = build_absolute_uri(request, force_text(settings.LOGIN_REDIRECT_URL))
        ctx = dict(user=user, login_url=login_url)
        EmailNotification.create_notification(user, 'create_account', ctx).send()

    @classmethod
    def new_training_provider(cls, request, user):
        recipients = get_user_model().objects.database_admins()
        ctx = dict(user=user)
        notifications = [EmailNotification.create_notification(recipient, 'new_training_provider', ctx, commit=False)
                         for recipient in recipients]
        EmailNotification.objects.bulk_create(notifications)

    @classmethod
    def training_provider_accepted(cls, request, user):
        login_url = build_absolute_uri(request, force_text(settings.LOGIN_REDIRECT_URL))
        ctx = dict(user=user, login_url=login_url)
        EmailNotification.create_notification(user, 'training_provider_accepted', ctx)

    @classmethod
    def password_reset(cls, request, user, lost_password):
        path = reverse('account_reset_password_from_key', args=(lost_password.activation_code,))
        password_reset_url = build_absolute_uri(request, path)
        ctx = dict(user=user, password_reset_url=password_reset_url)
        EmailNotification.create_notification(user, 'password_reset', ctx).send()

    @classmethod
    def user_account_created(cls, request, user, password):
        login_url = build_absolute_uri(request, force_text(settings.LOGIN_REDIRECT_URL))
        ctx = dict(user=user, login_url=login_url, password=password)
        EmailNotification.create_notification(user, 'user_account_created', ctx).send()

    @classmethod
    def order(cls, user, order):
        ctx = dict(order=order, user=user)
        EmailNotification.create_notification(user, 'order', ctx).send()

    @classmethod
    def mailout(cls, user, subject, body):
        context = {'user': user, 'body': body}
        message_template = 'emails/mailout/body.html'
        subject = subject.replace('\n', '').replace('\r', '').replace('\t', ' ').strip()
        prefix = settings.ACCOUNT_EMAIL_SUBJECT_PREFIX
        body = render_to_string(message_template, context)
        return cls(recipient=user, subject=prefix + subject, body=body)

    @classmethod
    def generate_notification_mail(cls, users, template):
        notifications = []
        for user in users:
            login_url = '{proto}://{domain}{url}'.format(proto=get_protocol(), domain=Site.objects.get_current().domain,
                                                         url=force_text(settings.LOGIN_REDIRECT_URL))
            ctx = dict(user=user, login_url=login_url)
            notifications.append(EmailNotification.create_notification(user, template, ctx, commit=False))
        EmailNotification.objects.bulk_create(notifications)

    @classmethod
    def training_records_expiring_soon(cls, users):
        cls.generate_notification_mail(users, 'training_records_expiring_soon')

    @classmethod
    def training_records_expired(cls, users):
        cls.generate_notification_mail(users, 'training_records_expired')

    @classmethod
    def delegate_account_expiring_soon(cls, users):
        cls.generate_notification_mail(users, 'delegate_account_expiring_soon')

    @classmethod
    def delegate_account_expired(cls, users):
        cls.generate_notification_mail(users, 'delegate_account_expired')

    @classmethod
    def training_provider_certificates_expiring_soon(cls, users, training_providers):
        notifications = []
        for user in users:
            for training_provider in training_providers:
                ctx = dict(user=user, training_provider=training_provider)
                notifications.append(EmailNotification.create_notification(
                    user, 'training_provider_certificates_expiring_soon', ctx, commit=False))
        EmailNotification.objects.bulk_create(notifications)

    @classmethod
    def organization_account_expiring_soon(cls, users):
        cls.generate_notification_mail(users, 'organization_account_expiring_soon')

    @classmethod
    def organization_account_expired(cls, users):
        cls.generate_notification_mail(users, 'organization_account_expired')

    @classmethod
    def new_training_records(cls, users, training_records):
        training_records.sort(key=lambda training: training.delegate_id)
        records = groupby(training_records, lambda training: training.delegate_id)
        current_delegates = {user.pk: user for user in users}
        notifications = []
        for pk, training_records in records:
            user = current_delegates[pk]
            ctx = dict(user=user, training_records=training_records)
            notifications.append(EmailNotification.create_notification(user, 'new_training_records', ctx, commit=False))
        EmailNotification.objects.bulk_create(notifications)
