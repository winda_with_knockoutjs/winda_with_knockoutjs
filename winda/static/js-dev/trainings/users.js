$(document).ready(function() {
    initializeDataTable('table#training_provider_user', {
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "first_name"},
            { "data": "last_name" },
            { "data": "email" },
            { "data": "role" },
            { "data": "is_active" },
            { "data": "can_upload" },
            { "data": "can_use_credit" },
            { "data": "can_buy_credit" }
        ],
        "order": [[ 3, 'asc' ]],
       'columnDefs': [{
           'targets': 1,
           'searchable':false,
           'orderable':false
       }]
    });
});

