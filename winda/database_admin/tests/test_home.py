# -*- coding: utf-8 -*-
from datetime import timedelta

from django.conf import settings
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from users.factories import UserFactory, TEST_USER_PASSWORD, DatabaseAdminFactory
from trainings.factories import TrainingProviderFactory
from orders.models import Transaction


class DatabaseAdminViewsTestCase(TestCase):

    def setUp(self):
        self.url = reverse('home')
        self.user_count = get_user_model().objects.count()
        self.user = UserFactory.create(role=get_user_model().DATABASE_ADMIN)

    def test_home(self):
        url = reverse('database_admin:home')
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertRedirects(response, url)

    def test_award_credits(self):
        db_admin = DatabaseAdminFactory.create()
        tprovider = TrainingProviderFactory.create()
        url = reverse('database_admin:training_provider_award_credits', kwargs={'pk': tprovider.pk})
        self.client.login(username=db_admin, password=TEST_USER_PASSWORD)

        data = {}

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)

        data = {
            'transaction_value': -200000,
            'reason': 'test'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)

        data['transaction_value'] = 10
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Transaction.objects.filter(
            training_provider=tprovider,
            transaction_value=10,
            reason='test'
        ).exists())


class DatabaseAdminReportsTestCase(TestCase):

    def setUp(self):
        self.reports_url = reverse('database_admin:reports')
        self.search_report_url = reverse('database_admin:search_report')
        self.payment_report_url = reverse('database_admin:payment_report')
        self.training_record_report_url = reverse('database_admin:training_record_report')
        self.training_statistics_report_url = reverse('database_admin:training_statistics_report')
        self.database_growth_report_url = reverse('database_admin:database_growth_report')
        self.user = DatabaseAdminFactory.create()

    def test_reports_page(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        response = self.client.get(self.reports_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Reports')

    def test_search_report_url(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        data = {
            'date_from': timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT),
            'date_until': (timezone.now() + timedelta(days=2)).strftime(settings.TEMPLATE_DATE_FORMAT)
        }
        response = self.client.post(self.search_report_url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_payment_report_url(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        data = {
            'date_from': timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT),
            'date_until': (timezone.now() + timedelta(days=2)).strftime(settings.TEMPLATE_DATE_FORMAT)
        }
        response = self.client.post(self.payment_report_url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_training_record_report_url(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        data = {
            'date_from': timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT),
            'date_until': (timezone.now() + timedelta(days=2)).strftime(settings.TEMPLATE_DATE_FORMAT)
        }
        response = self.client.post(self.training_record_report_url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_training_statistics_report_url(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        data = {
            'date_from': timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT),
            'date_until': (timezone.now() + timedelta(days=2)).strftime(settings.TEMPLATE_DATE_FORMAT)
        }
        response = self.client.post(self.training_statistics_report_url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_database_growth_report_url(self):
        self.client.login(username=self.user, password=TEST_USER_PASSWORD)
        data = {
            'date_from': timezone.now().strftime(settings.TEMPLATE_DATE_FORMAT),
            'date_until': (timezone.now() + timedelta(days=2)).strftime(settings.TEMPLATE_DATE_FORMAT)
        }
        response = self.client.post(self.database_growth_report_url, data=data)
        self.assertEqual(response.status_code, 200)

