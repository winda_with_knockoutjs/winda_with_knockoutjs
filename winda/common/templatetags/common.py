# -*- coding: utf-8 -*-
from django import template
from django.core.urlresolvers import reverse

from registration.models import Registration

register = template.Library()


@register.filter()
def get_registration_type_display(reg_type):
    return dict(Registration.REGISTRATION_TYPE_CHOICE).get(reg_type, '')


@register.filter()
def get_confirmation_url(registration_type, activation_code):
    return reverse('account_confirm_email',
                   args=[Registration.REGISTRATION_URLS[registration_type],
                         activation_code])


@register.filter()
def split(value, delimiter=' '):
    return value.split(delimiter)
