# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import yesno
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView, ListView, DetailView, FormView

from skd_tools.mixins import ActiveTabMixin
from reversion.models import Revision, Version
import reversion

from courses.models import Course, CourseProvider
from courses.forms import AdminCourseCreateForm, AdminCourseEditForm
from common.models import EmailNotification, Country
from common.mixins import DatabaseAdminAccessMixin, FilterMixin, ParentObjectMixin, UserMixin
from core.views import CreateView, UpdateView
from database_admin.forms import (
    TrainingProviderFilterForm, TrainingProviderForm, CoursesFilterForm, CertificatesFilterForm,
    EmailNotificationFilterForm, MailoutForm, TrainingProviderCertificateForm, AccreditationFilterForm,
    AccreditationForm, TrainingRecordReportForm, SearchReportForm, PaymentReportForm, TrainingStatisticsReportForm,
    ChangeHistoryFilterForm, DatabaseGrowthReportForm, UserFilterForm, DelegateUserForm,
    OrganizationUserForm, TrainingProviderUserForm)
from trainings.models import TrainingProvider, TrainingProviderCertificate
from orders.forms import AwardCreditForm


User = get_user_model()


class DatabaseAdminHomeView(DatabaseAdminAccessMixin, TemplateView):
    template_name = 'database_admin/home.html'


class CoursesListView(DatabaseAdminAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    template_name = 'database_admin/courses_list.html'
    model = Course
    context_object_name = 'courses'
    form_class = CoursesFilterForm
    active_tab = 'courses'

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'code': '<a href="{0}">{1}</a>'.format(obj.get_absolute_url(), obj.code),
            'title': obj.title,
            'course_type': obj.get_course_type_display().title(),
            'validity_months': obj.validity_months
        } for obj in object_list]


class CourseCreateView(DatabaseAdminAccessMixin, ActiveTabMixin, CreateView):

    model = Course
    form_class = AdminCourseCreateForm
    template_name = 'database_admin/course_create.html'
    active_tab = 'courses'
    success_url = reverse_lazy('database_admin:courses')


class CourseUpdateView(DatabaseAdminAccessMixin, ActiveTabMixin, UpdateView):

    model = Course
    form_class = AdminCourseEditForm
    template_name = 'database_admin/course_edit.html'
    active_tab = 'courses'
    success_url = reverse_lazy('database_admin:courses')


class TrainingProviderListView(DatabaseAdminAccessMixin, ActiveTabMixin, FilterMixin, ListView):
    model = TrainingProvider
    form_class = TrainingProviderFilterForm
    template_name = 'database_admin/training_providers.html'
    active_tab = 'training_provider'

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'pk': obj.pk,
            'name': '<a href="{}">{}</a>'
                .format(reverse('database_admin:training_provider_detail', args=(obj.pk,)), obj.name),
            'country': obj.country.name if obj.country else '',
            'postal_code': obj.postal_code,
            'is_active': yesno(obj.is_active),
            'accepted_at': obj.accepted_at.strftime(settings.TEMPLATE_DATETIME_FORMAT) if obj.accepted_at else ''
        } for obj in object_list]


class TrainingProviderDetailView(DatabaseAdminAccessMixin, ActiveTabMixin, DetailView):
    template_name = 'database_admin/training_providers_detail.html'
    active_tab = 'training_provider'
    model = TrainingProvider


class TrainingProviderCreateView(DatabaseAdminAccessMixin, ActiveTabMixin, CreateView):
    template_name = 'database_admin/training_providers_form.html'
    active_tab = 'training_provider'
    model = TrainingProvider
    form_class = TrainingProviderForm
    success_url = reverse_lazy('database_admin:training_providers')

    def form_valid(self, form):
        self.object = form.save(commit=True)
        self.object.is_active = True
        self.object.accepted_at = timezone.now()
        self.object.accepted_by = self.request.user
        for user in self.object.user_users.all():
            EmailNotification.training_provider_accepted(self.request, user)
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class TrainingProviderUpdateView(DatabaseAdminAccessMixin, ActiveTabMixin, UpdateView):
    template_name = 'database_admin/training_providers_form.html'
    active_tab = 'training_provider'
    model = TrainingProvider
    form_class = TrainingProviderForm
    success_url = reverse_lazy('database_admin:training_providers')

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if 'reject' in self.request.POST:
            self.object.delete()
            return HttpResponseRedirect(self.get_success_url())
        return response

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if 'accept' in self.request.POST and not self.object.accepted_at:
            self.object.is_active = True
            self.object.accepted_at = timezone.now()
            self.object.accepted_by = self.request.user
            for user in self.object.user_users.all():
                EmailNotification.training_provider_accepted(self.request, user)
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class CertificatesListView(DatabaseAdminAccessMixin, ActiveTabMixin, FilterMixin, ParentObjectMixin, ListView):

    active_tab = 'training_provider'
    template_name = 'database_admin/certifications_list.html'
    model = TrainingProviderCertificate
    parent_model = TrainingProvider
    form_class = CertificatesFilterForm

    def get_queryset(self):
        if not self.parent_object:
            self.parent_object = self.get_parent_object()
        return self.model.objects.filter(training_provider_id=self.parent_object.id)

    def get_filtered_data(self, object_list):
        return [{
            '':'',
            'certificate_number': '<a href="{0}">{1}</a>'.format(obj.get_dba_absolute_url(), obj.certificate_number),
            'valid_from': obj.valid_from.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_from else '',
            'valid_until': obj.valid_until.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_until else '',
            'certificate_file': '<a href="{0}">Download Certificate</a>'.format(obj.get_certificate_url()),
            'is_deleted': 'Yes' if obj.is_deleted else 'No'
        } for obj in object_list]


class CertificateCreateView(DatabaseAdminAccessMixin, ActiveTabMixin, ParentObjectMixin, CreateView):

    active_tab = 'training_provider'
    template_name = 'database_admin/certifications_form.html'
    form_class = TrainingProviderCertificateForm
    model = TrainingProviderCertificate
    parent_model = TrainingProvider

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.training_provider = self.parent_object
        return super(CertificateCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('database_admin:training_provider_certifications',
                            kwargs={'parent_pk': self.parent_object.pk})


class CertificateUpdateView(DatabaseAdminAccessMixin, ActiveTabMixin, ParentObjectMixin, UpdateView):

    active_tab = 'training_provider'
    template_name = 'database_admin/certifications_form.html'
    form_class = TrainingProviderCertificateForm
    model = TrainingProviderCertificate
    parent_model = TrainingProvider

    def get_success_url(self):
        return reverse_lazy('database_admin:training_provider_certifications',
                            kwargs={'parent_pk': self.parent_object.pk})


class AccreditationListView(DatabaseAdminAccessMixin, ActiveTabMixin, FilterMixin, ParentObjectMixin, ListView):

    active_tab = 'training_provider'
    template_name = 'database_admin/accreditation_list.html'
    model = CourseProvider
    parent_model = TrainingProvider
    form_class = AccreditationFilterForm

    def get_queryset(self):
        if not self.parent_object:
            self.parent_object = self.get_parent_object()
        return self.model.objects.filter(training_provider_id=self.parent_object.id).select_related('course')

    def get_filtered_data(self, object_list):
        return [{
            '':'',
            'course__code': '<a href="{0}">{1}</a>'.format(obj.get_dba_absolute_url(), obj.course.code),
            'course__title': obj.course.title,
            'valid_from': obj.valid_from.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_from else '',
            'valid_until': obj.valid_until.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.valid_until else '',
            'is_deleted': 'Yes' if obj.is_deleted else 'No'
        } for obj in object_list]


class AccreditationCreateView(DatabaseAdminAccessMixin, ActiveTabMixin, ParentObjectMixin, CreateView):

    active_tab = 'training_provider'
    template_name = 'database_admin/accreditation_form.html'
    form_class = AccreditationForm
    model = CourseProvider
    parent_model = TrainingProvider

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.training_provider = self.parent_object
        return super(AccreditationCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('database_admin:training_provider_accreditations',
                            kwargs={'parent_pk': self.parent_object.pk})


class AccreditationUpdateView(DatabaseAdminAccessMixin, ActiveTabMixin, ParentObjectMixin, UpdateView):

    active_tab = 'training_provider'
    template_name = 'database_admin/accreditation_form.html'
    form_class = AccreditationForm
    model = CourseProvider
    parent_model = TrainingProvider

    def get_success_url(self):
        return reverse_lazy('database_admin:training_provider_accreditations',
                            kwargs={'parent_pk': self.parent_object.pk})


class DatabaseAdminAwardCreditView(DatabaseAdminAccessMixin, ActiveTabMixin, ParentObjectMixin, CreateView):

    parent_model = TrainingProvider
    parent_slug = 'pk'
    form_class = AwardCreditForm
    template_name = 'database_admin/award_credits.html'
    active_tab = 'training_provider'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['training_provider'] = self.parent_object
        return kwargs

    def get_success_url(self):
        return reverse_lazy('database_admin:training_provider_detail',
                            kwargs={'pk': self.parent_object.pk})


class DatabaseAdminReportsView(DatabaseAdminAccessMixin, ActiveTabMixin, TemplateView):

    template_name = 'database_admin/reports_page.html'
    active_tab = 'report'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['search_report_form'] = SearchReportForm
        context['payment_report_form'] = PaymentReportForm
        context['training_statistics_report_form'] = TrainingStatisticsReportForm
        context['training_records_report_form'] = TrainingRecordReportForm
        context['database_growth_report_form'] = DatabaseGrowthReportForm
        context['search_report_form_active'] = True
        return context


class ReportView(ActiveTabMixin, FormView):

    success_url = reverse_lazy('database_admin:reports')
    template_name = 'database_admin/reports_page.html'
    active_tab = 'report'
    context_base_form = ''

    def form_valid(self, form):
        response = form.save()
        return response

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['search_report_form'] = SearchReportForm
        context['payment_report_form'] = PaymentReportForm
        context['training_statistics_report_form'] = TrainingStatisticsReportForm
        context['training_records_report_form'] = TrainingRecordReportForm
        context['database_growth_report_form'] = DatabaseGrowthReportForm
        # Replace default blank form with form with errors
        if 'form' in context:
            context[self.context_base_form] = context['form']
            context['{}_active'.format(self.context_base_form)] = True
        else:
            context['search_report_form_active'] = True
        return context


class TrainingRecordReportView(DatabaseAdminAccessMixin, ReportView):

    form_class = TrainingRecordReportForm
    context_base_form = 'training_records_report_form'


class SearchReportView(DatabaseAdminAccessMixin, ReportView):

    form_class = SearchReportForm
    context_base_form = 'search_report_form'


class PaymentReportView(DatabaseAdminAccessMixin, ReportView):

    form_class = PaymentReportForm
    context_base_form = 'payment_report_form'


class TrainingStatisticsReportView(DatabaseAdminAccessMixin, ReportView):

    form_class = TrainingStatisticsReportForm
    context_base_form = 'training_statistics_report_form'


class DatabaseGrowthReportView(DatabaseAdminAccessMixin, ReportView):

    form_class = DatabaseGrowthReportForm
    context_base_form = 'database_growth_report_form'


class NotificationHistory(DatabaseAdminAccessMixin, FilterMixin, ListView):
    model = EmailNotification
    form_class = EmailNotificationFilterForm
    template_name = 'database_admin/notification_history.html'

    def get_queryset(self):
        self.user = get_object_or_404(User, pk=self.kwargs['pk'])
        return super().get_queryset().filter(recipient_id=self.user.pk)

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': obj.pk,
            'subject': obj.subject,
            'created': obj.created.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.created else '',
            'posted_at': obj.posted_at.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.posted_at else '',
            'notified_at': obj.notified_at.strftime(settings.TEMPLATE_DATE_FORMAT) if obj.notified_at else '',
            'notification_type': obj.get_notification_type_display()
        } for obj in object_list]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['winda_user'] = self.user
        return context


class MailoutCreateView(DatabaseAdminAccessMixin, ActiveTabMixin, CreateView):
    model = EmailNotification
    form_class = MailoutForm
    template_name = 'database_admin/mailout.html'
    active_tab = 'mailout'
    success_url = reverse_lazy('database_admin:mailout')

    def form_valid(self, form):
        cd = form.cleaned_data
        queryset = User.objects.active().exclude(pk=self.request.user.pk)
        keys = {
            'role': Q(role__in=cd['role']),
            'country': Q(country=cd['country'], role__in=[User.ORGANIZATION, User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN]),
            'training_provider': Q(delegate_training_records__training_provider=cd['training_provider']),
            'course': Q(delegate_training_records__course=cd['course']),
            'completed_on_start': Q(delegate_training_records__completed_on__gte=cd['completed_on_start']),
            'completed_on_end': Q(delegate_training_records__completed_on__lte=cd['completed_on_end']),
        }

        for field, value in form.cleaned_data.items():
            if field in keys and value:
                queryset = queryset.filter(keys[field])

        notifications = []
        for user in queryset.distinct():
            notifications.append(EmailNotification.mailout(user, cd['subject'], cd['body']))

        if notifications:
            EmailNotification.objects.bulk_create(notifications)
            self.get_success_message()
        else:
            self.get_not_successful_message()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_message(self):
        messages.add_message(self.request, messages.SUCCESS, _('Emails were sent successfully.'))

    def get_not_successful_message(self):
        messages.add_message(self.request, messages.WARNING, _('Recipients list is empty, please, add emails.'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['default_from_email'] = settings.DEFAULT_FROM_EMAIL
        context['prefix'] = settings.ACCOUNT_EMAIL_SUBJECT_PREFIX
        return context

    def get_success_url(self):
        return self.success_url


class TrainingProviderChangeHistoryView(DatabaseAdminAccessMixin, ActiveTabMixin, FilterMixin, ListView):

    origin_model = TrainingProvider
    model = Revision
    template_name = 'database_admin/training_provider_history.html'
    active_tab = 'training_provider'
    form_class = ChangeHistoryFilterForm
    fields_mapping = {
        'refresher_for_course_id': ('refresher_for_course', Course, 'code'),
        'course_id': ('course', Course, 'code'),
        'country_id': ('country', Country, 'name'),
        'training_provider_id': ('training_provider', TrainingProvider, 'name'),
    }
    detail_fields = ['name', 'address_1', 'address_2', 'city', 'postal_code', 'country', 'is_active']

    def dispatch(self, request, *args, **kwargs):
        self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        revisions = Version.objects.filter(content_type_id=self.origin_content_type.id,
                                           object_id=self.object.id).values_list('revision_id', flat=True)
        return Revision.objects.filter(id__in=revisions).distinct()

    def get_object(self):
        self.object = get_object_or_404(self.origin_model, pk=self.kwargs['pk'])
        self.origin_content_type = ContentType.objects.get_for_model(self.origin_model)
        return self.object

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['object'] = self.object
        return context

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': obj.id,
            'date_created': obj.date_created.strftime(settings.TEMPLATE_DATETIME_FORMAT) if obj.date_created else '',
            'user': obj.user.get_full_name() if obj.user_id else '',
            'comment': obj.comment,
            'detail': self.get_obj_repr(obj),
        } for obj in object_list]

    def get_obj_repr(self, revision):
        representation =revision.version_set.filter(content_type_id=self.origin_content_type.id, object_id=self.object.id).first()
        if representation:
            result = []
            for field, value in representation.field_dict.items():
                if field in self.detail_fields:
                    result.append('{0}: {1}'.format(field, value))
                else:
                    mapping = self.fields_mapping.get(field)
                    if mapping and mapping[0] in self.detail_fields:
                        if value:
                            value = getattr(mapping[1].objects.get(id=value), mapping[2])
                        result.append('{0}: {1}'.format(mapping[0], value))
            return mark_safe('<br>'.join(result))
        else:
            return ''


class CourseChangeHistoryView(TrainingProviderChangeHistoryView):

    origin_model = Course
    template_name = 'database_admin/course_change_history.html'
    detail_fields = ['code', 'title', 'course_type', 'refresher_for_course', 'require_existing_training_record',
                     'validity_months']


class CertificateChangeHistoryView(ParentObjectMixin, TrainingProviderChangeHistoryView):

    origin_model = TrainingProviderCertificate
    template_name = 'database_admin/certification_change_history.html'
    parent_model = TrainingProvider
    detail_fields = ['training_provider', 'certificate_number', 'ceritificate_file', 'valid_from', 'valid_until',
                     'is_deleted']


class AccreditationChangeHistoryView(ParentObjectMixin, TrainingProviderChangeHistoryView):

    origin_model = CourseProvider
    template_name = 'database_admin/accreditation_change_history.html'
    parent_model = TrainingProvider
    detail_fields = ['course', 'valid_from', 'valid_until', 'is_deleted']


class UsersChangeHistoryView(TrainingProviderChangeHistoryView):

    origin_model = User
    template_name = 'database_admin/users_change_history.html'
    total_fields = ['id', 'role', 'archived_at', 'first_name', 'last_name', 'email', 'email_status', 'is_active',
                    'delegate_id', 'archived_from', 'archived_at', 'job_title', 'organization_name', 'training_provider',
                    'phone', 'timezone', 'country', 'address_1', 'address_2', 'city', 'postal_code', 'can_upload',
                    'can_buy_credit', 'can_use_credit']
    delegate_fields = ['id', 'role', 'archived_at', 'first_name', 'last_name', 'email', 'email_status', 'is_active',
                       'delegate_id']
    organization_fields = ['id', 'role',  'archived_from', 'archived_at', 'first_name', 'last_name', 'email',
                           'email_status', 'is_active', 'job_title', 'phone', 'organization_name', 'country',
                           'address_1', 'address_2', 'city', 'postal_code']
    training_provider_fields = ['id', 'role',  'archived_from', 'archived_at', 'first_name', 'last_name', 'email',
                                'email_status', 'is_active', 'training_provider', 'can_upload', 'can_buy_credit',
                                'can_use_credit']
    active_tab = 'users'

    def get_object(self):
        obj = super().get_object()
        if obj.is_delegate:
            self.detail_fields = self.delegate_fields
        elif obj.is_organization:
            self.detail_fields = self.organization_fields
        elif obj.is_training_provider_admin or obj.is_training_provider_user:
            self.detail_fields = self.training_provider_fields
        else:
            self.detail_fields = self.total_fields
        return obj


class UserListView(DatabaseAdminAccessMixin, UserMixin, FilterMixin, ListView):
    model = User
    form_class = UserFilterForm
    template_name = 'database_admin/users.html'

    def get_filtered_queryset(self, queryset):
        search = self.filter_data.get('search')
        if search:
            queryset = queryset.filter(Q(id__iexact=search) | Q(first_name__icontains=search) |
                                       Q(last_name__icontains=search) |
                                       Q(email__icontains=search) |
                                       Q(delegate_id__iexact=search) | Q(organization_name__icontains=search) |
                                       Q(training_provider__name__icontains=search))
        return super().get_filtered_queryset(queryset)

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': '<a href="{}">{}</a>'
                .format(reverse('database_admin:user_detail', args=(obj.pk,)), obj.pk),
            'first_name': obj.first_name.title(),
            'last_name': obj.last_name.title(),
            'email': obj.email,
            'email_status': obj.get_email_status_display().title(),
            'role': obj.get_role_display(),
            'is_active': yesno(obj.is_active).title(),
            'last_login': obj.last_login.strftime(settings.TEMPLATE_DATETIME_FORMAT) if obj.last_login else '',
            'delegate_id': obj.delegate_id,
            'organization_name': obj.organization_name,
            'training_provider': getattr(obj.training_provider, 'name', ''),
        } for obj in object_list]


class UserDetailView(DatabaseAdminAccessMixin, UserMixin, UpdateView):
    model = User
    form_class = DelegateUserForm
    template_name = 'database_admin/user_detail.html'

    def post(self, request, *args, **kwargs):
        if 'archive' in request.POST:
            return self.archive()
        return super().post(request, *args, **kwargs)

    def get_form_class(self):
        if self.object.is_delegate:
            return DelegateUserForm
        elif self.object.is_organization:
            return OrganizationUserForm
        elif self.object.is_training_provider_admin or self.object.is_training_provider_user:
            return TrainingProviderUserForm
        raise Http404()

    def form_valid(self, form):
        self.get_success_message()
        return super().form_valid(form)

    def get_success_message(self):
        messages.add_message(self.request, messages.SUCCESS, _('User was successfully updated.'))

    def get_success_archive_message(self):
        messages.add_message(self.request, messages.SUCCESS, _('User was archived successfully.'))

    def get_success_url(self):
        return reverse_lazy('database_admin:user_detail', kwargs={'pk': self.object.pk})

    def archive(self):
        reversion.set_comment('Archived')
        reversion.set_user(self.request.user)
        self.object = self.get_object()
        self.object.archived_at = timezone.now()
        self.object.archived_by = self.request.user
        self.object.last_name = self.object.first_name = ugettext('ARCHIVED')
        self.object.email = self.object.username = ugettext('ARCHIVED {}'.format(self.object.pk))
        self.object.is_active = False
        self.object.save()
        self.get_success_archive_message()
        return HttpResponseRedirect(self.get_success_url())
