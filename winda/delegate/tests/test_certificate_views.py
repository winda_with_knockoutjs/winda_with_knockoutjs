# -*- coding: utf-8 -*-
import time
import os
from unittest import mock
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from weasyprint import HTML

from users.factories import TEST_USER_PASSWORD, DelegateFactory
from delegate.models import DelegateCertificate
from delegate.factories import TrainingRecordFactory


def write_pdf(*args, **kwargs):
    return b'certificate content'


class DelegateCertificateViewsTestCase(TestCase):

    def setUp(self):
        self.user_count = get_user_model().objects.count()
        self.user = DelegateFactory.create(delegate_id='test')
        self.url = reverse('delegate:generate-certificate', kwargs={'delegate_id': self.user.delegate_id})

    def tearDown(self):
        for certificate in DelegateCertificate.objects.all():
            if certificate.certificate_file and os.path.exists(certificate.certificate_file.path):
                certificate.certificate_file.delete()

    @mock.patch.object(HTML, 'write_pdf', write_pdf)
    def test_generate_certificate_view(self):
        now = timezone.now().date()
        TrainingRecordFactory.create_batch(4, delegate=self.user, valid_from=now, valid_until=now)

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 1)
        certificate = DelegateCertificate.objects.filter(delegate_id=self.user.id).latest('created')
        certificate_url = reverse('delegate-certificate', kwargs={'certificate_code': certificate.certificate_code})
        self.assertRedirects(response, certificate_url)

    @mock.patch.object(HTML, 'write_pdf', write_pdf)
    def test_regenerate_certificate_view(self):
        now = timezone.now().date()
        TrainingRecordFactory.create_batch(4, delegate=self.user, valid_from=now, valid_until=now)

        time.sleep(1)

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 1)
        certificate = DelegateCertificate.objects.filter(delegate_id=self.user.id).latest('created')
        certificate_url = reverse('delegate-certificate', kwargs={'certificate_code': certificate.certificate_code})
        self.assertRedirects(response, certificate_url)

        # Second time request
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 1)

        time.sleep(1)
        # Create new record and request certificate
        record = TrainingRecordFactory.create(delegate=self.user)
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 2)

        time.sleep(1)
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 2)

        time.sleep(1)
        record.invalidated_at = timezone.now()
        record.save()
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 3)

        time.sleep(1)
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 3)

    def test_no_records(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        TrainingRecordFactory.create_batch(4, delegate=self.user, invalidated_at=timezone.now())
        response = self.client.get(self.url)
        self.assertEqual(DelegateCertificate.objects.count(), 0)
        self.assertContains(response, 'No current training records exist for this delegate.')
