# -*- coding: utf-8 -*-
import json
from datetime import timedelta

from django.conf import settings
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from users.factories import UserFactory, TEST_USER_PASSWORD
from courses.factories import CourseFactory, CourseProviderFactory
from courses.models import CourseProvider
from trainings.factories import TrainingProviderFactory


class DatabaseAdminAccreditationsViewsTestCase(TestCase):

    def setUp(self):
        self.user = UserFactory.create(role=get_user_model().DATABASE_ADMIN)
        self.training_provider = TrainingProviderFactory.create()
        self.list_url = reverse('database_admin:training_provider_accreditations',
                                kwargs={'parent_pk': self.training_provider.pk})
        self.create_url = reverse('database_admin:training_provider_accreditations_create',
                                  kwargs={'parent_pk': self.training_provider.pk})

    def test_list(self):
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Accreditations')

    def test_filter(self):
        CourseProviderFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 0,
            'sort_asc': '',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_sort(self):
        CourseProviderFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': '1',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 10)
        self.assertEqual(len(result['data']), 10)
        self.assertEqual(result['recordsFiltered'], 10)

    def test_invalid_sort(self):
        CourseProviderFactory.create_batch(10, training_provider=self.training_provider)
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'draw': 1,
            'start': 0,
            'length': 10,
            'sort_column': 'hello',
            'sort_asc': '-',
        }
        response = self.client.get(self.list_url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        result = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(result['recordsTotal'], 0)
        self.assertEqual(len(result['data']), 0)
        self.assertEqual(result['recordsFiltered'], 0)

    def test_create_accreditation(self):
        course = CourseFactory.create()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'course': course.id,
            'valid_from': (timezone.now() + timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT),
            'valid_until': (timezone.now() + timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT)
        }

        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(CourseProvider.objects.count(), 0)

        data['valid_from'] = (timezone.now() + timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT)
        data['valid_until'] = (timezone.now() - timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT)

        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(CourseProvider.objects.count(), 0)

        data['valid_from'] = (timezone.now() - timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT)
        data['valid_until'] = (timezone.now() + timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT)
        response = self.client.post(self.create_url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(CourseProvider.objects.count(), 1)
        self.assertEqual(CourseProvider.objects.filter(course__code=course.code).count(), 1)
        self.assertEqual(CourseProvider.objects.filter(created_by=self.user.pk).count(), 1)

    def test_update_accreditation(self):
        course = CourseFactory.create()
        accreditation = CourseProviderFactory.create()
        url = accreditation.get_dba_absolute_url()
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = {
            'course': course.id,
            'valid_from': (timezone.now() + timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT),
            'valid_until': (timezone.now() + timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT),
            'is_deleted': False
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(CourseProvider.objects.filter(valid_from=accreditation.valid_from).count(), 1)

        data['valid_from'] = (timezone.now() + timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT)
        data['valid_until'] = (timezone.now() - timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT)

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(CourseProvider.objects.filter(valid_from=accreditation.valid_from).count(), 1)

        data['valid_from'] = (timezone.now() - timedelta(days=1)).strftime(settings.PARSE_DATE_FORMAT)
        data['valid_until'] = (timezone.now() + timedelta(days=2)).strftime(settings.PARSE_DATE_FORMAT)
        data['is_deleted'] = True
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(CourseProvider.objects.count(), 1)
        self.assertEqual(CourseProvider.objects.filter(valid_from=accreditation.valid_from).count(), 0)
        self.assertEqual(CourseProvider.objects.filter(is_deleted=True).count(), 1)
