# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView, ListView, DetailView, RedirectView
from quickpay_api_client.exceptions import ApiError
from skd_tools.mixins import ActiveTabMixin

from common.mixins import TrainingProviderAccessMixin, BaseRoleAccessMixin, FilterMixin
from common.models import EmailNotification
from core.views import CreateView
from orders.forms import PurchaseCreditForm, OrderForm, PaymentStatusForm, TransactionForm
from orders.mixins import OrderMixin, TransactionMixin
from orders.models import Order, Item, PaymentStatus, Transaction
from orders.utils import QuickPayClient
from trainings.models import TrainingProvider

User = get_user_model()
logger = logging.getLogger('sentry.errors')


class CreditTemplateView(TrainingProviderAccessMixin, ActiveTabMixin, TemplateView):
    template_name = 'orders/credit.html'
    active_tab = 'credit'


class PurchaseCreditView(TrainingProviderAccessMixin, ActiveTabMixin, FormView):
    template_name = 'orders/purchase_credit.html'
    active_tab = 'credit'
    form_class = PurchaseCreditForm

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'item': Item.objects.filter(pk=self.request.session.get('purchase-credit-item')).first(),
            'quantity': self.request.session.get('purchase-credit-quantity', 1)
        })
        return initial

    def form_valid(self, form):
        cd = form.cleaned_data
        self.request.session['purchase-credit-item'] = cd['item'].pk
        self.request.session['purchase-credit-quantity'] = cd['quantity']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('purchase_credit_confirm', args=(self.request.user.training_provider.pk,))


class PurchaseConfirmView(TrainingProviderAccessMixin, ActiveTabMixin, TemplateView):
    template_name = 'orders/purchase_credit.html'
    active_tab = 'credit'
    model = Order

    def dispatch(self, request, *args, **kwargs):
        self.item = Item.objects.items().filter(pk=self.request.session.get('purchase-credit-item')).first()
        if self.request.session.get('purchase-credit-item') and self.request.session.get('purchase-credit-quantity') and \
            self.item and self.item.prices_list:
            self.price = self.item.prices_list[0]
            return super().dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse_lazy('purchase_credit', args=(self.request.user.training_provider.pk,)))

    def post(self, request, *args, **kwargs):
        quantity = self.request.session['purchase-credit-quantity']
        self.model.objects.create(
            training_provider=self.request.user.training_provider, created_by=self.request.user,
            currency=self.price.currency, quantity=quantity, total_credits=self.price.total_credits(quantity),
            item_code=self.item.item_code, item_name=self.item.item_name, item_description=self.item.item_description,
            item_unit_price_including_vat=self.price.unit_price_including_vat, item_vat_rate=self.price.vat_rate,
            total_price_including_vat=self.price.total_price_including_vat(quantity))

        del self.request.session['purchase-credit-item']
        del self.request.session['purchase-credit-quantity']
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['item'] = self.item
        context['quantity'] = self.request.session['purchase-credit-quantity']
        context['price'] = self.price
        context['total_credits'] = self.price.total_credits(context['quantity'])
        context['total_price'] = self.price.get_total_price_including_vat_display(context['quantity'])
        return context

    def get_success_url(self):
        return reverse_lazy('orders', args=(self.request.user.training_provider.pk,))


class OrderView(BaseRoleAccessMixin, OrderMixin, FilterMixin, ListView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    template_name = 'orders/orders.html'
    model = Order
    form_class = OrderForm

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': '<a href="{}">{}</a>'.format(obj.get_absolute_url(), obj.order_id),
            'created': obj.created.strftime(settings.TEMPLATE_DATE_FORMAT),
            'created_by': obj.created_by.get_full_name(),
            'payment_success': obj.get_payment_success_display(),
            'total_credits': obj.total_credits,
            'total_price_including_vat': '{0} {1:.2f}'.format(obj.get_currency_display(), obj.get_total_price_including_vat_display)
        } for obj in object_list]


class OrderDetailView(BaseRoleAccessMixin, OrderMixin, DetailView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    template_name = 'orders/order_detail.html'
    model = Order
    pk_url_kwarg = 'order_pk'


class OrderPaymentLinkView(TrainingProviderAccessMixin, OrderMixin, RedirectView):
    model = Order
    permanent = False

    def get_object(self):
        try:
            return self.get_queryset().get(pk=self.kwargs['order_pk'])
        except Order.DoesNotExist:
            raise Http404()

    def get_redirect_url(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.payment_success:
            try:
                client = QuickPayClient()
                if not self.object.token_link:
                    payment = client.payments(self.request.user, self.object)
                    self.object.token_link = payment['id']
                    self.object.save()
                return client.link(self.request, self.object)['url']
            except ApiError as e:
                messages.add_message(self.request, messages.ERROR, e.body)
        return reverse_lazy('order_detail', args=(self.kwargs['pk'], self.kwargs['order_pk']))


class EmailOrderDetailView(BaseRoleAccessMixin, OrderMixin, RedirectView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    model = Order
    permanent = False

    def get_object(self):
        try:
            return self.get_queryset().get(pk=self.kwargs['order_pk'])
        except Order.DoesNotExist:
            raise Http404()

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy('order_detail', args=(self.kwargs['pk'], self.kwargs['order_pk']))

    def get(self, request, *args, **kwargs):
        EmailNotification.order(self.request.user, self.get_object())
        return super().get(request, *args, **kwargs)


class QuickPayCallback(CreateView):
    http_method_names = ['post']
    model = PaymentStatus
    form_class = PaymentStatusForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        instance = form.save()
        order = form.cleaned_data['order']
        if instance.is_approved:
            order.payment_success = True
            order.save()
            Transaction.objects.create(created_by=order.created_by, training_provider=order.training_provider,
                                       transaction_value=order.total_credits, order=order)
        return HttpResponse(status=200)

    def form_invalid(self, form):
        logger.error('{} - {}'.format(form.errors.as_text(), self.request.body))
        return HttpResponse(status=400)


class TransactionView(BaseRoleAccessMixin, TransactionMixin, FilterMixin, ListView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    template_name = 'orders/transaction.html'
    model = Transaction
    form_class = TransactionForm

    def get_filtered_data(self, object_list):
        return [{
            '': '',
            'id': '<a href="{}">{}</a>'.format(obj.get_absolute_url(), obj.pk),
            'created': obj.created.strftime(settings.TEMPLATE_DATE_FORMAT),
            'created_by': obj.created_by.get_full_name(),
            'transaction_value': '{:+}'.format(obj.transaction_value),
            'balance': obj.balance,
            'order': '<a href="{}">{}</a>'.format(obj.order.get_absolute_url(), obj.order.order_id)
                if obj.order_id else '',
            'upload_batch': '<a href="{}">{}</a>'
                .format(obj.upload_batch.get_absolute_url(), obj.upload_batch.pk) if obj.upload_batch_id else '',
            'description': obj.description.title(),
        } for obj in object_list]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['training_provider'] = get_object_or_404(TrainingProvider, pk=self.kwargs['pk']) \
            if self.request.user.is_database_admin else self.request.user.training_provider
        return context


class TransactionDetailView(BaseRoleAccessMixin, TransactionMixin, DetailView):
    roles = [User.TRAINING_PROVIDER, User.TRAINING_PROVIDER_ADMIN, User.DATABASE_ADMIN]
    template_name = 'orders/transaction_detail.html'
    model = Transaction
    pk_url_kwarg = 'transaction_pk'
