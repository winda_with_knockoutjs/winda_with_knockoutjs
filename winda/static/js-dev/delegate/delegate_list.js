$(document).ready(function() {
    initializeDataTable('table#training_record', {
        "order": [[5, 'asc']],
        "columns": [
            { "data": "", 'searchable':false, 'orderable':false, "className": "control" },
            { "data": "course__title"},
            { "data": "course__code" },
            { "data": "training_provider__name"},
            { "data": "country__name" },
            { "data": "completed_on"},
            { "data": "valid_from"},
            { "data": "valid_until"},
            { "data": "status" }
        ],
        'columnDefs': [{
            'targets': 3,
            'searchable':false,
            'orderable':false
        }, {
            'targets': 8,
            'searchable': false,
            'orderable': false
        }]
    });
});
