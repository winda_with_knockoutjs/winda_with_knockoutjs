# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import migrations
from django.utils import timezone


def create_system_user(apps, schema_editor):
    apps.get_model('users', 'User').objects\
        .get_or_create(pk=settings.SYSTEM_USER_ID,
                       email=settings.DEFAULT_FROM_EMAIL, username='system',
                       created_by=None, last_updated_by=None,
                       expires_at=timezone.datetime(2099, 12, 31, tzinfo=timezone.UTC()))


def remove_system_user(apps, schema_editor):
    apps.get_model('users', 'User').objects.filter(pk=settings.SYSTEM_USER_ID).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('common', '0001_initial'),
        ('trainings', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_system_user, remove_system_user),
    ]
