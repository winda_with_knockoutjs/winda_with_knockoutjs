$(document).ready(function() {
    initializeDataTable('table#delegate-search-result', {
        "order": [[ 1, 'desc' ], [ 8, 'asc' ]],
        "columns": [
            {"data": "", 'searchable': false, 'orderable': false, "className": "control"},
            {"data": "delegate__delegate_id"},
            {"data": "delegate__first_name"},
            {"data": "delegate__last_name"},
            {"data": "course__title"},
            {"data": "course__code"},
            {"data": "training_provider__name"},
            {"data": "country"},
            {"data": "completed_on"},
            {"data": "valid_from"},
            {"data": "valid_until"},
            {"data": "status", 'searchable': false, 'orderable': false}
        ],
        "language": {
          "emptyTable": "The Winda ID entered does not match any of our records.",
          "oPaginate": {
              "sNext": '&raquo;',
              "sPrevious": '&laquo;'
          }
        }
    });
});
