# -*- coding: utf-8 -*-
import random
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from users.factories import TrainingProviderAdminFactory, DelegateFactory
from delegate.factories import TrainingRecordFactory, SearchFactory
from courses.factories import CourseFactory, CourseProviderFactory
from trainings.factories import UploadBatchFactory, UploadBatchRecordFactory
from registration.factories import CountryFactory
from orders.factories import ItemFactory
from common.models import Country
from users.models import User
from courses.models import Course

class Command(BaseCommand):
    help = 'Fill database with data upto 2 years load'

    def add_arguments(self, parser):
        parser.add_argument('--only-records',
            dest='only_records',
            action='store_true',
            help='Only fill records')

    def handle(self, *args, **options):
        # provider ~ 250
        # Delegates ~ 100 000
        # Training records ~ 1 000 000
        # Searches ~ 750 000
        # Courses ~ 20
        # Countries ~ 300
        # Items ~ 5
        only_records = options.get('only_records')
        if not only_records:
            print ('Create providers')
            providers = TrainingProviderAdminFactory.create_batch(250)
            print ('Create delegates')
            delegates = DelegateFactory.create_batch(100000)
            delegates_count = 100000
            print ('Create courses')
            courses = CourseFactory.create_batch(20)
            countries = CountryFactory.create_batch(300)
            countries_count = 300
        else:
            print ('Retrieve data for filling')
            providers = User.objects.training_providers()
            delegates = User.objects.delegates()
            delegates_count = delegates.count()
            courses = list(Course.objects.all())
            countries = Country.objects.all()
            countries_count = countries.count()

        print ('Start creating records')
        for provider_user in providers:
            provider = provider_user.training_provider
            print ('Create records for {}'.format(provider.name))
            current_courses = random.sample(courses, int(len(courses)/2))
            print ('Associate courses')
            for course in current_courses:
                CourseProviderFactory.create(course=course, training_provider=provider)
            print ('Create records')
            upload_batch = UploadBatchFactory.create(training_provider=provider)
            for i in range(0, 1000):
                random_index = random.randint(0, delegates_count - 1)
                delegate = delegates[random_index]
                course_d = random.choice(current_courses)
                random_index = random.randint(0, countries_count - 1)
                country = countries[random_index]
                for i in range(0, 4):
                    record = UploadBatchRecordFactory.create(upload_batch=upload_batch)
                    TrainingRecordFactory.create(delegate=delegate, training_provider=provider,
                                                 course=course_d, country=country,
                                                 created_by_record=record)
                SearchFactory.create(created_by=provider_user, delegate=delegate)
