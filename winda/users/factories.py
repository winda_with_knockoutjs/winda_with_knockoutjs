# -*- coding: utf-8 -*-
import factory
from factory import fuzzy

from string import ascii_lowercase
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

from registration.factories import CountryFactory
from trainings.factories import TrainingProviderFactory

TEST_USER_PASSWORD = 'P4ssw0rd'
TEST_TECHNICAL_SUPPORT_PASSWORD = 'adminadmin'


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    archived_at = None
    is_active = True
    first_name = factory.Sequence(lambda n: 'user_first{0}'.format(n))
    last_name = factory.Sequence(lambda n: 'user_last{0}'.format(n))
    username = factory.Sequence(lambda n: 'user_{0}'.format(n))
    email = factory.Sequence(lambda n: 'user_{0}@email.com'.format(n))
    password = make_password(TEST_USER_PASSWORD)
    expires_at = factory.LazyAttribute(lambda a: timezone.now() + timezone.timedelta(days=5))
    timezone = 'Europe/London'


class TechnicalSupportFactory(UserFactory):
    email = factory.Sequence(lambda n: 'admin_{0}@email.com'.format(n))
    role = get_user_model().TECHNICAL_SUPPORT
    password = make_password(TEST_TECHNICAL_SUPPORT_PASSWORD)
    is_staff = True
    is_superuser = True


class DatabaseAdminFactory(UserFactory):
    role = get_user_model().DATABASE_ADMIN


class DatabaseSuperAdminFactory(UserFactory):
    role = get_user_model().DATABASE_SUPER_ADMIN


class TrainingProviderAdminFactory(UserFactory):
    role = get_user_model().TRAINING_PROVIDER_ADMIN
    training_provider = factory.SubFactory(TrainingProviderFactory)
    can_upload = True
    can_buy_credit = True
    can_use_credit = True


class TrainingProviderUserFactory(UserFactory):
    role = get_user_model().TRAINING_PROVIDER
    training_provider = factory.SubFactory(TrainingProviderFactory)
    can_upload = True
    can_buy_credit = True
    can_use_credit = True


class DelegateFactory(UserFactory):
    role = get_user_model().DELEGATE
    delegate_id = fuzzy.FuzzyText(length=8, chars=ascii_lowercase)


class OrganizationFactory(UserFactory):
    role = get_user_model().ORGANIZATION
    organization_name = fuzzy.FuzzyText()
    job_title = fuzzy.FuzzyText()
    address_1 = fuzzy.FuzzyText()
    city = fuzzy.FuzzyText()
    postal_code = fuzzy.FuzzyText()
    phone = fuzzy.FuzzyText()
    country = factory.SubFactory(CountryFactory)
