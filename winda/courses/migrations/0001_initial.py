# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models, connection
from django.conf import settings


def course_code_trigger(apps, schema_editor):
    cursor = connection.cursor()
    cursor.execute('DROP TRIGGER IF EXISTS course_code_trigger_insert;')
    cursor.execute('DROP TRIGGER IF EXISTS course_code_trigger_update;')
    cursor.execute(
        'CREATE TRIGGER course_code_trigger_insert BEFORE INSERT ON courses_course '
        'FOR EACH ROW BEGIN '
        'SET NEW.code = UCASE(NEW.code); '
        'END; ')
    cursor.execute(
        'CREATE TRIGGER course_code_trigger_update BEFORE UPDATE ON courses_course '
        'FOR EACH ROW BEGIN '
        'SET NEW.code = UCASE(NEW.code); '
        'END; ')


def reverse_course_code_trigger(apps, schema_editor):
    cursor = connection.cursor()
    cursor.execute('DROP TRIGGER IF EXISTS course_code_trigger_insert;')
    cursor.execute('DROP TRIGGER IF EXISTS course_code_trigger_update;')


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0003_trainingprovidercertificate'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(unique=True, verbose_name='code', max_length=8)),
                ('title', models.CharField(verbose_name='title', max_length=50)),
                ('course_type', models.CharField(verbose_name='course type', max_length=10, choices=[('full', 'full'), ('refresher', 'refresher')])),
                ('require_existing_training_record', models.BooleanField()),
                ('validity_months', models.IntegerField(verbose_name='validity months')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1, related_name='course_created_by')),
                ('last_updated_by', models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL, related_name='course_last_updated_by')),
                ('refresher_for_course', models.ForeignKey(blank=True, null=True, to='courses.Course', verbose_name='refresher for course')),
            ],
            options={
                'verbose_name_plural': 'Courses',
                'verbose_name': 'Course',
            },
        ),
        migrations.CreateModel(
            name='CourseProvider',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('valid_from', models.DateField(verbose_name='valid from')),
                ('valid_until', models.DateField(verbose_name='valid until')),
                ('is_deleted', models.BooleanField(verbose_name='is deleted')),
                ('course', models.ForeignKey(to='courses.Course', verbose_name='course', related_name='course_providers')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1, related_name='courseprovider_created_by')),
                ('last_updated_by', models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL, related_name='courseprovider_last_updated_by')),
                ('training_provider', models.ForeignKey(to='trainings.TrainingProvider', related_name='training_provider_courseprovider')),
            ],
            options={
                'verbose_name_plural': 'Course Providers',
                'verbose_name': 'Course Provider',
            },
        ),
        migrations.RunPython(course_code_trigger, reverse_course_code_trigger),
    ]
