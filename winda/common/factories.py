# -*- coding: utf-8 -*-
import factory
from factory import fuzzy

from common.models import EmailNotification
from users.factories import UserFactory


class EmailNotificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = EmailNotification

    subject = fuzzy.FuzzyText(length=8)
    body = fuzzy.FuzzyText()
    recipient = factory.SubFactory(UserFactory)
    ses_message_id = fuzzy.FuzzyText()
