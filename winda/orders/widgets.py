# -*- coding: utf-8 -*-
from django.forms import Select
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe


class ItemPriceSelectWidget(Select):
    def render_option(self, selected_choices, option_value, item):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = mark_safe('')

        if option_value and item.prices_list:
            price = item.prices_list[0]
            selected_html += mark_safe(
                ' data-currency="{}" data-description="{}" data-credits_per_unit="{}" data-vat_rate="{}" '
                'data-unit_price_including_vat="{}"'.format(price.get_currency_display(), item.item_description,
                    price.credits_per_unit, price.vat_rate, price.get_unit_price_including_vat_display))

        return format_html('<option value="{}"{}>{}</option>', option_value, selected_html, force_text(item.item_name))

    def render_options(self, choices, selected_choices):
        # Normalize to strings.
        selected_choices = set(force_text(v) for v in selected_choices)
        output = []
        for item in self.choices.queryset:
            output.append(self.render_option(selected_choices, item.pk, item))
        return '\n'.join(output)
