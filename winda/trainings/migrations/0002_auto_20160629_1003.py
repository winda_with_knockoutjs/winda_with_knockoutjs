# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('common', '0002_auto_20160629_1003'),
        ('trainings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingprovider',
            name='accepted_by',
            field=models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='trainingprovider',
            name='country',
            field=models.ForeignKey(blank=True, verbose_name='country', null=True, to='common.Country'),
        ),
        migrations.AddField(
            model_name='trainingprovider',
            name='created_by',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL, related_name='trainingprovider_created_by'),
        ),
        migrations.AddField(
            model_name='trainingprovider',
            name='last_updated_by',
            field=models.ForeignKey(blank=True, null=True, to=settings.AUTH_USER_MODEL, related_name='trainingprovider_last_updated_by'),
        ),
    ]
