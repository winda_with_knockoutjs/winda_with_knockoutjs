# -*- coding: utf-8 -*-
from django.utils import timezone


class TimezoneMiddleware(object):

    def process_request(self, request):
        tz = getattr(request.user, 'timezone', None)
        if tz:
            timezone.activate(tz)
        else:
            timezone.deactivate()
