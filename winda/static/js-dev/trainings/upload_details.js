$(function() {

    function update_fields() {
        var value = $('#id_course').val();
        var show = $('#id_course').find('option[value="' + value + '"]').eq(0).data('type') == 'refresher';
        var method = show?'show':'hide';
        $('.single-upload').find('#id_previous_course_valid_until').parents('.form-group')[method]()
    }

    update_fields();

    $(document).on('change', '#id_course', function(e){
        update_fields();
    })
});
