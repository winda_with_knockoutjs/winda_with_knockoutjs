from django.contrib import admin
from core.admin import ModelAdmin
from registration.models import Registration, LostPassword


@admin.register(Registration)
class RegistrationAdmin(ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'registration_type',
                    'organization_name', 'training_provider')


@admin.register(LostPassword)
class LostPasswordAdmin(ModelAdmin):
    list_display = ('user', 'expires_at', 'claimed_at', 'activation_code')
