# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0008_auto_20160705_0659'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='timezone',
            field=timezone_field.fields.TimeZoneField(default='Europe/London'),
        ),
    ]
