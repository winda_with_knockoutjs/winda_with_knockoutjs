# -*- coding: utf-8 -*-
import factory
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase

from common.models import EmailNotification
from registration.factories import CountryFactory
from users.factories import DatabaseAdminFactory, TEST_USER_PASSWORD, DelegateFactory, TrainingProviderAdminFactory, \
    TrainingProviderUserFactory, OrganizationFactory, DatabaseSuperAdminFactory, UserFactory


User = get_user_model()


class UserFilterTestCase(TestCase):

    def setUp(self):
        self.user = DatabaseSuperAdminFactory()
        self.users_list = [DelegateFactory(), OrganizationFactory(), TrainingProviderUserFactory(),
                           TrainingProviderAdminFactory()]
        self.database_admin = DatabaseAdminFactory()
        self.url = reverse('database_super_admin:home')
        self.list_url = reverse('database_super_admin:users')
        self.create_url = reverse('database_super_admin:user_create')
        self.update_url = reverse('database_super_admin:user_update', args=(self.database_admin.pk,))
        self.country = CountryFactory()
        self.user_count = User.objects.count()

    def test_access(self):
        """
        Only Database Super Admin has access
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

        for url in [self.url, self.create_url, self.update_url]:
            for user in self.users_list:
                self.client.login(username=user.username, password=TEST_USER_PASSWORD)
                response = self.client.get(url)
                self.assertEqual(response.status_code, 404)

        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        for url in [self.url, self.create_url, self.update_url]:
            response = self.client.get(url)
            self.assertContains(response, 'User')

    def test_create(self):
        """
        Database Super Admin create new database admin
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.get(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'New User', 2)

        data = factory.build(dict, FACTORY_CLASS=UserFactory, country=self.country.pk, role=User.DATABASE_ADMIN,
                             expires_at='2019-03-01')
        response = self.client.post(self.create_url, data)
        self.assertRedirects(response, self.list_url)
        self.assertEqual(User.objects.count(), self.user_count + 1)
        self.assertEqual(EmailNotification.objects.count(), 1)

        user = User.objects.last()
        for field in ('first_name', 'last_name', 'email', 'role'):
            self.assertEqual(getattr(user, field), data.get(field, ''))
        self.assertEqual(user.created_by, self.user)
        self.assertEqual(user.expires_at.strftime('%Y-%m-%d'), '2019-03-01')
        self.assertTrue(user.is_active)

    def test_create_fail(self):
        """
        Database Super Admin submit blank create new user form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.create_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertContains(response, 'This field is required.', 5)

    def test_update_user(self):
        """
        Database Super Admin update user form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)

        response = self.client.get(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Manage User', 1)

        data = factory.build(dict, FACTORY_CLASS=UserFactory, country=self.country.pk,
                             role=User.DATABASE_ADMIN, is_active=True, expires_at='2019-03-01', email_status=User.DELIVERY)
        response = self.client.post(self.update_url, data)
        self.assertRedirects(response, self.list_url)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertEqual(EmailNotification.objects.count(), 0)

        self.database_admin.refresh_from_db()
        for field in ('first_name', 'last_name', 'email_status', 'role'):
            self.assertEqual(getattr(self.database_admin, field), data.get(field, ''))
        self.assertTrue(self.database_admin.is_active)
        self.assertEqual(self.database_admin.expires_at.strftime('%Y-%m-%d'), '2019-03-01')

    def test_update_fail(self):
        """
        Database Super Admin submit blank update user form
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        response = self.client.post(self.update_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), self.user_count)
        self.assertContains(response, 'This field is required.', 5)

    def test_Database_Super_Admin_update_yourself(self):
        """
        Database Super Admin try to update yourself and get 404
        """
        self.client.login(username=self.user.username, password=TEST_USER_PASSWORD)
        data = factory.build(dict, FACTORY_CLASS=UserFactory, country=self.country.pk, role=User.TRAINING_PROVIDER,
                             is_active=True)
        response = self.client.post(reverse('database_super_admin:user_update', args=(self.user.pk,)), data)
        self.assertEqual(response.status_code, 404)
