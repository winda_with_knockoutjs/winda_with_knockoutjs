# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def replace_field_valid(apps, schema_editor):
    TrainingRecord = apps.get_model('delegate', 'TrainingRecord')
    for record in TrainingRecord.objects.all().iterator():
        record.training_provider = record.old_training_provider.training_provider
        record.save()

class Migration(migrations.Migration):

    dependencies = [
        ('delegate', '0008_auto_20160715_1631'),
    ]

    operations = [
        migrations.RunPython(replace_field_valid),
    ]
